# README

### INSTALL YARN

- Install  `node v10.22` or later

- Install `yarn` (https://yarnpkg.com/getting-started/install)

### HOW TO START?

- Create `.env.local` file base on `.env.local.example`

- `yarn install`

- `yarn start`

### HOW TO DEPLOY STAGING?

- `git checkout staging`

- `yarn build`

- Go to https://console.cloud.google.com/storage/browser/odinpoker-staging

- Upload and replace old build folder with new build folder to the bucket

- Open Google Cloud Shell on browser or run `gcloud alpha cloud-shell ssh` in terminal

* In Google Cloud Shell, run:

- `gcloud config set project bigdongryan-217503`

- `cd odinpoker-staging`

- `gsutil rsync -r gs://odinpoker-staging .`

- `gcloud app deploy`

### HOW TO DEPLOY PRODUCTION?

- `git checkout master`

- `yarn build`

- Go to https://console.cloud.google.com/storage/browser/odinpoker

- Upload and replace old build folder with new build folder to the bucket

- Open Google Cloud Shell on browser or run `gcloud alpha cloud-shell ssh` in terminal

* In Google Cloud Shell, run:

- `gcloud config set project bigdongryan-217503`

- `cd odinpoker`

- `gsutil rsync -r gs://odinpoker .`

- `gcloud app deploy`


