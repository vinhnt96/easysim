import { rangeExplorerActions } from '../constants/index';
import { convertTurnRiverCard } from 'services/convert_cards_services'
import { isEmpty } from 'lodash'

export const updateSuitsForFilterRE = (suits) => {
  return {
    type: rangeExplorerActions.UPDATE_SUITS_FOR_FILTER_RE,
    payload: suits
  }
}

export const updateDecisionFilterRE = (decision) => {
  return {
    type: rangeExplorerActions.UPDATE_DECISION_FILTER_RE,
    payload: decision
  }
}

export const changeClickedHandItemRE = (handItem) => {
  return {
    type: rangeExplorerActions.CHANGE_CLICKED_HAND_ITEM_RE,
    payload: handItem
  }
}

export const updateHandCategory = (handCategoryData) => {
  return {
    type: rangeExplorerActions.UPDATE_HAND_CATEGORY,
    payload: handCategoryData
  }
}

export const fetchHandCategoryData = (cardsConvertingTemplate, game) => {
  return dispatch => {
    const { convertedFlopCards, turnCard, riverCard } = game
    const convertedTurnCard = isEmpty(turnCard) ? '' : convertTurnRiverCard(turnCard, cardsConvertingTemplate)
    const convertedRiverCard = isEmpty(riverCard) ? '' : convertTurnRiverCard(riverCard, cardsConvertingTemplate)
    const board = convertedFlopCards.join('') + convertedTurnCard + convertedRiverCard 

    window.axios.get(`/hand_categories/get_hand_category?board=${board}`).then( res => {
      const handCategoryData = res.data.hand_category
      dispatch(updateHandCategory(handCategoryData))
    }).catch(err => console.error(err))
  }
}

export const fetchingRangeExplorerData = (payload) => {
  return {
    type: rangeExplorerActions.FETCHING_RANGE_EXPLORER_DATA,
    payload: payload
  }
}