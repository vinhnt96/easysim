import { generateAuthActions } from 'redux-token-auth'
import { config } from '../config/redux-token-auth'

export const {
  registerUser,
  signInUser,
  signOutUser,
  verifyCredentials,
} = generateAuthActions(config)
