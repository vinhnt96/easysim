import axios from 'axios'
import { subscriptionActions } from '../constants/index';

export const fetchPlansSucceeded = (plans) => {
  return {
    type: subscriptionActions.FETCH_PLANS_SUCCEEDED,
    payload: plans
  }
}

export const fetchPlans = () => {
  return dispatch => {
    axios.get('/plans').then(function(res){
      dispatch(fetchPlansSucceeded(res.data.plans))
    })
  }
}

export const togglePopup = (name) => {
  return {
    type: subscriptionActions.TOGGLE_POPUP,
    payload: name
  }
}

export const codeDiscount = (code) => {
  return {
    type: subscriptionActions.DISCOUNT,
    payload: code
  }
}
