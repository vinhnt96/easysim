
export const fetchRoles = (roles) => {
  return {
    type: 'FETCH_ROLES_FULFILLED',
    payload: roles
  }
}
