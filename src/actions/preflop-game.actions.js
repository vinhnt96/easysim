import axios from 'axios';
import { batch } from 'react-redux'
import { preflopGameActions } from '../constants/index';
import { getNextPosition, generateNewNode, roundedRaiseAction, generateStackSizeParam } from 'services/preflop_game_service'
import {
  CASH_GAME_INITIAL_TOTAL_POT,
  MTT_GAME_INITIAL_TOTAL_POT,
  BIG_BLIND_BET,
  INITIAL_POSITION_NODES,
  PREFLOP_ANTES,
  HANDS_LIST,
  PREFLOP_ACTION_DECIMAL,
} from 'config/constants'
import {map, round, sum, last} from 'lodash'
import { combosGenerator, calcSumStrat } from 'utils/matrix'

export const updatePreflopGameData = (data) => {
  return {
    type: preflopGameActions.UPDATE_PREFLOP_DATA,
    payload: data
  }
}

export const updateInitialPreflopData = (trackedData, currentPosition, gameType, stackSize) => {
  return {
    type: preflopGameActions.UPDATE_INITIAL_PREFLOP_DATA,
    payload: initialPreflopData(trackedData, currentPosition, gameType, stackSize)
  }
}

export const resetPreplopGame = (trackedData, currentPosition, gameType, stackSize) => {
  return {
    type: preflopGameActions.RESET_PREFLOP_GAME,
    payload: initialPreflopData(trackedData, currentPosition, gameType, stackSize)
  }
}

export const resetToInitialPreflopData = () => {
  return {
    type: preflopGameActions.RESET_TO_INITIAL_PREFLOP_DATA,
  }
}

const initialPreflopData = (trackedData, currentPosition, gameType, initialStack) => {
  let newTrackedData = { ...trackedData }
  let totalPot = gameType === 'cash' ?
    CASH_GAME_INITIAL_TOTAL_POT : MTT_GAME_INITIAL_TOTAL_POT;
  Object.keys(newTrackedData).forEach(position => {
    let newPositionData = { ...newTrackedData[position] }
    let bet = 0;
    if(position === 'sb') {
      bet = BIG_BLIND_BET / 2;
    } else if(position === 'bb') {
      bet = BIG_BLIND_BET;
    }

    newPositionData.remaining = parseInt(initialStack) - bet;
    newPositionData.initialStack = parseInt(initialStack);
    newPositionData.lastAction = '' ;
    newPositionData.bet = bet;
    newPositionData.heightPercentages = {};
    newTrackedData[position] = newPositionData;
  })

  return {
    antes: PREFLOP_ANTES[gameType],
    currentPosition: currentPosition,
    trackedData: newTrackedData,
    totalPot: totalPot,
    biggestBet: BIG_BLIND_BET,
  }
}

export const fetchCurrentPreflop = (preflop_params) => {
  return dispatch => {
    let url = `/preflops/get_preflop_data?game_type=${preflop_params.game_type}&`+
                `stack_size=${preflop_params.stack_size}&`+
                `node=${preflop_params.node}&`+
                `specs=${preflop_params.specs}`;

    axios.get(url).then(res => {
      let preflop = res.data;

      if(preflop.game_ended) {
        preflop.action_options = [];
        preflop.action_ranges = {};
      }

      dispatch(updatePreflopGameData(preflop))
    })
  }
}

export const resetPreplopGameDispatch = (trackedData, currentPosition, gameType, stackSize, specs) => {
  return dispatch => {
    let url = `/preflops/get_preflop_data?game_type=${gameType}&`+
                `stack_size=${stackSize}&`+
                `node=${encodeURIComponent(INITIAL_POSITION_NODES[currentPosition])}&`+
                `specs=${specs}`

    axios.get(url).then(res => {
      let preflop = res.data;

      if(preflop.game_ended) {
        preflop.action_options = [];
      }
      batch(() => {
        dispatch(resetPreplopGame(trackedData, currentPosition, gameType, stackSize));
        dispatch(updatePreflopGameData(preflop));
      })
    })
  }
}

export const rollbackPreflopGameDispatch = (preflopData) => {
  return dispatch => {
    let preflop = { ...preflopData };
    let lastData = {}
    lastData = last(preflopData.history)
    if(lastData) {
      let { position, totalPot, biggestBet, node, positionData } = lastData;
      preflop.currentPosition = position;
      preflop.totalPot = totalPot;
      preflop.biggestBet = biggestBet;
      let newTrackedData = { ...preflop.trackedData };
      newTrackedData[position] = positionData;
      preflop.trackedData = newTrackedData;
      preflop.history = [...preflop.history].slice(0, -1);

      let url = `/preflops/get_preflop_data?game_type=${preflopData.game_type}&`+
              `stack_size=${preflopData.stack_size}&`+
              `node=${encodeURIComponent(node)}&`+
              `specs=${preflopData.specs}`;

      axios.get(url).then(res => {
        preflop = { ...preflop, ...res.data };

        dispatch(updatePreflopGameData(preflop));
      })
    }
  }
}

export const updatePreflopGame = (selectedAction, preflopData, data={}, specs='') => {
  return dispatch => {
    let { currentPosition, trackedData, game_type, node, biggestBet, totalPot, antes } = preflopData;
    let nextPosition = getNextPosition(currentPosition, trackedData, game_type, specs);
    let preflop = { ...preflopData };
    preflop.trackedData = { ...trackedData };
    let currentPositionData = { ...preflop.trackedData[currentPosition] };

    currentPositionData['lastAction'] = selectedAction.value;
    currentPositionData['heightPercentages'] = calHeightPercentages(data, currentPositionData['heightPercentages'], selectedAction.value);
    let newbiggestBet = calculateBiggestBet(preflopData.biggestBet, currentPositionData);
    currentPositionData['bet'] = calculateBet(preflopData.biggestBet, currentPositionData);
    currentPositionData['remaining'] = calculateRemainingStack(preflopData.biggestBet, currentPositionData);
    preflop['biggestBet'] = newbiggestBet;
    preflop.trackedData[currentPosition] = currentPositionData;
    preflop['totalPot'] = calculateTotalPot(antes, preflop.trackedData);

    let history = [...preflop.history]
    history.push({
      position: currentPosition,
      node: node,
      biggestBet: biggestBet,
      totalPot: totalPot,
      positionData: { ...preflopData.trackedData[currentPosition] },
    });
    preflop.history = history;

    let newNode = generateNewNode(selectedAction.value, node, currentPosition, nextPosition);
    let url = `/preflops/get_preflop_data?game_type=${preflopData.game_type}&`+
                `stack_size=${preflopData.stack_size}&`+
                `node=${encodeURIComponent(newNode)}&`+
                `specs=${specs}`;

    axios.get(url).then(res => {
      if(res.data.game_ended) {
        preflop.game_ended = true;
        preflop.action_options = [];
      } else {
        preflop = { ...preflop, ...res.data };
        preflop['currentPosition'] = nextPosition;
      }

      dispatch(updatePreflopGameData(preflop));
    })
  }
}

const calHeightPercentages = (data, lastHeightPercentages, selectedAction) => {
  let handHeightPercentages = {};

  HANDS_LIST.forEach(hand => {
    const combo = combosGenerator(hand);
    const handHeightPercentage = calHandHeightPercentage(combo, data, lastHeightPercentages[hand] || 100, selectedAction);

    handHeightPercentages[hand] = handHeightPercentage;
  })

  return handHeightPercentages;
}


export const calHandHeightPercentage = (combos, strategyForEachCombos, lastHeightPercentage, lastAction) => {
  let avgStrategy = {}
  let actions = map(strategyForEachCombos[`${combos[0]}`], (value, key) => {
    return key;
  })
  actions.forEach(action => {
    let sum = 0
    combos.forEach(combo => {
      sum = sum + parseFloat(strategyForEachCombos[combo][action])
    })
    let num = combos.filter(combo => calcSumStrat(strategyForEachCombos[combo], actions) > 0 ).length
    num = num === 0 ? 1 : num

    const roundedAction = roundedRaiseAction(action, PREFLOP_ACTION_DECIMAL);
    if(roundedAction === lastAction) {
      avgStrategy[roundedAction] = (sum / num) * (lastHeightPercentage)
    }
  })

  return round(avgStrategy[lastAction]);
}

export const initPreflopPage = (preflopGame, strategySelection, currentPosition) => {
  return dispatch => {
    const preflop_params = {
      game_type: strategySelection.game_type,
      stack_size: generateStackSizeParam(strategySelection),
      node: encodeURIComponent(INITIAL_POSITION_NODES[currentPosition]),
      specs: strategySelection.specs
    }
    batch(() => {
      dispatch(updateInitialPreflopData(preflopGame.trackedData, currentPosition, strategySelection.game_type, strategySelection.stack_size))
      dispatch(fetchCurrentPreflop(preflop_params))
    })
  }
}

const calculateBiggestBet = (biggestBet, currentPositionData) => {
  switch(currentPositionData['lastAction']) {
    case 'call':
    case 'fold':
      return biggestBet;
    case 'allin':
      return currentPositionData.initialStack;
    default:
      return Math.max(biggestBet, parseFloat(currentPositionData['lastAction'].replace('bb', '')));
  }
}

const calculateRemainingStack = (biggestBet, currentPositionData) => {
  switch(currentPositionData['lastAction']) {
    case 'call':
      let remainingStack = currentPositionData.initialStack >= biggestBet ?
        currentPositionData.initialStack - biggestBet : 0;
      return remainingStack;
    case 'fold':
      return currentPositionData['remaining']
    case 'allin':
      return 0
    default:
      return currentPositionData.initialStack - parseFloat(currentPositionData['lastAction'].replace('bb', ''))
  }
}

const calculateBet = (biggestBet, currentPositionData) => {
  switch(currentPositionData['lastAction']) {
    case 'call':
      let newBet = currentPositionData.initialStack >= biggestBet ?
        biggestBet :
        currentPositionData.initialStack;

      return newBet;
    case 'fold':
      return currentPositionData.bet;
    case 'allin':
      return currentPositionData.initialStack;
    default:
      return parseFloat(currentPositionData['lastAction'].replace('bb', ''));
  }
}

const calculateTotalPot = (antes, trackedData) => {
  return antes + sum(map(Object.values(trackedData), 'bet'));
}
