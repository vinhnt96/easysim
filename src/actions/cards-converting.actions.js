import { cardsConvertingActions } from '../constants/index';

export const updateCardsConvertingTemplate = (template) => {
  return {
    type: cardsConvertingActions.UPDATE_CARDS_CONVERTING_TEMPLATE,
    payload: template
  }
}
