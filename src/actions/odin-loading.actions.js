import { odinLoadingActions } from '../constants/index';

export const updateOdinLoadingShowingValue = (value) => {
  return {
    type: odinLoadingActions.UPDATE_FETCHING_VALUE,
    payload: value
  }
}
