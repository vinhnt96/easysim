export const preflopGameActions = {
  UPDATE_PREFLOP_DATA: 'UPDATE_PREFLOP_DATA',
  UPDATE_INITIAL_PREFLOP_DATA: 'UPDATE_INITIAL_PREFLOP_DATA',
  RESET_PREFLOP_GAME: 'RESET_PREFLOP_GAME',
  RESET_TO_INITIAL_PREFLOP_DATA: 'RESET_TO_INITIAL_PREFLOP_DATA',
}
