import { round, sortBy, isEqual, sum, isEmpty, toLower, max } from 'lodash'
import { generateDecisionFromFlop, positionsForQueryBuilder } from '../services/game_play_service'
import {
  DEFAULT_BLINDS,
  SPECIAL_SIMS,
  FREE_TRIAL_USER_EMAILS,
  DECISION_PERCENT_COMBOS_PRECISION,
  DECISION_NUM_COMBOS_PRECISION,
  PADS_ACCESS_EMAILS_LIST,
  URL_TO_AFFILIATE_CODE_MAPPING
} from 'config/constants'
import moment from 'moment-timezone'


export const decisionCombosButtons = (game, preferences, currentPosition, decisions) => {
  let { fold_color, check_and_call_color, bet_and_raise_color, hide_strategies_less_than } = preferences
  let totalCombosNumber = 0.0
  const decisionButtons = game.flops.map((flop, index) => {
    let flopOption = generateDecisionFromFlop(flop, game, decisions)
    let combos = parseFloat(flopOption[`num_combos_${currentPosition}`])
    const { action_display: flopActionDisplay, action, bet_level, betPercentOfPot } = flopOption
    let action_display = isNaN(flopActionDisplay) ? flopActionDisplay : `B${flopActionDisplay}`
    let colorScheme = action === 'b' ? `${bet_and_raise_color}-scheme` : action === 'c' ? `${check_and_call_color}-scheme` : `${fold_color}-scheme`
    let colorLevel = bet_level > 0 ? `level-${bet_level}` : 'base'
    let nodeArray = flop.nodes.split(':')
    totalCombosNumber += combos

    return {
      title: action_display,
      info: combos,
      colorClass: `${colorScheme} ${colorLevel}`,
      value: nodeArray[nodeArray.length-1],
      raw_value: flopActionDisplay,
      betPercentOfPot: betPercentOfPot,
    }
  })
  totalCombosNumber = round(totalCombosNumber, DECISION_NUM_COMBOS_PRECISION)

  //calculate percent combos
  decisionButtons.forEach((comboElement, index) => {
    comboElement['percent'] = totalCombosNumber === 0.0 ? '0%' : `${ round( comboElement.info / totalCombosNumber * 100, DECISION_PERCENT_COMBOS_PRECISION) }%`
    comboElement['info'] = `${round(comboElement.info, DECISION_NUM_COMBOS_PRECISION)}c`
  })
  let betDecisionArray = decisionButtons.filter(comboElement => !['CHECK', 'FOLD', 'CALL'].includes(comboElement.raw_value))
  let maxPercent = max(betDecisionArray.map( decision => parseFloat(decision['percent'].replace('%', '')) ))

  //Apply hide strategy less than function and apply highlight for bigest percent decision
  decisionButtons.forEach( comboElement => {
    comboElement['isMaxPercent'] = ['CHECK', 'FOLD', 'CALL'].includes(comboElement.raw_value) ? false : parseFloat(comboElement['percent'].replace('%', '')) === maxPercent
  })
  //if it have more than 1 hightest percent value, don't set isMax for any element
  let isOneHighestFrequency = decisionButtons.filter( comboElement => comboElement['isMaxPercent'] === true).length === 1
  if(!isOneHighestFrequency) {
    decisionButtons.forEach( comboElement => { comboElement['isMaxPercent'] = false })
  }

  let decisionButtonsDisplay = decisionButtons.filter(
    comboElement => parseFloat(comboElement.percent.replaceAll('%', '')) >= round(hide_strategies_less_than*100, 2)
    || [comboElement.raw_value, comboElement.title].includes('CHECK') //alway show CHECK action
  )

  decisionButtonsDisplay.push({
    title: 'ALL',
    info: `${totalCombosNumber}c`,
    colorClass: `bg-white`,
    value: 'all',
    raw_value: 'ALL',
    percent: '100%',
    combosNumber: totalCombosNumber,
  })

  return decisionButtonsDisplay
}

export const generateCompareEVoptions = (game, decisions) => {
  const options = game.flops.map((flop, index) => {
    let flopOption = generateDecisionFromFlop(flop, game, decisions)
    let action_display = flopOption.action_display;

    return {
      text: isNaN(action_display) ? action_display : `BET ${action_display}`,
      value: isNaN(action_display) ? action_display.toLowerCase()[0] : `b${action_display}`,
      raw_value: action_display,
    }
  })

  return options;
}

export const orderCompareEVOptions = (options) => {
  let keys = Object.keys(options)
  let preOrderKeys = keys.map((key) => {
    if(key === 'f') {
      return { key: key, order: 1 }
    } else if(key === 'c') {
      return { key: key, order: 2 }
    } else {
      return { key: key, order: 3 }
    }
  })

  if(preOrderKeys.filter(key => key.order === 3).length === 2) {
    return sortBy(preOrderKeys, keyObj => { return parseFloat(keyObj.key.replace('b', '')) }, ['asc']).map(orderedKey => {
      return orderedKey.key
    })
  } else {
    return sortBy(preOrderKeys, 'order', ['asc']).map(orderedKey => {
      return orderedKey.key
    })
  }
}

export const checkIsSpecialSim = () => {
  const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections"))
  const { sim_type, stack_size, positions } = strategySelection
  const simType = simTypeMapping(sim_type)
  const positionsBuilder = positionsForQueryBuilder(positions)
  const sim = `${stack_size}-${simType}-${positionsBuilder}`
  const simReverse = `${stack_size}-${simType}-${positionsBuilder.split('v').reverse().join('v')}`
  return [sim, simReverse].some( simName => SPECIAL_SIMS.includes(simName) )
}

export const formatToBigBlind = (chips, precision=1) => {
  if(isNaN(chips))
    return chips
  const value = bigBlindValue(chips, precision)
  return `${value}bb`;
}

export const bigBlindValue = (chips, precision=1) => {
  return round(chips / blindRatio(), precision)
}

export const blindRatio = () => {
  return checkIsSpecialSim() ? 10 : DEFAULT_BLINDS.big_blind
}

export const roundEVValue = (value, displayInBB=false, precision=2) => {
  return !round(value, precision) ? '' : displayInBB ? `${round(value, precision)}bb` : `${round(value, precision)}`
}

export const formatFromBigBlind = (bigBlind='') => {
  return parseFloat(bigBlind.split('bb')[0] || 0)*100;
}

export const objectWithoutProperties = (obj, keys) => {
  var target = {};
  for (var i in obj) {
    if (keys.indexOf(i) >= 0) continue;
    if (!Object.prototype.hasOwnProperty.call(obj, i)) continue;
    target[i] = obj[i];
  }
  return target;
}

const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export const randomInts = (n, min, max, minSum, maxSum) => {
  if (min * n > maxSum || max * n < minSum) {
    return
  }

  var ints = [];
  while (n--) {
      // calculate a lower bound for this number
      // n * max is the max of the next n numbers
      var thisMin = Math.max(min, minSum - n * max);
      // calculate an upper bound for this number
      // n * min is the min of the next n numbers
      var thisMax = Math.min(max, maxSum - n * min);

      var int = getRandomInt(thisMin, thisMax);
      minSum -= int;
      maxSum -= int;
      ints.push(int);
  }
  return ints;
}

// this function is used to fix the editor library's formatting issue
export const formatHTMLText = (text) => {
  return text.replaceAll('<p></p>', '').trim();
}

export const notify = (message, notifyFunction, config={}) => {
  notifyFunction(message, {
    position: "top-center",
    autoClose: config['time'],
    hideProgressBar: true,
    closeOnClick: true,
    pauseOnHover: false,
    draggable: false,
    progress: 0,
  });
}

export const checkFreeTrialUser = (email) => {
  return FREE_TRIAL_USER_EMAILS.includes(email);
}

export const simTypeMapping = (simType) => {
  switch(simType) {
    case 'limped':
     return 'limp'
    case '3bet':
      return '3bp'
    case '4bet':
      return '4bp'
    case 'l3bet':
      return 'l3b'
    case 'l4bet':
      return 'l4b'
    case 'iso':
      return 'liso'
    default:
      return simType
  }
}

export const objectRequestSims = () => {
  return JSON.parse(localStorage.getItem("requests")) || {}
}

export const keysRequestSims = () => {
  return Object.keys(objectRequestSims()) || []
}

export const checkRemoveTypeSRP = (strategySelection) => {
  const { game_type, stack, positions } = strategySelection;
  const positionsTemp = [...positions]
  return isEqual(game_type, 'mtt') && (
          (isEqual(parseInt(stack), parseInt('30')) && isEqual(positionsTemp.sort(), ['sb', 'b'].sort())) ||
          (isEqual(parseInt(stack), parseInt('20')) &&
            ( isEqual(positionsTemp.sort(), ['b1', 'b3'].sort()) ||
              isEqual(positionsTemp.sort(), ['b3', 'b5'].sort())
            )
          )
        )
}

export const checkRemoveTypeLimp = (strategySelection) => {
  const { game_type, stack, positions, players } = strategySelection;
  const positionsTemp = [...positions]
  return (isEqual(game_type, 'mtt') && (
          ( isEqual(parseInt(stack), parseInt('15')) && (
              isEqual(positionsTemp.sort(), ['bb', 'b3'].sort()) ||
              isEqual(positionsTemp.sort(), ['bb', 'b4'].sort())
            )
          ) || ( isEqual(parseInt(stack), parseInt('100')) )))
          || ( isEqual(game_type, 'cash') && isEqual(players, '8max'))
}

export const browser = () => {
  let sBrowser, sUsrAg = navigator.userAgent;
    // The order matters here, and this may report false positives for unlisted browsers.

  if (sUsrAg.indexOf("Firefox") > -1) {
    sBrowser = "Mozilla Firefox";
    // "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:61.0) Gecko/20100101 Firefox/61.0"
  } else if (sUsrAg.indexOf("SamsungBrowser") > -1) {
    sBrowser = "Samsung Internet";
    // "Mozilla/5.0 (Linux; Android 9; SAMSUNG SM-G955F Build/PPR1.180610.011) AppleWebKit/537.36 (KHTML, like Gecko) SamsungBrowser/9.4 Chrome/67.0.3396.87 Mobile Safari/537.36
  } else if (sUsrAg.indexOf("Opera") > -1 || sUsrAg.indexOf("OPR") > -1) {
    sBrowser = "Opera";
    // "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36 OPR/57.0.3098.106"
  } else if (sUsrAg.indexOf("Trident") > -1) {
    sBrowser = "Microsoft Internet Explorer";
    // "Mozilla/5.0 (Windows NT 10.0; WOW64; Trident/7.0; .NET4.0C; .NET4.0E; Zoom 3.6.0; wbx 1.0.0; rv:11.0) like Gecko"
  } else if (sUsrAg.indexOf("Edge") > -1) {
    sBrowser = "Microsoft Edge";
    // "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36 Edge/16.16299"
  } else if (sUsrAg.indexOf("Chrome") > -1) {
    sBrowser = "Google Chrome or Chromium";
    // "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Ubuntu Chromium/66.0.3359.181 Chrome/66.0.3359.181 Safari/537.36"
  } else if (sUsrAg.indexOf("Safari") > -1) {
    sBrowser = "Apple Safari";
    // "Mozilla/5.0 (iPhone; CPU iPhone OS 11_4 like Mac OS X) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/11.0 Mobile/15E148 Safari/604.1 980x1306"
  } else {
    sBrowser = "unknown";
  }

  return sBrowser
}

export const device = () => {
  let device = "Unknown";
  const ua = {
      "Generic Linux": /Linux/i,
      "Android": /Android/i,
      "BlackBerry": /BlackBerry/i,
      "Bluebird": /EF500/i,
      "Chrome OS": /CrOS/i,
      "Datalogic": /DL-AXIS/i,
      "Honeywell": /CT50/i,
      "iPad": /iPad/i,
      "iPhone": /iPhone/i,
      "iPod": /iPod/i,
      "macOS": /Macintosh/i,
      "Windows": /IEMobile|Windows/i,
      "Zebra": /TC70|TC55/i,
  }
  Object.keys(ua).map(v => navigator.userAgent.match(ua[v]) && (device = v));
  return device;
}

export const recalculateStrategyObject = (objData, decimal=0) => {
  if(!Object.keys(objData).length) return {};

  Object.keys(objData).forEach(key => {
    objData[key] = round(objData[key]*100, decimal);
  });

  let sumVal = sum(Object.values(objData));
  let diff = 100 - sumVal;
  let largestDecision = Object.keys(objData).reduce((a, b) => objData[a] > objData[b] ? a : b);

  objData[largestDecision] = round(objData[largestDecision] + diff, decimal);
  return objData;
}

export const addPotValueToRawEvData = (evArray, potValue) => {
  if(isEmpty(evArray)) return ''

  evArray = Array.isArray(evArray) ? evArray : evArray.split(' ')
  return evArray.map( evValue => isNaN(evValue) ? 0.0 : parseFloat(evValue) + parseFloat(potValue)).join(' ')
}

export const addPotValueToRawEvRescaleData = (evArray, potValue) => {
  if(isEmpty(evArray)) return ''

  evArray = Array.isArray(evArray) ? evArray : evArray.split(' ')
  return evArray.map( evValue => isNaN(evValue) ? evValue : parseFloat(evValue) + parseFloat(potValue)).join(' ')
}

export const addPotValueToEvObjectData = (evObject, potValue) => {
  for( let key in evObject) {
    evObject[key] = parseFloat(evObject[key]) + parseFloat(potValue)
  }
  return evObject
}

export const fullAccessWithFreeAWeek = (code, createdDateString, email = '') => {
  const createdDate = new Date(moment(createdDateString).tz("America/Los_Angeles").format("YYYY-MM-DD HH:mm:ss z")).getTime()
  const expiredDate = new Date("2021-09-01 00:00:00 PDT").getTime()
  const currentDate = new Date(moment().tz("America/Los_Angeles").format("YYYY-MM-DD HH:mm:ss z")).getTime()
  return (isEqual(code, 'pads') || PADS_ACCESS_EMAILS_LIST.includes(email)) && createdDate < expiredDate && currentDate < expiredDate
}

export const userSubscriptionInvalid = (user) => {
  return !isEmpty(user) &&
         !isEqual(user.role, 'admin') &&
         (isEmpty(user.admin_access) || isEqual(user.admin_access, 'free')) &&
         isEmpty(user.accessable_game_type)
}

export const codeDiscountAfiliate = (code) => {
  switch(code) {
    case 'pokercode':
      return 'PC45'
    case 'pc':
      return 'POKERCODE'
    case 'pads':
      return 'PADS35'
    case 'pav':
      return 'PAV35'
    case code:
      return code
    default:
      return ''
  }
}

export const revertCodeSubscribe = (pathname) => {
  if(toLower(pathname)) {
    return URL_TO_AFFILIATE_CODE_MAPPING[pathname]
  } else {
    return ''
  }
}
