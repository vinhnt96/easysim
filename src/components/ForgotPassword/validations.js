import * as yup from 'yup';

export const buildSchema = (locale, translation) => {
  let t = translation

  let emailRules = locale === 'en' ?
    yup.string().required().email() :
    yup.string()
       .required(t('validation.required'))
       .email(t('validation.wrong_format'))

  return yup.object({
    email: emailRules
  })
}
