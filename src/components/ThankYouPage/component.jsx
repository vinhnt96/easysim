import React, { Component,Fragment } from 'react'
import './style.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { isEmpty } from 'lodash'
import ReactPixel from 'react-facebook-pixel'

class ThankYouPage extends Component {

  componentDidMount() {
    const state = this.props.location.state || {}
    const details = state.details || {}
    if(isEmpty(details)) return this.props.history.replace('/')
    
    const options = {
      autoConfig: true,
      debug: false
    };
    const { totalInPayoutCurrency, payoutCurrency, live, id} = details
    if(live) {  // TRACKING ONLY FOR LIVE
      ReactPixel.init('1775670809282014', options);
      ReactPixel.fbq('track', 'Purchase', { currency: payoutCurrency, value: totalInPayoutCurrency }, { eventID: `subs-${id}` })
    }
  }  

  planNameDisplay(productId) {
    switch(productId) {
      case 'cash':
        return 'CASH';
      case 'cash-3-monthly':
        return 'CASH 3 Monthly';
      case 'cash-yearly':
        return 'CASH yearly';
      case 'mtts':
        return 'MTTs';
      case 'mtts-3-monthly':
        return 'MTTs 3 mothly';
      case 'mtts-yearly':
        return 'MTTs yearly';
      case 'cash-mtts':
        return 'CASH + MTTs';
      case 'cash-mtts-3-monthly':
        return 'CASH + MTTs 3 Monthly';
      case 'cash-mtts-yearly':
        return 'CASH + MTTs yearly';
      default:
        return '';
    }
  }

  render() {
    const t = this.context.t
    const state = this.props.location.state || {}
    const { notSignIn } = state
    const details = state.details || {}
    if(isEmpty(details)) return <Fragment> </Fragment>

    const { items, totalInPayoutCurrencyDisplay, payoutCurrency, taxInPayoutCurrencyDisplay,
      discountWithTaxInPayoutCurrencyDisplay, discountInPayoutCurrency, subtotalInPayoutCurrency } = details
    let formatter = new Intl.NumberFormat('en-US', {
      style: 'currency',
      currency: payoutCurrency,
    });
    const totalBeforeDiscount = `${formatter.format(discountInPayoutCurrency + subtotalInPayoutCurrency)} ${payoutCurrency}`
    
    return (
      <div className='thank-you-page'>
        <div className='text-center w-100 title heading-font'>THANK YOU FOR YOUR ORDER</div>
        <div className={`content-container ${notSignIn && 'closed-margin'}`}>
          <div className='content'>
            {
              notSignIn &&
              <div className='message-block text-secondary'> 
                <div className='message'>
                  <p>Congratulations on your new subscription!</p>
                  <p>We've sent you an email to set up your account, please check your inbox.</p>
                  <p>In case you don't receive an email, please contact: <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a></p>
                  <p>Thanks you!</p>
                </div>
              </div>
            }
            <br/>
            <div className='font-weight-bold mb-3 font-size-20'>Order details:</div>
            <table className='w-100'>
              <thead>
                <tr>
                  <th>Item</th>
                  <th>Offer Price</th>
                  <th>Tax</th>
                  <th>Item Total</th>
                </tr>
              </thead>
              <tbody>
              {
                items.map((item) => {
                  return (
                    <tr>
                      <td className='align-top text-white'>{this.planNameDisplay(item.product)}</td>
                      <td>
                        <div className='text-white'>{item.subtotalDisplay} {payoutCurrency}</div>
                        <div className='font-size-12 mt-2'>
                          List Price: {formatter.format(item.discountInPayoutCurrency + item.subtotalInPayoutCurrency)} {payoutCurrency}
                        </div>
                        { item.coupon &&
                          <div className='font-size-12 mt-3'>
                            Coupon Code: {item.coupon}
                          </div>
                        }
                      </td>
                      <td className='align-top text-white'>{taxInPayoutCurrencyDisplay} {payoutCurrency}</td>
                      <td className='align-top text-white font-weight-bold'>{item.subtotalInPayoutCurrencyDisplay} {payoutCurrency}</td>
                    </tr>
                  )
                })
              }
              </tbody>
            </table>
            <hr />
            <div className='row p-0 justify-content-end'>
              <div className='col-4 m-0'>
                <div className='d-flex justify-content-between'><div>Subtotal: </div><div className='font-weight-bold'>{totalBeforeDiscount}</div></div>
                <div className='d-flex justify-content-between'><div>Discount: </div><div>{discountWithTaxInPayoutCurrencyDisplay}</div></div>
                <div className='d-flex justify-content-between'><div>Tax: </div><div>{taxInPayoutCurrencyDisplay}</div></div>
                <div className='d-flex justify-content-between'><div>Total: </div><div className='font-weight-bold'>{totalInPayoutCurrencyDisplay}</div></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ThankYouPage.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({

})

export default connect(
  mapStoreToProps,
  null
)(ThankYouPage)
