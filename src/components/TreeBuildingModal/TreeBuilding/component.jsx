import React, { Component, Fragment } from 'react'
import './stylesheet.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Card from 'components/Card/component'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleDown } from '@fortawesome/free-solid-svg-icons'
import CustomCheckbox from 'components/CustomCheckbox/CustomCheckbox'
import { dataForEachComboGenerator } from 'services/matrix_service'
import { SUITS_FOR_FILTER } from 'config/constants'
import Loader from 'components/Shared/Loader/Loader'
import loadable from '@loadable/component'
const RangeMatrix = loadable(() => import('components/MainPage/RangeMatrix/RangeMatrix'))
class CTreeBuilding extends Component {

  constructor(props) {
    super(props)

    this.positions = ['ip', 'oop']
    this.round = ['flop', 'turn', 'river']
  }

  cardElements(cards) {
    return cards.map((card, index) =>
      <Card
        key={index}
        rank={card.slice(0, -1) || ''}
        suit={card[card.length-1] || ''}
        className='board-card'
        size='small'
        index={index}/>
    );
  }

  convertSizesToPercentValue(sizes){
    if(sizes === 'None') return sizes
    return typeof sizes === "string" ? sizes.split(',').filter(size => !!size).map( size => `${size}%`).join(',') : ''
  }

  renderOptionBotton() {
    return this.positions.map( (position, idx) => {
      return (
        <div key={position + idx} className={`params-option option-${position} row mb-4`}>
          {
            this.round.map( (round, idx) => {
             return this.renderOption(round,position)
          } )}
        </div>
      )
    })
  }

  renderOption(round, position) {
    let t = this.context.t
    let { treeInfo } = this.props.game

    return (
      <div key={round} className={`col-md-4 col-sm-6 seperate-collumn row no-gutters ${round}-option`}>
        <div className='text-left'>
          <h5 className='title'> {round.charAt(0).toUpperCase() + round.slice(1).toLowerCase() + ' ' + position.toUpperCase() } </h5>
          <div className='option-info'>
            <p className='sizes-content font-size-14'> {t('tree_building.bet_size')} : <span> {this.convertSizesToPercentValue(treeInfo[position][round].bet_size)} </span> </p>
            <p className='sizes-content font-size-14'> {t('tree_building.raise_size')}: <span> {this.convertSizesToPercentValue(treeInfo[position][round].raise_size)} </span> </p>
            {
              position === 'oop' && ['turn', 'river'].includes(round) &&
              <p className='sizes-content font-size-14'> {t('tree_building.donk_size')}: <span> {this.convertSizesToPercentValue(treeInfo[position][round].donk_bet_size)} </span> </p>
            }
            <div className='chk-div'>
              <CustomCheckbox className='chk-add-allin' data={`add-allin-${round}-${position}`}
                              checked={treeInfo[position][round].add_allin === "True"}
                              handleChecked={this.handleChange} />
              <label htmlFor={`chk-add-allin-${round}-${position}`} className='font-size-15 cursor-pointer ml-3'> {t('tree_building.add_allin')} </label>
            </div>
            {
              position === 'ip' &&
              <div className='chk-div'>
                <CustomCheckbox className='chk-donk-bet' data={`donk-bet-${round}-${position}`}
                                checked={false}
                                handleChecked={this.handleChange} />
                <label htmlFor={`chk-donk-bet-${round}-${position}`} className='font-size-15 cursor-pointer ml-3'> {t('tree_building.dont_bet')} </label>
              </div>
            }
          </div>
        </div>
      </div>
    )
  }

  handleChange() {
  }

  render(){
    const t = this.context.t
    const { game, handMatrix } = this.props
    const { treeInfo, flopCards } = game
    const { fetching } = treeInfo
    const combosWithIndex = handMatrix ? handMatrix.combosWithIndex : {}
    let rangeIpData = !!treeInfo.range_ip ? dataForEachComboGenerator({ ranges_ip: treeInfo.range_ip.join(' '), flopCards, view: 'ranges_ip', combosWithIndex }) : {}
    let rangeOopData = !!treeInfo.range_oop ? dataForEachComboGenerator({ ranges_oop: treeInfo.range_oop.join(' '), flopCards, view: 'ranges_oop', combosWithIndex }) : {}

    return <div className='tree-building-container'>
      <div className={`content-body ${fetching !== 'done' ? 'd-flex justify-content-center' : ''}`}>
      { fetching !== 'done' && <Loader /> }
      { fetching === 'done' &&
        <Fragment>
          <div className='top-section row'>
            <div className='col-md-5 left-col'>
              <div className='select-board d-flex align-items-center justify-content-between'>
                <div className='card-elements'>
                  { this.cardElements(flopCards) }
                </div>
                <FontAwesomeIcon icon={faAngleDown} className='icon-down' />
              </div>
              <div className='select-range'>
                <div className='row justify-content-around mb-3'>
                  <div className='col-sm-6 range-oop'>
                    <RangeMatrix
                      dataForEachCombos={rangeOopData}
                      view='ranges_oop'
                      suitsForFilter={SUITS_FOR_FILTER}
                      handleOnHoverOnHand={() => {}}
                      changeClickedHandItem={() => {}}
                      rangeExplorerShowing={true}
                      weightedStrategy={true}
                      hideHandItemContent={true}
                      decisionFilter=''
                    />
                    <div className='title'>
                      OOP (OOP)
                    </div>
                  </div>
                  <div className='col-sm-6 range-ip'>
                    <RangeMatrix
                      dataForEachCombos={rangeIpData}
                      view='ranges_ip'
                      suitsForFilter={SUITS_FOR_FILTER}
                      handleOnHoverOnHand={() => {}}
                      changeClickedHandItem={() => {}}
                      rangeExplorerShowing={true}
                      weightedStrategy={true}
                      hideHandItemContent={true}
                      decisionFilter=''
                    />
                    <div className='title'>
                      IP (IP)
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className='right-col col-md-7'>
              <div className='mb-3'>
                <div className='col-info col-info-4 pl-0'>
                  <div>
                    <p>  {t('tree_building.starting_pot')} : <span> {treeInfo.starting_pot} </span>  </p>
                    <span>   {t('tree_building.effect_stack')} : <span> {treeInfo.effective_stacks} </span> </span>
                  </div>
                </div>
                <div className='col-info col-info-2 separate-line-div'>
                  <div className='seperate collumn'></div>
                </div>
                <div className='col-info col-info-4'>
                  <div>
                    <p>  {t('tree_building.min_bet_size')} : <span> {treeInfo.minimum_betsize} </span> </p>
                    <span>  {t('tree_building.max_spread')} : <span> 20 </span> </span>
                  </div>
                </div>
              </div>
              <p className='mt-4'> All-in threshold: <span> {treeInfo.allin_threshold} </span>% of the initial effective stack </p>
              <p> "Add allin" only if less than <span> {treeInfo.add_allin_less_than} </span>% of pot </p>
            </div>
          </div>
          <div className="seperate my-3"></div>
          <div className='bottom-section'>
            { this.renderOptionBotton() }
          </div>
        </Fragment>
      }
      </div>
    </div>
  }

}

CTreeBuilding.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  game: store.game,
})

const mapDispatchToProps = (dispatch) => ({

})

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CTreeBuilding)
