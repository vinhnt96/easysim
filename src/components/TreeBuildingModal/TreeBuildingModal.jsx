import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import './TreeBuildingModal.scss'
import loadable from '@loadable/component'
const TreeBuilding = loadable(() => import('./TreeBuilding/component'))

export default class TreeBuildingModal extends Component {

  render() {
    const { modalShow, setModalShow } = this.props;

    return (
        <Modal
          show={modalShow}
          onHide={() => { setModalShow('') }}
          dialogClassName='tree-building-modal'
          backdrop="static"
          aria-labelledby='tree-building-modal'
        >
          <Modal.Header closeButton>
            <Modal.Title id="tree-building-modal"> Tree Building Parameters </Modal.Title>
          </Modal.Header>
          <Modal.Body bsPrefix='custom-modal-body'>
            <TreeBuilding closeModal={setModalShow} />
          </Modal.Body>
        </Modal>
      )
  }

}
