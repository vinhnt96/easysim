import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import Modal from 'react-bootstrap/Modal'
import './TimedOutNotifyModal.scss'
import { userSubscriptionInvalid, fullAccessWithFreeAWeek } from 'utils/utils'

class CTimedOutNotifyModal extends Component {
  render() {
    const { modalShow, currentUser } = this.props;
    const { affiliate_code, createdAt, email } = currentUser
    const freeAWeek = fullAccessWithFreeAWeek(affiliate_code, createdAt, email)
    return (
      <Modal
        show={modalShow}
        dialogClassName='modal-l'
        backdrop="static"
        aria-labelledby='refresh-page-modal'
      >
        <Modal.Body bsPrefix='custom-modal-body'>
          <div className='content pt-2 pb-0 px-0'>
            <div className='p-3 text-center pt-4'>
                You've got timed out after 15 minutes.
                { (freeAWeek || !userSubscriptionInvalid(currentUser)) && <p>Please revisit Strategy Selection page or My Sims to re-open the simulation.</p>}
            </div>

            { !freeAWeek && userSubscriptionInvalid(currentUser) ?
              <div className='my-3 row m-0 p-0 justify-content-center pb-4'>
                <div className='col-6'>
                  <div className='btn-primary'>
                    <Link to={{ pathname: '/'}} className='font-size-14 text-white font-weight-bold  p-3 w-100' style={{ textDecoration: 'none' }}>
                      Return To Homepage
                    </Link>
                  </div>
                </div>
              </div> :
              <div className='col-12 my-3 row m-0 p-0 justify-content-center pb-4'>
              <div className='col-9 row m-0 p-0'>
                <div className='col-6 p-1 pr-3'>
                  <div className='btn-primary'>
                    <Link to={{ pathname: '/strategy-select'}} className='font-size-14 text-white font-weight-bold  p-3 w-100' style={{ textDecoration: 'none' }}>
                      New Sim
                    </Link>
                  </div>
                </div>
                <div className='col-6 p-1 pl-3'>
                  <div className='btn-primary'>
                    <Link to={{ pathname: '/my-sims'}} className='font-size-14 text-white font-weight-bold  p-3 w-100' style={{ textDecoration: 'none' }}>
                      My Sims
                    </Link>
                  </div>
                </div>
              </div>
            </div>}
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}

CTimedOutNotifyModal.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CTimedOutNotifyModal)
