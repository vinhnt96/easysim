import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from 'react-bootstrap/Modal'
import './CompareEVModal.scss'
import CustomCheckbox from 'components/CustomCheckbox/CustomCheckbox'
import { generateCompareEVoptions, formatToBigBlind, decisionCombosButtons } from 'utils/utils'
import { changeMatrixView } from 'actions/hand-matrix.actions'
import { dispatchCompareEV } from 'actions/game.actions'
import { calculateStrategyKey } from 'services/game_play_service'

class CCompareEVModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedOptions: this.props.game.comparedDecisions,
    }

    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleOnlickOk = this.handleOnlickOk.bind(this);
  }

  handleOnChange(e, option) {
    let selectedOptions = [...this.state.selectedOptions];
    let newSelectedOptions = selectedOptions;

    if(e.currentTarget.checked) {
      if(selectedOptions.length < 2) {
        newSelectedOptions.push(option);
      } else {
        newSelectedOptions.shift();
        newSelectedOptions.push(option);
      }
    } else {
      newSelectedOptions.splice(newSelectedOptions.indexOf(option), 1)
    }

    this.setState({ selectedOptions: newSelectedOptions });
  }

  handleOnlickOk() {
    this.props.dispatchCompareEV(this.state.selectedOptions)
    this.props.setModalShow('')
  }

  compareEVOptionEles() {
    const { game, currentUser: { preferences, strategy_selection }, decisions } = this.props
    const { currentPosition } = game
    const options = generateCompareEVoptions(game, decisions)
    const listKeyDisplay = decisionCombosButtons(game, preferences, currentPosition, decisions).slice(0, -1).map( decision => decision.value)
    const stack_size = parseInt(strategy_selection.stack_size)
    const optionsDisplay = options.filter( option => listKeyDisplay.includes(option.value))

    return optionsDisplay.map((option, index) => {
      const selectedOptions = this.state.selectedOptions;
      return (
        <div key={index} className='col-3 p-0 d-flex align-items-center'>
          <CustomCheckbox
            data={option.value}
            handleChecked={this.handleOnChange}
            checked={selectedOptions.includes(option.value)}
          />
          <label htmlFor={`cbx-${option.value}`} className='mb-1'>
            <span className='ml-2 mb-1 cursor-pointer'>
              { stack_size ? formatToBigBlind( isNaN(parseInt(option.raw_value)) ? option.raw_value : calculateStrategyKey(option.raw_value, game)) : option.text}
            </span>
          </label>
        </div>
      );
    });
  }

  render() {
    const { modalShow, setModalShow } = this.props;
    let t = this.context.t

    return (
      <React.Fragment>
        <Modal
        show={modalShow}
        onHide={() => { setModalShow('') }}
        dialogClassName='compare-ev-modal'
        backdrop="static"
        aria-labelledby='compare-ev-modal'
        >
          <Modal.Body bsPrefix='custom-modal-body'>
            <Modal.Header bsPrefix='custom-modal-header d-flex justify-content-between align-items-center' closeButton>
              <Modal.Title id="compare-e-modal">Compare EV</Modal.Title>
            </Modal.Header>
            <div className='content'>
              <div className='my-2'>
                { t('compare_ev_select_two_action') }
              </div>
              <div className='row p-0 m-0'>
                {this.compareEVOptionEles()}
              </div>
              <div className='col-12 mt-3 row m-0 p-0 justify-content-end mb-2'>
                <div className='col-8 row m-0 p-0'>
                  <div className='col-6 p-2'>
                    <div
                      className={`btn-primary ${![0, 2].includes(this.state.selectedOptions.length) ? 'cursor-not-allowed' : ''}`}
                      onClick={ this.handleOnlickOk }
                    >
                      Ok
                    </div>
                  </div>
                  <div className='col-6 p-2'>
                    <div className='btn-primary' onClick={() => {setModalShow('')}}>Cancel</div>
                  </div>
                </div>
              </div>
            </div>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    )
  }
}

CCompareEVModal.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  i18nState: store.i18nState,
  game: store.game,
  handMatrix: store.handMatrix,
  decisions: store.decisionsTree.decisions,
})

const mapDispatchToProps = {
  changeMatrixView,
  dispatchCompareEV
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CCompareEVModal)


