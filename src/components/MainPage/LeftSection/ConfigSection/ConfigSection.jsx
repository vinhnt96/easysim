import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './ConfigSection.scss'
import { OPTIONS_LEFT, OPTIONS_RIGHT, RUNOUTS_EXPLORER_DEFAULT_VIEW, TWO_MINUTES } from 'config/constants'
import { changeMatrixView, updateDecisionFilter } from 'actions/hand-matrix.actions'
import { positionsForQueryBuilder, addMoreToGameData } from 'services/game_play_service'
import { convertFlopCards } from 'services/convert_cards_services'
import { handleClickOnViewOptions,
         handleOnOpeningRunoutsExplorer,
         updateGameData,
         fetchDataForTurnRiverRound,
         updateTreeInfo,
         dispatchCompareEV } from 'actions/game.actions'
import {isEqual, chunk, cloneDeep} from 'lodash'
import axios from 'axios'
import { Button } from 'react-bootstrap'
import { simTypeMapping, generateCompareEVoptions, decisionCombosButtons } from 'utils/utils'
import { fetchingRangeExplorerData } from 'actions/range-explorer.actions'
import TooltipQuestion from 'components/Shared/TooltipQuestion/component'
import OdinLoader from 'components/Shared/OdinLoader/OdinLoader'
import loadable from '@loadable/component'

const RangeExporerModal = loadable(() => import('../RangeExplorerModal/RangeExplorerModal'))
const OptionsModal = loadable(() => import('../OptionsModal/OptionsModal'))
const RunoutsExplorerModal = loadable(() => import('../RunoutsExplorerModal/RunoutsExplorerModal'))
const TreeBuildingModal = loadable(() => import('components/TreeBuildingModal/TreeBuildingModal'))
const CompareEVModal = loadable(() => import('../CompareEVModal/CompareEVModal'))
const NotesPopup = loadable(() => import('components/MySim/NotesPopup/NotesPopup'))

class CConfigSection extends Component {
  constructor() {
    super();

    this.state = {
      modalShow: '',
      showPopupTryAgain: false,
      disabled: false
    }
    this.timer = null
    this.setModalShow = this.setModalShow.bind(this)
    this.handleOnClickLeftOption = this.handleOnClickLeftOption.bind(this)
    this.handleOnClickRightButton = this.handleOnClickRightButton.bind(this)
  }

  setModalShow(modalName) {
    this.setState({ modalShow: modalName })
  }

  getTreeInfo(strategy_selection, cardsConvertingTemplate, modalName){
    const { updateTreeInfo } = this.props
    const simParam = {
      stack_size: parseInt(strategy_selection.stack_size),
      positions: positionsForQueryBuilder(strategy_selection.positions),
      sim_type: simTypeMapping(strategy_selection.sim_type),
      flop: convertFlopCards(strategy_selection.flop_cards, cardsConvertingTemplate)
    }

    axios.post(`/tree_building_parameter/get_tree_info`,simParam).then( res => {
      const treeData = res.data.data
      updateTreeInfo({...treeData, fetching: 'done'})
    }).catch(err => console.error(err))

    this.setState({modalShow: modalName})
  }

  componentWillUnmount() {
    return () => clearTimeout(this.timer)
  }

  handleActionTryAgain = () => {
    this.setState({
      showPopupTryAgain: false,
      modalShow: '',
      disabled: true
    })
    this.props.fetchDataForTurnRiverRound({ fetching: '' })
  }

  handleOnClickLeftOption(value) {
    let {view} = this.props.handMatrix
    let { nodes, node_count, currentRound } = this.props.game
    let cardsConvertingTemplate = this.props.cardsConvertingTemplate || {}
    let viewValue = value !== view ? value : ''
    const strategy_selection = JSON.parse(sessionStorage.getItem("strategy_selections")) || this.props.currentUser.strategy_selection
    if(!!viewValue && currentRound === 'flop' ) {
      let dataParams = {
        game_type: strategy_selection.game_type,
        stack_size: strategy_selection.stack_size,
        specs: strategy_selection.specs,
        sim_type: strategy_selection.sim_type,
        flops: convertFlopCards(strategy_selection.flop_cards, cardsConvertingTemplate).join(''),
        positions: positionsForQueryBuilder(strategy_selection.positions),
        nodes: nodes,
        node_count: node_count
      }
      this.props.handleClickOnViewOptions({...dataParams, view: viewValue}, this.props.game)
    } else {
      this.props.changeMatrixView(viewValue)
    }
  }

  async fetchNextFlopsDetailsData() {
    const { game, cardsConvertingTemplate } = this.props;
    const { nodes, node_count } = game
    const strategy_selection = JSON.parse(sessionStorage.getItem("strategy_selections")) || this.props.currentUser.strategy_selection
    let dataParams = {
      game_type: strategy_selection.game_type,
      stack_size: strategy_selection.stack_size,
      specs: strategy_selection.specs,
      sim_type: strategy_selection.sim_type,
      flops: convertFlopCards(strategy_selection.flop_cards, cardsConvertingTemplate).join(''),
      positions: positionsForQueryBuilder(strategy_selection.positions),
      nodes: nodes,
      node_count: node_count,
      view: 'equity_ip, equity_oop, ev_oop, ev_ip, ev_oop2, ev_ip2'
    }
    this.props.fetchingRangeExplorerData(true)
    const res = await axios.post('/nodes/next_flops_details_data', dataParams)
    let moreData = res.data.data
    let newGameData = addMoreToGameData(moreData, this.props.game)
    this.props.updateGameData(newGameData)
  }

  compareEVOptions() {
    const { game, currentUser: { preferences }, decisions } = this.props
    const { currentPosition } = game
    const listKeyDisplay = decisionCombosButtons(game, preferences, currentPosition, decisions).slice(0, -1).map( decision => decision.value)
    return generateCompareEVoptions(game, decisions).filter( option => listKeyDisplay.includes(option.value))
  }

  handleOnClickRightButton(value) {
    const { game, handleOnOpeningRunoutsExplorer } = this.props;
    const { nodes, currentRound } = game
    if(currentRound === 'flop' && ['range-explorer', 'compare-ev'].includes(value)) {
      if(value === 'range-explorer') {
        this.fetchNextFlopsDetailsData().then(res => {
          this.props.fetchingRangeExplorerData(false)
        })
        this.setState({ modalShow: value })
      } else {
        switch(this.compareEVOptions().length) {
          case 0:
          case 1:
            return;
          case 2:
            this.fetchNextFlopsDetailsData().then(res => {
              this.props.dispatchCompareEV(this.compareEVOptions().map(option => { return option.value}))
            })
            break;
          default:
            this.fetchNextFlopsDetailsData()
            this.setState({ modalShow: value })
        }
      }
    } else {
      switch(value) {
        case 'runouts-explorer':
          if(['turn', 'river'].includes(currentRound)) {
            this.timer = setTimeout(() => this.setState({ showPopupTryAgain: true }), TWO_MINUTES)
            handleOnOpeningRunoutsExplorer(RUNOUTS_EXPLORER_DEFAULT_VIEW, nodes, this.timer)
          } else { return; }
          this.setState({ modalShow: value, disabled: false })
          break;
        case 'tree-building':
          const { game: { treeInfo: { fetching } } } = this.props
          if(isEqual(fetching, 'done')) {
            this.setState({modalShow: value})
          } else {
            const { currentUser, cardsConvertingTemplate } = this.props
            const strategy_selection = JSON.parse(sessionStorage.getItem("strategy_selections")) || currentUser.strategy_selection
            this.getTreeInfo(strategy_selection, cardsConvertingTemplate, value)
          }
          break;
        default:
          this.setState({ modalShow: value, disabled: false })
      }
    }
  }

  renderOptionsLeft(t) {
    let { handleWeightedStrategyOnChange, weightedStrategy, currentUser } = this.props
    let { view } = this.props.handMatrix
    let preferences = currentUser.preferences || {}
    let hideExplanation = preferences.hide_explanation

    return <div className='col-md-6 left-button-section px-0 d-flex flex-column mb-2'>
      {OPTIONS_LEFT.map((option, index) => {
        let viewValueIp = option.value === 'strategy' ? 'strategy-ev' : `${option.value}_ip`
        let viewValueOop = option.value === 'strategy' ? '' : `${option.value}_oop`

        return (
          <div key={`left-${index}`} className={`row mb-2 d-flex align-items-center no-gutters d-flex align-items-stretch`}>
            <div className='col-4 p-0 m-0 d-flex align-items-center justify-content-left option'>
              {t(option.label)}
              { !hideExplanation &&
                <Fragment>
                  {
                    option.label === 'equity' &&
                      <TooltipQuestion explanationName='equity-explanation' place='right' type='dark'>
                        {t('tooltip_mainpage.equity')}
                      </TooltipQuestion>
                  }
                  {
                    option.label === 'ev' &&
                      <TooltipQuestion explanationName='ev-explanation' place='right' type='dark'>
                        {t('tooltip_mainpage.ev')}
                      </TooltipQuestion>
                  }
                </Fragment>
              }
            </div>
            <div className='col-4 m-0 option-button-containers-left'>
              <div onClick={(e) => this.handleOnClickLeftOption(viewValueOop)}
                   className={`${view === viewValueOop ? 'selected' : ''} oop-button d-flex align-items-center justify-content-center`}>
                { option.label === 'strategy' ? 'Strat' : t('oop') }
              </div>
            </div>
            <div className='col-4 m-0 option-button-containers-left'>
              <div onClick={(e) => this.handleOnClickLeftOption(viewValueIp)}
                   className={`${view === viewValueIp ? 'selected' : ''} ip-button d-flex align-items-center justify-content-center ${ option.label === 'strategy' ? 'font-size-13' : '' }`}>
                { option.label === 'strategy' ? 'Strat + EV' : t('ip') }
              </div>
            </div>
          </div>
        )
      })}
      <div className="row no-gutters">
        <div className="col-4"></div>
        <div className="col-8">
          { !hideExplanation &&
            <TooltipQuestion explanationName='weighted-strategy-explanation' place='top' type='dark' offset={{top: 0, right: 50}}>
              <p>Displays your range relative to how often it arrives at this node with those combos.</p>
              <div>For eg. if you begin the hand with all 4 combos of T9s and only get to the turn with 1 combo of T9s, the T9s square will be filled 25% of the way.</div>
            </TooltipQuestion>
          }
          <div onClick={handleWeightedStrategyOnChange}
               className={`weighted-strategy-button ${weightedStrategy ? 'selected' : ''} d-flex align-items-center justify-content-center`}>
            {t('weighted_strategy')}
          </div>
        </div>
      </div>
    </div>
  }

  renderOptionRight(t) {
    let optionButtons = chunk(OPTIONS_RIGHT, 2).map((options, idx) => {
      return (
        <div className='d-flex justify-content-around' key={`chunk-${idx}`}>
          {options.map((option, index) => this.renderOptionRightButton(index, option, t))}
        </div>
      )
    })

    return (
      <div className='col-md-6 right-button-section pr-0 pl-0 option-buttons d-flex flex-column mb-2'>
        <div className='right-buttons d-flex flex-column justify-content-between h-100'>
          { optionButtons }
        </div>
      </div>
    )
  }

  renderOptionRightButton(index, option, t) {
    let { currentUser, game } = this.props
    let preferences = currentUser.preferences || {}
    let hideExplanation = preferences.hide_explanation
    const isBordered = this.props.handMatrix.view === option
    let compareEVOptions = []
    if(option === 'compare-ev') {
      compareEVOptions = this.compareEVOptions()
    }
    let btnClass = (option === 'runouts-explorer' && game.currentRound === 'flop') || (option === 'compare-ev' && compareEVOptions.length < 2) ? 'disabled' : ''

    return (
      <div key={`right-${index}`} className={`${ index % 2 !== 0 ? 'mr-15' : ''} col-6 flex-grow-1 option-button-containers-right position-relative`}>
        <div
          className={`btn-primary d-flex align-items-center justify-content-center ${isBordered && 'bordered'} ${btnClass}`}
          onClick={() => { this.handleOnClickRightButton(option) }}
        >
          {t(option)}
        </div>
        { !hideExplanation &&
          <Fragment>
          {
            option === 'compare-ev' &&
              <TooltipQuestion explanationName='compare-ev-explanation' place='top' type='dark'>
                <div>This will visually display the EV gained by one bet size over another.</div>
                <div className="d-flex align-items-center"><div className='color-square dark-red'></div> = Large EV loss</div>
                <div className="d-flex align-items-center"><div className='color-square light-red'></div> = Slight EV loss</div>
                <div className="d-flex align-items-center"><div className='color-square orange'></div> = EV neutral</div>
                <div className="d-flex align-items-center"><div className='color-square yellow'></div> = Slight EV gain</div>
                <div className="d-flex align-items-center"><div className='color-square green'></div> = Large EV gain</div>
              </TooltipQuestion>
          }
          {
            option === 'runouts-explorer' &&
              <TooltipQuestion explanationName='runnouts-explorer-explanation' place='top' type='dark'>
                Displays the strategy and EV/EQ across every possible turn/river card
              </TooltipQuestion>
          }
          </Fragment>
        }
      </div>
    )
  }

  renderModal() {
    const { showPopupTryAgain, disabled } = this.state;
    if (!showPopupTryAgain)
      return null
    return (
      <div className="popup-warning">
        <p>Please try again later</p>
        <Button
          className="btn btn-primary"
          onClick={this.handleActionTryAgain}
          disabled={disabled}
        >OK</Button>
      </div>
    )
  }

  render() {
    const { game } = this.props;
    const fetching = game.fetching;
    const t = this.context.t
    const { currentUser } = this.props
    let sim = JSON.parse(sessionStorage.getItem("user_strategy_selection")) ||
      cloneDeep(currentUser.user_strategy_selection) || { id: null };

    return (
      <div className='config-section'>
        { this.renderModal() }
        <div className="section row no-gutters justify-content-center ">
          {this.renderOptionsLeft(t)}
          {this.renderOptionRight(t)}
          { this.state.modalShow === 'range-explorer' &&
            <RangeExporerModal
              modalShow={this.state.modalShow === 'range-explorer'}
              setModalShow={this.setModalShow} />
          }
          <RunoutsExplorerModal
            modalShow={this.state.modalShow === 'runouts-explorer'}
            setModalShow={this.setModalShow} />
          <OptionsModal
            modalShow={this.state.modalShow === 'options'}
            setModalShow={this.setModalShow} />
          <TreeBuildingModal
            modalShow={this.state.modalShow === 'tree-building'}
            setModalShow={this.setModalShow} />
          {
            this.state.modalShow === 'compare-ev'
            &&  <CompareEVModal
                modalShow={this.state.modalShow === 'compare-ev'}
                setModalShow={this.setModalShow}/> 
          }
          {this.state.modalShow === 'notes' && <NotesPopup
            sim={sim}
            show={true}
            setShowNotesPopup={this.setModalShow}
          />}
        </div>
        { fetching === 'show_runouts_loading' && <OdinLoader
            disableCloseBtn={true}
            disableOdinAnimation={true}
          />
        }
      </div>
    )
  }
}

CConfigSection.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  handMatrix: store.handMatrix,
  game: store.game,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  cardsConvertingTemplate: store.cardsConverting.cardsConvertingTemplate,
  decisions: store.decisionsTree.decisions,
})

const mapDispatchToProps = {
  changeMatrixView,
  updateDecisionFilter,
  handleClickOnViewOptions,
  handleOnOpeningRunoutsExplorer,
  fetchDataForTurnRiverRound,
  updateGameData,
  updateTreeInfo,
  fetchingRangeExplorerData,
  dispatchCompareEV
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CConfigSection)
