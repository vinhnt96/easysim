import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import loadable from '@loadable/component'
const SimDeck = loadable(() => import('./SimDeck/SimDeck'))
const Header = loadable(() => import('components/Shared/Header/Header'))
const SelectTurnCard = loadable(() => import('./SelectTurnCard/SelectTurnCard'))
const ConfigSection = loadable(() => import('./ConfigSection/ConfigSection'))

class CLeftSection extends Component {
  render(){
    const { turnCard, riverCard, handleWeightedStrategyOnChange, weightedStrategy, cancelToken, cancelObj, setShowNotifyModal, betCompleted } = this.props
    return (
      <div className='left h-100 d-flex flex-column'>
        <Header
          setShowNotifyModal={setShowNotifyModal}
          showNodes={true}
          showDropDownButton={false}
        />
        <SimDeck
          turnCard={turnCard}
          riverCard={riverCard}
          additionalClass='flex-grow-1'
          page='main-page'
        />
        <div className="collapse-container">
          { betCompleted &&
            <SelectTurnCard
              betCompleted={betCompleted}
              turnCard={turnCard}
              riverCard={riverCard}
              cancelToken={cancelToken}
              cancelObj={cancelObj}
            />
          }
        </div>
        <div className="line"/>
        <ConfigSection
          weightedStrategy={weightedStrategy}
          handleWeightedStrategyOnChange={handleWeightedStrategyOnChange}
        />
      </div>
    )
  }
}

CLeftSection.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CLeftSection)
