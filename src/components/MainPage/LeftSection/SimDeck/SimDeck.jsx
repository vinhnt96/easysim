import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import  './SimDeck.scss'
import {
  positionsForDisplayBuilder,
  calcCurrentRoundPot,
  alternativePositionsForDisplayBuilder,
  calculatePotMainDisplay,
  addBbSuffixToValue
} from 'services/game_play_service'
import {
  CASH_GAME_PLAYER_POSITIONS,
  MTT_GAME_PLAYER_POSITIONS,
  HU_GAME_PLAYER_POSITIONS,
  IP_OOP_POSITIONS_ORDER,
  DOWNCASE_POSITIONS_MAPPING,
  DISPLAY_IN_BB,
} from 'config/constants'
import { updateStacksize } from 'actions/game.actions'
import { formatToBigBlind } from 'utils/utils'
import coint_icon from 'images/coins/coins_icon.png'
import { calculateFigures } from 'services/background_color_services'
import red_coins from 'images/coins/red_coins.png'
import dealer_button from 'images/coins/dealer_button.png'
import poker_code from 'images/Logos/poker_code.png'
import { isEqual, round } from 'lodash'
import Card from 'components/Card/component'

class EmptyCard extends Component {
  render(){
    return (
      <div className='empty-card card-on-sim-deck'></div>
    )
  }
}

class CSimDeck extends Component {

  constructor(props){
    super(props)

    this.state={
      initialStacksize: 0
    }
  }

  componentDidUpdate(prevProps){
    if(!prevProps.game.stackSize && this.props.game.stackSize && !this.state.initialStacksize) {
      this.setState({initialStacksize: this.props.game.stackSize})
    }
  }

  getCurrentPosition() {
    const { currentUser, game } = this.props
    let currentPosition = game.currentPosition;
    const strategy_selections = JSON.parse(sessionStorage.getItem("strategy_selections")) || currentUser.strategy_selection
    let positions = strategy_selections.positions;

    if(IP_OOP_POSITIONS_ORDER.indexOf(positions[0]) > IP_OOP_POSITIONS_ORDER.indexOf(positions[1])) {
      return currentPosition === 'ip' ? positions[0] : positions[1];
    } else {
      return currentPosition === 'ip' ? positions[1] : positions[0];
    }
  }

  playerPositionEles() {
    const { game, currentUser, handMatrix, decisions } = this.props;
    const strategy_selection = JSON.parse(sessionStorage.getItem("strategy_selections")) || currentUser.strategy_selection
    const { game_type, positions, specs } = strategy_selection;
    let playerPositions = []
    if(game_type === 'cash') {
      playerPositions = isEqual(specs, 'hu') ? HU_GAME_PLAYER_POSITIONS : isEqual(specs, '8max') ? MTT_GAME_PLAYER_POSITIONS : CASH_GAME_PLAYER_POSITIONS
    } else {
      playerPositions = MTT_GAME_PLAYER_POSITIONS
    }

    let ipOopPositions = IP_OOP_POSITIONS_ORDER.indexOf(positions[0]) > IP_OOP_POSITIONS_ORDER.indexOf(positions[1]) ?
      { ip: positions[0], oop: positions[1] }:
      { ip: positions[1], oop: positions[0] };

    let remainingStackSize = this.state.initialStacksize || 0
    let pot_ip = game.pot_ip || 0;
    let pot_oop = game.pot_oop || 0;
    let numCombosOop = round(game.num_combos_oop, 1)
    let numCombosIp = round(game.num_combos_ip, 1)
    let { view } = handMatrix;
    let isEvEqView = view.startsWith('ev') || view.startsWith('equity') || ['strategy-ev'].includes(view)
    let total_eq_ev_oop = !!view ? calculateFigures(view, 'oop', game) : '';
    let total_eq_ev_ip = !!view ? calculateFigures(view, 'ip', game) : '';
    let currentRoundBettingValueOop = calcCurrentRoundPot(decisions, game, 'oop', DISPLAY_IN_BB) // just a treat, to prevent the chips animation is show when fetching data
    let currentRoundBettingValueIp = calcCurrentRoundPot(decisions, game, 'ip', DISPLAY_IN_BB)

    const preferences = currentUser.preferences || {}
    const displayPositionsByDefault = preferences.display_positions_by_default || false

    return playerPositions.map((position, index) => {
      const selected = positions.includes(position);
      const highlighted = position ===  this.getCurrentPosition() ;
      const pstDisplay = displayPositionsByDefault ? position.toUpperCase() : DOWNCASE_POSITIONS_MAPPING[position].toUpperCase()
      let shouldShowTotalCombos = positions.includes(position) && view.startsWith('ranges')

      return (
        <div key={index} className={`position-block ${position} ${specs}`}>
          <div className={`position-point ${highlighted ? 'active' : ''} ${selected ? 'selected-position' : ''}`}>
            <div className='position font-size-14 '>{pstDisplay}</div>
            {
              positions.includes(position) &&
                <div className='score'>
                  { ipOopPositions.oop === position && !!remainingStackSize && formatToBigBlind(remainingStackSize - pot_oop) }
                  { ipOopPositions.ip === position && !!remainingStackSize && formatToBigBlind(remainingStackSize - pot_ip) }
                </div>
            }
          </div>
          {
            position === 'b' &&
              <img className='dealer-button' src={dealer_button} alt=''/>
          }
          {
            positions.includes(position) &&
              <div className={`total_eq_ev ${isEvEqView ? '' : 'visibility'}`}>
                { ipOopPositions.oop === position && total_eq_ev_oop }
                { ipOopPositions.ip === position && total_eq_ev_ip }
              </div>

          }
          {
            shouldShowTotalCombos &&
            <div className='total_combos'>
                <span>Combos</span>
              { ipOopPositions.oop === position && <p>{numCombosOop}</p> }
              { ipOopPositions.ip === position && <p>{numCombosIp}</p> }
            </div>
          }
          {
            ipOopPositions.oop === position && currentRoundBettingValueOop > 0 &&
              <div className={`chip-animation`}>
                <img className='' src={red_coins} alt=''/>
                <div className='position-pot font-size-12'>
                  { addBbSuffixToValue(currentRoundBettingValueOop, DISPLAY_IN_BB)}
                </div>
              </div>
          }
          {
            ipOopPositions.ip === position && currentRoundBettingValueIp > 0 &&
              <div className={`chip-animation`}>
                <Fragment>
                  <img className='' src={red_coins} alt=''/>
                  <div className='position-pot font-size-12'>
                    { addBbSuffixToValue(currentRoundBettingValueIp, DISPLAY_IN_BB) }
                  </div>
                </Fragment>
             </div>
          }
        </div>
      )
    });
  }

  render(){
    let t = this.context.t
    let {turnCard, riverCard, game, currentUser, decisions } = this.props
    const strategy_selection = JSON.parse(sessionStorage.getItem("strategy_selections")) || currentUser.strategy_selection
    const preferences = currentUser.preferences || {}
    const displayPositionsByDefault = preferences.display_positions_by_default || false
    let { stack_size, sim_type, positions, game_type, specs } = strategy_selection;
    let positionsDisplay = displayPositionsByDefault ? positionsForDisplayBuilder(positions) : alternativePositionsForDisplayBuilder(positions)
    let { potMainDisplay } = game;
    let total_pot = calculatePotMainDisplay(decisions, game, DISPLAY_IN_BB)
    let isPokerCodeUser = currentUser.role === 'pokercode_user';

    let turnCardOnSimDeck = turnCard ?
      <Card key={3}
        rank={turnCard.slice(0, -1) || ''}
        suit={turnCard[turnCard.length-1] || ''}
        className='card-on-sim-deck cursor-pointer'
        size='big'
        index={3}/> : <EmptyCard />
    let riverCardOnSimDeck = riverCard ?
      <Card key={4}
        rank={riverCard.slice(0, -1) || ''}
        suit={riverCard[riverCard.length-1] || ''}
        className='card-on-sim-deck cursor-pointer'
        size='big'
        index={4}/> : <EmptyCard />

    let deckInfo = `${stack_size || '0'}bb ${positionsDisplay} ${(sim_type || 'nan').toUpperCase()}`
    const classSimDeck = isEqual(game_type, 'cash') &&  isEqual(specs, '8max') ? 'mtt' : game_type
    return (
      <div className='sim-deck-section'>
        <div className='sim-deck'>
          <div className='deck-container d-flex'>
            <div className={`deck position-relative ${isPokerCodeUser ? 'poker-code-user' : ''}`}>
              <div className='deck-info cursor-pointer'>
                {deckInfo}
              </div>
              <div className='current-turn-pot cursor-pointer text-center font-weight-bold'>
                {`${t('total_pot')} ${ addBbSuffixToValue(total_pot, DISPLAY_IN_BB) }`}
              </div>
              {
                isPokerCodeUser &&
                  <div className='role-logo'>
                    <img src={poker_code} alt='coint_image' />
                  </div>
              }
              <div className='community-cards d-flex justify-content-between'>
                {
                  game.flopCards.map((flopCard, index) => {
                    return <Card key={index}
                                rank={flopCard.slice(0, -1) || ''}
                                suit={flopCard[flopCard.length-1] || ''}
                                className='card-on-sim-deck cursor-pointer'
                                size='big'
                                index={index}/>
                  })
                }
                {turnCardOnSimDeck}
                {riverCardOnSimDeck}
              </div>
              <div className='coins mt-2'>
                <img src={coint_icon} alt='coint_image' />
              </div>
              <div className='total-pot cursor-pointer pot_main text-center font-weight-bold mt-3'>
                {potMainDisplay}
              </div>
              <div className={`${classSimDeck}`}>
                { this.playerPositionEles() }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CSimDeck.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  i18nState: store.i18nState,
  game: store.game,
  handMatrix: store.handMatrix,
  decisions: store.decisionsTree.decisions,
  preflopGame: store.preflopGame
})

const mapDispatchToProps = {
  updateStacksize
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CSimDeck)
