import React, { Component, Fragment } from 'react'
import './NodesTree.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { formatToBigBlind } from 'utils/utils'

class CNodesTree extends Component {

  calculateStrategyKeyForDecisionNode(decision) {
    const { currentRound, bettedPot } = this.props.game
    const { action_display, round: decisionRound, position: decisionPosition } = decision
    if(currentRound === 'flop') return action_display

    switch(decisionRound) {
      case 'flop': return action_display
      case 'turn': return parseInt(action_display) - parseInt(bettedPot.flop[`pot_${decisionPosition}`])
      case 'river': return parseInt(action_display) - parseInt(bettedPot.turn[`pot_${decisionPosition}`])
      default: return "Couldn't calculate"
    }
  }

  render() {
    let { decisionsTree } = this.props;
    let decisions = decisionsTree.decisions || [];

    return (
      <div>
        <div className='ml-1 decisions d-flex flex-wrap align-items-center'>
          <Fragment>
            <div className='mx-1'>ROOT</div>
            { !!decisions.length &&
              <i className='decision-arrow'/>
            }
          </Fragment>
          {
            decisions.filter(decision => decision.active).map((decision, idx) => {
              const { action, action_display } = decision
              let actionDisplay = action === 'b' ? this.calculateStrategyKeyForDecisionNode(decision) : action_display
              return (
                <Fragment key={`decision-${idx}`}>
                  {
                    ['select_turn_card', 'select_river_card'].includes(action) ? (
                      <div className='d-inline-block font-weight-bold mx-1'>
                        <span>{action_display.slice(0,-1)}</span>
                        <i className={`icon icon-${action_display.slice(-1)} background-size-contain`}></i>
                      </div>
                    ) : <div className='mx-1'>{formatToBigBlind(actionDisplay)}</div>
                  }
                  { idx !== decisions.filter(decision => decision.active).length - 1 &&
                    <i className='decision-arrow'/>
                  }
                </Fragment>
              )
            })
          }
        </div>
      </div>
    )
  }
}

CNodesTree.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  decisionsTree: store.decisionsTree,
  game: store.game,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  handMatrix: store.handMatrix,
  cardsConvertingTemplate: store.cardsConverting.cardsConvertingTemplate
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CNodesTree)
