import React, { Component } from 'react'
import { connect } from 'react-redux'
import Modal from 'react-bootstrap/Modal'
import './RangeExplorerModal.scss'
import { updateDecisionFilter } from 'actions/hand-matrix.actions'
import { updateHandCategory } from 'actions/range-explorer.actions'
import { convertTurnRiverCard } from 'services/convert_cards_services'
import { isEmpty }  from 'lodash'
import loadable from '@loadable/component'
import axios from 'axios'

const DualRange = loadable(() => import('./RangeExplorer/DualRangeModal/DualRange/DualRange'))

class RangeExporerModal extends Component {

  componentDidMount() {
    this.fetchHandCategoryData()
  }

  fetchHandCategoryData() {
    const { game, cardsConvertingTemplate } = this.props
    const { convertedFlopCards, turnCard, riverCard } = game
    const convertedTurnCard = isEmpty(turnCard) ? '' : convertTurnRiverCard(turnCard, cardsConvertingTemplate)
    const convertedRiverCard = isEmpty(riverCard) ? '' : convertTurnRiverCard(riverCard, cardsConvertingTemplate)
    const board = convertedFlopCards.join('') + convertedTurnCard + convertedRiverCard 
    axios.get(`/hand_categories/get_hand_category?board=${board}`).then( res => {
      const handCategoryData = res.data.hand_category
      this.props.updateHandCategory(handCategoryData)
    }).catch(err => console.error(err))
  }

  render() {
    const { modalShow, setModalShow } = this.props

    return (
      <Modal
        show={modalShow}
        onHide={() => { setModalShow('') }}
        dialogClassName='range-explorer-modal'
        backdrop="static"
        aria-labelledby='range-explorer-modal'
      >
        <Modal.Body bsPrefix='custom-modal-body'>
          <Modal.Header bsPrefix='custom-modal-header' closeButton>
            <Modal.Title id="range-explorer-modal"></Modal.Title>
          </Modal.Header>
          <DualRange />
        </Modal.Body>
      </Modal>
    )
  }
}

const mapStoreToProps = (store) => ({
  game: store.game,
  cardsConvertingTemplate: store.cardsConverting.cardsConvertingTemplate,
})

const mapDispatchToProps = {
  updateDecisionFilter,
  updateHandCategory,
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(RangeExporerModal)
