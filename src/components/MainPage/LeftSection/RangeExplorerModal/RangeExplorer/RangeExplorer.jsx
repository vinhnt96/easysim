import React, { Component, Fragment } from 'react'
import Row from 'react-bootstrap/Row'
import Col from 'react-bootstrap/Col'
import { Collapse } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleUp, faAngleDown } from '@fortawesome/free-solid-svg-icons'
import './RangeExplorer.scss'
import { updateDecisionFilterRE, changeClickedHandItemRE } from 'actions/range-explorer.actions'
import { without, round } from 'lodash'
import Card from 'components/Card/component'
import OdinLoader from 'components/Shared/OdinLoader/OdinLoader'
import HandTypes from '../HandTypes/HandTypes'
import NodesTree from '../NodesTree/NodesTree'
import loadable from '@loadable/component'

const RangeMatrix = loadable(() => import('../../../RangeMatrix/RangeMatrix'))
const DecisionCombos = loadable(() => import('../../../DecisionCombos/DecisionCombos'))
const HandMatrix = loadable(() => import('../../../HandMatrix/HandMatrix'))
const LeftBottom = loadable(() => import('../LeftBottom/LeftBottom'))
const DualRangeModal = loadable(() => import('./DualRangeModal/DualRangeModal'))

class CRangeExplorer extends Component {
  constructor() {
    super()
    this.state = {
      handHovered: '',
      oopShown: true,
      ipShown: true,
      modalShow: '',
    }
  }

  cardsEles() {
    const { flopCards, turnCard, riverCard } = this.props.game
    const cards = without([...flopCards, turnCard, riverCard], '')

    return cards.map((card, index) =>
      <Card
        key={index}
        rank={card.slice(0, -1) || ''}
        suit={card[card.length - 1] || ''}
        className='board-card'
        size='small'
        index={index} />
    );
  }

  render() {
    const t = this.context.t
    const { modalShow, oopShown, ipShown, handHovered } = this.state
    const { data, position, viewOption, decisionsArray, rangeExplorerShowing,
      oopHandTypeData, ipHandTypeData, changeViewOption, previousGameExplorer,
      changePosition, handTypeFilter, handTypeOnClick, game, weightedStrategy, rangesData,
      handleChangeWeightedStrategy, updateDecisionFilterRE, changeClickedHandItemRE, rangeExplorer } = this.props
    const { suitsForFilter, decisionFilter, handClickedItem, fetching } = rangeExplorer

    return (
        <div className='range-explorer'>
          { fetching && 
            <OdinLoader
              handleCloseLoader={() => {}}
              disableProgressBar={true}
              disableOdinAnimation={true}
            /> }
          { !fetching &&
            <Fragment>
              <Row>
                <Col xs={12} lg={6} className="left-col">
                  <div className='d-flex mt-2'>
                    <div className='flex-shrink-0'>
                      {this.cardsEles()}
                    </div>
                    <NodesTree />
                  </div>
                  <div className='sim-deck mt-2'>
                    <RangeMatrix
                      handClickedItem={handClickedItem}
                      dataForEachCombos={data}
                      handleOnHoverOnHand={(hand) =>  this.setState({ handHovered: hand })}
                      view={viewOption ? `${viewOption}_${position}` : ''}
                      position={position}
                      changeClickedHandItem={changeClickedHandItemRE}
                      previousGameExplorer={previousGameExplorer}
                      rangeExplorerShowing={rangeExplorerShowing}
                      suitsForFilter={suitsForFilter}
                      decisionFilter={decisionFilter}
                      weightedStrategy={weightedStrategy}
                      showHandItemPopup={false}
                    />
                  </div>
                  <div className='decision-combos my-3'>
                    <DecisionCombos
                      decisionCombos={decisionsArray}
                      disabled={viewOption !== ''}
                      decisionFilter={decisionFilter}
                      isHidden={viewOption !== '' || position !== game.currentPosition}
                      updateDecisionFilter={updateDecisionFilterRE}/>
                  </div>
                  <hr></hr>
                  <div>
                    <LeftBottom
                      viewOption={viewOption}
                      position={position}
                      changeViewOption={changeViewOption}
                      changePosition={changePosition}
                      weightedStrategy={weightedStrategy}
                      handleChangeWeightedStrategy={handleChangeWeightedStrategy}
                      setModalShow={(modalName) => this.setState({ modalShow: modalName })}
                    />
                  </div>
                </Col>
                <Col xs={12} lg={6} className='right-col pr-0 d-flex flex-column'>
                  <div className='right-container'>
                    <div className='oop-hand-types mt-4 mb-2'>
                      <div>
                        <span className='font-size-18 cursor-pointer font-weight-bold mr-2'>
                          {t('range_explorer.combos_number', { number: round(game.num_combos_oop, 1), position: 'OOP'})}
                        </span>
                        <FontAwesomeIcon
                          icon={this.state.oopShown ? faAngleDown : faAngleUp}
                          className='angle-icon cursor-pointer'
                          onClick={() => { this.setState({ oopShown: !oopShown }) }}
                        />
                      </div>
                      <Collapse in={this.state.oopShown}>
                        <div>
                          <HandTypes
                            handTypes={oopHandTypeData}
                            color='primary'
                            handTypeFilter={handTypeFilter}
                            handTypeOnClick={handTypeOnClick}
                          />
                        </div>
                      </Collapse>
                    </div>
                    <hr className='seperate'></hr>
                    <div className='ip-hand-types mb-3'>
                      <div className='mb-3'>
                        <span className='font-size-18 cursor-pointer font-weight-bold mr-2'>
                          {t('range_explorer.combos_number', { number: round(game.num_combos_ip, 1), position: 'IP' })}
                        </span>
                        <FontAwesomeIcon
                          icon={this.state.ipShown ? faAngleDown : faAngleUp}
                          className='angle-icon cursor-pointer'
                          onClick={() => { this.setState({ ipShown: !ipShown }) }}
                        />
                      </div>
                      <Collapse in={ipShown}>
                        <div>
                          <HandTypes
                            handTypes={ipHandTypeData}
                            color='green'
                            handTypeFilter={handTypeFilter}
                            handTypeOnClick={handTypeOnClick}
                          />
                        </div>
                      </Collapse>
                    </div>
                    <div className='hand-matrix flex-grow-1'>
                      <HandMatrix
                        dataForEachCombos={data}
                        hand={handHovered}
                        view={viewOption ? `${viewOption}_${position}` : ''}
                        position={position}
                        previousGameExplorer={previousGameExplorer}
                        rangeExplorerShowing={rangeExplorerShowing}
                        suitsForFilter={suitsForFilter}
                        decisionFilter={decisionFilter}
                        handClickedItem={handClickedItem}
                        weightedStrategy={weightedStrategy}
                        rangesData={rangesData}
                      />
                    </div>
                  </div>
                </Col>
              </Row>
              <DualRangeModal
                modalShow={modalShow === 'dual_range'}
                setModalShow={(modalName) => this.setState({ modalShow: modalName })}
              />
            </Fragment>
          }
        </div>
    )
  }
}

CRangeExplorer.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  rangeExplorer: store.rangeExplorer,
  game: store.game,
})

const mapDispatchToProps = {
  updateDecisionFilterRE: updateDecisionFilterRE,
  changeClickedHandItemRE: changeClickedHandItemRE
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CRangeExplorer)
