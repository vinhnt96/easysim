import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import './DualRangeModal.scss'
import loadable from '@loadable/component'
const DualRange = loadable(() => import('./DualRange/DualRange'))
export default class DualRangeModal extends Component {

  render() {
    const { modalShow, setModalShow } = this.props;

    return (
      <Modal
        show={modalShow}
        onHide={() => { setModalShow('') }}
        dialogClassName='dual-range-modal'
        backdrop="static"
        aria-labelledby='dual-range-modal'
      >
        <Modal.Body bsPrefix='custom-modal-body'>
          <Modal.Header bsPrefix='custom-modal-header' closeButton>
            <Modal.Title id="dual-range-modal"></Modal.Title>
          </Modal.Header>
          <DualRange/>
        </Modal.Body>
      </Modal>
    )
  }
}
