import React, { Component, Fragment } from 'react'
import './DualRange.scss'
import CustomRadio from 'components/CustomRadio/CustomRadio'
import { Row, Col, Collapse } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {without, round} from 'lodash'
import { dataForEachComboGenerator } from 'services/matrix_service'
import { DUAL_RANGE_VIEW_OPTIONS, SUITS_FOR_FILTER } from 'config/constants'
import { calculateHandTypes, filterMatrixByHandType, filterDataToAppendExistedData, convertDataToZeroValue } from 'services/range_explorer_service'
import {
  buildRangesForEachCombos,
  positionsForDisplayBuilder,
  alternativePositionsForDisplayBuilder,
  calculatePotMainDisplay
} from 'services/game_play_service'
import HandTypes from 'components/MainPage/LeftSection/RangeExplorerModal/HandTypes/HandTypes'
import Card from 'components/Card/component'
import TooltipQuestion from 'components/Shared/TooltipQuestion/component'
import NodesTree from '../../../NodesTree/NodesTree'
import loadable from '@loadable/component'
import OdinLoader from 'components/Shared/OdinLoader/OdinLoader'
import Filter from '../../../../../Filter/Filter'
import { calculateFigures } from 'services/background_color_services'
import { decisionCombosButtons } from 'utils/utils'

const RangeMatrix = loadable(() => import('components/MainPage/RangeMatrix/RangeMatrix'))
const DecisionCombos = loadable(() => import('components/MainPage/DecisionCombos/DecisionCombos'))
const HandMatrix = loadable(() => import('components/MainPage/HandMatrix/HandMatrix'))
class CDualRange extends Component {
  constructor(props) {
    super(props);

    this.state = {
      weightedStrategy: false,
      oopViewState: props.game.currentPosition === 'oop' ? '' : 'ev_per_eq',
      oopDecisionFilter: '',
      oopHandTypeFilter: [],
      showOopCombos: true,
      oopHandClickedItem: '',
      ipViewState: props.game.currentPosition === 'ip' ? '' : 'ev_per_eq',
      ipDecisionFilter: '',
      ipHandTypeFilter: [],
      showIpCombos: true,
      ipHandClickedItem: '',
      suitsForFilterOop: SUITS_FOR_FILTER,
      suitsForFilterIp: SUITS_FOR_FILTER,
    };
    this.toggleWeightedStrategy = this.toggleWeightedStrategy.bind(this)
    this.updateOopDecisionFilter = this.updateOopDecisionFilter.bind(this);
    this.updateIpDecisionFilter = this.updateIpDecisionFilter.bind(this);
    this.changeOopViewOption = this.changeOopViewOption.bind(this);
    this.changeIpViewOption = this.changeIpViewOption.bind(this);
    this.oopHandTypeOnClick = this.oopHandTypeOnClick.bind(this);
    this.ipHandTypeOnClick = this.ipHandTypeOnClick.bind(this);
    this.handleOopClickedHandItem = this.handleOopClickedHandItem.bind(this);
    this.handleIpClickedHandItem = this.handleIpClickedHandItem.bind(this);
    this.updateSuitsForFilterOop = this.updateSuitsForFilterOop.bind(this)
    this.updateSuitsForFilterIp = this.updateSuitsForFilterIp.bind(this)
  }

  oopHandTypeOnClick(e) {
    const newStateOopHandTypeFilter = this.state.oopHandTypeFilter.includes(e.currentTarget.dataset.type) ?
                                      this.state.oopHandTypeFilter.filter( handType => handType !== e.currentTarget.dataset.type)
                                      : [...this.state.oopHandTypeFilter, e.currentTarget.dataset.type]
    this.setState({ oopHandTypeFilter: newStateOopHandTypeFilter });
  }

  ipHandTypeOnClick(e) {
    const newStateIpHandTypeFilter = this.state.ipHandTypeFilter.includes(e.currentTarget.dataset.type) ?
                                     this.state.ipHandTypeFilter.filter( handType => handType !== e.currentTarget.dataset.type)
                                     : [...this.state.ipHandTypeFilter, e.currentTarget.dataset.type]
    this.setState({ ipHandTypeFilter: newStateIpHandTypeFilter });
  }

  updateOopDecisionFilter(betValue) {
    this.setState({ oopDecisionFilter: betValue });
  }

  updateIpDecisionFilter(betValue) {
    this.setState({ ipDecisionFilter: betValue });
  }

  cardsEles() {
    let game = this.props.game
    let cards = without([...game.flopCards, game.turnCard, game.riverCard], '')

    return cards.map((card, index) =>
      <Card
        key={index}
        rank={card.slice(0, -1) || ''}
        suit={card[card.length-1] || ''}
        className='board-card'
        size='small'
        index={index} />
    );
  }

  changeOopViewOption(e) {
    this.setState({
      oopViewState: e.currentTarget.value,
      oopDecisionFilter: '',
    });
  }

  changeIpViewOption(e) {
    this.setState({
      ipViewState: e.currentTarget.value,
      ipDecisionFilter: '',
    });
  }

  oopViewOptionEles(name, view) {
    const { currentPosition } = this.props.game
    const viewOption = view === 'oop' ? this.state.oopViewState : this.state.ipViewState
    const handleOnClick = view === 'oop' ? this.changeOopViewOption : this.changeIpViewOption

    return <div className='d-flex'>
      {DUAL_RANGE_VIEW_OPTIONS.map((option, index) => {
        const hideViewOption = currentPosition !== view && option.value === ''
  
        return !hideViewOption &&
          <div key={index} className='view-option cursor-pointer'>
            <CustomRadio
              name={name}
              data={option.value}
              checked={viewOption === option.value}
              handleOnClick={handleOnClick}
            />
            <span>{option.label}</span>
            {
              option.value === 'ev' &&
              this.evTooltipQuestionElement()
            }
          </div>
       })
      }
    </div>
  }

  handleOopClickedHandItem(handItem) {
    this.setState({ oopHandClickedItem: handItem });
  }

  handleIpClickedHandItem(handItem) {
    this.setState({ ipHandClickedItem: handItem });
  }

  evTooltipQuestionElement() {
    return (
      <TooltipQuestion className='ml-1' explanationName='ev-rescale-option-explanation' place='top' type='dark'>
        <span> {this.context.t('tooltip_range_explorer.ev_rescale')}</span>
      </TooltipQuestion>
    )
  }

  updateSuitsForFilterOop(suitsForFilter) {
    this.setState({
      suitsForFilterOop: suitsForFilter
    })
  }

  updateSuitsForFilterIp(suitsForFilter) {
    this.setState({
      suitsForFilterIp: suitsForFilter
    })
  }

  toggleWeightedStrategy() {
    this.setState({
      weightedStrategy: !this.state.weightedStrategy
    })
  }

  viewToLable(view) {
    switch(view) {
      case 'ev_rescale_oop':
      case 'ev_rescale_ip':
        return 'EV rescaled';
      case 'equity_oop':
      case 'equity_ip':
        return 'Equity';
      case 'ev_per_eq_oop':
      case 'ev_per_eq_ip':
        return 'EV/Equity';
      default:
        return '';
    }
  }

  renderAvgValueByViewOop(game, totalPotOopView) {
    let avgValueEV = 0
    let avgValueEQ = 0
    avgValueEV = calculateFigures('ev_oop', 'oop', game, totalPotOopView)
    avgValueEQ = calculateFigures('equity_oop', 'oop', game)
    return (
      <div className='col-5 text-right position font-size-16 pr-0'>
        <span>
          <span className='mr-2'>EV:</span>
          <span>{avgValueEV}</span>
        </span>
        <span className='ml-3'>
          <span className='mr-2'>EQ:</span>
          <span>{avgValueEQ}</span>
        </span>
      </div>
    )
  }

  renderAvgValueByViewIp(game, totalPotIpView) {
    let avgValueEV = 0
    let avgValueEQ = 0
    avgValueEV = calculateFigures('ev_ip', 'ip', game, totalPotIpView)
    avgValueEQ = calculateFigures('equity_ip', 'ip', game)
    return (
      <div className='col-5 text-left position font-size-16 p-0'>
        <span>
          <span className='mr-2'>EV:</span>
          <span>{avgValueEV}</span>
        </span>
        <span className='ml-3'>
          <span className='mr-2'>EQ:</span>
          <span>{avgValueEQ}</span>
        </span>
      </div>
    )
  }

  render() {
    const t = this.context.t
    const { oopViewState, oopDecisionFilter, oopHandTypeFilter, showOopCombos, oopHandClickedItem,
      ipViewState, ipDecisionFilter, ipHandTypeFilter, showIpCombos, ipHandClickedItem,
      suitsForFilterOop, suitsForFilterIp, weightedStrategy } = this.state
    const { handMatrix, game, decisionsTree, currentUser, rangeExplorer } = this.props
    const { fetching } = rangeExplorer
    const { decisions } = decisionsTree
    const strategy_selection = currentUser.strategy_selection;
    const preferences = currentUser.preferences || {}
    const displayPositionsByDefault = preferences.display_positions_by_default || false
    const stack_size = parseInt(strategy_selection.stack_size);
    const positionsDisplay = displayPositionsByDefault ? positionsForDisplayBuilder(strategy_selection.positions) : alternativePositionsForDisplayBuilder(strategy_selection.positions)

    const oopView = [''].includes(oopViewState) ? oopViewState : `${oopViewState}_oop`
    const ipView = [''].includes(ipViewState) ? ipViewState : `${ipViewState}_ip`

    const ranges = buildRangesForEachCombos(game.flops, game.nodes, game.currentPosition, game.flopCards)
    const oopHandTypeData = calculateHandTypes(game.flopCards, ranges.oopRanges, game.num_combos_oop, 'oop', rangeExplorer.hand_category)
    const ipHandTypeData = calculateHandTypes(game.flopCards, ranges.ipRanges, game.num_combos_ip, 'ip', rangeExplorer.hand_category)
    const totalPotOopView = calculatePotMainDisplay(decisions, game, !['ev_rescale_oop', 'ev_rescale_ip'].includes(oopView))
    const totalPotIpView = calculatePotMainDisplay(decisions, game, !['ev_rescale_oop', 'ev_rescale_ip'].includes(ipView))
    const decisionArray = decisionCombosButtons(game, preferences, game.currentPosition, decisions).slice(0, -1)

    let oopData = {};
    if((game.currentPosition === 'oop' || oopView !== '') && !fetching) {
      const oopDataForEachCombos = dataForEachComboGenerator(
        {...handMatrix, ...game, ...decisionsTree, preferences: currentUser.preferences, view: oopView, totalPot: totalPotOopView })

      oopData = convertDataToZeroValue(oopDataForEachCombos)
      oopHandTypeFilter.forEach( handType => {
        let newData = filterMatrixByHandType(oopDataForEachCombos, game.flopCards, handType, rangeExplorer.hand_category)
        newData = filterDataToAppendExistedData(newData)
        oopData = {...oopData, ...newData}
      })
      if(!oopHandTypeFilter.length) {
        oopData = oopDataForEachCombos
      }
    }

    let ipData = {};
    if((game.currentPosition === 'ip' || ipView !== '') && !fetching) {
      const ipDataForEachCombos = dataForEachComboGenerator(
        {...handMatrix, ...game, ...decisionsTree, preferences: currentUser.preferences, view: ipView, totalPot: totalPotIpView })
      ipData = convertDataToZeroValue(ipDataForEachCombos)
      ipHandTypeFilter.forEach( handType => {
        let newData = filterMatrixByHandType(ipDataForEachCombos, game.flopCards, handType, rangeExplorer.hand_category)
        newData = filterDataToAppendExistedData(newData)
        ipData = {...ipData, ...newData}
      })
      if(!ipHandTypeFilter.length) {
        ipData = ipDataForEachCombos
      }
    }

    return (
      <div className='dual-range'>
        { fetching && 
          <OdinLoader
            handleCloseLoader={() => {}}
            disableProgressBar={true}
            disableOdinAnimation={true}
          /> 
        }
        { !fetching && 
          <Fragment>
            <div className='preflop-subset'>
              {`${stack_size || '0'}bb ${positionsDisplay} SRP`}
            </div>
            <Row>
              <Col xs={12} className='d-flex cards-node'>
                <div className='board-cards flex-shrink-0'>
                  {this.cardsEles()}
                </div>
                <NodesTree />
              </Col>
              <Col xs={12} lg={6} className="left-col">
                <div className='position text-center'>OOP</div>
                <RangeMatrix
                  dataForEachCombos={oopData}
                  view={oopView}
                  suitsForFilter={suitsForFilterOop}
                  handleOnHoverOnHand={() => {}}
                  rangeExplorerShowing={true}
                  decisionFilter={oopDecisionFilter}
                  handClickedItem={oopHandClickedItem}
                  changeClickedHandItem={this.handleOopClickedHandItem}
                  weightedStrategy={weightedStrategy}
                />
              </Col>
              <Col xs={12} lg={6} className="right-col pr-0">
                <div className='position text-center'>IP</div>
                <RangeMatrix
                  dataForEachCombos={ipData}
                  view={ipView}
                  suitsForFilter={suitsForFilterIp}
                  handleOnHoverOnHand={() => {}}
                  rangeExplorerShowing={true}
                  decisionFilter={ipDecisionFilter}
                  handClickedItem={ipHandClickedItem}
                  changeClickedHandItem={this.handleIpClickedHandItem}
                  weightedStrategy={weightedStrategy}
                />
              </Col>
              <Col xs={12} lg={12} className="p-0 m-0 my-2 row d-flex justify-content-between align-items-center middle-section">
                { oopView &&
                  this.renderAvgValueByViewOop(game, totalPotOopView)
                }
                { !oopView &&
                  <div className='decision-combos col-5 pr-0'>
                    <DecisionCombos
                      decisionCombos={decisionArray}
                      disabled={oopView !== ''}
                      decisionFilter={oopDecisionFilter}
                      isHidden={oopView !== ''}
                      updateDecisionFilter={this.updateOopDecisionFilter}
                      className='justify-content-start'
                      isMainPage={false}/>
                  </div>
                }
                <div className='col-2'>
                  <div onClick={this.toggleWeightedStrategy}
                        className={`weighted-strategy-button ${weightedStrategy ? 'selected' : ''} m-0`} >
                    {t('weighted_strategy')}
                  </div>
                </div>
                { ipView &&
                  this.renderAvgValueByViewIp(game, totalPotIpView)
                }
                { !ipView &&
                  <div className='decision-combos col-5 p-0'>
                    <DecisionCombos
                      decisionCombos={decisionArray}
                      disabled={ipView !== ''}
                      decisionFilter={ipDecisionFilter}
                      isHidden={ipView !== ''}
                      updateDecisionFilter={this.updateIpDecisionFilter}
                      className='justify-content-end'
                      isMainPage={false}/>
                  </div>
                }
              </Col>
              <Col xs={12} lg={6} className="left-col">
                <div className='col-12 row no-gutter'>
                  <div className='suits-filter-oop col-4 p-0'>
                    <Filter
                      direction='horizontal'
                      suitsForFilter={suitsForFilterOop}
                      updateSuitsForFilter={this.updateSuitsForFilterOop}
                    />
                  </div>
                </div>
                <div className='oop-view-options d-flex col-12 p-0 justify-content-between align-items-center'>
                  {this.oopViewOptionEles('oop_view_option','oop')}
                  <span className='font-size-18 cursor-pointer font-weight-bold'>
                    {t('range_explorer.combos_number', { number: round(game.num_combos_oop, 1), position: '' })}
                  </span>
                </div>
                <div className='oop-hand-types mt-4 mb-2'>
                  <Collapse in={showOopCombos}>
                    <div>
                      <HandTypes
                        handTypes={oopHandTypeData}
                        color='primary'
                        handTypeFilter={oopHandTypeFilter}
                        handTypeOnClick={this.oopHandTypeOnClick}
                      />
                    </div>
                  </Collapse>
                </div>
              </Col>
              <Col xs={12} lg={6} className="right-col pr-0">
                <div className='col-12 row no-gutter justify-content-end m-0 p-0'>
                  <div className='suits-filter-ip col-4 pr-0'>
                    <Filter
                      direction='horizontal'
                      suitsForFilter={suitsForFilterIp}
                      updateSuitsForFilter={this.updateSuitsForFilterIp}
                      />
                  </div>
                </div>
                <div className='ip-view-options d-flex col-12 p-0 justify-content-between align-items-center'>
                  <span className='font-size-18 cursor-pointer font-weight-bold'>
                    {t('range_explorer.combos_number', { number: round(game.num_combos_ip, 1), position: '' })}
                  </span>
                  {this.oopViewOptionEles('ip_view_option', 'ip')}
                </div>
                <div className='ip-hand-types mt-4 mb-2'>
                  <Collapse in={showIpCombos}>
                    <div>
                      <HandTypes
                        handTypes={ipHandTypeData}
                        color='green'
                        handTypeFilter={ipHandTypeFilter}
                        handTypeOnClick={this.ipHandTypeOnClick}
                      />
                    </div>
                  </Collapse>
                </div>
              </Col>
            </Row>
            {
              oopHandClickedItem !== '' &&
              <div className='oop-submatrix-item-popup'>
                <div className='popup-close text-right font-size-20'>
                  <p className='p-0 m-0 font-weight-bold cursor-pointer'
                  onClick={() => {this.handleOopClickedHandItem('')}}>×</p>
                </div>
                <HandMatrix
                  dataForEachCombos={oopData}
                  view={oopView}
                  position='oop'
                  rangeExplorerShowing={true}
                  suitsForFilter={suitsForFilterOop}
                  decisionFilter={oopDecisionFilter}
                  handClickedItem={oopHandClickedItem}
                  weightedStrategy={weightedStrategy}
                />
              </div>
            }
            {
              ipHandClickedItem !== '' &&
              <div className='ip-submatrix-item-popup'>
                <div className='popup-close text-right font-size-20'>
                  <p className='p-0 m-0 font-weight-bold cursor-pointer'
                      onClick={() => {this.handleIpClickedHandItem('')}}>×</p>
                </div>
                <HandMatrix
                  dataForEachCombos={ipData}
                  view={ipView}
                  position='ip'
                  rangeExplorerShowing={true}
                  suitsForFilter={suitsForFilterIp}
                  decisionFilter={ipDecisionFilter}
                  handClickedItem={ipHandClickedItem}
                  weightedStrategy={weightedStrategy}
                />
              </div>
            }
          </Fragment>
        }
      </div>
    )
  }
}

CDualRange.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  game: store.game,
  rangeExplorer: store.rangeExplorer,
  handMatrix: store.handMatrix,
  decisionsTree: store.decisionsTree
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CDualRange)
