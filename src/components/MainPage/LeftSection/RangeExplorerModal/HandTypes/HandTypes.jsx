import React, { Component } from 'react'
import './HandTypes.scss'
import { chunk, isEmpty } from 'lodash'
import PropTypes from 'prop-types'

export default class HandTypes extends Component {

  handTypesEles(handTypes) {
    const { color, handTypeFilter } = this.props
    const t = this.context.t

    return handTypes.map((handType, index) => {
      const percent = `${handType.percent}%`
      const selectedClass = handTypeFilter.includes(handType.type) ? 'selected' : ''

      return (
        <div
          className={`hand-type-container ${selectedClass}`}
          key={index} data-type={handType.type}
          onClick={this.props.handTypeOnClick}
        >
          <div className='d-flex align-items-end justify-content-between'>
            <p className='hand-type-name font-weight-bold mb-1'>{t(handType.name_key)}</p>
            <p className='hand-type-info font-weight-bold mb-1'>{handType.info}</p>
          </div>
          <div className='percentage-bar'>
            <div className={`hand-type-percent ${color}`} style={{width: percent}}>
            </div>
          </div>
        </div>
      )
    });
  }

  render() {
    const handTypes = this.props.handTypes
    const handTypesFilter = handTypes.filter(i => i.percent > 0)
    const dataChunk = chunk(handTypesFilter, 5);
    const classNameDefault = `hantype-column col-lg-${handTypesFilter.length > 15 ? '3' : '4'} col-md-6 col-12`

    return <div className='hand-types row'>
      <div className={classNameDefault}>
        {this.handTypesEles(dataChunk[0] || [])}
      </div>
      <div className={classNameDefault}>
        {this.handTypesEles(dataChunk[1] || [])}
      </div>
      <div className={classNameDefault}>
        {this.handTypesEles(dataChunk[2] || [])}
      </div>
      { !isEmpty(dataChunk[3]) &&
        <div className={classNameDefault}>
          {this.handTypesEles(dataChunk[3])}
        </div>
      }
    </div>
  }
}

HandTypes.contextTypes = {
  t: PropTypes.func
}
