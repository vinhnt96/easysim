import React, { Component } from 'react'
import BlueMatrix from '../../../../../images/matrixes/blue-matrix.png'
import GreenMatrix from '../../../../../images/matrixes/green-matrix.png'
import PropTypes from 'prop-types'
import CustomRadio from 'components/CustomRadio/CustomRadio'
import { connect } from 'react-redux'
import './LeftBottom.scss'
import { updateSuitsForFilterRE } from 'actions/range-explorer.actions'
import { VIEW_OPTIONS } from 'config/constants'
import TooltipQuestion from 'components/Shared/TooltipQuestion/component'
import loadable from '@loadable/component'
const Filter = loadable(() => import('../../../Filter/Filter'))

class CLeftBottom extends Component {

  showOnGridOptionEles() {
    const t = this.context.t
    const { position, game, currentUser: { preferences } } = this.props
    const hideExplanation = preferences.hide_explanation

    return VIEW_OPTIONS.map((option, index) => {
      let hideViewOption = position !== game.currentPosition && option.value === '';

      return !hideViewOption &&
        <div key={index} className='cursor-pointer'>
          <CustomRadio
            name='show_option'
            data={option.value}
            checked={this.props.viewOption === option.value}
            handleOnClick={this.props.changeViewOption}
          />
          <span>{option.label}</span>
          {
            !hideExplanation && option.value === 'ev_rescale' &&
            <TooltipQuestion
              className='ml-1'
              explanationName='ev-rescale-option-explanation'
              place='top'
              type='dark'>
              <span> {t('tooltip_range_explorer.ev_rescale')}</span>
            </TooltipQuestion>
          }
          {
            !hideExplanation && option.value === 'ev_per_eq' &&
            <TooltipQuestion
              className='ml-1'
              explanationName='ev-per-eq-explanation'
              place='top'
              type='dark'>
              <span> {t('tooltip_range_explorer.ev_per_eq')}</span>
            </TooltipQuestion>
          }
        </div>

    });
  }

  render() {
    let t = this.context.t
    let { position, changePosition, setModalShow, rangeExplorer,
      updateSuitsForFilterRE, weightedStrategy, handleChangeWeightedStrategy } = this.props;

    return (
      <div className='left-bottom mt-3 row'>
        <div className='col-xl-5 col col-6 pr-0 select-position'>
          <div className='mt-2 font-size-14 d-flex justify-content-around'>
            <span className='maxtrix-img'>
              <div className={`${position === 'oop' ? 'selected' : ''}`}
                   onClick={() => {changePosition('oop')}}>
                <img src={BlueMatrix} alt=''/>
              </div>
              <p className='font-weight-bold text-center'>{t('range_explorer.oop')}</p>
            </span>
            <span className='maxtrix-img'>
              <div className={`${position === 'ip' ? 'selected' : ''}`}
                   onClick={() => {changePosition('ip')}}>
                <img  src={GreenMatrix} alt=''/>
              </div>
              <p className='font-weight-bold text-center'>{t('range_explorer.ip')}</p>
            </span>
          </div>
          <div
            className='btn-red'
            onClick={() => {setModalShow('dual_range') }}
          >
            Dual Range Viewer
          </div>
        </div>
        <div className='col-xl-3 col-6 pr-0 show-on-grid'>
          <p className='font-weight-bold font-size-16 cursor-pointer'>{t('range_explorer.show_on_grids')}</p>
          <div>
            {this.showOnGridOptionEles()}
          </div>
        </div>
        <div className='col-xl-4 col-12 suits pl-4 mt-2 suilt-filter'>
          <Filter
            direction='horizontal'
            suitsForFilter={rangeExplorer.suitsForFilter}
            updateSuitsForFilter={updateSuitsForFilterRE}/>
          <div>
            <p className='font-weight-bold mb-1'>{t('range_explorer.options')}</p>
            <div
              onClick={handleChangeWeightedStrategy}
              className={`weighted-strategy-button ${weightedStrategy ? 'selected' : ''}`}
            >
              {t('weighted_strategy')}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CLeftBottom.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  game: store.game,
  rangeExplorer: store.rangeExplorer,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
  updateSuitsForFilterRE: updateSuitsForFilterRE
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CLeftBottom)
