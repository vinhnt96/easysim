import React, {Component, Fragment} from 'react'
import './SelectTurnCard.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  CARD_OPTIONS,
  DEFAULT_BEGIN_ROUND_POSITION,
  SUITED_CARD_NUMBER, DISPLAY_IN_BB} from 'config/constants'
import {
  updateTurnCard,
  updateRiverCard,
  fetchTurnRiverData,
  fetchDataForTurnRiverRound,
  updateComparedDecisions
} from 'actions/game.actions'
import { addDecisionToTree, rollbackDecision } from 'actions/decision.actions'// rollbackDecision function is passed as prop to handleBackToHistory
import {
  positionsForQueryBuilder,
  calculatePotMainDisplay,
  addBbSuffixToValue,
  handleBackToHistory
} from 'services/game_play_service'
import {convertTurnRiverCard} from 'services/convert_cards_services'
import { Collapse } from 'react-bootstrap'
import { togglePopup } from 'actions/subscription.actions'
import { checkFreeTrialUser } from 'utils/utils'
import { cloneDeep, chunk, isEmpty } from 'lodash'
import { updateOdinLoadingShowingValue } from 'actions/odin-loading.actions'
import Card from 'components/Card/component'

class CSelectTurnCard extends Component {

  constructor() {
    super()
    this.state = {
      showCardSelect: false,
      flop: '',
    }
    this.timeOut = 0

    this.handleClickCard = this.handleClickCard.bind(this)
    this.handleCloseCardPopup = this.handleCloseCardPopup.bind(this);
  }

  componentWillReceiveProps({ game }) {
    const { game: { fetchTurnRiverSuccess } } = this.props;
    const { flop } = this.state;
    if ( !fetchTurnRiverSuccess && game.fetchTurnRiverSuccess && !isEmpty(flop) ) {
      this.fetchTurnRiver(flop)
    }
  }

  componentDidMount() {
    const { fetching, betCompleted, stackSize, riverCard, pot_oop, pot_ip } = this.props.game
    const endGame = stackSize - pot_oop <= 0 && stackSize - pot_ip <= 0

    if(betCompleted && !endGame && !fetching && !riverCard) {
      this.timeOut = setTimeout(() => {
        this.setState({ showCardSelect: true })
      }, 1000);
    }
  }

  componentDidUpdate() {
    const { fetching, betCompleted, stackSize, riverCard, pot_oop, pot_ip } = this.props.game
    const endGame = stackSize - pot_oop <= 0 && stackSize - pot_ip <= 0

    if(betCompleted && !endGame && !fetching && !riverCard && !this.state.showCardSelect ) {
      this.timeOut = setTimeout(() => {
        this.setState({ showCardSelect: true })
      }, 1000);
    }

    if(!betCompleted && this.state.showCardSelect) {
      this.setState({ showCardSelect: false })
    }
  }

  createSelectCardDecision(card, round, game) {
    let { decisionsTree, cardsConvertingTemplate } = this.props
    let decisions = decisionsTree.decisions || []
    const {ranges_oop, ranges_ip, num_combos_ip, num_combos_oop, pot_value, pot_oop, pot_ip, pot_value_in_bb } = game

    return {
      order_num: decisions.length+1,
      position: 'oop',
      action: `select_${round}_card`,
      action_display: card,
      action_value: convertTurnRiverCard(card, cardsConvertingTemplate),
      round: round,
      active: true,
      ranges_oop,
      ranges_ip,
      num_combos_ip,
      num_combos_oop,
      pot_value,
      pot_oop,
      pot_ip,
      pot_value_in_bb
    }
  }

  handleClickCard(value) {
    const { currentUser, game: { fetchTurnRiverSuccess }, fetchDataForTurnRiverRound, togglePopup, updateOdinLoadingShowingValue } = this.props
    if(checkFreeTrialUser(currentUser.email)) {
      togglePopup('main-page-subscribe-now');
      return
    }
    fetchDataForTurnRiverRound({fetching: true})
    updateOdinLoadingShowingValue('show_odin_loading')
    this.setState({ flop: value })
    if(fetchTurnRiverSuccess === 'done') {
      this.fetchTurnRiver(value)
    }
    clearTimeout(this.timeOut);
  }

  fetchTurnRiver(value) {
    const { turnCard, currentUser: { strategy_selection } , fetchTurnRiverData, game, decisionsTree, cancelToken, cancelObj, cardsConvertingTemplate } = this.props
    const  { stack_size, sim_type, flop_cards, positions } = strategy_selection
    const { nodes, bettedPot, pot_ip, pot_oop } = game
    const currentRound = turnCard ? 'river' : 'turn'
    const decision = this.createSelectCardDecision(value, currentRound, game)
    const potMain = currentRound === 'turn' ? 'potMainTurnRound' : 'potMainRiverRound'
    let potMainDisplay = addBbSuffixToValue(calculatePotMainDisplay(decisionsTree.decisions, game, DISPLAY_IN_BB), DISPLAY_IN_BB)
    const previousStreet = currentRound === 'turn' ? 'flop' : 'turn'
    const bettedPotDataParam = cloneDeep(bettedPot)
    bettedPotDataParam[previousStreet] = { pot_ip, pot_oop }

    let dataParams = {
      game: game,
      currentPosition: DEFAULT_BEGIN_ROUND_POSITION,
      stack_size: stack_size,
      sim_type: sim_type,
      positions: positionsForQueryBuilder(positions),
      flops: flop_cards,
      nodes: nodes + `:${convertTurnRiverCard(value, cardsConvertingTemplate)}`,
      node_count: `${nodes}:${value}`.split(':').length,
      currentRound: currentRound,
      potMainDisplay: potMainDisplay,
      fetching: 'show_odin_loading',
      bettedPot: bettedPotDataParam,

    }
    dataParams[`${currentRound}Card`] = value
    dataParams[`${potMain}`] = potMainDisplay

    fetchTurnRiverData(dataParams, decision, cancelToken, cancelObj);
  }

  handleCloseCardPopup() {
    let decisions = this.props.decisionsTree.decisions || [];
    decisions = decisions.filter(decision => decision.active);
    let previousDecision = decisions[decisions.length - 2];

    handleBackToHistory(this.props, previousDecision)
  }

  render(){
    const t = this.context.t;
    let {turnCard, riverCard, game} = this.props;
    let cardOptionsArray = chunk(CARD_OPTIONS, SUITED_CARD_NUMBER);

    return (
      <Collapse in={this.state.showCardSelect}>
        <div className='select-turn-card'>
          <div className='head font-size-16 font-weight-600'>
            { ['flops', 'turn'].includes(game.currentRound) ? t('main_page.select_river_card.title') : t('main_page.select_turn_card.title')}
            <div className="close-button" onClick={this.handleCloseCardPopup}>×</div>
          </div>
          <div className='body'>
            {
              cardOptionsArray.map((suitedCards, index) => {
                return (
                  <div key={index} className='d-flex'>
                    {
                      suitedCards.map((card, index) => {
                        let cardValue = `${card.rank}${card.suit}`
                        return (
                          <Fragment key={index}>
                            { !(turnCard === cardValue || riverCard === cardValue || game.flopCards.findIndex(card => card === cardValue) !== -1) ?
                              <div key={index} className='small-card-wrapper flex-grow-1 text-nowrap' onClick={(e) => this.handleClickCard(cardValue)}>
                                <Card rank={card.rank}
                                    suit={card.suit}
                                    className='d-flex justify-content-center align-items-center'
                                    size='small' />
                              </div> :
                              <div className='small-card-wrapper cursor-auto'></div>
                            }
                          </Fragment>
                        )
                      })
                    }
                  </div>
                )
              })
            }
          </div>
        </div>
      </Collapse>
    )
  }
}

CSelectTurnCard.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  game: store.game,
  decisionsTree: store.decisionsTree,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  handMatrix: store.handMatrix,
  cardsConvertingTemplate: store.cardsConverting.cardsConvertingTemplate
})

const mapDispatchToProps = {
  updateTurnCard,
  updateRiverCard,
  addDecisionToTree,
  fetchTurnRiverData,
  updateComparedDecisions,
  rollbackDecision,
  togglePopup,
  fetchDataForTurnRiverRound,
  updateOdinLoadingShowingValue
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CSelectTurnCard)
