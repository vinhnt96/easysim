import React, { Component, Fragment } from 'react'
import './RunoutsExplorer.scss'
import { connect } from 'react-redux'
import { RUNOUTS_EXPLORER_VIEWS, CARDS } from 'config/constants'
import { backgroundColorForPreview } from 'services/background_color_services'
import { isEmpty, keys, values, map, min, max, round, isEqual, cloneDeep } from 'lodash'
import { fetchRunoutsExplorerData, fetchDataForTurnRiverRound } from 'actions/game.actions'
import {
  RUNOUTS_EXPLORER_DEFAULT_VIEW,
  RUNOUTS_EXPLORER_EV_DECIMAL,
  RUNOUTS_EXPLORER_STRATEGY_DECIMAL,
} from 'config/constants'
import { backgroundColorEVEQForCombo } from 'services/backgroundServices/handMatrixBGColorService'
import { formatToBigBlind, recalculateStrategyObject, decisionCombosButtons, addPotValueToEvObjectData } from 'utils/utils'
import {convertTurnRiverCard} from 'services/convert_cards_services'
import { formatKeyToDisplay } from 'services/game_play_service'

class RunoutsExplorer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      view: RUNOUTS_EXPLORER_DEFAULT_VIEW,
    }

    this.handleOnClick = this.handleOnClick.bind(this);
  }

  handleOnClick(view) {
    const { game: { nodes, runoutsExplorerData }, fetchRunoutsExplorerData, fetchDataForTurnRiverRound } = this.props;

    if(isEmpty(runoutsExplorerData[view])) {
      fetchDataForTurnRiverRound({ fetching: 'show_runouts_loading' });
      fetchRunoutsExplorerData(view, nodes);
    }

    this.setState({ view });
  }

  optionsEles() {
    return RUNOUTS_EXPLORER_VIEWS.map((view, index) => {
      return (
        <button
          key={index}
          className={`btn option-btn ${view.value === this.state.view ? 'selected' : ''}`}
          onClick={() => { this.handleOnClick(view.value) }}
        >
          {view.text}
        </button>
      );
    });
  }

  suitBtnsEles() {
    return (
      <Fragment>
        <div className="suited-btn d-flex align-items-center justify-content-center">
          <div className='suited-icon club-suited-icon'></div>
        </div>
        <div className="suited-btn d-flex align-items-center justify-content-center">
          <div className='suited-icon diamond-suited-icon'></div>
        </div>
        <div className="suited-btn d-flex align-items-center justify-content-center">
          <div className='suited-icon spade-suited-icon'></div>
        </div>
        <div className="suited-btn d-flex align-items-center justify-content-center">
          <div className='suited-icon heart-suited-icon'></div>
        </div>
      </Fragment>
    );
  }

  ranksItemsEles() {
    const ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];

    return (
      ranks.map((rank, index) => <div key={index} className='rank-item'>{rank}</div>)
    );
  }

  mobileView() {
    return (
      <div className='runouts-explorer-content-mobile'>
        <div className='options-group'>
          {this.optionsEles()}
        </div>
        <div className='suits-groups'>
          {this.suitBtnsEles()}
        </div>
        <div className='d-flex'>
          <div className='ranks-group'>
            <div className='ranks'>
              {this.ranksItemsEles()}
            </div>
          </div>
          <div className='hand-matrix-items-group cursor-pointer'>
            {this.handMatrixItemEles()}
          </div>
        </div>

      </div>
    )
  }

  sortDataObj(dataObj) {
    return keys(dataObj)
      .sort((first, second) => {
        const parseIntFirst = parseInt(first.slice(1, first.length))
        const parseIntSecond = parseInt(second.slice(1, second.length))
        return (parseIntFirst < parseIntSecond) ? 1 : ((parseIntFirst > parseIntSecond) ? -1 : 0)
      }).reduce(function (result, key) {
        result[key] = dataObj[key];
        return result;
      }, {});
  }

  strategyView() {
    const { game, cardsConvertingTemplate, currentUser, decisions } = this.props;
    const { preferences } = currentUser
    const { turnCard, currentRound, currentPosition } = game;
    const runoutsExplorerData = game.runoutsExplorerData[this.state.view] || {};
    const listKeyDisplay = decisionCombosButtons(game, preferences, currentPosition, decisions).slice(0, -1).map( decision => decision.value)
    const isRiverRound = currentRound === 'river'
    if(isRiverRound) {
      const convertedTurnCard = convertTurnRiverCard(turnCard, cardsConvertingTemplate)
      runoutsExplorerData[convertedTurnCard] = {}
    }
    const handMatrixItemElesArray = [];
    CARDS.forEach((card, index) => {
      const convertedCard = convertTurnRiverCard(card, cardsConvertingTemplate);
      let objData = (runoutsExplorerData[convertedCard] || {})
      objData = this.sortDataObj(objData);
      const style = backgroundColorForPreview(objData, preferences)
      objData = recalculateStrategyObject(objData, RUNOUTS_EXPLORER_STRATEGY_DECIMAL);

      handMatrixItemElesArray.push(
        <div key={index} className='matrix-item' style={style}>
          { map(objData, (value, key) => {

              return listKeyDisplay.includes(key) ? (
                <div key={key} className='row no-gutters matrix-item-info'>
                  <div className='col-7 text-left'>{formatKeyToDisplay(key, game, currentUser)}</div>
                  <div className='col-5 text-right'>{`${value}%`}</div>
                </div>
              )
              : <Fragment key={key}> </Fragment>
            })
          }
        </div>);
    });

    return handMatrixItemElesArray;
  } 

  convertValue(data, view) {
    const valuesArr = values(data) 
    if (isEmpty(valuesArr)) return valuesArr
    const minVal = min(valuesArr), maxVal = max(valuesArr)
    if (isEqual(view, 'ev'))
      return { min: this.roundValue(minVal), max: this.roundValue(maxVal) }
    return { min: this.roundValue(minVal * 100, 0), max: this.roundValue(maxVal * 100, 0) } // for equity view
  }

  roundValue(v, decimal = RUNOUTS_EXPLORER_EV_DECIMAL) {
    return round(v, decimal)
  }

  showValue(value, view) {  
    if (value <= 0 || isEqual(value, NaN))
      return ''

    if (isEqual(view, 'ev'))
      return formatToBigBlind(value, RUNOUTS_EXPLORER_EV_DECIMAL)
    return `${value}%`
  }

  evEqView(view) {
    const { game, cardsConvertingTemplate } = this.props;
    const { turnCard, runoutsExplorerData, currentRound } = game;
    const { view: viewSelect } = this.state
    let runoutsExplorerDataValues = cloneDeep(runoutsExplorerData[viewSelect])
    const isEvView = isEqual(view, 'ev')
    if(isEvView) {
      const potValue = game[`pot_${viewSelect.replace('ev_','')}`]
      runoutsExplorerDataValues = addPotValueToEvObjectData(runoutsExplorerDataValues, potValue)
    }
    const values = this.convertValue(runoutsExplorerDataValues, view)
    if (isEmpty(values)) return null
    const isRiverRound = currentRound === 'river'
    if(isRiverRound) {
      const convertedTurnCard = convertTurnRiverCard(turnCard, cardsConvertingTemplate)
      runoutsExplorerDataValues[convertedTurnCard] = 0
    }

    const handMatrixItemElesArray = [];
    CARDS.forEach((card, index) => {
      const convertedCard = convertTurnRiverCard(card, cardsConvertingTemplate)
      let value = runoutsExplorerDataValues[convertedCard]
      value = isEqual(view, 'ev') ? this.roundValue(value) : this.roundValue(value * 100, 0)
      const style = value > 0 ? backgroundColorEVEQForCombo(viewSelect, value, values.max, RUNOUTS_EXPLORER_EV_DECIMAL, values.min) : {}
      handMatrixItemElesArray.push(
        <div key={index} className='matrix-item text-center' style={style}>
          {this.showValue(value, view)}
        </div>
      );
    });

    return handMatrixItemElesArray;
  }

  handMatrixItemEles() {
    const { view } = this.state

    switch(view) {
      case 'strategy':
        return this.strategyView();
      case 'ev_oop':
      case 'ev_ip':
      case 'eq_oop':
      case 'eq_ip':
        return this.evEqView(view.split('_')[0]);
      default:
        return null;
    }
  }

  render() {
    return (
      <div className='runouts-explorer'>
        <div className='runouts-explorer-content'>
          <div className='ranks-group clearfix'>
            <div className='ranks'>
              {this.ranksItemsEles()}
            </div>
          </div>
          <div className='d-flex'>
            <div className='options-group pr-3'>
              {this.optionsEles()}
            </div>
            <div className='suits-groups'>
              {this.suitBtnsEles()}
            </div>
            <div className='hand-matrix-items-group'>
              {this.handMatrixItemEles()}
            </div>
          </div>
        </div>
        {this.mobileView()}
      </div>
    )
  }
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  game: store.game,
  cardsConvertingTemplate: store.cardsConverting.cardsConvertingTemplate,
  decisions: store.decisionsTree.decisions,
})

const mapDispatchToProps = {
  fetchRunoutsExplorerData,
  fetchDataForTurnRiverRound
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(RunoutsExplorer)
