import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import './RunoutsExplorerModal.scss'
import {clearRunoutsExplorerData} from 'actions/game.actions'
import { connect } from 'react-redux'
import loadable from '@loadable/component'
const RunoutsExplorer = loadable(() => import('./RunoutsExplorer/RunoutsExplorer'))

class CRunoutsExplorerModal extends Component {
  constructor(props) {
    super(props);

    this.handleOnHide = this.handleOnHide.bind(this);
  }

  handleOnHide() {
    const { setModalShow, clearRunoutsExplorerData } = this.props;

    clearRunoutsExplorerData();
    setModalShow('');
  }

  render() {
    const { modalShow } = this.props;

    return (
        <Modal
          show={modalShow}
          onHide={this.handleOnHide}
          dialogClassName='runouts-explorer-modal'
          backdrop="static"
          aria-labelledby='runouts-explorer-modal'
        >
          <Modal.Header closeButton>
            <Modal.Title id="runouts-explorer-modal"> Runouts explorer</Modal.Title>
          </Modal.Header>
          <Modal.Body bsPrefix='custom-modal-body'>
            <RunoutsExplorer/>
          </Modal.Body>
        </Modal>
      )
  }
}

const mapStoreToProps = (store) => ({
})

const mapDispatchToProps = {
  clearRunoutsExplorerData,
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CRunoutsExplorerModal)
