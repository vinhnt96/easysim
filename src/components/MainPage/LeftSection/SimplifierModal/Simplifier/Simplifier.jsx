import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Simplifier.scss'
import { SIMPLIFIER } from 'config/constants'
import { connect } from 'react-redux'
import axios from 'axios'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import TooltipQuestion from 'components/Shared/TooltipQuestion/component'

class Simplifier extends Component {

  constructor(props) {
    super(props)

    this.state = {
      simplifier: {
        round_strategies_to_closest: '',
        hide_strategies_less_than: ''
      }
    }

    this.handleOnChange = this.handleOnChange.bind(this)
    this.handleSave = this.handleSave.bind(this)
  }

  componentDidMount() {
    let preferences = this.props.currentUser.preferences || {}
    let { round_strategies_to_closest, hide_strategies_less_than } = preferences
    let simplifier = { round_strategies_to_closest, hide_strategies_less_than }
    this.setState({ simplifier: simplifier })
  }

  handleOnChange(e){
    let simplifier = {...this.state.simplifier}
    let value = e.target.value
    simplifier[e.target.name] = value
    this.handleSave(simplifier)
    this.setState({ simplifier: simplifier })
  }

  handleSave(simplifierParams={}) {
    let dataParams = simplifierParams || { ...this.state.simplifier }
    let { currentUser } = this.props
    axios.put(`/user_preferences/${currentUser.preferences.id}`, dataParams).then(res => {
      const preferences = res.data.user.preferences
      let { round_strategies_to_closest, hide_strategies_less_than } = preferences
      let simplifier = { round_strategies_to_closest, hide_strategies_less_than }
      this.setState({ simplifier: simplifier })
      this.props.updateCurrentUserAttributesToReduxTokenAuth(res.data.user)
    })
  }

  generateSelectOption(renderView){
    return SIMPLIFIER[renderView].percentageOptions.map((option, index) => <option key={index} value={option.value}>{option.label}</option>)
  }

  render() {
    const { simplifier } = this.state
    const { currentUser: { preferences } } = this.props
    const hideExplanation = preferences.hide_explanation
    const t = this.context.t

    return (
      <div className='simplifier simple-view'>
        <div className='strategy-actions d-flex justify-content-between'>
          <div className='round-strategy'>
            <span className='mr-2'> {t('round_strategies_to_closest')} </span>
            <select name='round_strategies_to_closest' className='custom-dropdown' value={simplifier.round_strategies_to_closest} onChange={this.handleOnChange} >
              {this.generateSelectOption('roundStrategiesToClosest')}
            </select>
          </div>
          <div className='hide-strategy'>
            <span className='mr-2'> {t('hide_strategies_less_than')} </span>
            <select name='hide_strategies_less_than' className='custom-dropdown' value={simplifier.hide_strategies_less_than} onChange={this.handleOnChange} >
              {this.generateSelectOption('hideStrategiesLessThan')}
            </select>
            {
              !hideExplanation &&
              <TooltipQuestion explanationName='hide-strategy-explanation' place='left' type='dark'>
                <p>This function hides excessive sizes that don’t occur more then the frequency set.</p>
                <p>Example: if the strategy bets all in 0.03% and the threshold is set to 0.05% the bet would be hidden making the solution less cluttered</p>
              </TooltipQuestion>
            }
          </div>
        </div>
      </div>
    )
  }
}

Simplifier.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
  updateCurrentUserAttributesToReduxTokenAuth
}
export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(Simplifier)
