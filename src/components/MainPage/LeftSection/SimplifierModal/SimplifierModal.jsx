import React, { Component } from 'react'
import Modal from 'react-bootstrap/Modal'
import './SimplifierModal.scss'
import Simplifier from './Simplifier/Simplifier'

export default class SimplifierModal extends Component {

  render() {
    const { modalShow, setModalShow } = this.props;

    return (
        <Modal
          show={modalShow}
          onHide={() => { setModalShow('') }}
          dialogClassName='simplifier-modal'
          backdrop="static"
          aria-labelledby='simplifier-modal'
        >
          <Modal.Header closeButton>
            <Modal.Title id="simplifier-modal">Simplifier</Modal.Title>
          </Modal.Header>
          <Modal.Body bsPrefix='custom-modal-body'>
            <Simplifier closeModal={setModalShow} />
          </Modal.Body>
        </Modal>
      )
  }

}
