import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from 'react-bootstrap/Modal'
import './OptionsModal.scss'
import { faAngleLeft } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import loadable from '@loadable/component'
const OptionsModalContent = loadable(() => import('./OptionsModalContent'))
const ColourOptions = loadable(() => import('./ColourOptions/ColourOptions'))

const bodyComponentMaps = {
  home: {
    component: OptionsModalContent,
    title: 'Options',
  },
  colorOptions: {
    component: ColourOptions,
    title: 'Change Color Options',
  }
}

class COptionsModal extends Component {
  constructor(props) {
    super(props)

    this.state = {
      bodyKey: 'home'
    }
    this.changeModelBody = this.changeModelBody.bind(this)
    this.hideModal = this.hideModal.bind(this)
  }

  hideModal() {
    this.setState({bodyKey: 'home'})
    this.props.setModalShow('')
  }

  changeModelBody(bodyKey='home') {
    this.setState({bodyKey: bodyKey})
  }

  render() {
    const { modalShow, setModalShow } = this.props;
    const bodyKey = this.state.bodyKey
    const bodyData = bodyComponentMaps[bodyKey];
    const BodyComponent = bodyData.component
    const dialogClassName = bodyKey === 'colorOptions' ? 'modal-xl' : ''

    return (
      <Modal
        show={modalShow}
        onHide={() => this.hideModal()}
        dialogClassName={dialogClassName}
        backdrop="static"
        aria-labelledby='main-page-options-modal'
      >
        <Modal.Body bsPrefix='custom-modal-body'>
          <Modal.Header bsPrefix='custom-modal-header d-flex justify-content-between align-items-center' closeButton>
            {bodyKey !== 'home' && <div className='back-icon' onClick={() => this.changeModelBody('home')} >
              <FontAwesomeIcon icon={faAngleLeft}/>
            </div>}
            <Modal.Title id="options-modal">{bodyData.title}</Modal.Title>
          </Modal.Header>
          <BodyComponent
            changeModelBody={this.changeModelBody}
            setModalShow={setModalShow}/>
        </Modal.Body>
      </Modal>
    )
  }
}

COptionsModal.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(COptionsModal)
