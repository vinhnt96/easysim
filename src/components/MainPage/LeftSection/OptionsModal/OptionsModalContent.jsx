import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './OptionsModal.scss'
import chipsIcon from '../../../../images/chips-icon.png'
import explanationIcon from '../../../../images/explanation-icon.png'
import colourIcon from '../../../../images/colour-icon.png'
import nextArrow from '../../../../images/next-arrow.png'
import CustomCheckbox from 'components/CustomCheckbox/CustomCheckbox'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import axios from 'axios'

class CustomRadio extends Component {

  render() {
    let { data, disabled, checked, handleOnChange, className } = this.props;
    return (
      <Fragment>
        <input
          className="inp-cbx"
          id={`cbx-${data}`}
          checked={checked}
          type="radio"
          style={{display: 'none'}}
          onChange={(e) => handleOnChange(e, data)}
          disabled={disabled}
        />
        <label
          className={`cbx ${className} ${disabled ? 'cursor-not-allowed' : ''}`}
          htmlFor={`cbx-${data}`} >
          <span>
            <svg width="12px" height="10px" viewBox="0 0 12 10">
              <polyline points="1.5 6 4.5 9 10.5 1" />
            </svg>
          </span>
        </label>
      </Fragment>
    )
  }
}

class COptionsModalContent extends Component {

  constructor(props) {
    super(props)

    this.state = {
      displayPositionsByDefault: false,
      isHiddenExplanations: false
    }
    this.handleChecked = this.handleChecked.bind(this);
    this.handleSave = this.handleSave.bind(this)
  }

  componentDidMount() {
    let { currentUser } = this.props
    if (currentUser && currentUser.preferences) {
      let { hide_explanation, display_positions_by_default } = currentUser.preferences
      this.setState({
        isHiddenExplanations: hide_explanation,
        displayPositionsByDefault: display_positions_by_default
      })
    }
  }

  handleChecked(e, data) {
    switch (data) {
      case 'hide-explanation':
        this.setState({isHiddenExplanations: !this.state.isHiddenExplanations})
        break
      case 'display-positions-by-default':
        this.setState({displayPositionsByDefault: !this.state.displayPositionsByDefault})
        break
      default:
        this.setState({isHiddenExplanations: !this.state.isHiddenExplanations})
    }
  }

  handleSave() {
    let dataParams = {
      hide_explanation: this.state.isHiddenExplanations,
      display_positions_by_default: this.state.displayPositionsByDefault
    }
    let { currentUser } = this.props
    axios.put(`/user_preferences/${currentUser.preferences.id}`,{user_preference: dataParams}).then(res => {
      const preferences = res.data.user.preferences
      this.setState({
        isHiddenExplanations: preferences.hide_explanation
      })
      this.props.updateCurrentUserAttributesToReduxTokenAuth(res.data.user)
      this.props.setModalShow('')
    })
  }

  render() {
    let t = this.context.t

    return <div className='content pt-2 pb-0 px-0'>
      <div className='option row d-flex align-items-center'>
        <label htmlFor="cbx-display-positions-by-default" className='col-7 d-flex p-0 m-0 align-items-center'>
          <div className='mr-2 icon chips-icon d-flex align-items-center justify-content-center'>
            <img src={chipsIcon} alt='chips-icon'/>
          </div>
          <div className='d-flex w-100 justify-content-between align-items-center'>
            <span>BTN, CO, HJ...</span>
            <CustomRadio
              name='display-positions-by-default'
              data={!this.state.displayPositionsByDefault}
              checked={!this.state.displayPositionsByDefault}
              handleOnChange={(e) => this.handleChecked(e, 'display-positions-by-default')}
              className='mb-0 mr-4'
            />
          </div>
        </label>
        <label htmlFor="cbx-display-positions-by-default" className='col-5 d-flex p-0 m-0 align-items-center'>
          <div className='d-flex w-100 justify-content-between align-items-center'>
            <span className='ml-5'>B, B1, B2...</span>
            <CustomRadio
              name='display-positions-by-default'
              data={this.state.displayPositionsByDefault}
              checked={this.state.displayPositionsByDefault}
              handleOnChange={(e) => this.handleChecked(e, 'display-positions-by-default')}
              className='mb-0'
            />
          </div>
        </label>
      </div>

      <label htmlFor="cbx-hide-explanation" className='d-flex p-0 m-0'>
        <div className='option d-flex align-items-center m-0'>
          <div className='mr-2 icon explanation-icon d-flex align-items-center justify-content-center'>
            <img src={explanationIcon} alt='explanation-icon'/>
          </div>
          <div className='w-100 d-flex justify-content-between align-items-center'>
            <span>{t('hide_explanation')}</span>
            <CustomCheckbox
              data='hide-explanation'
              checked={this.state.isHiddenExplanations}
              handleChecked={ (e) => this.handleChecked(e, 'hide-explanation')}
            />
          </div>
        </div>
      </label>
      <div className='option d-flex justify-content-between align-items-center'
            onClick={() => this.props.changeModelBody('colorOptions')}>
        <div className='d-flex align-items-center'>
          <div className='mr-2 icon colour-icon d-flex align-items-center justify-content-center'>
            <img src={colourIcon} alt='colour-icon'/>
          </div>
          <div>Colour Options</div>
        </div>
        <img src={nextArrow} alt='next-icon' className='next-icon'/>
      </div>
      <div className='col-12 my-3 row m-0 p-0 justify-content-end'>
        <div className='col-9 row m-0 p-0'>
          <div className='col-6 p-1 pr-3'>
            <div className='btn-primary' onClick={() => { this.handleSave() }}>OK</div>
          </div>
          <div className='col-6 py-1 pl-0 pr-3'>
            <div className='btn-primary' onClick={() => { this.props.setModalShow('') }}>Cancel</div>
          </div>
        </div>
      </div>
    </div>
  }
}

COptionsModalContent.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
  updateCurrentUserAttributesToReduxTokenAuth: updateCurrentUserAttributesToReduxTokenAuth
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(COptionsModalContent)
