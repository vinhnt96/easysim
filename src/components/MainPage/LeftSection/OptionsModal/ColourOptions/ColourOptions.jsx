import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './ColourOptions.scss'
import checkIcon from '../../../../../images/check-solid.svg'
import axios from 'axios'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import { COLOR_SECTION_TITLES } from "config/constants"
import { backgroundColorForPreview } from 'services/background_color_services'

class CColourOptions extends Component {
  constructor() {
    super()

    this.state = {
      selectedColors: {
        fold_color: '',
        check_and_call_color: '',
        bet_and_raise_color: ''
      }
    }

    this.handleSave = this.handleSave.bind(this)
    this.preview_data = {};
    this.colors = [
      'blue', 'orange', 'yellow', 'green', 'red', 'purple'
    ]
    this.hands_list_for_preview = [
      "A7s", "A6s", "A5s", "A4s", "A3s", "A2s",
      "K7s", "K6s", "K5s", "K4s", "K3s", "K2s",
      "Q7s", "Q6s", "Q5s", "Q4s", "Q3s", "Q2s",
      "J7s", "J6s", "J5s", "J4s", "J3s", "J2s",
      "T7s", "T6s", "T5s", "T4s", "T3s", "T2s"
    ];
    this.mockup_decisions = ['b182', 'b363', 'c', 'f'];
  }

  generateDataForPreview() {
    let datas_for_preview = {}
    this.hands_list_for_preview.forEach((hand, index) => {
      let total_decision_percent = 0;
      datas_for_preview[hand] = {}
      this.mockup_decisions.forEach((decision, index) => {
        const isLastItem = index === this.mockup_decisions.length - 1
        let decision_percent = isLastItem ? 1 - total_decision_percent : Math.random() * (1 - total_decision_percent - 0.1) + 0.1;
        datas_for_preview[hand][decision] = decision_percent;
        total_decision_percent = total_decision_percent + decision_percent;
      })
    });
    return datas_for_preview;
  }

  componentDidMount() {
    let { currentUser } = this.props
    if (currentUser && currentUser.preferences) {
      let { fold_color, check_and_call_color, bet_and_raise_color } = currentUser.preferences
      let selectedColors = {
        fold_color: fold_color,
        check_and_call_color: check_and_call_color,
        bet_and_raise_color: bet_and_raise_color
      }
      this.setState({
        selectedColors: selectedColors
      })
    }
    this.preview_data = this.generateDataForPreview();
  }

  handleSave() {
    let dataParams = { ...this.state.selectedColors }
    let { currentUser } = this.props
    axios.put(`/user_preferences/${currentUser.preferences.id}`, dataParams).then(res => {
      const preferences = res.data.user.preferences
      const { fold_color, check_and_call_color, bet_and_raise_color } = preferences
      const selectedColors = {
        fold_color: fold_color,
        check_and_call_color: check_and_call_color,
        bet_and_raise_color: bet_and_raise_color
      }
      this.setState({
        selectedColors: selectedColors
      })
      this.props.updateCurrentUserAttributesToReduxTokenAuth(res.data.user)
      this.props.changeModelBody('home')
    })
  }

  renderColorSchemeSection(t) {
    const colorTitlesLength = COLOR_SECTION_TITLES.length
    return COLOR_SECTION_TITLES.map((title, titleIndex) => {
      const isLastScheme = titleIndex === colorTitlesLength - 1
      return (
        <div key={titleIndex + Math.random() + title} className={`color-scheme-section ${isLastScheme ? 'last' : ''}`}>
          <label className='mt-2 font-weight-bold'>{t(title)}</label>
          <div className='row p-0 m-0'> {this.renderColorSchemeElements(title)} </div>
        </div>
      )
    })
  }

  renderColorSchemeElements(title) {
    const { selectedColors } = this.state
    return this.colors.map((color, index) => {
      return (
        <div key={index} className='col-2 p-0'>
          <div onClick={() => this.handleChangeColor(title, color)}
            className={`color-option-item d-flex align-items-center justify-content-center cursor-pointer ${color}`}>
            {selectedColors[`${title}_color`] === color &&
              <img alt={`${title}-color-checked`} src={checkIcon} />
            }
          </div>
        </div>
      )
    })
  }

  handleChangeColor(title, color) {
    const { selectedColors } = this.state
    selectedColors[`${title}_color`] = color
    this.setState({selectedColors: selectedColors})
  }

  render() {
    const t = this.context.t
    return (
      <div className='content'>
        <div className='row p-0 m-0'>
          <div className='col-6 p-0'> {this.renderColorSchemeSection(t)} </div>
          <div className='col-6 p-0'>
            <label className='mt-2 font-weight-bold'>Preview</label>
            <div className='row range-matrix p-0 m-0'>
              { this.hands_list_for_preview.map((item, index) => {
                let style = backgroundColorForPreview(this.preview_data[item], this.state.selectedColors)
                return (
                  <div key={index} className='matrix-element p-1' style={style}> {item} </div>
                )
              })}
            </div>
          </div>
          <div className='col-12 mt-3 row m-0 p-0 justify-content-end'>
            <div className='col-8 row m-0 p-0'>
              <div className='col-6 p-1'>
                <div className='btn-primary' onClick={this.handleSave}>Ok</div>
              </div>
              <div className='col-6 p-1'>
                <div className='btn-primary' onClick={() => this.props.changeModelBody('home')}>Cancel</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CColourOptions.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
  updateCurrentUserAttributesToReduxTokenAuth: updateCurrentUserAttributesToReduxTokenAuth
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CColourOptions)
