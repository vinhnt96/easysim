import React, { Component, Fragment } from 'react'
import './stylesheet.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import { positionsForQueryBuilder  } from 'services/game_play_service'
import { convertFlopCards, buildConvertSuitsTemplate } from 'services/convert_cards_services'
import { dataForEachComboGenerator } from 'services/matrix_service'
import { Redirect } from 'react-router-dom'
import { freeTree, loadTreeRequest } from 'services/load_tree_request_service.js'
import axios from 'axios'
import {
  recieveFlopData,
  fetchCurrentFlop,
  fetchDataForTurnRiverRound,
  updateFetchTurnRiverSuccess,
  refeshGameData,
  initGameData,
  reloadSimProcess
} from 'actions/game.actions'
import { togglePopup } from 'actions/subscription.actions'
import { checkFreeTrialUser, notify } from 'utils/utils'
import IdleTimer from 'react-idle-timer'
import {updateCardsConvertingTemplate} from 'actions/cards-converting.actions'
import { isEmpty, isEqual } from 'lodash'
import { refreshDecisionsTree } from 'actions/decision.actions'
import { refeshHandMatrix, updateComboWithIndex } from 'actions/hand-matrix.actions'
import { renderCombosWithIndexToRedux } from 'utils/matrix'
import { WEIGHTED_STRATEGY_DEFAULT_STATUS } from 'config/constants'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import loadable from '@loadable/component'
import { toast } from 'react-toastify';

const LeftSection = loadable(() => import('./LeftSection/component'))
const RightSection = loadable(() => import('./RightSection/component'))
const SubscribeNowPopup = loadable(() => import('components/Shared/SubscribeNowPopup/SubscribeNowPopup'))
const TimedOutNotifyModal = loadable(() => import('./TimedOutNotifyModal/TimedOutNotifyModal'))
const NotifyModal = loadable(() => import('components/NotifyModal/component'))
const ReloadPageModal = loadable(() => import('./ReloadPageModal/ReloadPageModal'))

const cancelToken = axios.CancelToken;
let cancelObj = {};

class CMainPageContent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      positionSelected: '',
      sidebarOpen: false,
      weightedStrategy: WEIGHTED_STRATEGY_DEFAULT_STATUS,
      loadedTreeInfo: false,
      isInactive: false,
      showNotifyModal: false,
      notAccept: false
    }

    this.handleWeightedStrategyOnChange = this.handleWeightedStrategyOnChange.bind(this);
    this.handleCloseLoader = this.handleCloseLoader.bind(this);
    this.handleCloseSubscribePopUp = this.handleCloseSubscribePopUp.bind(this);
    this.idleTimer = null
    this.handleOnIdle = this.handleOnIdle.bind(this)
    this.setShowNotifyModal = this.setShowNotifyModal.bind(this)
    this.handleOnReloadSim = this.handleOnReloadSim.bind(this)
  }

  componentDidMount() {
    const { state } = this.props.history.location
    sessionStorage.setItem("unMount", false);
    window.removeEventListener("beforeunload", this.handleUpdateTreeList, true)
    if (state && state.accept ) {
      const { handMatrix, currentUser, game: { flopCards, fetchTurnRiverSuccess },
        updateCardsConvertingTemplate, initGameData, cardsConvertingTemplate, updateComboWithIndex } = this.props
      const { strategy_selection } = currentUser
      const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || strategy_selection
      const treeInfo = JSON.parse(sessionStorage.getItem("treeInfo")) || {}
      const { street, flop_cards, game_type, stack_size, sim_type, positions, specs } = strategySelection
      if (isEmpty(handMatrix.combosWithIndex)) {
        const cards = isEmpty(flopCards) ? flop_cards : flopCards
        updateComboWithIndex(renderCombosWithIndexToRedux(cards))
      }
      let cardsConvertingTemplateCopy = { ...cardsConvertingTemplate }

      if(isEmpty(cardsConvertingTemplateCopy)) {
        cardsConvertingTemplateCopy = buildConvertSuitsTemplate(flop_cards)
        updateCardsConvertingTemplate(cardsConvertingTemplateCopy)
      }
      const convertedFlopCards = convertFlopCards(flop_cards, cardsConvertingTemplateCopy)

      if(isEqual(street, 'postflop')) {
        const dataParams = {
          game_type,
          stack_size,
          specs,
          sim_type,
          flops: convertedFlopCards.join(''),
          positions: positionsForQueryBuilder(positions),
          nodes: "r:0",
          node_count: 2,
          view: handMatrix.view || ''
        }
        initGameData({...strategySelection, flopCards: strategySelection.flop_cards , convertedFlopCards}, dataParams, cardsConvertingTemplateCopy)

        sessionStorage.setItem("strategy_selections", JSON.stringify({
          ...strategySelection,
          convertedFlopCards
        }))
      }

      if(!fetchTurnRiverSuccess) {
        sessionStorage.setItem("added", true)

        loadTreeRequest({
          ...strategySelection,
          convertedFlopCards,
          updateFetchTurnRiverSuccess
        }).then(res => {
          if (isEqual(res, 'success'))
            this.props.updateFetchTurnRiverSuccess('done')
        })
      }

      if(!isEmpty(treeInfo['channelId']) && !isEqual(fetchTurnRiverSuccess, 'done')) {
        this.props.updateFetchTurnRiverSuccess('done')
      }

      window.addEventListener("beforeunload", this.handleUpdateTreeList, true)

    } else {
      this.setPopupModalAccept()
    }
  }

  componentDidUpdate() {
    const { location: { state } } = this.props
    const { notAccept } = this.state
    if ( state && !state.accept && !notAccept ) {
      this.setPopupModalAccept()
      window.removeEventListener( 'popstate', e => e.preventDefault() )
    } 
  }

  componentWillMount() {
    const { location: { state } , history } = this.props
    window.addEventListener('popstate', (e) => {
      if (state && state.accept && isEqual(history.location.pathname, '/main-page') && !this.state.notAccept) {
        history.replace('/main-page', { accept: false })
      }
    })
  }

  componentWillUnmount() {
    this.props.refeshGameData()
    this.props.refeshHandMatrix()
    this.props.refreshDecisionsTree()
    this.freeKey()
    sessionStorage.setItem("unMount", true)
  }

  setPopupModalAccept() {
    this.setState({
      notAccept: true,
      showNotifyModal: true
    })
  }

  handleUpdateTreeList = (e) => {
    if (e !== undefined)
      e.preventDefault()
    if (sessionStorage.getItem("unMount") === 'false')
      this.freeKey()
  }

  freeKey() {
    const treeInfo = JSON.parse(sessionStorage.getItem("treeInfo")) || {}

    // just call free_tree if this tab have loaded tree and this is the last tab open this sim
    if(Object.keys(treeInfo).length > 0) {
      freeTree(treeInfo.channelId)
    }
  }

  removeSim(sim, object, num, id) {
    num > 0 ? object[sim.id] = num : delete object[sim.id]
    localStorage.setItem("requests", JSON.stringify(object))
    sessionStorage.setItem("added", false);
    const payload = { sim_ids: JSON.stringify(Object.keys(object)), add: false }
    if (sim) {
      window.axios.put(`/users/${id}`, payload)
    }
  }

  handleCloseLoader() {
    this.props.fetchDataForTurnRiverRound({ fetching: false, betCompleted: true });
    cancelObj.cancel();
  }

  handleWeightedStrategyOnChange() {
    this.setState({ weightedStrategy: !this.state.weightedStrategy });
  }

  handleCloseSubscribePopUp() {
    this.props.togglePopup('');
  }

  handleOnIdle (event) {
    // lock screen here
    this.setState({ isInactive: true })
    this.handleUpdateTreeList(event)
  }

  setShowNotifyModal(value=false) {
    this.setState({ showNotifyModal: value })
    if (this.state.notAccept)
      this.props.history.replace('/strategy-select')
  }

  handleOnReloadSim() {
    const { cardsConvertingTemplate, currentUser } = this.props
    const { strategy_selection } = currentUser
    const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || strategy_selection
    const { flop_cards } = strategySelection
    const convertedFlopCards = convertFlopCards(flop_cards, cardsConvertingTemplate)

    this.props.reloadSimProcess('fetching')
    loadTreeRequest({
      ...strategySelection,
      convertedFlopCards,
      updateFetchTurnRiverSuccess
    }).then(res => {
      if (isEqual(res, 'success')) {
        notify("Reload simulation successfully", toast.success, {time: 3000});
        this.props.reloadSimProcess('done')
      }
    })
  }

  render() {
    const t = this.context.t
    const { positionSelected, weightedStrategy, isInactive, notAccept, showNotifyModal } = this.state
    const { subscriptions: { popUp },
            currentUser: { preferences, email, strategy_selection },
            game: { ranges_ip, ranges_oop, ev_oop, ev_ip, equity_ip, equity_oop, flops, turnCard, riverCard,
                    nodes, currentPosition, comparedDecisions, flopCards, totalPot, pot_oop, pot_ip, betCompleted, shouldReloaded},
            handMatrix: { combosWithIndex, view} } = this.props
    const evData = { ev_oop, ev_ip }
    const paramsForDataGenerator = {
      ranges_ip, ranges_oop, ev_oop, ev_ip, equity_ip, equity_oop,
      flops, nodes, currentPosition, comparedDecisions, flopCards,
      totalPot, pot_oop, pot_ip, combosWithIndex, view }
    const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || strategy_selection
    
    if(isEmpty(strategySelection)) return <Redirect to='/strategy-select' />
    if(isEqual(strategySelection.street, 'preflop')) return <Redirect to='/preflop-page' />

    const dataForEachCombos = dataForEachComboGenerator({...paramsForDataGenerator , preferences: preferences})
    let strategyAndEvData = {}
    if(isEqual(view, 'strategy-ev') && evData[`ev_${currentPosition}`]) {
      strategyAndEvData['evData'] = dataForEachComboGenerator({...paramsForDataGenerator, view: `ev_${currentPosition}`})
      strategyAndEvData['strategyData'] = dataForEachComboGenerator({...paramsForDataGenerator, view: ''})
    }
    const showSubscribePopUp = checkFreeTrialUser(email) && isEqual(popUp, 'main-page-subscribe-now');
    const messageNoti = notAccept ? t('notify_modal.not_accept') : t('notify_modal.can_not_open_more_than_1_sim_message')
    return (
      <Fragment>
        <NotifyModal
          modalShow={showNotifyModal}
          infomation={{title: 'Action not available', message: messageNoti}}
          handleOnClose={e => this.setShowNotifyModal()}
        />
        {!notAccept && <div
          className='main-interface-page' >
          <IdleTimer
            ref={ref => { this.idleTimer = ref }}
            timeout={1000*60*15}
            onIdle={this.handleOnIdle}
            debounce={250}
          />
          <div className={`main-interface mx-auto ${showSubscribePopUp ? 'blurring-20' : ''}`}>
            <div className='row p-0 m-0'>
              <div className='col-lg-6 col-sm-12 p-0 m-0'>
                <LeftSection
                  positionSelected={positionSelected}
                  turnCard={turnCard}
                  riverCard={riverCard}
                  weightedStrategy={weightedStrategy}
                  handleWeightedStrategyOnChange={this.handleWeightedStrategyOnChange}
                  cancelToken={cancelToken}
                  cancelObj={cancelObj}
                  setShowNotifyModal={this.setShowNotifyModal}
                  betCompleted={betCompleted}
                />
              </div>
              <div className='col-lg-6 col-sm-12 p-0 m-0'>
                <RightSection
                  dataForEachCombos={dataForEachCombos}
                  strategyAndEvData={strategyAndEvData}
                  weightedStrategy={weightedStrategy}
                />
              </div>
            </div>
          </div>
        </div>
        }
        { isInactive &&
          <TimedOutNotifyModal
            modalShow={isInactive}/>
        }
        { shouldReloaded &&
          <ReloadPageModal 
            modalShow={shouldReloaded}
            handleOnReloadSim={this.handleOnReloadSim}/>
        }
        <SubscribeNowPopup
          message={t('free_trial.subscribe_popup.unclock_main_page')}
          isShown={showSubscribePopUp}
          handleClose={this.handleCloseSubscribePopUp}
        />
      </Fragment>
    )
  }
}

CMainPageContent.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  i18nState: store.i18nState,
  game: store.game,
  handMatrix: store.handMatrix,
  subscriptions: store.subscriptions,
  cardsConvertingTemplate: store.cardsConverting.cardsConvertingTemplate
})

const mapDispatchToProps = {
  recieveFlopData,
  fetchCurrentFlop,
  fetchDataForTurnRiverRound,
  togglePopup,
  updateCardsConvertingTemplate,
  updateFetchTurnRiverSuccess,
  refeshGameData,
  refreshDecisionsTree,
  refeshHandMatrix,
  updateComboWithIndex,
  updateCurrentUserAttributesToReduxTokenAuth,
  reloadSimProcess,
  initGameData
}

const MainPageContent = connect(
  mapStoreToProps,
  mapDispatchToProps
)(CMainPageContent)

export default withRouter(MainPageContent)
