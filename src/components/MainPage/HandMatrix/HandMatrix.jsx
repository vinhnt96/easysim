import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { handCombinationsGenerator, combosGenerator, getPositionClass, renderDataComboRouded } from '../../../utils/matrix'
import { chunk, times, pick, isEqual, round, findKey, max} from 'lodash'
import './HandMatrix.scss'
import { backgroundServices } from 'services/background_color_services'
import { filterCombosBySuit } from 'services/filter_combos_by_suit_service'
import { NUMBER_OF_ROW_IN_EACH_COL, EV_EQ_PRECISION, EMAILS_ABLE_TO_VIEW_BLACK_DESIGN, COMPARE_EV_PRECISION } from "config/constants"
import { formatToBigBlind, bigBlindValue, roundEVValue, decisionCombosButtons, orderCompareEVOptions} from 'utils/utils'
import { preflopBackgroundColorForComboItem } from 'services/preflop_game_service'
import { calculatePotMainDisplay, formatKeyToDisplay } from 'services/game_play_service'

class CHandMatrix extends Component {

  renderStrategy(strategy, combo, isBigCombinations, listKeyDisplay, viewOption) {
    const { decisionFilter, game, isPreflopPage, currentUser } = this.props
    strategy = !!decisionFilter && Object.keys(strategy).includes(decisionFilter) ? pick(strategy, decisionFilter) : strategy
    const strategyKeys = Object.keys(strategy).filter((strategyElement) => listKeyDisplay.includes(strategyElement))
    const numberOfRow = Object.keys(strategy).length >= 9 ? 5 : NUMBER_OF_ROW_IN_EACH_COL
    const numberOfCol = Math.ceil(strategyKeys.length / numberOfRow)
    const dataChunk = chunk(strategyKeys, numberOfRow)
    return times(numberOfCol, (col) => {
      return (
        <div
          key={col + Date.now + Math.random()}
          className={`data-col-${col} ${isBigCombinations ? 'col-5 mx-2' : `${ numberOfCol > 1 ? 'col-6' : 'col-9'}`} p-0`} >
          {
            dataChunk[col].map((key) => {
              let data = viewOption === '' ? parseFloat(strategy[key]['rounded']) : parseFloat(strategy[key])
              const roundedValue = isEqual(viewOption, 'strategy-ev') ?
                                    roundEVValue(data, true, EV_EQ_PRECISION) :
                                    `${Math.round(data * 10000) / 100}%`

              return (
                <div
                  key={combo + key + data}
                  className={`${numberOfCol === 1 ? 'font-size-13' : 'font-size-10'} ${isBigCombinations ? '' : 'small-font'}`}
                >
                  <span className=''>
                    {formatKeyToDisplay(key, game, currentUser, isPreflopPage)}
                  </span>
                  <span className='float-right'>{roundedValue}</span>
                </div>
              )
          })}
        </div>
      )
    })
  }

  calculateInfo = (data, viewOption) => {
    switch(viewOption) {
      case 'ev_oop':
      case 'ev_ip':
        return isNaN(data) ? '' : `${round(parseFloat(data), EV_EQ_PRECISION)}bb`
      case 'ev_rescale_oop':
      case 'ev_rescale_ip':
        return isNaN(data) ? '' : `${round(parseFloat(data), EV_EQ_PRECISION)}%`
      case 'ev_per_eq_oop':
      case 'ev_per_eq_ip':
        return isNaN(data) ? '' : `${round(parseFloat(data)/100, EV_EQ_PRECISION)}bb`
      case 'equity_oop':
      case 'equity_ip':
        const noValue = isNaN(data) || parseFloat(data) === 0
        return noValue ? 0 : `${round(parseFloat(data) * 100)}%`
      default:
        return parseFloat(data)
    }
  }

  renderByViewOption(data, isBigCombinations, viewOption) {
    const cssClass = 'text-large text-right w-100 mr-1'
    const styleClass = isBigCombinations ? cssClass : `small-font ${cssClass}`
    const info = this.calculateInfo(data, viewOption)

    return(
      <div className={styleClass}>
        {!!info && info}
      </div>
    )
  }

  renderCompareEVData(data) {
    const dataValues = Object.values(data)
    const hasEmptyOrNaNValue = dataValues.some( v => isNaN(v) || v === '' )
    if(hasEmptyOrNaNValue) return <Fragment> </Fragment>
    
    const { game, currentUser, isPreflopPage } = this.props
    const orderedKey = orderCompareEVOptions(data)
    const firstValue = data[orderedKey[1]];
    const secondValue = data[orderedKey[0]];

    if (firstValue === 0 && secondValue === 0) return null

    const higherDecision = findKey(data, v => v === max([firstValue, secondValue]));

    let valueDifference = Math.abs(round(bigBlindValue(firstValue, COMPARE_EV_PRECISION) - bigBlindValue(secondValue, COMPARE_EV_PRECISION), COMPARE_EV_PRECISION))
    valueDifference = valueDifference > 0 ? `+${valueDifference}` : valueDifference

    const shownData = <Fragment>
                        <div>
                          <i>{formatKeyToDisplay(orderedKey[1], game, currentUser, isPreflopPage)}</i>
                          <i className='float-right'>{formatToBigBlind(data[orderedKey[1]], COMPARE_EV_PRECISION)}</i>
                        </div>
                        <div>
                          <i>{formatKeyToDisplay(orderedKey[0], game, currentUser, isPreflopPage)}</i>
                          <i className='float-right'>{formatToBigBlind(data[orderedKey[0]], COMPARE_EV_PRECISION)}</i>
                        </div>
                      </Fragment>

    return (
      <div className='w-100 px-2 font-size-12'>
        {shownData}
        <div>
          <i>{formatKeyToDisplay(higherDecision, game, currentUser, isPreflopPage)}</i>
          <i className='float-right'>{valueDifference}bb</i>
        </div>
      </div>
    );
  }

  renderComboInfo(data, combo, isBigCombinations, listKeyDisplay) {
    const { view, handMatrix: { view: handMatrixView }, rangeExplorerShowing } = this.props
    const viewOption = rangeExplorerShowing ? view : handMatrixView
    let info = [];

    switch(viewOption) {
      case '':
      case 'strategy-ev':
        info = this.renderStrategy(data, combo, isBigCombinations, listKeyDisplay, viewOption)
        break;
      case 'compare-ev':
        info = this.renderCompareEVData(data)
        break;
      default:
        info = this.renderByViewOption(data, isBigCombinations, viewOption)
    }

    return(
      <Fragment>
        {info}
      </Fragment>
    )
  }

  specialEvView(viewOption) {
    return ['ev_rescale_oop', 'ev_rescale_ip', 'ev_per_eq_ip', 'ev_per_eq_oop'].includes(viewOption)
  }

  readDataByViewOption(combo, dataForEachCombos, viewOption, preferences) {
    if(this.specialEvView(viewOption)) {
      return dataForEachCombos[combo]
    } else {
     return viewOption === '' && preferences.round_strategies_to_closest > 0 ? renderDataComboRouded(dataForEachCombos[combo] || {}) : dataForEachCombos[combo] || {}
    }
  }

  render() {
    const { game, currentUser: { preferences, role, email }, dataForEachCombos, strategyAndEvData, view, handMatrix: { view: handMatrixView }, suitsForFilter, weightedStrategy,
      rangeExplorerShowing, handClickedItem, previousGameExplorer, isPreflopPage, rangesData, decisions, hand, decisionFilter } = this.props
    const focusedHandItem = handClickedItem || hand
    const combinations =  handCombinationsGenerator(focusedHandItem || "AA")
    const isBigCombinations = combinations.length === 4
    const combos = combosGenerator(focusedHandItem || "AA")
    const gridNum = isBigCombinations ? 6 : combinations.length === 12 ? 3 : 4
    const viewOption = rangeExplorerShowing ? view : handMatrixView
    const isStrategyEvView = isEqual(viewOption, 'strategy-ev')
    const filteredCombos = filterCombosBySuit(focusedHandItem, combos, suitsForFilter)
    const userRole = EMAILS_ABLE_TO_VIEW_BLACK_DESIGN.includes(email) ? 'black_design_user' : role;
    let gameState = previousGameExplorer || game
    gameState = {...gameState, weightedStrategy}
    const listKeyDisplay = decisionCombosButtons(game, preferences, game.currentPosition, decisions).slice(0, -1).map( decision => decision.value)
    return (
      <div className='row p-0 m-0 h-100 w-100'>
        {
          combinations.map((combination, index) => {
            const positionClass = getPositionClass(combinations.length, index)
            const { first: { rank: firstRank, suit: firstSuit }, second: { rank: secondRank, suit: secondSuit } } = combination
            const combo = firstRank + firstSuit + secondRank + secondSuit
            const data = this.readDataByViewOption(combo, dataForEachCombos, viewOption, preferences)
            const deFilter = decisionFilter || '';
            const totalPotInBB = !this.specialEvView(viewOption)
            const totalPot = calculatePotMainDisplay(decisions, game, totalPotInBB)
            const style = isPreflopPage ? preflopBackgroundColorForComboItem(data, preferences)
                                      : backgroundServices.comboBackgroundColorGenerator({ preferences, deFilter, dataForEachCombos, totalPot, userRole, view: viewOption, comboData: data, game: gameState })

            let strategyStyle = {}, evStyle = {};
            if(isStrategyEvView) {
              strategyStyle = backgroundServices.comboBackgroundColorGenerator({ game, preferences, deFilter, userRole, view: '', comboData: (strategyAndEvData.strategyData[combo] || {}) });
              evStyle = backgroundServices.comboBackgroundColorGenerator({ game, preferences, deFilter, userRole, view: `ev_${game.currentPosition}`, comboData: strategyAndEvData.evData[combo] || {} });
              strategyStyle['height'] = '50%';
              strategyStyle['top'] = '0';
              evStyle['height'] = '50%';
            }
            const styleVisible = {'visibility': filteredCombos.includes(combo) ? 'unset' : 'hidden'}

            let backgroundBlockStyle = {}
            if(!!weightedStrategy && !!rangesData) {
              let comboHeight = parseFloat(rangesData[combo]) * 100
              backgroundBlockStyle = {height: `${comboHeight}%`}
            }

            return (
              <div key={index} className={`hand-matrix-item-container cursor-pointer col-${gridNum} p-0 item-grid-${gridNum}`}>
                <div className={`hand-matrix-item ${positionClass} row no-gutters`} style={styleVisible}>
                  <div className={`background-block-container ${positionClass}`}>
                    <div className='background-block' style={backgroundBlockStyle}>
                      <div className="background" style={isStrategyEvView ? strategyStyle : style}></div>
                      {
                        isStrategyEvView && <div className='ev-background' style={evStyle}></div>
                      }
                    </div>
                  </div>
                  <div className={`row no-gutters w-100 pt-1`}>
                    <div className={`rank-container ${isBigCombinations ? 'ml-3' : 'ml-2'} w-100`}>
                      <div className='d-inline-block font-weight-bold mr-1'>
                        <span>{firstRank}</span>
                        <i className={`icon icon-${firstSuit} background-size-contain`}></i>
                      </div>
                      <div className='d-inline-block font-weight-bold'>
                        <span>{secondRank}</span>
                        <i className={`icon icon-${secondSuit} background-size-contain`}></i>
                      </div>
                    </div>
                  </div>
                  <div className={`combo-info font-weight-bold row my-1 mx-0 w-100 ${gridNum === 3 ? ' d-flex justify-content-between' : ''}`}>
                    {this.renderComboInfo(data, combo, isBigCombinations, listKeyDisplay)}
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    )
  }
}

CHandMatrix.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  handMatrix: store.handMatrix,
  game: store.game,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  decisions: store.decisionsTree.decisions,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CHandMatrix)
