import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import OdinLoader from 'components/Shared/OdinLoader/OdinLoader'
import loadable from '@loadable/component'
const MainPageContent = loadable(() => import('./MainPageContent'))

let cancelObj = {};
class CMainPage extends Component {

  constructor(props) {
    super(props)

    this.handleCloseLoader = this.handleCloseLoader.bind(this);
  }

  handleCloseLoader() {
    this.props.fetchDataForTurnRiverRound({ fetching: false, betCompleted: true });
    cancelObj.cancel();
  }

  render() {
    const { odinLoading } = this.props

    return (
      <Fragment>
        <MainPageContent />
        { odinLoading.fetching === 'show_odin_loading' &&
          <OdinLoader
            handleCloseLoader={this.handleCloseLoader}
            disableProgressBar={true}
          />
        }
      </Fragment>
    )
  }
}

CMainPage.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  odinLoading: store.odinLoading
})

const mapDispatchToProps = {
}

const MainPage = connect(
  mapStoreToProps,
  mapDispatchToProps
)(CMainPage)

export default withRouter(MainPage)
