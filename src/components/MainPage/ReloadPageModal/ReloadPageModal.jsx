import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from 'react-bootstrap/Modal'
import './ReloadPageModal.scss'

class CReloadPageModal extends Component {
  render() {
    const { modalShow } = this.props;

    return (
      <Modal
        show={modalShow}
        dialogClassName='modal-l'
        backdrop="static"
        aria-labelledby='refresh-page-modal'
      >
        <Modal.Body bsPrefix='custom-modal-body'>
          <div className='content pt-2 pb-0 px-0'>
            <div className='p-3 text-center pt-4'>
              The simulation has timed out, please load it again.
            </div>

            <div className='my-3 row m-0 p-0 justify-content-center pb-4'>
              <div className='col-6'>
                <button className='btn btn-primary font-size-14 font-weight-bold p-2 w-100'
                        onClick={this.props.handleOnReloadSim}>
                  Reload simulation
                </button>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}

CReloadPageModal.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CReloadPageModal)
