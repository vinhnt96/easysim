import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './DecisionCombos.scss'
import { formatToBigBlind } from 'utils/utils'
import { calculateStrategyKey } from 'services/game_play_service'
import TooltipQuestion from 'components/Shared/TooltipQuestion/component'

class CDecisionCombos extends Component {
  constructor() {
    super();

    this.handleOnclick = this.handleOnclick.bind(this);
  }

  handleOnclick(e) {
    if(!this.props.disabled) {
      let selectedDecision = e.currentTarget.dataset.decision;
      let newDecision = this.props.decisionFilter === selectedDecision ? '' : selectedDecision
      this.props.updateDecisionFilter(newDecision);
    }
  }

  generateFontSize() {
    let decisionCombos = this.props.decisionCombos;
    let fontSize = {};

    switch(decisionCombos.length) {
      case 1:
      case 2:
      case 3:
      case 4:
        fontSize['topText'] = 'font-size-15';
        fontSize['bottomText'] = 'font-size-12'
        break;
      case 5:
        fontSize['topText'] = 'font-size-14';
        fontSize['bottomText'] = 'font-size-12'
        break;
      case 6:
        fontSize['topText'] = 'font-size-13';
        fontSize['bottomText'] = 'font-size-11'
        break;
      case 7:
        fontSize['topText'] = 'font-size-12';
        fontSize['bottomText'] = 'font-size-11'
        break;
      case 8:
        fontSize['topText'] = 'font-size-11';
        fontSize['bottomText'] = 'font-size-10'
        break;
      case 9:
      case 10:
        fontSize['topText'] = 'font-size-9';
        fontSize['bottomText'] = 'font-size-8'
        break;
      default:
        fontSize['topText'] = 'font-size-7';
        fontSize['bottomText'] = 'font-size-6'
        break;
    }

    return fontSize;
  }

  render(){
    const { decisionCombos, decisionFilter, disabled, currentUser, game, isMainPage } = this.props;
    const preferences = currentUser.preferences || {}
    const hideExplanation = preferences.hide_explanation
    const strategy_selection = JSON.parse(sessionStorage.getItem("strategy_selections")) || currentUser.strategy_selection
    const stack_size = parseInt(strategy_selection.stack_size);
    const lastIndex = decisionCombos.length - 1;
    const fontSize = this.generateFontSize();
    const className = this.props.className || ''

    const comboEles = decisionCombos.map((combo, index) => {
      const selectedClass = decisionFilter === combo.value ? 'selected' : '';
      let decisionValue = ['FOLD', 'CHECK', 'CALL'].includes(combo.raw_value) ? combo.raw_value : calculateStrategyKey(combo.raw_value, game)

      return (
        <div key={index}
          data-decision={combo.value}
          className={`action font-weight-600 ${combo.colorClass} ${selectedClass} ${disabled ? 'cursor-not-allowed' : 'cursor-pointer'}`}
          onClick={this.handleOnclick} >
          <div className={`w-100 h-100 d-flex justify-content-center align-items-center ${combo.isMaxPercent && 'glow'}`}>
            <div>
              <div className={`top-text ${fontSize['topText']}`}>
                <span className='position-relative'>
                  {stack_size ? formatToBigBlind(decisionValue) : combo.title}
                  {combo.betPercentOfPot}
                  {
                    isMainPage && index === lastIndex && !hideExplanation &&
                    <TooltipQuestion
                      questionIconClassName='question-mask-container'
                      explanationName='decision-combos-explanation'
                      place='left'
                      type='dark'>
                      <div>The % of the total range that chooses this bet size | </div>
                      <div>The number of individual combinations that chooses this bet size.</div>
                    </TooltipQuestion>
                  }
                </span>
              </div>
              <div className={`bottom-text ${fontSize['bottomText']}`}>
                <span>
                  {`${combo.percent} | ${combo.info}`}
                </span>
              </div>
            </div>
          </div>
        </div>
      )
    });

    return (
      <div className={`combos row p-0 m-0 align-items-center ${this.props.isHidden ? 'invisible' : '' } ${className}`}>
        {comboEles}
      </div>
    )
  }
}

CDecisionCombos.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  i18nState: store.i18nState,
  game: store.game,
  decisions: store.decisionsTree.decisions,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CDecisionCombos)
