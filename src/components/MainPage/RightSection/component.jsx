import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { decisionCombosButtons } from 'utils/utils'
import { updateSuitsForFilter, updateDecisionFilter, changeClickedHandItem} from 'actions/hand-matrix.actions'
import { dataForEachComboGenerator } from 'services/matrix_service'
import Simplifier from '../LeftSection/SimplifierModal/Simplifier/Simplifier'
import TooltipQuestion from 'components/Shared/TooltipQuestion/component'
import loadable from '@loadable/component'
const RangeMatrix = loadable(() => import('../RangeMatrix/RangeMatrix'))
const DecisionCombos = loadable(() => import('../DecisionCombos/DecisionCombos'))
const Filter = loadable(() => import('../Filter/Filter'))
const HandMatrix = loadable(() => import('../HandMatrix/HandMatrix'))

class CRightSection extends Component {
  constructor(props) {
    super(props)
    this.state = {
      handHovered: '',
    }

    this.handleOnHoverOnHand = this.handleOnHoverOnHand.bind(this);
  }

  handleOnHoverOnHand(hand) {
    this.setState({
      handHovered: hand
    })
  }

  render(){
    const { 
      game: { currentPosition, ranges_oop, ranges_ip, flopCards },
      currentUser, dataForEachCombos, strategyAndEvData, weightedStrategy, handMatrix, decisions } = this.props
    const preferences = currentUser.preferences || {}
    const hideExplanation = preferences.hide_explanation
    const { suitsForFilter, decisionFilter, handClickedItem, view, combosWithIndex } = handMatrix
    let rangesView = ['', 'strategy-ev', 'compare-ev'].includes(view) ? `ranges_${currentPosition}` : view.includes('oop') ? 'ranges_oop' : 'ranges_ip'
    let rangesData = {}
    if(!!weightedStrategy) {
      rangesData = dataForEachComboGenerator({ view: rangesView, ranges_oop, ranges_ip, flopCards, combosWithIndex })
    }
    return (
      <div className='right h-100 d-flex flex-column pb-0'>
        <RangeMatrix
          suitsForFilter={suitsForFilter}
          decisionFilter={decisionFilter}
          handClickedItem={handClickedItem}
          dataForEachCombos={dataForEachCombos}
          strategyAndEvData={strategyAndEvData}
          changeClickedHandItem={this.props.changeClickedHandItem}
          handleOnHoverOnHand={this.handleOnHoverOnHand}
          showHandItemPopup={false}
          weightedStrategy={weightedStrategy} />
        <div className='decisions-container'>
          <DecisionCombos
            decisionFilter={decisionFilter}
            decisionCombos={decisionCombosButtons(this.props.game, preferences, currentPosition, decisions).slice(0, -1)}
            disabled={!['', 'strategy-ev'].includes(this.props.handMatrix.view)}
            updateDecisionFilter={this.props.updateDecisionFilter}
            isMainPage={true}
          />
        </div>
        <div className="simplifier-container">
          <Simplifier />
        </div>
        <div className='row flex-grow-1 mb-4 no-gutters'>
          <div className='filter-suited combination-height col-md-2 pr-2 text-center'>
            <Filter
              direction='vertical'
              suitsForFilter={suitsForFilter}
              updateSuitsForFilter={this.props.updateSuitsForFilter}
            />
            { !hideExplanation &&
              <TooltipQuestion className='pt-0' explanationName='suit-filter-explanation' place='top' type='dark'>
                <div>Select to view the strategy for the specific suit combinations.</div>
                <div>Eg. if you choose the Spade/Spade option, the matrix will display the strategy for all combos suited in spades.</div>
              </TooltipQuestion>
            }
          </div>
          <div className='hand-combinations-matrix combination-height col-md-10 d-flex'>
            <HandMatrix
              dataForEachCombos={dataForEachCombos}
              strategyAndEvData={strategyAndEvData}
              hand={this.state.handHovered}
              suitsForFilter={suitsForFilter}
              decisionFilter={decisionFilter}
              weightedStrategy={weightedStrategy}
              rangesData={rangesData}
              handClickedItem={handClickedItem} />
          </div>
        </div>
      </div>
    )
  }
}

CRightSection.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  game: store.game,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  handMatrix: store.handMatrix,
  decisions: store.decisionsTree.decisions,
})

const mapDispatchToProps = {
  updateSuitsForFilter: updateSuitsForFilter,
  updateDecisionFilter: updateDecisionFilter,
  changeClickedHandItem: changeClickedHandItem,
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CRightSection)
