import React, {Component, Fragment} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './Filter.scss'

class CFilter extends Component {
  constructor(props) {
    super(props)
  
    this.handleOnClickFirstSuit = this.handleOnClickFirstSuit.bind(this)
    this.handleOnClickSecondSuit = this.handleOnClickSecondSuit.bind(this)
  }

  handleOnClickFirstSuit(e, suit) {
    e.preventDefault();
    let {suitsForFilter} = this.props
    let {included, excluded} = suitsForFilter
    let firstExcluded = excluded.first || ''
    let firstIncluded = included.first || ''
    if(e.type === "contextmenu") {
      firstExcluded = !!firstExcluded && excluded.first === suit ? '' : suit
      firstIncluded = ''
    } else {
      firstIncluded = !!firstIncluded && included.first === suit ? '' : suit
      firstExcluded = ''
    }
    suitsForFilter = {...suitsForFilter, included: {...included, first: firstIncluded}, excluded: {...excluded, first: firstExcluded}}
    this.props.updateSuitsForFilter(suitsForFilter)
  }

  handleOnClickSecondSuit(e, suit) {
    e.preventDefault();
    let {suitsForFilter} = this.props
    let {included, excluded} = suitsForFilter
    let secondExcluded = excluded.second || ''
    let secondIncluded = included.second || ''
    if(e.type === "contextmenu") {
      secondExcluded = !!secondExcluded && excluded.second === suit ? '' : suit
      secondIncluded = ''
    } else {
      secondIncluded = !!secondIncluded && included.second === suit ? '' : suit
      secondExcluded = ''
    }
    suitsForFilter = {...suitsForFilter, included: {...included, second: secondIncluded}, excluded: {...excluded, second: secondExcluded}}
    this.props.updateSuitsForFilter(suitsForFilter)
  }


  filterEles(direction) {
    const {suitsForFilter} = this.props
    let {included, excluded} = suitsForFilter
    const clubIcon1st =
      <div onClick={(e) => this.handleOnClickFirstSuit(e, 'c')}
           onContextMenu={(e) => this.handleOnClickFirstSuit(e, 'c')}
           className={`${included.first === 'c' ? 'included' : ''} ${excluded.first === 'c' ? 'excluded' : ''} item-suited d-flex align-items-center justify-content-center`}>
        <div className='club-suited-icon suited-icon'/>
      </div>
    const clubIcon2nd =
      <div onClick={(e) => this.handleOnClickSecondSuit(e,'c')}
           onContextMenu={(e) => this.handleOnClickSecondSuit(e, 'c')}
           className={`${included.second === 'c' ? 'included' : ''} ${excluded.second === 'c' ? 'excluded' : ''} item-suited d-flex align-items-center justify-content-center`}>
        <div className='club-suited-icon suited-icon'/>
      </div>

    const diamonIcon1st =
      <div onClick={(e) => this.handleOnClickFirstSuit(e, 'd')}
           onContextMenu={(e) => this.handleOnClickFirstSuit(e, 'd')}
           className={`${included.first === 'd' ? 'included' : ''} ${excluded.first === 'd' ? 'excluded' : ''} item-suited d-flex align-items-center justify-content-center`}>
        <div className='diamond-suited-icon suited-icon'/>
      </div>
    const diamonIcon2nd =
      <div onClick={(e) => this.handleOnClickSecondSuit(e,'d')}
           onContextMenu={(e) => this.handleOnClickSecondSuit(e, 'd')}
           className={`${included.second === 'd' ? 'included' : ''} ${excluded.second === 'd' ? 'excluded' : ''} item-suited d-flex align-items-center justify-content-center`}>
        <div className='diamond-suited-icon suited-icon'/>
      </div>

    const spadeIcon1st =
      <div onClick={(e) => this.handleOnClickFirstSuit(e, 's')}
           onContextMenu={(e) => this.handleOnClickFirstSuit(e, 's')}
           className={`${included.first === 's' ? 'included' : ''} ${excluded.first === 's' ? 'excluded' : ''} item-suited d-flex align-items-center justify-content-center`}>
        <div className='spade-suited-icon suited-icon'/>
      </div>
    const spadeIcon2nd =
      <div onClick={(e) => this.handleOnClickSecondSuit(e,'s')}
           onContextMenu={(e) => this.handleOnClickSecondSuit(e, 's')}
           className={`${included.second === 's' ? 'included' : ''} ${excluded.second === 's' ? 'excluded' : ''} item-suited d-flex align-items-center justify-content-center`}>
        <div className='spade-suited-icon suited-icon'/>
      </div>

    const heartIcon1st =
      <div onClick={(e) => this.handleOnClickFirstSuit(e, 'h')}
           onContextMenu={(e) => this.handleOnClickFirstSuit(e, 'h')}
           className={`${included.first === 'h' ? 'included' : ''} ${excluded.first === 'h' ? 'excluded' : ''} item-suited d-flex align-items-center justify-content-center`}>
        <div className='heart-suited-icon suited-icon'/>
      </div>
    const heartIcon2nd =
      <div onClick={(e) => this.handleOnClickSecondSuit(e,'h')}
           onContextMenu={(e) => this.handleOnClickSecondSuit(e, 'h')}
           className={`${included.second === 'h' ? 'included' : ''} ${excluded.second === 'h' ? 'excluded' : ''} item-suited d-flex align-items-center justify-content-center`}>
        <div className='heart-suited-icon suited-icon'/>
      </div>

    if(direction === 'vertical') {
      return (
        <div className='vertical h-100'>
          <div className='icons-group'>
            {clubIcon1st}
            {clubIcon2nd}
          </div>
          <div className='icons-group'>
            {diamonIcon1st}
            {diamonIcon2nd}
          </div>
          <div className='icons-group'>
            {spadeIcon1st}
            {spadeIcon2nd}
          </div>
          <div className='icons-group'>
            {heartIcon1st}
            {heartIcon2nd}
          </div>
        </div>
      )
    }

    if(direction === 'horizontal') {
      return (
        <div className='horizontal'>
          <div className='d-flex align-items-center justify-content-between'>
            {clubIcon1st}
            {diamonIcon1st}
            {heartIcon1st}
            {spadeIcon1st}
          </div>
          <div className='d-flex align-items-center justify-content-between'>
            {clubIcon2nd}
            {diamonIcon2nd}
            {heartIcon2nd}
            {spadeIcon2nd}
          </div>
        </div>
      )
    }
  }

  render() {
    const direction = this.props.direction
    return (
      <Fragment>
        {this.filterEles(direction)}
      </Fragment>
    )
  }
}

CFilter.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  handMatrix: store.handMatrix
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CFilter)
