import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { backgroundServices } from 'services/background_color_services'
import { filterCombosBySuit } from 'services/filter_combos_by_suit_service'
import { combosGenerator, roundValueToNearest, buildItemDisplayForRangeMatrix } from '../../../utils/matrix'
import './RangeMatrix.scss'
import { preflopBackgroundColorForHandItem } from 'services/preflop_game_service'
import { EV_EQ_PRECISION, EMAILS_ABLE_TO_VIEW_BLACK_DESIGN, RANGES_PRECISION, HANDS_LIST } from 'config/constants'
import { isEmpty, round, isEqual} from 'lodash'
import { calculatePotMainDisplay } from 'services/game_play_service'

class CRangeMatrix extends Component {

  constructor(props) {
    super(props)

    this.state = {
      styleForEachHand: {}
    }
  }

  renderHand(handItemDisplay, handItem) {
    if(!isEmpty(handItemDisplay)) {
      const { first, second } = handItemDisplay
      const { rank: firstRank, suit: firstSuit } = first
      const { rank: secondRank, suit: secondSuit} = second
      return(
        <div className='d-flex'>
          <div className='d-inline-block'>
            <i>{firstRank}</i>
            { firstSuit ? <i className={`icon icon-${firstSuit} background-size-contain`}></i> : <i>x</i> }
          </div>
          <div className='d-inline-block'>
            <i>{secondRank}</i>
            { secondSuit ? <i className={`icon icon-${secondSuit} background-size-contain`}></i> : <i>x</i> }
          </div>
        </div>
      )
    } else {
      return handItem
    }
  }

  calAverageValue = (value, optionType) => {
    const isEvView = optionType.includes('ev_rescale') || optionType.includes('ev_')
    if( isNaN(value) || (value === 0 && !isEvView)) return ''
    
    switch(optionType) {
      case 'ev_oop':
      case 'ev_ip':
        return `${round(value, EV_EQ_PRECISION)}bb`
      case 'ev_rescale_oop':
      case 'ev_rescale_ip':
        return `${round(value, EV_EQ_PRECISION)}%`
      case 'ev_per_eq_oop':
      case 'ev_per_eq_ip':
        return `${round(value/100, EV_EQ_PRECISION)}bb`
      case 'ranges_ip':
      case 'ranges_oop':
        return round(roundValueToNearest(value, 0.05), RANGES_PRECISION)
      case 'equity_ip':
      case 'equity_oop':
        return `${round(value * 100)}%`
      default:
        return round(value, EV_EQ_PRECISION)
    }
  }

  renderAverageValue(handItem, dataForEachCombos, suitsForFilter) {
    const { handMatrix: { view: handMatrixView } , view, rangeExplorerShowing } = this.props
    const viewOption = rangeExplorerShowing ? view : handMatrixView
    if(!['', 'strategy-ev'].includes(viewOption)) {
      let combos = combosGenerator(handItem)
      combos = filterCombosBySuit(handItem, combos, suitsForFilter)
      let averageValue = 0.0
      let counter = 0
      let counterNaN = 0
      combos.forEach((combo) => {
        const valueConvert = isNaN(dataForEachCombos[combo]) ? 0.0 : parseFloat(dataForEachCombos[combo])
        if(isNaN(dataForEachCombos[combo])) counterNaN += 1
        const isEvView = viewOption.includes('ev_rescale') || viewOption.includes('ev_')
        counter = ( isEvView && !isNaN(dataForEachCombos[combo])) || valueConvert > 0 ? counter + 1  : counter
        averageValue += (isEvView && !isNaN(dataForEachCombos[combo])) || valueConvert > 0 ? valueConvert : 0
      })
      averageValue = counterNaN === combos.length ? 'NaN' : counter === 0 ? 0.0 : averageValue/counter
      averageValue = this.calAverageValue(averageValue, viewOption)
      return(
        <p className="hand-value">{averageValue}</p>
      )
    }
  }

  renderHandItemContent(handItemDisplay, handItem, suitsForFilter, style, handClickedItem) {
    const { dataForEachCombos, weightedStrategy, hideHandItemContent } = this.props
    let stypeCopied = {...style}
    if(!weightedStrategy) {
      stypeCopied['height'] = 'inherit'
      stypeCopied['borderTopLeftRadius'] = 'inherit'
      stypeCopied['borderTopRightRadius'] = 'inherit'
    }

    return(
      <Fragment>
        <div className='background' style={stypeCopied}></div>
        {
          !hideHandItemContent &&
          <div className='content no-select'>
            {this.renderHand(handItemDisplay, handItem)}
            {this.renderAverageValue(handItem, dataForEachCombos, suitsForFilter)}
          </div>
        }
      </Fragment>
    )
  }

  specialEvView(viewOption) {
    return ['ev_rescale_oop', 'ev_rescale_ip', 'ev_per_eq_ip', 'ev_per_eq_oop'].includes(viewOption)
  }
  
  componentDidMount() {
    const { view, rangeExplorerShowing } = this.props
    if(!!view || rangeExplorerShowing) {
      const {handMatrix: { view: viewHandMatrix, combosWithIndex }, suitsForFilter, decisionFilter, weightedStrategy, isPreflopPage} = this.props
      const viewOption = rangeExplorerShowing ? view : viewHandMatrix
      const { role, preferences, email } = this.props.currentUser
      const data = isEqual(viewOption, 'strategy-ev') ? this.props.strategyAndEvData.strategyData : this.props.dataForEachCombos
      const gameState = this.props.previousGameExplorer || this.props.game
      const userRole = EMAILS_ABLE_TO_VIEW_BLACK_DESIGN.includes(email) ? 'black_design_user' : role;
      const totalPotInBB = !this.specialEvView(viewOption)
      const totalPot = calculatePotMainDisplay(this.props.decisions, this.props.game, totalPotInBB)
      const paramsForhandItemBackgroundColorGenerator = {
        suitsForFilter, decisionFilter, totalPot, userRole, weightedStrategy, combosWithIndex,
        view: viewOption, game: gameState, dataForEachCombos: data, preferences: preferences
      }
      let styleForEachHand = {}

      HANDS_LIST.forEach((handItem, index) => {
        styleForEachHand[handItem] = isPreflopPage ? preflopBackgroundColorForHandItem(handItem, data, this.props.heightPercentages[handItem] || 100, preferences, gameState.flopCards) :
          backgroundServices.handItemBackgroundColorGenerator({...paramsForhandItemBackgroundColorGenerator, handItem})
      })
      this.setState({
        styleForEachHand: styleForEachHand
      })
    }

  }

  componentDidUpdate(prevProps) {
    const { handMatrix: { view: viewHandMatrix,  combosWithIndex }, view, suitsForFilter, decisionFilter, weightedStrategy, isPreflopPage, rangeExplorerShowing} = this.props
    const { role, preferences, email } = this.props.currentUser
    const prevWeightedStrategy = prevProps.weightedStrategy
    const prevViewOption = prevProps.rangeExplorerShowing ? prevProps.view : prevProps.handMatrix.view
    const viewOption = rangeExplorerShowing ? view : viewHandMatrix
    const prevData = isEqual(prevViewOption, 'strategy-ev') ? prevProps.strategyAndEvData.strategyData : prevProps.dataForEachCombos
    const data = isEqual(viewOption, 'strategy-ev') ? this.props.strategyAndEvData.strategyData : this.props.dataForEachCombos

    if((!isEmpty(prevData) && !isEqual(prevData, data)) || !isEqual(prevViewOption, viewOption) ||
       !isEqual(prevProps.suitsForFilter, this.props.suitsForFilter) || !isEqual(prevProps.decisionFilter, this.props.decisionFilter) ||
       !isEqual(prevProps.currentUser.preferences, this.props.currentUser.preferences) || !isEqual(prevWeightedStrategy, weightedStrategy)) {

      const gameState = this.props.previousGameExplorer || this.props.game
      const userRole = EMAILS_ABLE_TO_VIEW_BLACK_DESIGN.includes(email) ? 'black_design_user' : role;
      const totalPotInBB = !this.specialEvView(viewOption)
      const totalPot = calculatePotMainDisplay(this.props.decisions, this.props.game, totalPotInBB)
      const paramsForhandItemBackgroundColorGenerator = {
        suitsForFilter, decisionFilter, totalPot, userRole, weightedStrategy, combosWithIndex,
        view: viewOption, game: gameState, dataForEachCombos: data, preferences: preferences
      }
      let styleForEachHand = {}

      HANDS_LIST.forEach((handItem, index) => {
        styleForEachHand[handItem] = isPreflopPage ? preflopBackgroundColorForHandItem(handItem, data, this.props.heightPercentages[handItem] || 100, preferences, gameState.flopCards) :
          backgroundServices.handItemBackgroundColorGenerator({...paramsForhandItemBackgroundColorGenerator, handItem})
      })
      this.setState({
        styleForEachHand: styleForEachHand
      })
    }
  }

  handleClickedHandItem(handItem, handClickedItem) {
    const handValue = handClickedItem === handItem ? '' : handItem
    this.props.changeClickedHandItem(handValue)
  }

  render(){
    const { handleMouseMove, suitsForFilter, handClickedItem, isPreflopPage, rangeMatrixClassName, handleOnHoverOnHand } = this.props

    return (
      <div className={`range-matrix row p-0 m-auto d-flex align-items-center ${rangeMatrixClassName}`}>
        {
          HANDS_LIST.map((handItem, index) => {
            const style = this.state.styleForEachHand[handItem]
            const handItemDisplay = buildItemDisplayForRangeMatrix(handItem, suitsForFilter)
            return(
              <div
                key={`hand-item-${index}`}
                onMouseEnter={(e) => handleOnHoverOnHand(handItem)}
                onMouseOut={isPreflopPage ? (e) => handleOnHoverOnHand('') : () => {}}
                onMouseMove={handleMouseMove}
                onClick={(e) => this.handleClickedHandItem(handItem, handClickedItem)}
                className={handClickedItem === handItem ? 'matrix-element selected' : 'matrix-element'}
              >
                {this.renderHandItemContent(handItemDisplay, handItem, suitsForFilter, style, handClickedItem)}
              </div>
            )
          })
        }
      </div>
    )
    
  }
}

CRangeMatrix.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  handMatrix: store.handMatrix,
  game: store.game,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  decisions: store.decisionsTree.decisions,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CRangeMatrix)
