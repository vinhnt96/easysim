import React, { Component, Fragment } from 'react'
import './FreePostflopSims.scss'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import {
  HUNL_CASH_GAME_STRATEGY,
  SIX_MAX_CASH_GAME_STRATEGY,
  MTT_GAME_STRATEGY,
} from 'config/constants'
import { recieveFlopData, updateGameData } from 'actions/game.actions'
import { convertFlopCards, buildConvertSuitsTemplate } from 'services/convert_cards_services'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import { updateCardsConvertingTemplate } from 'actions/cards-converting.actions'
import { Button, Spinner } from 'react-bootstrap'
import { cloneDeep, isNull, isEqual } from 'lodash'
import { checkExistingChannels } from 'services/load_tree_request_service.js'
import loadable from '@loadable/component'
const NotifyModal = loadable(() => import('components/NotifyModal/component'))

class FreePostflopSims extends Component {

  constructor(props) {
    super(props)
    
    this.strategySelectionList = [
      { name: 'hunl', strategy: cloneDeep(HUNL_CASH_GAME_STRATEGY) },
      { name: '6-max', strategy: cloneDeep(SIX_MAX_CASH_GAME_STRATEGY) },
      { name: 'mtt', strategy: cloneDeep(MTT_GAME_STRATEGY) },
    ]
    this.state = {
      loadingOption: '',
      valid: true,
      sims: [],
      existingChannel: false
    }

    this.handleClickBtn = this.handleClickBtn.bind(this)
  }

  componentDidMount() {
    this.fetchCurrentUser()
    sessionStorage.setItem("treeInfo", null)
  }

  handleClickBtn(strategyName) {
    const { currentUser: { role } } = this.props
    const strategyOption = this.strategySelectionList.find( strategyOption => strategyOption.name === strategyName)
    const payload = cloneDeep(strategyOption.strategy)

    this.setState({ loadingOption: strategyName })

    checkExistingChannels().then(res => {
      const data = res.data.data
      const { number_of_open_channels } = data
      if(number_of_open_channels <= 0 || isEqual(role, 'admin')) {
        this.fetchCurrentUser(payload)
      } else {
        this.setState({ existingChannel: true, loadingOption: '' })
      }

    })
    
  }

  fetchCurrentUser(payload=null) {
    const { currentUser } = this.props
    const { id } = currentUser
    window.axios.get(`/users/${id}`).then(({ data }) => {
      const sims = data.user.sim_ids
      const userUpdated = {...currentUser, sims: data.user.sim_ids}
      this.props.updateCurrentUserAttributesToReduxTokenAuth(userUpdated)
      if (!isNull(payload)) {
        this.setState({ sims })
        this.getStategySelection(payload)
      } else {
        this.setState({ sims, loadingOption: '' })
      }
    })
  }

  getStategySelection(payload) {
    window.axios.post('strategy_selections', { strategy_selection: payload }).then(({ data }) => {
      const { currentUser, updateCardsConvertingTemplate, recieveFlopData, updateCurrentUserAttributesToReduxTokenAuth, history } = this.props
      const { strategy_selection, user_strategy_selection } = data
      const { flop_cards } = strategy_selection
      const cardsConvertingTemplate = buildConvertSuitsTemplate(flop_cards)
      const convertedFlopCards = convertFlopCards(flop_cards, cardsConvertingTemplate)
      const dataStrategySelection = { ...strategy_selection, convertedFlopCards }
      sessionStorage.setItem("strategy_selections", JSON.stringify(dataStrategySelection))
      updateCardsConvertingTemplate(cardsConvertingTemplate)
      recieveFlopData(dataStrategySelection)
      updateCurrentUserAttributesToReduxTokenAuth({
        ...currentUser,
        strategy_selection,
        user_strategy_selection
      })
      sessionStorage.removeItem("user_strategy_selection")
      this.setState({ loadingOption: '' })
      history.push('/main-page', { accept: true })
    }, err => { alert("Something went wrong!") })
  }

  handleOnCloseNotifyModal() {
    if (this.state.valid) {
      this.props.history.push('/')
    } else {
      this.setState({ valid: true })
    }
  }

  render() {
    const t = this.context.t
    const { loadingOption, valid, existingChannel } = this.state
    const { currentUser: { role } } = this.props

    return (
      <Fragment>
        <div id='free-postflop-page'>
          <div className='free-postflop-container row no-gutters mx-auto h-100'>
            <div className='col-12 h-100'>
              <p className='free-postflop-title p-3 mb-0'>{t('free_postflop_sims')}</p>
              <div className='free-postflop-content'>
                <div className='button-group'>
                  {
                    this.strategySelectionList.map( strategyOption => {
                      return (
                        <Button onClick={() => this.handleClickBtn(strategyOption.name)}> 
                          { loadingOption === strategyOption.name ? <Spinner animation="border" size="sm" /> : strategyOption.name.toUpperCase()}
                        </Button>
                      )
                    })
                  }
                </div>
              </div>
            </div>

          </div>
        </div>
        <NotifyModal
          modalShow={!valid || (!loadingOption && existingChannel && !isEqual(role, 'admin'))}
          infomation={{title: t('notify_modal.title'), message: valid ? t('notify_modal.can_not_open_more_than_1_sim_message') : ''}}
          handleOnClose={this.handleOnCloseNotifyModal.bind(this)}
        />
      </Fragment>
    )
  }
}

FreePostflopSims.contextTypes = {
  t: PropTypes.func
}


const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  game: store.game,
})

const mapDispatchToProps = {
  recieveFlopData: recieveFlopData,
  updateCurrentUserAttributesToReduxTokenAuth: updateCurrentUserAttributesToReduxTokenAuth,
  updateGameData: updateGameData,
  updateCardsConvertingTemplate
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(FreePostflopSims)