import React, { Component } from 'react'
import './UniversalNotesComponent.scss'
import PropTypes from 'prop-types'
import { CLEAR_ICON_TEXT_LIST, DEFAULT_UNIVERSAL_NOTE_ITEMS_PER_PAGE } from 'config/constants'
import { Button } from 'react-bootstrap'
import ReactPaginate from 'react-paginate'
import EditorComponent from 'components/MySim/EditorComponent/EditorComponent'

class UniversalNotesComponent extends Component {

  constructor(props) {
    super(props)

    this.state = {
      activePage: 0
    }

    this.handlePageClick = this.handlePageClick.bind(this)
  }

  handlePageClick = (pageData) => {
    this.setState({activePage: pageData.selected})
  };


  renderPaginate(pageCount) {
    return (
      <ReactPaginate
        previousLabel={'previous'}
        nextLabel={'next'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        pageCount={pageCount}
        forcePage={this.state.activePage}
        marginPagesDisplayed={3}
        pageRangeDisplayed={5}
        onPageChange={this.handlePageClick}
        containerClassName={'pagination'}
        subContainerClassName={'pages pagination'}
        pageClassName={'page-item'}
        nextClassName={'page-item page-next text-uppercase'}
        previousClassName={'page-item page-previous text-uppercase'}
        activeClassName={'active'} />
    )
  }

  generateUniversalNoteList(notes) {
    let t = this.context.t

    return  notes.map((note) => {
      return (
        <div key={note.key} className='universal-note'>
          <EditorComponent
            readOnly={false}
            isUniversalNoteEditor={true}
            commentInfo={`notes_${note.key}`}
            value={note.value}
            placeholder={t('mysim.typing_note')}
            showClearIcon={!CLEAR_ICON_TEXT_LIST.includes(note.value)}
            toggleDialogForUniversalNotes={this.props.toggleDialogForUniversalNotes}
            onChange={(comment) => {this.props.handleChangeUniversalNotes(comment, `notes_${note.key}`)}}
          />
        </div>
      )
    })
  }

  render() {
    let t = this.context.t
    let defaultNote = this.props.universalNoteData.defaultNote
    let btnAddNoteClassName = !!defaultNote ? 'd-block' : 'd-none'

    let data = this.props.universalNoteData.notes
    const offset = this.state.activePage * DEFAULT_UNIVERSAL_NOTE_ITEMS_PER_PAGE;
    const currentPageData = data.slice(offset, offset + DEFAULT_UNIVERSAL_NOTE_ITEMS_PER_PAGE)
    const pageCount = Math.ceil(data.length / DEFAULT_UNIVERSAL_NOTE_ITEMS_PER_PAGE);

    const btnAddNote = offset === 0 ? 
          <Button className={btnAddNoteClassName} onClick={ () => this.props.handleAddUniversalNote(defaultNote, Date.now())}>
            Add Note 
          </Button>
        : <Button onClick={ () => this.handlePageClick({selected: 0})}>
            Add New Note 
          </Button>

    return (
      <div className='universal-content'>
        <div className='d-flex justify-content-end'>
          { btnAddNote }
        </div>
        { 
          offset === 0 &&
          <div className='universal-note'>
            <EditorComponent
              readOnly={false}
              isUniversalNoteEditor={true}
              commentInfo='defaultNote'
              value={defaultNote}
              placeholder={t('mysim.typing_note')}
              showClearIcon={!CLEAR_ICON_TEXT_LIST.includes(defaultNote)}
              toggleDialogForUniversalNotes={this.props.toggleDialogForUniversalNotes}
              onChange={(comment) => {this.props.handleChangeUniversalNotes(comment, 'defaultNote')}}
            />
          </div>
        }
        { this.generateUniversalNoteList(currentPageData) }
        { 
          pageCount > 1 &&
          <div className='pagination justify-content-center'>
            {this.renderPaginate(pageCount)}
          </div>
        }
      </div>
    )
  }
}

UniversalNotesComponent.contextTypes = {
  t: PropTypes.func
}

export default UniversalNotesComponent
