import React, { Component, Fragment } from 'react'
import './StreetComponent.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Button, Form, Row, Col, Collapse } from 'react-bootstrap'
import { CLEAR_ICON_TEXT_LIST, EMAILS_ABLE_TO_VIEW_BLACK_DESIGN, NOTES_POPUP_ROUND } from 'config/constants'
import UnSelectCardBlue from 'images/Cards/unselect_card.png'
import UnSelectCardOrange from 'images/Cards/unselect_card_orange.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleUp, faAngleDown } from '@fortawesome/free-solid-svg-icons'
import {isEmpty} from 'lodash'
import Card from 'components/Card/component'
import EditorComponent from 'components/MySim/EditorComponent/EditorComponent'

class StreetComponent extends Component {

  constructor(props) {
    super(props)
    this.state = {
      roundShowingList: this.showCollapseRounds()
    }

    this.handleCollapseRound = this.handleCollapseRound.bind(this)
  }

  showCollapseRounds() {
    const { streetViewData } = this.props
    const newList = Object.keys(streetViewData).filter(key => {
      const item = streetViewData[key]
      return !!item.note.length || !isEmpty(item.default) || !isEmpty(item.oop) || !isEmpty(item.ip)
    })
    return newList.concat('general')
  }

  renderFirstSelectCardDiv(listSelectedCard, round) {
    let selectCardDiv = round !== 'flop' && (
      round === 'general' ? this.renderSelectCardElement({...listSelectedCard, cardListLength: 2}) : this.renderSelectCardElement({...listSelectedCard, cardListLength: 1})
    )
    return selectCardDiv
  }

  renderSelectCardElement(listSelectedCard, title = '') {
    let { email } = this.props.currentUser;
    let newListSelectedCard = listSelectedCard.cardList.slice(0, listSelectedCard.cardListLength)
    let unSelectCard = EMAILS_ABLE_TO_VIEW_BLACK_DESIGN.includes(email) ? UnSelectCardOrange : UnSelectCardBlue;

    return (
      <Col sm='auto' className='cards-container'>
        <div className={`card-selection ${!!title ? 'have-title' : ''} d-flex justify-content-around ${listSelectedCard.cardListLength === 1 ? 'unique' : '' }`}>
          { !!title && <div className='title p-0'> {title.toUpperCase()} </div> }
          {
            newListSelectedCard.length ? newListSelectedCard.map((card, index) => {
              return <Card
                  key={index}
                  rank={card.slice(0, -1) || ''}
                  suit={card[card.length-1] || ''}
                  size='big'
                  miniSize={true}
                  index={index}
                />
            }) :
            <Fragment>
              <img src={unSelectCard} alt='card' className='card' />
              {listSelectedCard.cardListLength === 2 && <img src={unSelectCard} alt='card' className='card' /> }
            </Fragment>
          }
          <Button className='btn-add' onClick={(e) => this.props.handleShowSelectCardPopup(e, listSelectedCard, true)}> + </Button>
        </div>
      </Col>
    )
  }

  handleCollapseRound(round) {
    let newRoundShowingList = [...this.state.roundShowingList]
    if(newRoundShowingList.includes(round))
      newRoundShowingList = newRoundShowingList.filter( roundItem => roundItem !== round)
    else
      newRoundShowingList = [...newRoundShowingList, round]
    this.setState({ roundShowingList: newRoundShowingList })
  }

  render() {
    let t = this.context.t
    let { toggleDialog, streetViewData, handleChange, handleChangeNote } = this.props

    return (
      <div className='street-content'>
        {
          NOTES_POPUP_ROUND.map((round, idx) => {
            return (
              <div key={Math.random + idx}>
                {
                  !!idx && <div className='seperate'></div>
                }
                <Form.Group>
                  <Form.Label className='street-label'> 
                    {round.charAt(0).toUpperCase() + round.slice(1).toLowerCase()} 
                    <FontAwesomeIcon
                      icon={this.state.roundShowingList.includes(round) ? faAngleDown : faAngleUp}
                      className='angle-icon cursor-pointer'
                      onClick={ () => this.handleCollapseRound(round)}
                    />
                  </Form.Label>
                  <Collapse in={this.state.roundShowingList.includes(round)}>
                    <div className='mt-3'>
                      <Row className='note'>
                        <Col>
                          <EditorComponent
                            readOnly={false}
                            commentInfo={round}
                            value={streetViewData[round].note.trim()}
                            placeholder={t('mysim.typing_note')}
                            showClearIcon={!CLEAR_ICON_TEXT_LIST.includes(streetViewData[round].note.trim())}
                            toggleDialog={toggleDialog}
                            onChange={(comment) => {handleChangeNote(comment, round)}}
                          />
                        </Col>
                      </Row>
                      {
                        round !== 'flop' && streetViewData[round].default.map( (selectedCard, idx) => {
                          let listCardFocusing = {cardList: selectedCard.cards, info: round + '_default_' + idx};
                          return (
                            <Row className={'first-row'} key={listCardFocusing.info}>
                              {this.renderFirstSelectCardDiv(listCardFocusing, round)}
                              <Col >
                                <EditorComponent
                                  readOnly={false}
                                  commentInfo={listCardFocusing.info}
                                  cardsInfo={listCardFocusing.info}
                                  value={selectedCard.note.trim()}
                                  placeholder={t('mysim.typing_note')}
                                  showClearIcon={true}
                                  toggleDialog={toggleDialog}
                                  onChange={(comment) => {handleChange(comment, listCardFocusing.info)}}
                                />
                              </Col>
                            </Row>
                          )
                        })
                      }
                      {
                        round !== 'flop' &&
                        <Row>
                          {this.renderFirstSelectCardDiv({cardList: [],info: round + '_default_new'}, round)}
                          <Col>
                            <EditorComponent
                              readOnly={true}
                              value=''
                              placeholder={t('mysim_choose_card')}
                              showClearIcon={false}
                              toggleDialog={() => {}}
                              onChange={() => {}}
                            />
                          </Col>
                        </Row>
                      }
                      {
                        round !== 'general' &&
                        <Row className='row-second'>
                          <Col sm='6'>
                            {
                              streetViewData[round].oop.map( (listSelectedOopCard, idx) => {
                                let listCardFocusing = {
                                  cardList: listSelectedOopCard.cards,
                                  info: round + '_oop_' + idx,
                                  cardListLength: 2
                                }
                                return (
                                  <Row key={listCardFocusing.info} className='first-row have-title'>
                                    {this.renderSelectCardElement(listCardFocusing, 'OOP')}
                                    <Col>
                                      <EditorComponent
                                        readOnly={false}
                                        commentInfo={listCardFocusing.info}
                                        cardsInfo={listCardFocusing.info}
                                        value={listSelectedOopCard.note.trim()}
                                        placeholder={t('mysim.typing_note')}
                                        showClearIcon={true}
                                        toggleDialog={toggleDialog}
                                        onChange={(comment) => {handleChange(comment, listCardFocusing.info)}}
                                      />
                                    </Col>
                                  </Row>
                                )
                              })
                            }
                            <Row>
                              {this.renderSelectCardElement({cardList: [], info: round + '_oop_new', cardListLength: 2}, 'OOP')}
                              <Col>
                                <EditorComponent
                                  readOnly={true}
                                  value=''
                                  placeholder={t('mysim_choose_card')}
                                  showClearIcon={false}
                                  toggleDialog={() => {}}
                                  onChange={() => {}}
                                />
                              </Col>
                            </Row>
                          </Col>
                          <Col sm='6'>
                            {
                              streetViewData[round].ip.map( (listSelectedIpCard, idx) => {
                                let listCardFocusing = {
                                  cardList: listSelectedIpCard.cards,
                                  info: round + '_ip_' + idx,
                                  cardListLength: 2
                                }
                                return (
                                  <Row key={listCardFocusing.info} className='first-row have-title'>
                                    {this.renderSelectCardElement(listCardFocusing, 'IP')}
                                    <Col>
                                      <EditorComponent
                                        readOnly={false}
                                        commentInfo={listCardFocusing.info}
                                        value={listSelectedIpCard.note.trim()}
                                        placeholder={t('mysim.typing_note')}
                                        cardsInfo={listCardFocusing.info}
                                        showClearIcon={true}
                                        toggleDialog={toggleDialog}
                                        onChange={(comment) => {handleChange(comment, listCardFocusing.info)}}
                                      />
                                    </Col>
                                  </Row>
                                )
                              })
                            }
                            <Row>
                              {this.renderSelectCardElement({cardList: [], info: round + '_ip_new', cardListLength: 2}, 'IP')}
                              <Col>
                                <EditorComponent
                                  readOnly={true}
                                  value=''
                                  placeholder={t('mysim_choose_card')}
                                  showClearIcon={false}
                                  toggleDialog={() => {}}
                                  onChange={() => {}}
                                />
                              </Col>
                            </Row>
                          </Col>
                        </Row>
                      }
                    </div>
                  </Collapse>
                </Form.Group>
              </div>
            )
          })
        }
      </div>



    )
  }

}

StreetComponent.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes
})

export default connect(
  mapStoreToProps,
  null
)(StreetComponent)
