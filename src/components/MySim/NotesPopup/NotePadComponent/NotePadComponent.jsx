import React, { Component } from 'react'
import './NotePadComponent.scss'
import PropTypes from 'prop-types'
import { CLEAR_ICON_TEXT_LIST } from 'config/constants'
import EditorComponent from 'components/MySim/EditorComponent/EditorComponent'

class NotePadComponent extends Component {
  render() {
    let t = this.context.t

    return (
      <div className='notepad-content'>
        <EditorComponent
          readOnly={false}
          commentInfo='notepad'
          value={this.props.notepadViewData.note.trim()}
          placeholder={t('mysim.typing_note')}
          showClearIcon={!CLEAR_ICON_TEXT_LIST.includes(this.props.notepadViewData.note.trim())}
          toggleDialog={this.props.toggleDialog}
          onChange={(comment) => {this.props.handleChange(comment, 'notepad')}}
        />    
      </div>
      
    )
  }
}

NotePadComponent.contextTypes = {
  t: PropTypes.func
}

export default NotePadComponent
