import { INITIAL_NOTE_POPUP_DATA, SELECT_CARD_POPUP, NOTES_POPUP_ROUND } from 'config/constants'
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { cloneDeep, isEqual } from 'lodash'
import { Form, Collapse } from 'react-bootstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faWindowClose } from '@fortawesome/free-solid-svg-icons'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import './NotesPopup.scss'
import SelectCard from './SelectCard/SelectCard'
import NotePadComponent from './NotePadComponent/NotePadComponent'
import UniversalNotesComponent from './UniversalNotesComponent/UniversalNotesComponent'
import loadable from '@loadable/component'
const ConfirmDialog = loadable(() => import('components/Shared/ConfirmDialog/ConfirmDialog'))
const StreetComponent = loadable(() => import('./StreetComponent/StreetComponent'))

class NotesPopUp extends Component {

  constructor(props){
    super(props)

    this.state = {
      notes: this.getNotesData(),
      oldNotes: this.getNotesData(),
      offset: 0,
      openDiaglog: false,
      clearedTarget: '',
      clearedElement: {},
      viewShowing: 'street',
      selectCardPopup: {
        showing: false,
        style: {}
      },
      listCardFocusing: {
        cardList: [],
        cardListLength: 0,
        areChangedListCard: false
      },
      universalNoteData: this.getNotesData('universal'),
      oldUniversalNoteData: this.getNotesData('universal'),
      confirmDiaglogForUniversal: {
        openDiaglog: false,
        clearedElement: ' '
      }
    }

    this.listView = ['street', 'notepad', 'universal']
    this.notePopupContainerRef = React.createRef();
    this.notePopupRef = React.createRef();

    this.handleClickTabMenuButton = this.handleClickTabMenuButton.bind(this)
    this.handleSetShowSelectCardPopup = this.handleSetShowSelectCardPopup.bind(this)
    this.handleClickCard = this.handleClickCard.bind(this)
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleClickOkBtn = this.handleClickOkBtn.bind(this);
    this.handleChangeNote = this.handleChangeNote.bind(this);
    this.clearTextArea = this.clearTextArea.bind(this);
    this.clearCards = this.clearCards.bind(this);
    this.toggleDialog = this.toggleDialog.bind(this);
    this.handleSubmitStreetAndNoteView = this.handleSubmitStreetAndNoteView.bind(this)
    //Function for universal note 
    this.handleChangeUniversalNotes = this.handleChangeUniversalNotes.bind(this)
    this.handleAddUniversalNote = this.handleAddUniversalNote.bind(this)
    this.handleSubmitUniversalNotes = this.handleSubmitUniversalNotes.bind(this)
    this.toggleDialogForUniversalNotes = this.toggleDialogForUniversalNotes.bind(this)
  }

  componentDidMount() {
    const { default_note: defaultNote, notes } = JSON.parse(this.props.currentUser.universal_note.note_data)
    const newUniversalNoteData = { defaultNote, notes }
    this.setState({ universalNoteData: cloneDeep(newUniversalNoteData), oldUniversalNoteData: cloneDeep(newUniversalNoteData) })
  }

  getNotesData(view) {
    if(view === 'universal') return { defaultNote: '', notes: [] }
    const notesData = this.props.sim.notes || INITIAL_NOTE_POPUP_DATA

    return cloneDeep(notesData)
  }

  handleScroll(e) {
    e.preventDefault()
    this.setState({offset: this.notePopupContainerRef.current.scrollTop})
  }

  renderNoteContent() {
    const {viewShowing, notes: {streetView: streetViewData, notepadView: notepadViewData}, universalNoteData} = this.state

    return (
      <Form className='popup-content' >
      { 
        viewShowing === 'street' &&
        <StreetComponent 
          streetViewData={streetViewData}
          handleShowSelectCardPopup={this.handleSetShowSelectCardPopup}
          toggleDialog={this.toggleDialog}
          handleChangeNote={this.handleChangeNote}
          handleChange={this.handleChange}
        />
      }
      {
        viewShowing === 'notepad' &&
        <NotePadComponent 
          notepadViewData={notepadViewData} 
          toggleDialog={this.toggleDialog}
          handleChange={this.handleChange}
        />
      }
      {
        viewShowing === 'universal' &&
          <UniversalNotesComponent 
            universalNoteData={universalNoteData}
            handleChangeUniversalNotes={this.handleChangeUniversalNotes}
            handleAddUniversalNote={this.handleAddUniversalNote}
            handleSubmitUniversalNotes={this.handleSubmitUniversalNotes}
            toggleDialogForUniversalNotes={this.toggleDialogForUniversalNotes}
          />          
      }
      </Form>
    )
  }

  handleSubmitStreetAndNoteView() {
    const { notes, oldNotes } = this.state
    if(isEqual(notes, oldNotes)) return

    const updateData = {notes, id: this.props.sim.id}
    window.axios.put(`/my_sims/${this.props.sim.id}`, updateData).then(() => {
      if(this.props.loadSimsInfoFromServer) {
        this.props.loadSimsInfoFromServer();
      }
    })
    sessionStorage.setItem("user_strategy_selection", JSON.stringify(updateData))
  }

  handleSubmit() {
    this.handleSubmitStreetAndNoteView()
    this.handleSubmitUniversalNotes()
    this.props.setShowNotesPopup()
  }

  handleChange(comment, name) {
    const newNoteState = {...this.state.notes}
    switch(name) {
      case 'notepad':
        newNoteState.notepadView.note = comment
        break
      default:
        const infoKeyArray = name.split('_')
        const round = infoKeyArray[0]
        const firstKey = infoKeyArray[1]
        const secondKey = infoKeyArray[2]
        newNoteState.streetView[round][firstKey][secondKey] = {...newNoteState.streetView[round][firstKey][secondKey], note: comment}
        break
    }
    this.setState({notes: newNoteState});
  }

  handleClickTabMenuButton(view) {
    if(this.state.viewShowing !== view) {
      this.setState({viewShowing: view});
    }
  }

  handleSetShowSelectCardPopup(e, listCardFocusingParams, isButtonAddClick = false) {
    const { selectCardPopup, listCardFocusing: listCardFocusingState, offset } = this.state
    if(selectCardPopup.showing && isButtonAddClick) return

    if(!selectCardPopup.showing) {
      const newListCarForcusing = {...listCardFocusingState, ...listCardFocusingParams}
      const buttonStyle = e.currentTarget.getBoundingClientRect()
      const buttonHalfHeight = buttonStyle.height / 2;
      const clientViewPortWidth = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
      const clientViewPortHeight = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0)
      const halfViewPortHeightTop = offset + clientViewPortHeight / 2
      const newSelectCardPopupStyle = {
        top: buttonStyle.top + offset < halfViewPortHeightTop ?
          buttonStyle.top + offset + buttonHalfHeight + 3 :
          buttonStyle.top + offset - SELECT_CARD_POPUP.height - buttonHalfHeight - 3,
        left: buttonStyle.left < (clientViewPortWidth / 2) ? SELECT_CARD_POPUP.left_position_of_left_pop_up : SELECT_CARD_POPUP.left_position_of_right_pop_up }
      this.setState({
        selectCardPopup: { showing: true, style: newSelectCardPopupStyle },
        listCardFocusing: newListCarForcusing,
      })
    }
    else { this.setState({ selectCardPopup: {...selectCardPopup, showing: false} }) }
  }

  handleClickOkBtn() {
    const { listCardFocusing, notes: notesState, selectCardPopup: selectCardPopupState } = this.state
    const { info, cardList, cardListLength } = listCardFocusing
    if(cardList.length !== cardListLength) return

    const infoKeyArray = info.split('_')
    const round = infoKeyArray[0]
    const firstKey = infoKeyArray[1]
    const secondKey = infoKeyArray[2]
    const newListSelectedCard = {...notesState.streetView}

    if(secondKey === 'new') {
      if(cardList.length === cardListLength) {
        newListSelectedCard[round][firstKey].push({cards: cardList, note: ''})
        this.setState({
          selectCardPopup: {...selectCardPopupState, showing: false},
          notes: {...notesState, streetView: newListSelectedCard}
        })
      }
    } else {
      newListSelectedCard[round][firstKey][secondKey] = {...newListSelectedCard[round][firstKey][secondKey], cards: cardList}

      this.setState({
        selectCardPopup: {...selectCardPopupState, showing: false},
        notes: {...notesState, streetView: newListSelectedCard},
        listCardFocusing: {...listCardFocusing, cardList: cardList}
      })
    }
  }

  handleClickCard(cardValue) {
    let { cardList: newCardList, cardListLength } = this.state.listCardFocusing

    if(newCardList.indexOf(cardValue) !== -1) {
      newCardList = newCardList.filter(value => value !== cardValue)
    } else {
      if(newCardList.length === cardListLength) {
        newCardList.shift()
      }
      newCardList.push(cardValue)
    }

    this.setState({listCardFocusing: {...this.state.listCardFocusing, cardList: newCardList}})
  }

  toggleDialog(value, clearedTarget='', clearedElement='') {
    this.setState({
      openDiaglog: value,
      clearedTarget: clearedTarget,
      clearedElement: clearedElement,
    });
  }

  handleChangeNote(comment, round) {
    const newNoteState = {...this.state.notes}
    newNoteState.streetView[round].note = comment

    this.setState({notes: newNoteState})
  }

  clearTextArea(clearedElement) {
    if(NOTES_POPUP_ROUND.includes(clearedElement)) {
      this.handleChangeNote('', clearedElement);
    } else {
      this.handleChange('', clearedElement);
    }

    this.setState({ openDiaglog: false });
  }

  clearCards(info) {
    const infoKeyArray = info.split('_')
    const round = infoKeyArray[0]
    const firstKey = infoKeyArray[1]
    const secondKey = infoKeyArray[2]

    const newListSelectedCard = {...this.state.notes.streetView}
    newListSelectedCard[round][firstKey].splice(secondKey, 1);

    this.setState({
      openDiaglog: false,
      notes: {...this.state.notes, streetView: newListSelectedCard},
    })
  }

  confirmDialog() {
    const t = this.context.t
    const { clearedElement, clearedTarget, openDiaglog } = this.state
    const onClickYes = clearedTarget === 'note' ?
      () => { this.clearTextArea(clearedElement) } :
      () => { this.clearCards(clearedElement); }

    return (
      <ConfirmDialog
        isShown={openDiaglog}
        agreeBtnText={t('confirm_diaglog.yes')}
        denyBtnText={t('confirm_diaglog.no')}
        dialogMessage={t('mysim.delete_note', { target: clearedTarget })}
        handleClose={() => {this.toggleDialog(false)}}
        handleAgree={onClickYes}
      />
    )
  }

  //Start section universal note

  handleChangeUniversalNotes(comment, name) {
    const noteType = name.split('_')[0]
    const newUniversalNoteDataState = {...this.state.universalNoteData}
    if(noteType === 'defaultNote') {
      newUniversalNoteDataState.defaultNote = comment
    }
    else {
      const noteKey = name.split('_')[1]
      const noteIndex = newUniversalNoteDataState.notes.findIndex( note => note.key === parseFloat(noteKey) )
      newUniversalNoteDataState.notes[noteIndex] = { ...newUniversalNoteDataState.notes[noteIndex], value: comment }
    }
    this.setState({universalNoteData: newUniversalNoteDataState})
  }

  handleAddUniversalNote(noteValue, noteKey) {
    const { universalNoteData } = this.state
    const isNoteHaveText = !!universalNoteData.defaultNote
    if(isNoteHaveText) {
      let newUniversalNoteDataState = {...universalNoteData}
      newUniversalNoteDataState = {
        notes: [ {value: noteValue, key: noteKey}, ...newUniversalNoteDataState.notes ],
        defaultNote: ''
      }
      this.setState({universalNoteData: newUniversalNoteDataState})
    }
  }

  handleSubmitUniversalNotes() {
    const { universalNoteData, oldUniversalNoteData } = this.state

    if(isEqual(universalNoteData, oldUniversalNoteData)) return
    const noteDataParams = {default_note: universalNoteData.defaultNote, notes: universalNoteData.notes }
    window.axios.post(`/universal_notes/update_data`, {note_data: noteDataParams})
    .then((res) => {
      this.props.updateCurrentUserAttributesToReduxTokenAuth(res.data.user)
    })
  }

  toggleDialogForUniversalNotes(value, clearedElement='') {
    this.setState({
      confirmDiaglogForUniversal: {
        openDiaglog: value,
        clearedElement: clearedElement,
      }
    });
  }

  clearTextDefaultNote(clearedElement) {
    this.handleChangeUniversalNotes('', clearedElement)
    this.setState({confirmDiaglogForUniversal: {...this.state.confirmDiaglogForUniversal, openDiaglog: false} })
  }

  removeTextBoxNote(clearedElement){
    const { universalNoteData, confirmDiaglogForUniversal } = this.state
    const newUniversalNoteDataState = {...universalNoteData}
    const noteKey = clearedElement.split('_')[1]
    const noteIndex = newUniversalNoteDataState.notes.findIndex( note => note.key === parseFloat(noteKey) )
    newUniversalNoteDataState.notes.splice(noteIndex, 1)
    this.setState({
      universalNoteData: newUniversalNoteDataState,
      confirmDiaglogForUniversal: {...confirmDiaglogForUniversal, openDiaglog: false} 
    })
  }

  confirmDialogForUniversalNotes() {
    const t = this.context.t
    const { clearedElement, openDiaglog } = this.state.confirmDiaglogForUniversal
    let onClickYes = clearedElement === 'defaultNote' ?
      () => { this.clearTextDefaultNote(clearedElement) } :
      () => { this.removeTextBoxNote(clearedElement); }

    return (
      <ConfirmDialog
        isShown={openDiaglog}
        agreeBtnText={t('confirm_diaglog.yes')}
        denyBtnText={t('confirm_diaglog.no')}
        dialogMessage={t('mysim.delete_note', { target: 'note'  })}
        handleClose={() => {this.toggleDialogForUniversalNotes(false)}}
        handleAgree={onClickYes}
      />
    )
  }
  //End section universal note

  render() {
    const t = this.context.t
    const { selectCardPopup, viewShowing, listCardFocusing } = this.state

    return (
      <Fragment>
        <div ref={this.notePopupContainerRef} onScroll={this.handleScroll.bind(this)} className={`${!!this.props.show ? 'show' : '' } notes-popup-container hide-scrollbar`}>
          <div ref={this.notePopupRef} className={`note-popup fadeIn ${selectCardPopup.showing ? 'no-scroll' : ''}`}>
            <div className='title d-flex align-items-center'>
              {t('mysim.my_notes')}
              <div className='tab-menu'>
                {
                  this.listView.map( view => {
                    const tabName = view === 'universal' ? 'Universal' : view.charAt(0).toUpperCase() + view.slice(1).toLowerCase() + ' view'
                    return (
                      <div key={view} className={`btn view-button mr-3 ${viewShowing === view ? 'actived' : '' }`}
                              onClick={(e) => this.handleClickTabMenuButton(view)}>
                        {tabName}
                      </div>
                    )
                  })
                }
              </div>
            </div>
            {this.renderNoteContent()}
            <FontAwesomeIcon className='btn-close' icon={faWindowClose} onClick={this.handleSubmit} />
            <Collapse in={selectCardPopup.showing} style={selectCardPopup.style} className='select-card-container'>
              <div className='select-card-popup'>
                <SelectCard
                  handleSetShowSelectCardPopup={this.handleSetShowSelectCardPopup}
                  listCardFocusing={listCardFocusing}
                  handleClickCard={this.handleClickCard}
                  handleClickOkBtn={this.handleClickOkBtn}
                />
              </div>
            </Collapse>
          </div>
          <div className='popup-overlay' onClick={this.handleSubmit}>  </div>
        </div>
        {this.confirmDialog()}
        {this.confirmDialogForUniversalNotes()}
      </Fragment>
    );
  }
}

NotesPopUp.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes
})

const mapDispatchToProps = {
  updateCurrentUserAttributesToReduxTokenAuth
}
export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(NotesPopUp)
