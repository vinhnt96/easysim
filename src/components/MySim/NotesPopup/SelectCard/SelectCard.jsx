import React, {Component} from 'react'
import './SelectCard.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { CARD_OPTIONS, SUITED_CARD_NUMBER } from 'config/constants'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import {chunk} from 'lodash'
import Card from 'components/Card/component'
import CustomSelect from 'components/StrategySelectionPage/CustomSelect/component'

class CSelectCard extends Component {

  cardOptionElements() {
    let cardOptionsArray = chunk(CARD_OPTIONS, SUITED_CARD_NUMBER);

    return cardOptionsArray.map((suitedCards, index) => {
      return (
        <div key={index} className='d-flex justify-content-center'>
          {
            suitedCards.map((card, index) => {
              let value =`${card.rank}${card.suit}`;
              let additionalClass = this.props.listCardFocusing.cardList.includes(value) ? 'hightlighted-card' : '';

              let label = <Card
                key={index}
                rank={card.rank}
                suit={card.suit}
                className={`text-center ${additionalClass}`}
                size='small'
              />

              return <CustomSelect
                key={index}
                type='checkbox'
                name='select_card[]'
                label={label}
                value={value}
                handleOnClick={ (e) => this.props.handleClickCard(value)}
              />
            })
          }
        </div>
      )
    })
  }

  render(){
    let { listCardFocusing, handleSetShowSelectCardPopup, handleClickOkBtn } = this.props;
    let disabledClass = listCardFocusing.cardList.length === listCardFocusing.cardListLength ? '' : 'disabled'
    let t = this.context.t
    return (
      <div className='select-card'>
        <div className='head font-size-16 font-weight-600'>
          {t('select_card')}
        </div>
        <div className='body'>
          {this.cardOptionElements()}
        </div>
        <FontAwesomeIcon className='btn-close' icon={faTimes} onClick={handleSetShowSelectCardPopup}/>
        <div className='text-center mb-3'>
          <div
            className={`btn btn-primary ${disabledClass}`}
            onClick={handleClickOkBtn}
          >
            OK
          </div>
        </div>
      </div>
    )
  }
}

CSelectCard.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState
})

export default connect(
  mapStoreToProps,
  null
)(CSelectCard)
