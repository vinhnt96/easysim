import { DEFAULT_SIM_ITEM_PER_PAGE, DELETE_SIM_TYPE } from 'config/constants'
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import ReactPaginate from 'react-paginate'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { checkFreeTrialUser } from 'utils/utils'
import '../../custom_style.scss'
import './stylesheet.scss'
import { Button } from 'react-bootstrap'
import CustomCheckbox from 'components/CustomCheckbox/CustomCheckbox'
import loadable from '@loadable/component'
const NotesPopup = loadable(() => import('./NotesPopup/NotesPopup'))
const SimsInfoList = loadable(() => import('./SimInfoList/SimInfoList'))
const NotifyModal = loadable(() => import('components/NotifyModal/component'))

class CMySim extends Component {

  constructor(props) {
    super(props)

    this.state = {
      simsData: [],
      currentSim: {id: null},
      activePage: 1,
      totalPages: 0,
      sortColumn: '',
      isDefaultDirection: true,
      searchValue: '',
      searchData: [],
      isShowPopup: false,
      showNotifyModal: false,
      deleteSimState: {
        isShowing: false,
        deleteType: DELETE_SIM_TYPE[0],
        simIdSelectedList: []
      }
    }

    this.handleClickColumnHeader = this.handleClickColumnHeader.bind(this)
    this.handleOnChangeSearchValue = this.handleOnChangeSearchValue.bind(this)
    this.setShowNotesPopup = this.setShowNotesPopup.bind(this)
    this.loadSimsInfoFromServer = this.loadSimsInfoFromServer.bind(this)
    this.setShowNotifyModal = this.setShowNotifyModal.bind(this)
    this.handleShowDeleteSimView = this.handleShowDeleteSimView.bind(this)
    this.handleCheckedDeleteAllSim = this.handleCheckedDeleteAllSim.bind(this)
    this.handleCheckedDeleteSim = this.handleCheckedDeleteSim.bind(this)
    this.handleConfirmDeleteSim = this.handleConfirmDeleteSim.bind(this)
  }

  componentDidMount() {
    if(this.props.currentUser){
      this.loadSimsInfoFromServer()
    }
  }

  loadSimsInfoFromServer(){
    const { sortColumn, isDefaultDirection, activePage, searchValue } = this.state
    const sortParam = `${sortColumn}_${isDefaultDirection ? 'desc' : 'asc'}`
    const params = {
      page: activePage,
      per_page: DEFAULT_SIM_ITEM_PER_PAGE,
      search: searchValue,
      sort_column: sortParam
    }

    window.axios.get(`/my_sims`, {params}).then(res => {
      const { page, pages, data } = res
      this.setState({ simsData: data.user_strategy_selections, activePage: page, totalPages: pages })
    })
    .catch(err => console.log(err))
  }

  handleClickColumnHeader(column){
    const { sortColumn, isDefaultDirection: isDefaultDirectionState } = this.state
    const newState = sortColumn === column ? {isDefaultDirection: !isDefaultDirectionState}
                                           : {sortColumn: column, isDefaultDirection: true}
    this.setState(newState, () => {
      this.loadSimsInfoFromServer()
    })
  }

  handlePageClick = (pageData) => {
    this.setState({activePage: parseInt(pageData.selected + 1)}, () => {
      this.loadSimsInfoFromServer()
    })
  }

  handleOnChangeSearchValue(e){
    this.setState({searchValue: e.target.value })
  }

  setShowNotesPopup(sim) {
    const newState = {
      ...this.state,
      currentSim: sim || {},
      isShowPopup: !this.state.isShowPopup
    }

    this.setState(newState)
  }

  setShowNotifyModal(value=false) {
    this.setState({
      showNotifyModal: value
    })
  }

  renderPaginate() {
    const { totalPages } = this.state
    if(totalPages < 2)
    return <Fragment> </Fragment>

    return (
      <ReactPaginate
        previousLabel={'previous'}
        nextLabel={'next'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        pageCount={totalPages}
        marginPagesDisplayed={3}
        pageRangeDisplayed={5}
        onPageChange={this.handlePageClick}
        containerClassName={'pagination'}
        subContainerClassName={'pages pagination'}
        pageClassName={'page-item'}
        nextClassName={'page-item page-next text-uppercase'}
        previousClassName={'page-item page-previous text-uppercase'}
        activeClassName={'active'} />
    )
  }

  handleShowDeleteSimView() {
    this.setState({ deleteSimState: {...this.state.deleteSimState, isShowing: !this.state.deleteSimState.isShowing} })
  }

  renderDeleteSimButtonElement() {
    const deleteButtonElement = this.state.simsData.length ? <Button variant='danger' className='delete-btn' onClick={this.handleShowDeleteSimView}> Delete </Button>
                                                  : <Fragment></Fragment>
                                                  
    return this.state.deleteSimState.isShowing ? <div className='action-btn-group'>
      <Button variant='success' className='confirm-btn' onClick={this.handleConfirmDeleteSim}> Confirm </Button>
      <Button variant='danger' className='cancel-btn' onClick={this.handleShowDeleteSimView}> Cancel </Button>
    </div>
      : deleteButtonElement
  }

  handleCheckedDeleteAllSim(e, data) {
    const deleteTypeValue = e.currentTarget.checked ? DELETE_SIM_TYPE[1] : DELETE_SIM_TYPE[0]
    const newSimIdSelectedList = e.currentTarget.checked ? this.state.simsData.map( sim => sim.id) : []
    this.setState({ deleteSimState: {...this.state.deleteSimState, deleteType: deleteTypeValue, simIdSelectedList: newSimIdSelectedList} })
  }

  handleCheckedDeleteSim(e, id){
    const { simIdSelectedList } = this.state.deleteSimState
    const newSimIdSelectedList = e.currentTarget.checked ? [...simIdSelectedList, id] : simIdSelectedList.filter( simId => simId !== id)
    this.setState({ deleteSimState: {...this.state.deleteSimState, simIdSelectedList: newSimIdSelectedList, deleteType: DELETE_SIM_TYPE[0] }})
  }

  handleConfirmDeleteSim(){
    const { deleteType, simIdSelectedList } = this.state.deleteSimState
    const params = { delete_type: deleteType, id_list: simIdSelectedList}
    window.axios.post('/my_sims/delete_sims', params).then( res => {
      this.setState({
        deleteSimState: {
          isShowing: false,
          deleteType: DELETE_SIM_TYPE[0],
          simIdSelectedList: []
        }
      }, () => this.loadSimsInfoFromServer())
    }).catch(err => console.error(err))
  }

  render(){
    const t = this.context.t
    const { currentUser, history } = this.props
    const { sortColumn, simsData, currentSim, isShowPopup, showNotifyModal, isDefaultDirection, deleteSimState } = this.state
    const { isShowing: showDeleteSimView, deleteType, simIdSelectedList } = deleteSimState
    const columnHeader = [
      t('mysim.board'), t('mysim.positions'), t('mysim.stack_size'),t('mysim.pot_type'),t('mysim.players'),
      t('mysim.game_type'),t('mysim.notes'),t('mysim.last_date_view')
    ]

    return (
      <Fragment>
        <div className='my-sim-container'>
          <div className='header d-flex align-items-center'>
            {checkFreeTrialUser(currentUser.email) ? t('free_trial.mysim') : t('mysim')}
          </div>
          <div className='form-search'>
            <div className='form-inline form-group search-input row'>
              <input className='form-control col-10 input-search' type='text' value={this.state.searchValue}
                    onChange={this.handleOnChangeSearchValue} placeholder='Search for Sim' />
              <FontAwesomeIcon icon={faSearch} className='col-2' onClick={() => this.loadSimsInfoFromServer()}/>
            </div>
            {this.renderDeleteSimButtonElement()}
          </div>
          <div className='content-body'>
            <div className='table-container'>
              <table className='my-sim-table w-100 text-center'>
                <thead>
                  <tr>
                    {
                      showDeleteSimView &&
                      <th className='delete-column'>
                        <CustomCheckbox 
                          data={`delete-all`}
                          handleChecked={this.handleCheckedDeleteAllSim}
                          checked={deleteType === DELETE_SIM_TYPE[1]}
                        />
                      </th>
                    }
                    {
                      columnHeader.map( (header,idx) => {
                        const sort = header.toLowerCase().replaceAll(' ', '_')
                        return (!checkFreeTrialUser(currentUser.email) || header !== t('mysim.last_date_view')) &&
                          <th key = {idx + header}
                              onClick={() => this.handleClickColumnHeader(sort)}
                              className={`${sortColumn === sort ? (isDefaultDirection ? 'active sorted-desc' : 'active sorted-asc') : ''} no-select `}>
                            <span> {header} </span>
                          </th>
                    })}
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <SimsInfoList
                    data={simsData}
                    history={history}
                    showDeleteSimView={showDeleteSimView}
                    deleteSimType={deleteType}
                    simIdSelectedList={simIdSelectedList}
                    setShowNotesPopup={this.setShowNotesPopup}
                    setShowNotifyModal={this.setShowNotifyModal}
                    handleCheckedDeleteSim={this.handleCheckedDeleteSim}
                    />
                </tbody>
              </table>
            </div>
            <div className='pagination justify-content-center'>
              {this.renderPaginate()}
            </div>
            <NotesPopup
              key={currentSim.id}
              sim={currentSim}
              show={isShowPopup}
              setShowNotesPopup={this.setShowNotesPopup}
              loadSimsInfoFromServer={this.loadSimsInfoFromServer}
            />
          </div>
        </div>
        <NotifyModal
          modalShow={showNotifyModal}
          infomation={{title: 'Action not available', message: t('notify_modal.can_not_open_more_than_1_sim_message')}}
          handleOnClose={e => this.setShowNotifyModal()}
          />
      </Fragment>

    )
  }

}

CMySim.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes
})

const mapDispatchToProps = (dispatch) => ({

})

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CMySim)
