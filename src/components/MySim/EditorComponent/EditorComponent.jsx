import React, { Component, Fragment } from 'react'
import './EditorComponent.scss'
import { Editor } from "react-draft-wysiwyg";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";
import { EditorState, ContentState, convertToRaw } from 'draft-js';
import htmlToDraft from 'html-to-draftjs';
import { formatHTMLText } from 'utils/utils.js'

const toolbarConfig = {
  options: ['inline', 'list', 'colorPicker'],
  inline: {
    options: ['bold', 'italic', 'underline'],
  },
  list: {
    options: ['unordered', 'ordered'],
  },
  colorPicker: {
    popupClassName: 'color-picker-popup',
  }
}

export default class EditorComponent extends Component {

  constructor(props) {
    super(props);

    let blocksFromHtml = htmlToDraft(formatHTMLText(this.props.value) || '');
    let { contentBlocks, entityMap } = blocksFromHtml;

    this.state = {
      showToolbar: false,
      editorState: EditorState.createWithContent(
        ContentState.createFromBlockArray(contentBlocks, entityMap)
      ),
    };

    this.onEditorStateChange = this.onEditorStateChange.bind(this);
    this.toggleToolbar = this.toggleToolbar.bind(this);
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if(this.state.editorKey !==  prevState.editorKey){
      this.editorReferece.focus()
    }

    // update editorState when remove all text by clear icon
    if(this.props.value === '' && prevProps.value !== '') {
      let blocksFromHtml = htmlToDraft(this.props.value || '');
      let { contentBlocks, entityMap } = blocksFromHtml;

      let newState =
      {
        ...this.state,
        editorKey: Date.now(),
        editorState: EditorState.createWithContent(
          ContentState.createFromBlockArray(contentBlocks, entityMap)
        )
      }

      this.setState(newState);
    }
  }

  onEditorStateChange(editorState) {
    let { onChange } = this.props;
    import('draftjs-to-html').then(draftToHtml => {
      let htmlText = draftToHtml.default(convertToRaw(editorState.getCurrentContent()));

      onChange(formatHTMLText(htmlText));

      this.setState({
        editorState: editorState
      });
    })
   
  };

  toggleToolbar(isShown) {
    this.setState({ showToolbar: isShown });
  }

  setEditorReference = (ref) => {
    this.editorReferece = ref;
  }

  handToggleDialog() {
    let {toggleDialog, toggleDialogForUniversalNotes, value, commentInfo, cardsInfo, isUniversalNoteEditor} = this.props
    let clearedTarget = value === '' ? 'card(s)' : 'note';
    let clearedElement = value === '' ? cardsInfo : commentInfo;
    if(isUniversalNoteEditor) {
      clearedElement = commentInfo
    }
    return isUniversalNoteEditor ? toggleDialogForUniversalNotes(true, clearedElement) 
                                  : toggleDialog(true, clearedTarget, clearedElement)
  }

  render(){
    let { value, readOnly, placeholder, showClearIcon } = this.props;
    let { editorState } = this.state;

    return (
      <Fragment>
        <Editor
          editorRef={this.setEditorReference}
          key={this.state.editorKey}
          toolbarHidden={!this.state.showToolbar}
          readOnly={readOnly}
          placeholder={['', "<p></p>"].includes(value.trim()) ? placeholder : '' }
          onFocus={() => { this.toggleToolbar(true) }}
          onBlur={() => { this.toggleToolbar(false) }}
          editorState={editorState}
          onEditorStateChange={this.onEditorStateChange}
          toolbar={toolbarConfig}
          toolbarClassName="comment-toolbar"
          wrapperClassName="comment-wrapper overflow-visible"
          editorClassName="comment-editor"
        />
        {
          showClearIcon &&
            <div className='clear-icon' onClick={(e) => this.handToggleDialog()}>×</div>
        }
      </Fragment>
    )
  }
}
