import React, { Component } from 'react'
import '../stylesheet.scss'
import '../../../custom_style.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Card from '../../Card/component'
import Notes from '../../../images/notes.png'
import Noted from '../../../images/noted.png'
import { convertFlopCards, buildConvertSuitsTemplate } from 'services/convert_cards_services'
import { handleOnOpeningNewSim } from '../../../actions/game.actions'
import { Button } from 'react-bootstrap'
import { isEmpty, pick, isEqual} from 'lodash'
import { positionsForDisplayBuilder, alternativePositionsForDisplayBuilder } from 'services/game_play_service'
import { toast } from 'react-toastify'
import { notify, checkFreeTrialUser, objectRequestSims } from 'utils/utils'
import { Spinner } from 'react-bootstrap'
import CustomCheckbox from 'components/CustomCheckbox/CustomCheckbox'
import { DELETE_SIM_TYPE } from 'config/constants'

class SimsInfoList extends Component {

  constructor(props) {
    super(props)
    this.state = { loading: false, index: 0 }
  }

  cardsEles(cards) {
    const isBlankCard = cards.every( card => !card )
    if(isBlankCard) return this.context.t('preflop_sim_title')

    return cards.map((card, index) =>
        <Card
          key={index}
          rank={card.slice(0, -1) || ''}
          suit={card[card.length-1] || ''}
          className='board-card'
          size='small'
          index={index}
        />
    );
  }

  getStrategySelection(data, simInfo) {
    const { currentUser, history, handleOnOpeningNewSim } = this.props
    const { strategy_selection, notes } = data
    const { flop_cards } = strategy_selection
    const cardsConvertingTemplate = buildConvertSuitsTemplate(flop_cards)
    const convertedFlopCards = convertFlopCards(flop_cards, cardsConvertingTemplate)
    const dataStrategySelection = {
      ...strategy_selection,
      convertedFlopCards,
      notes
    }
    const object = objectRequestSims()
    const number = object[simInfo.id] || 0
    object[simInfo.id] = isEmpty(object) ? 1 : number + 1
    localStorage.setItem("requests", JSON.stringify(object));
    sessionStorage.setItem("strategy_selections", JSON.stringify(dataStrategySelection))
    sessionStorage.setItem("user_strategy_selection", JSON.stringify(pick(simInfo, ['id', 'notes'])));
    handleOnOpeningNewSim(cardsConvertingTemplate, dataStrategySelection, {...currentUser, strategy_selection: {...strategy_selection}})
    history.push({pathname: '/preflop-page', fromSimInfo: true})
  }

  handleOpenSimClick(simInfo, idx) {
    this.setState({ loading: true, index: idx })
    this.openSim(simInfo)
  }

  openSim(simInfo) {
    window.axios.get(`/strategy_selections/get_strategy_selection?user_strategy_selection_id=${simInfo.id}`).then(res => {
      this.getStrategySelection(res.data, simInfo)
    }).catch(errors => {
      this.setState({ loading: false, index: 0 })
      notify("Something went wrong", toast.error);
    })
  }

  isNoted(simInfo) {
    for( let view in simInfo.notes ) {
      for( let round in simInfo.notes[view]) {
        for (let key in simInfo.notes[view][round]) {
          if(simInfo.notes[view][round][key].length !== 0) return true
        }
      }
    }
    return false
  }

  disabledSim(gameType) {
    const value = gameType === 'mtt' ? 'mtts' : gameType
    const { admin_access, accessable_game_type } = this.props.currentUser
    if (admin_access === 'cash-mtts')
      return false
    if (!isEmpty(admin_access) && admin_access !== 'free')
      return admin_access !== value
    if (accessable_game_type === 'cash-mtts')
      return false
    return accessable_game_type !== value
  }

  render() {
    const t = this.context.t
    const { loading, index } = this.state
    const { data: simsData, currentUser, justRead, showDeleteSimView, deleteSimType, simIdSelectedList, handleCheckedDeleteSim } = this.props
    const { preferences, email } = currentUser
    const displayPositionsByDefault = preferences.display_positions_by_default || false
    return  simsData.map( (sim_info, idx) => {
      const { id, date_last_view, strategy_selection } = sim_info
      const { flop_cards , game_type, sim_type, stack_size, positions, specs } = strategy_selection
      const isNoted = this.isNoted(sim_info)
      const dateViewed = new Date(date_last_view).toISOString().split('T')[0];
      const positionsDisplay = displayPositionsByDefault ? positionsForDisplayBuilder(positions) : alternativePositionsForDisplayBuilder(positions)
      const checked = justRead ? false : ( deleteSimType === DELETE_SIM_TYPE[1] || simIdSelectedList.includes(id) )
      const disabled = this.disabledSim(game_type)
      return (
        <tr key={idx + Math.random()}>
          {
            showDeleteSimView &&
            <td className='delete-column'>
              <CustomCheckbox 
                data={id}
                handleChecked={handleCheckedDeleteSim}
                checked={checked}
              />
            </td>
          }
          <td>
            <div className='d-flex justify-content-center flex-grow-1'>
              {this.cardsEles(flop_cards)}
            </div>
          </td>
          <td>{positionsDisplay}</td>
          <td> {stack_size} </td>
          <td className='text-uppercase'> {sim_type} </td>
          <td className='text-uppercase'> { isEqual(game_type, 'mtt') ? '8MAX' : specs } </td>
          <td className='text-uppercase'> {game_type} </td>
          <td>
              <Button variant="default" disabled={justRead} onClick={() => this.props.setShowNotesPopup(sim_info)} >
                <img src={isNoted ? Noted : Notes} alt={` ${isNoted ? 'has been note' : 'no note for this'}`}/>
              </Button>
          </td>
          {
            !checkFreeTrialUser(email) &&
              <td>{dateViewed}</td>
          }
          {
            !justRead && !disabled ? 
              (<td className='open-sim-btn'>
                <Button
                  onClick={() => this.handleOpenSimClick(sim_info, idx)}
                  disabled={loading}
                >
                  { loading && index === idx ? <Spinner animation="border" size="sm" /> : t('mysim.open_sim') }
                </Button>
              </td>)
            : (<td></td>)
          }
        </tr>
      )
    })
  }

}

SimsInfoList.contextTypes = {
  t: PropTypes.func
}

SimsInfoList.defaultProps = {
  justRead: false
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  cardsConvertingTemplate: store.cardsConverting.cardsConvertingTemplate
})

const mapDispatchToProps = {
  handleOnOpeningNewSim
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(SimsInfoList)
