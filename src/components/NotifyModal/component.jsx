import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from 'react-bootstrap/Modal'
import './stylesheet.scss'

class CNotifyModal extends Component {

  render() {
    const { modalShow, infomation, handleOnClose } = this.props;

    return (
      <Modal
        show={modalShow}
        dialogClassName={'notify-modal'}
        backdrop="static"
        aria-labelledby='notify-modal'
      >
        <Modal.Header bsPrefix='notify-modal-header d-flex justify-content-between align-items-center' closeButton>
          <Modal.Title id="options-modal">{infomation.title}</Modal.Title>
        </Modal.Header>
        <Modal.Body bsPrefix='notify-modal-body'>
          <div className='mb-3'>
            {infomation.message}
          </div>
          <div className='w-100'>
            <div className='btn-primary m-auto'
                  onClick={handleOnClose}>
              Ok
            </div>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}

CNotifyModal.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CNotifyModal)
