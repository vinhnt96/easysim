import React, { Component } from 'react'
import './SignUpSuccessPopUp.scss'
import { Modal } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { withRouter } from 'react-router'

class SignUpSuccessPopUp extends Component {
  constructor(props) {
    super(props);

    this.handleContactUsBtnClick = this.handleContactUsBtnClick.bind(this);
  }

  handleContactUsBtnClick() {
    this.props.history.push('contact-us');
  }

  render() {
    const t = this.context.t;
    const { popupShow, setModalShow, email } = this.props

    return (
      <Modal
        show={popupShow}
        onHide={() => { setModalShow('') }}
        dialogClassName='sign-up-success-popup'
        backdrop="static"
        aria-labelledby='sign-up-success-popup'
      >
        <Modal.Header closeButton>
          <Modal.Title id="sign-up-success-popup">{t('signup.email_verification')}</Modal.Title>
        </Modal.Header>
        <Modal.Body bsPrefix='custom-modal-body'>
          <div className='p-3'>
            <p>
              Please check your email <code>{email}</code> to activate your account, it might take a few minutes to show up in your inbox.
              <br />
              If you cannot find your activation email, get support from us through&nbsp;
              <b className='cursor-pointer' onClick={this.handleContactUsBtnClick}>{t('contact_us')}</b>
            </p>
          </div>
          <div className='text-center mb-3'>
            <div className='btn btn-primary ok-btn' onClick={() => { setModalShow('') }}>
              {t('button.ok')}
            </div>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}

SignUpSuccessPopUp.contextTypes = {
  t: PropTypes.func
}

export default withRouter(SignUpSuccessPopUp);
