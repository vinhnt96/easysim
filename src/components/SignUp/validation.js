import * as yup from 'yup';

export const buildSchema = (locale, translation) => {
  let t = translation

  let emailRules = locale === 'en' ?
    yup.string().trim().required(t('required_field')).email('Must be a valid email') :
    yup.string().trim()
       .required(t('validation.required'))
       .email(t('validation.wrong_format'))

  let passwordRules = locale === 'en' ?
    yup.string()
       .required(t('required_field'))
       .matches(
        /^(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
          t('validations.password.sign_up_rules')
        ):
    yup.string()
       .required(t('validation.required'))
       .matches(
          /^(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
          t('validations.password.sign_up_rules')
        )

  let confirmPasswordRules = yup.string().oneOf([yup.ref('password'), null], "Passwords don't match")
                                .required(t('required_field'))
                                .matches(
                                  /^(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
                                  t('validations.password.sign_up_rules')
                                )

  let firstNameRules = locale === 'en' ?
    yup.string().trim().required(t('required_field')):
    yup.string().trim()
      .required(t('validation.required'))

  let lastNameRules = locale === 'en' ?
    yup.string().trim().required(t('required_field')):
    yup.string().trim()
      .required(t('validation.required'))

  let countryRules = locale === 'en' ?
    yup.string().trim().required(t('required_field')):
    yup.string().trim()
      .required(t('validation.required'))

  let birthdayRules = locale === 'en' ?
    yup.date().max(new Date(), 'Date cannot be in the future').required(t('required_field')):
    yup.date().max(new Date()).required(t('validation.required'));

  return yup.object({
    email: emailRules,
    password: passwordRules,
    first_name: firstNameRules,
    last_name: lastNameRules,
    country: countryRules,
    confirm_password: confirmPasswordRules,
    birthday: birthdayRules,
  })
}
