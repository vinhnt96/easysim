import React, {Component, Fragment} from 'react'
import './style.scss'
import ReactPlayer from 'react-player'
import { Form, Button, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import logo from '../../images/Logos/vertical_odin_logo.png'
import FeatureSection from 'components/Shared/FeatureSection/FeatureSection'
import { device, browser } from 'utils/utils'
import { isEmpty, toLower, sortBy } from 'lodash'
import moneyIcon from 'images/money.png'
import medalIcon from 'images/medal.png'
import { codeDiscount } from 'actions/subscription.actions'
import { DISCOUNT_CODES } from 'config/constants'
import { userSubscriptionInvalid, revertCodeSubscribe } from 'utils/utils'
import loadable from '@loadable/component'

const SubcribePageSection = loadable(() => import('components/SubscriptionPage/component'))
const GG_TUTORIAL_VIDEOS = [
  {name: 'Free Access', url: 'https://youtu.be/tp77icS3km0', order: 1},
  {name: 'Sim Selection Page', url: 'https://youtu.be/cAVw_A1QFPo', order: 2},
  {name: 'Preflop Loading Page', url: 'https://youtu.be/EIJUyXfH-PE', order: 3},
  {name: 'How To Navigate Odin', url: 'https://youtu.be/5OFCq5L_jkM', order: 4},
  {name: 'Strategy Views', url: 'https://youtu.be/DYSPUHX7rlY', order: 5},
  {name: 'Range Explorer', url: 'https://youtu.be/hQDnDC6YhUU', order: 6},
  {name: 'Runouts Explorer', url: 'https://youtu.be/qvWaBaMSJg0', order: 7},
  {name: 'Hide And Round Strategies', url: 'https://youtu.be/h1pvMS19zAE', order: 8},
]
class CLandingPage extends Component {

  constructor(props) {
    super(props)

    this.state = {
      notify: {
        text: '',
        color: '',
        isValidEmail: false,
      },
      firstVideo: {
        play: true,
        volume: 0.2,
        url: 'https://youtu.be/d8T7ZphP86I'
      },
      secondVideo: {
        play: false,
        url: 'https://www.youtube.com/watch?v=LOBU3UX88Fw'
      },
      emailValue: ''
    }
    this.onSubmit = this.onSubmit.bind(this)
    this.handleChangeEmail = this.handleChangeEmail.bind(this)
    this.handleClickStarted = this.handleClickStarted.bind(this);
  }

  componentDidMount() {
    const { currentUser, history } = this.props
    const { location } = history
    if (!isEmpty(location.state) && location.state.firstSignin && !userSubscriptionInvalid(currentUser.attributes)) {
        history.push('/strategy-select', {login: true})
    } else {
      const { pathname } = this.props.location
      const code = revertCodeSubscribe(toLower(pathname))
      if (DISCOUNT_CODES.includes(code)) {
        this.props.updateDiscountCode(code)
        localStorage.setItem('code', code)
      } else {
        this.props.updateDiscountCode('')
        localStorage.removeItem('code')
      }
      // This function used to set user onl/offline
      // window.removeEventListener("beforeunload", this.removeKey)
      // window.addEventListener("beforeunload", this.removeKey)
      const publicIp = require('public-ip');
      (async () => {
        if (!isEmpty(this.props.currentUser.attributes)) {
          const payload = {
            current_ip_v4: await publicIp.v4(),
            device: device(),
            browser: browser(),
            online: true
          }
          this.infoAfterLogin(payload)
        }
      })();
    }
  }

  removeKey = (e) =>  {
    e.preventDefault();
    if (!isEmpty(this.props.currentUser.attributes))
      this.infoAfterLogin({ online: false })
    localStorage.removeItem('first-sign-in')
  }

  infoAfterLogin(payload) {
    const { attributes: user } = this.props.currentUser
    window.axios.put(`/users/${user.id}/update_info_current_login`, payload)
    .then( res => { }).catch(error => { })
  }



  handleChangeEmail(e) {
    if(!!this.state.notify.text) this.setState({notify: {}})
    this.setState({ emailValue: e.currentTarget.value })
  }

  renderForm() {
    let t = this.context.t

    return <Form
      inline
      onSubmit={this.onSubmit}
      className='subscribe-form'
    >
      <Form.Control
        onChange={this.handleChangeEmail}
        value={this.state.emailValue}
        name='email'
        type="text"
        placeholder={t('email_placeholder')}
        className='col-sm-9'
      />
      <Button type='submit' className='btn-primary col-sm-3' >
        {t('landing_page.subscribe')}
      </Button>
    </Form>
  }

  onSubmit(e){
    //send information
    e.preventDefault()
    const t = this.context.t
    const { emailValue } = this.state
    if(!emailValue) {
      this.setState({notify: {text: t('required_field') , color: 'text-danger', isValidEmail: false}})
    }
    else {
      window.axios.post('/subscribers', { subscriber: {email: emailValue} })
      .then( res => {
        if(!!res.data.error_message) {
          this.setState({notify: {text: res.data.error_message , color: 'text-danger', isValidEmail: false}})
          return
        }

        this.setState({notify: {
          text: `You have successfully subscribed for email ${res.data.subscriber.email}`,
          color: 'text-success', isValidEmail: true
        }})
        setTimeout(() => this.setState({notify: {}}) ,5000)
      })
      .catch( err => { console.log(err) })
    }
  }

  handleClickStarted() {
    this.props.history.push('/subscribe');
  }

  renderLandingViewAdmin() {
    const t = this.context.t
    const { color, text } = this.state.notify
    return (
     <div className='landing-page padding-content'>
        <div className='logo-image text-center'>
          <img alt='logo' className='logo w-100' src={logo}/>
          <p className='text-white text-center title'> {t('landing_page.comming_soon')} </p>
        </div>
        <FeatureSection className='mr-top-100' />
        <div className='subcribe-box'>
          <p className='description'>
            {t('landing_page.first')} <br />
            {t('landing_page.second')}
          </p>
          {this.renderForm()}
          <label className={`${color} mt-2`}> {text} </label>
        </div>
      </div>
    )
  }

  renderGGTutorialVideos() {
    return sortBy(GG_TUTORIAL_VIDEOS, ['order']).map((video, index) => {
      return (
        <div className={`tutorial-background mb-5 col-md-6 px-3`}>
          <div className='font-size-20 mb-2 font-weight-bold  heading-font'>{index+1}. {video.name}</div>
          <div className='tutorial-video-wrapper'>
            <ReactPlayer
              className='react-player'
              url={video.url}
              playing={false}
              controls={true}
              width='100%'
              height='100%'
            />
          </div>
        </div>
      )
    })
  }

  renderViewNormal() {
    const t = this.context.t
    const pathname = window.location.pathname
    const { firstVideo, secondVideo } = this.state
    const { currentUser } = this.props
    const { isSignedIn, attributes: { emailConfirmed, affiliate_code, subscription } } = currentUser
    const userSignedIn = isSignedIn && emailConfirmed && !isEmpty(localStorage.getItem('access-token'))
    const code = isEmpty(currentUser.attributes) ? revertCodeSubscribe(pathname) : isEmpty(affiliate_code) ? 'launch35' : affiliate_code
    const isChangeSubscriptionView = subscription && subscription.status === 'active'

    return (
      <Fragment>
        <div className='landing-page padding-content'>
          <div className='introduce-section row no-gutters'>
            <div className='title-top col-md-4'>
              <p className="font-weight-bold">Never Use</p>
              <p className="font-weight-bold">a Solver Again</p>
              {!userSignedIn && <Link to={'/signup'} >
                <Button className='try-odin mt-4' >
                  {t('landing_page.title_try_odin')}
                </Button>
              </Link>}
            </div>
            <div className='introduce-background col-md-8'>
              <ReactPlayer
                className='react-player size-react-player float-right'
                url={firstVideo.url}
                playing={firstVideo.play}
                controls={true}
              />
            </div>
          </div>
          { code === 'ggplatinum' &&
            <div className='tutorial-section only-for-ggplatinum '>
              <div className='video-title'>GG Platinum Tutorial Videos</div>
              <div className='row no-gutters'>
                {this.renderGGTutorialVideos()}
              </div>
            </div>
          }
          <Row className="mr-top-100 justify-content-center view-comments">
            <Col className="view-comments-first" sm={5}>
              <div className="view-odin-first"></div>
              <div className="dot-icon"></div>
              <div className="view-conversation">
                <p className="mt-2 conversation-content">As someone who has always been very inquisitive yet incredibly lazy when it comes to studying, Odin is an absolute dream. All the poker answers, in detail, without any of the complicated inputs and hours of waiting.</p>
                <div className='bottom-conversation'>
                  <p className="mb-0 color-name">Kahle Burns</p>
                  <div className='d-flex'><img src={moneyIcon} alt=''/><p className="mb-0 color-desc">$10m+ in live earnings</p></div>
                  <div className='d-flex'><img src={medalIcon} alt=''/><p className="mb-0 color-desc">2x WSOP Gold Bracelet winner.</p></div>
                </div>
              </div>
            </Col>
            <Col sm={1}></Col>
            <Col className="view-comments-second" sm={5}>
              <div className="view-odin-second"></div>
              <div className="dot-icon"></div>
              <div className="view-conversation">
                <p className="mt-2 conversation-content">Odin gives you the ability to study basically any spot without ever touching a solver. The quality is by far the best I’ve ever seen. I think it’s a must have for anyone who wants to reach the next level in poker.</p>
                <div className="bottom-conversation">
                  <p className="mb-0 color-name">Fedor Holz</p>
                  <div className='d-flex'><img src={moneyIcon} alt=''/><p className="mb-0 color-desc">$30m+ in live earnings</p></div>
                  <div className='d-flex'><img src={medalIcon} alt=''/><p className="mb-0 color-desc">2x WSOP Gold Bracelet winner.</p></div>
                </div>
              </div>
            </Col>
          </Row>
          <div className="play-video">
            <div className='video-title'> {t('landing_page.what_is_odin')} </div>
            <ReactPlayer
              className='react-player w-100'
              width={100}
              url={secondVideo.url}
              playing={secondVideo.play}
              controls={true}
            />
          </div>
          <div className="compare-section">
            <div className="title-the-best mt-5 mb-5">Why are we the best?</div>
            <div className="features-table mx-auto"></div>
          </div>
        </div>
        <div className="landing-page-subscribe">
          <SubcribePageSection
            history={this.props.history}
            isPartOfLandingPage={true}
            isChangeSubscriptionView={isChangeSubscriptionView}
          />
        </div>
      </Fragment>
    )
  }

  render() {
    return this.renderViewNormal()
  }
}

CLandingPage.contextTypes = {
  t : PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  currentUser: store.reduxTokenAuth.currentUser
})

const mapDispatchToProps = {
  updateDiscountCode: codeDiscount
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CLandingPage)
