import React, { Component } from 'react'
import './PreflopHandMatrix.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { combosGenerator } from '../../../utils/matrix'
import { map, round} from 'lodash'
import { preflopBackgroundColorForHandItem, roundedRaiseAction } from 'services/preflop_game_service'
import { calcAvgStrategy } from 'utils/matrix'
import { PREFLOP_ACTION_DECIMAL } from 'config/constants'
import { recalculateStrategyObject } from 'utils/utils'

class CPreflopHandMatrix extends Component {
  renderStrategy(data) {
    return (
      <div className='font-size-18 w-50'>
      {
        map(recalculateStrategyObject(data, 0), (value, key) => {
          let keyShow = roundedRaiseAction(key, PREFLOP_ACTION_DECIMAL);
          return(
            <div key={keyShow} className='row no-gutters matrix-item-info p-1'>
              <div className='col-8 text-left'>{keyShow}</div>
              <div className='col-4 text-right'>{`${round(value)}%`}</div>
            </div>
          )
        })
      }
      </div>
    )
  }

  render() {
    let {currentUser, hand, dataForEachCombos, heightPercentages, weightedStrategy } = this.props
    const strategy_selection = JSON.parse(sessionStorage.getItem("strategy_selections")) || currentUser.strategy_selection
    let style = preflopBackgroundColorForHandItem(hand, dataForEachCombos, heightPercentages[hand] || 100, currentUser.preferences, strategy_selection.flop_cards)
    style = weightedStrategy ? style : { ...style, height: '100%', borderTopLeftRadius: 'inherit', borderTopRightRadius: 'inherit' };

    let combo = combosGenerator(hand);
    let data = calcAvgStrategy(combo, dataForEachCombos);

    return (
      <div className='row p-0 m-0 h-100 w-100'>
        <div className='hand-matrix-item-container cursor-pointer col-12 p-0 item-grid-12'>
          <div className='hand-matrix-item row no-gutters border-radius-7 h-100'>
            <div className='card-rank font-weight-bold font-size-20'>
              {this.props.hand || 'AA'}
            </div>
            <div className='background row no-gutters w-100 pt-1'
                 style={style}>
            </div>
            <div className='combo-info row my-1 mx-0 w-100'>
              {this.renderStrategy(data)}
            </div>
          </div>
        </div>
        {   }
      </div>
    )
  }
}

CPreflopHandMatrix.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  currentUser: store.reduxTokenAuth.currentUser.attributes
})

export default connect(
  mapStoreToProps,
  null
)(CPreflopHandMatrix)
