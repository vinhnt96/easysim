import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import  './PreflopSimDeck.scss'
import {
  CASH_GAME_PLAYER_POSITIONS,
  MTT_GAME_PLAYER_POSITIONS,
  HU_GAME_PLAYER_POSITIONS,
  DOWNCASE_POSITIONS_MAPPING,
  PREFLOP_POT_STACK_PRECISION
} from 'config/constants'
import dealer_button from 'images/coins/dealer_button.png'
import yellow_coins from 'images/coins/yellow_coins.png'
import red_coins from 'images/coins/red_coins.png'
import blue_coins from 'images/coins/blue_coins.png'
import { actionNameDisplay } from 'services/preflop_game_service'
import poker_code from 'images/Logos/poker_code.png'
import { isEqual, round } from 'lodash'

class CPreflopSimDeck extends Component {

  positionsForGame(isCashGame, specs) {
    if(isCashGame) {
      return isEqual(specs, 'hu') ? HU_GAME_PLAYER_POSITIONS : isEqual(specs, '8max') ? MTT_GAME_PLAYER_POSITIONS : CASH_GAME_PLAYER_POSITIONS
    } else {
      return MTT_GAME_PLAYER_POSITIONS
    }
  }
  
  playerPositionEles(isCashGame, specs) {
    const playerPositions = this.positionsForGame(isCashGame, specs);
    const { currentPosition, trackedData }= this.props.preflopGame
    let {currentUser} = this.props
    const preferences = currentUser.preferences || {}
    const displayPositionsByDefault = preferences.display_positions_by_default || false

    return playerPositions.map((position, index) => {
      const highlighted = position === currentPosition
      const lastAction = trackedData[position].lastAction;
      const bet = trackedData[position].bet;
      const actionClass = lastAction === 'fold' ? 'fold-action' : '';
      const textColor = lastAction === 'fold' ? 'text-red' : '';
      const positionDisplay = displayPositionsByDefault ? position.toUpperCase() : DOWNCASE_POSITIONS_MAPPING[position].toUpperCase()
      const remainingStack = isNaN(parseFloat(trackedData[position].remaining)) ? '' : `${round(trackedData[position].remaining, PREFLOP_POT_STACK_PRECISION)}bb`

      return (
        <div key={index} className={`position-block ${position} ${actionClass} ${specs}`}>
          <div className={`position-point ${highlighted ? 'active' : ''}`}>
            <div className='position font-size-14'>{positionDisplay}</div>
            <div className={`remaining-stack ${textColor}`}>
              { lastAction === 'fold' ? 'FOLD' : remainingStack }
            </div>
            <div className={`chip-animation ${specs}`}>
              {
                !['bb', 'sb'].includes(position) && trackedData[position].bet > 0 &&
                  <img className='' src={blue_coins} alt=''/>
              }
              {
                ['bb', 'sb'].includes(position) &&
                  <img className='' src={position === 'sb' ? yellow_coins : red_coins } alt=''/>
              }
              <div className='lastest-action font-size-12'>
                {  ['fold', ''].includes(lastAction) ? (bet ? `${bet}bb` : '') : actionNameDisplay(lastAction) }
              </div>
            </div>
            {
              position === 'b' &&
                <img className='dealer-button' src={dealer_button} alt=''/>
            }
          </div>
        </div>
      )
    });
  }

  render(){
    let { currentUser, preflopGame } = this.props
    let { totalPot } = preflopGame;
    const strategy_selection = JSON.parse(sessionStorage.getItem("strategy_selections")) || currentUser.strategy_selection
    const { stack_size, sim_type, game_type, street, specs } = strategy_selection;
    const isCashGame = game_type === 'cash'
    const isPostFlop = street === 'postflop'
    let deckInfo = '';

    if(isPostFlop) {
      deckInfo = `${stack_size || '0'}bb ${(sim_type || 'nan').toUpperCase()} Preflop`
    } else {
      deckInfo = `${stack_size || '0'}bb`;
    }
    let isPokerCodeUser = currentUser.role === 'pokercode_user';
    const classSimDeck = isEqual(game_type, 'cash') &&  isEqual(specs, '8max') ? 'mtt' : game_type
    return (
      <div className='preflop-sim-deck'>
        <div className='sim-deck'>
          <div className='deck-container d-flex position-relative'>
            <div className='deck d-flex flex-column align-items-center justify-content-center'>
              <div className='deck-info'>
                {deckInfo}
              </div>
              <div className='total-pot'>
                {`TOTAL POT: ${round(totalPot, PREFLOP_POT_STACK_PRECISION)}bb`}
              </div>
              {
                isPokerCodeUser &&
                  <div className='poker-code-block'>
                    <img src={poker_code} alt='poker-code' />
                  </div>
              }
              <div className={`${classSimDeck}`}>
                { this.playerPositionEles(isCashGame, specs) }
                <div className={`coins-icon d-flex flex-column align-items-center ${isCashGame ? 'hide' : ''}`}>
                  <div className='coins'>
                    <img src={red_coins} alt='' className='mx-2' />
                    <div className='font-weight-bold font-size-12 text-center'>1bb</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CPreflopSimDeck.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  preflopGame: store.preflopGame,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CPreflopSimDeck)
