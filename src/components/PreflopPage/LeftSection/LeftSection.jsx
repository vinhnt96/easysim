import React, { Component } from 'react'
import './LeftSection.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { resetPreplopGameDispatch, rollbackPreflopGameDispatch } from 'actions/preflop-game.actions'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBackward } from '@fortawesome/free-solid-svg-icons'
import { getActionColor, roundedRaiseAction } from 'services/preflop_game_service'
import { isEqual, isEmpty, round, without} from 'lodash'
import { PREFLOP_ACTION_DECIMAL, PREFLOP_NUM_COMBOS_PRECISION, PREFLOP_PERCENT_COMBOS_PRECISION } from 'config/constants'
import ActionOptions from './ActionOptions/ActionOptions'
import loadable from '@loadable/component'

const NotifyModal = loadable(() => import('components/NotifyModal/component'))
const Header = loadable(() => import('components/Shared/Header/Header'))
const PreflopSimDeck = loadable(() => import('components/PreflopPage/LeftSection/PreflopSimDeck/PreflopSimDeck'))

class CLeftSection extends Component {
  constructor(props) {
    super(props);
    this.state = ({
      isFirst: true,
      preflopGame: [],
      actionOptions: [],
      showNotifyModal: false
    })
    this.onClickReset = this.onClickReset.bind(this);
    this.onClickRollback = this.onClickRollback.bind(this);
  }

  componentDidMount() {
    const { preflopGame } = this.props;
    this.setState({
      preflopGame: preflopGame,
      actionOptions: this.generateActionOptions(preflopGame)
    })
  }

  componentWillReceiveProps(nextProps) {
    const { preflopGame, isFirst } = this.state;
    if (!isEqual(nextProps.preflopGame, preflopGame)) {
      let state = {
        preflopGame: nextProps.preflopGame,
        actionOptions: this.generateActionOptions(nextProps.preflopGame)
      }
      if (nextProps.preflopGame.history.length === 0 && !isFirst)
        state['isFirst'] = true
      this.setState(state)
    }
  }

  setCurrentPosition(strategySelection) {
    if(strategySelection.game_type === 'mtt') {
      return 'b5'
    } else {
      const specs = strategySelection.specs
      return specs === 'hu' ? 'b' : specs === '8max' ? 'b5' : 'b3'
    }
  }

  onClickReset() {
    const { preflopGame, currentUser } = this.props
    const { strategy_selection } = currentUser
    const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || strategy_selection
    const { specs } = strategySelection
    const currentPosition = this.setCurrentPosition(strategySelection);
    this.setState({ isFirst: true })
    this.props.resetPreplopGameDispatch(preflopGame.trackedData, currentPosition, preflopGame.game_type, preflopGame.stack_size, specs);
  }

  onClickRollback() {
    const { preflopGame } = this.props;
    const { strategy_selection } = this.props.currentUser
    const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || strategy_selection
    this.props.rollbackPreflopGameDispatch({...preflopGame, specs: strategySelection.specs});
  }

  generateActionOptions(preflopGame) {
    let { action_options, action_ranges } = preflopGame
    let totalCombos = 0
    if (isEmpty(action_options))
      return []
    const numbers = this.sortNoteActions(action_options)
    let actionOptions = action_options.map((option) => {
      let title = roundedRaiseAction(option.title, PREFLOP_ACTION_DECIMAL);
      let value = roundedRaiseAction(option.value, PREFLOP_ACTION_DECIMAL);
      let numCombos = round(action_ranges[`${option.value}`].split(' ').reduce( (sum, value) => sum + parseFloat(value), 0), PREFLOP_NUM_COMBOS_PRECISION)
      totalCombos += numCombos

      return {
        title: title,
        value: value,
        colorClass: getActionColor(option.value, this.props.currentUser.preferences),
        numCombos: numCombos,
        order: this.numberAction(option, action_options.length, numbers)
      }
    })

    actionOptions.forEach((actionOptionsElement, index) => {
      actionOptionsElement['percentCombos'] = round((actionOptionsElement.numCombos / totalCombos) * 100, PREFLOP_PERCENT_COMBOS_PRECISION);
    })
    return actionOptions
  }

  setNumberArrayAction(numbers, value) {
    return numbers.indexOf(parseFloat(value.slice(0, -2))) + 1
  }

  numberAction(action, lengthArray, numbers) {
    switch(action.value){
      case 'allin': return 0
      case 'fold': return lengthArray + 2
      case 'check': return lengthArray + 1
      case 'call': return lengthArray
      default: return this.setNumberArrayAction(numbers, action.value)
    }
  }

  sortNoteActions(actions) {
    const values = actions.map(i => i.value)
    const results = without(values, 'allin', 'call', 'check', 'fold')
    const numbers = results.map(i => parseFloat(i.slice(0, -2))).reverse()
    return numbers
  }

  render() {
    const t = this.context.t
    const { isFirst, actionOptions, showNotifyModal } = this.state;
    const { data } = this.props;

    return (
      <div className='left-section h-100 d-flex flex-column justify-content-between'>
        <Header
          setShowNotifyModal={() => this.setState({showNotifyModal: true})}
          showDropDownButton={true}
        />
        <NotifyModal
          modalShow={showNotifyModal}
          infomation={{title: 'Action not available', message: t('notify_modal.can_not_open_more_than_1_sim_message')}}
          handleOnClose={() => this.setState({showNotifyModal: false})}
        />
        <PreflopSimDeck
          positionSelected='ip'
          turnCard=''
          riverCard=''
          additionalClass='flex-grow-1'
        />
        <div className='button-group'>
          <div className='additional-buttons'>
            <div
              className='additional-button font-size-14'
              onClick={this.onClickRollback}
            >
              <FontAwesomeIcon
                icon={faBackward}
                className='angle-icon cursor-pointer'
              />
            </div>
            <div
              className='additional-button font-size-14'
              onClick={this.onClickReset}
            >Reset</div>
          </div>
          <ActionOptions
            data={data}
            isFirst={isFirst}
            setIsFirst={() => this.setState({ isFirst: false })}
            actionOptions={actionOptions}
          />
        </div>
      </div>
    );
  }
}

CLeftSection.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  preflopGame: store.preflopGame,
})

const mapDispatchToProps = {
  resetPreplopGameDispatch,
  rollbackPreflopGameDispatch,
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CLeftSection)
