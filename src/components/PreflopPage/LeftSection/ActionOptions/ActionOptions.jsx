import React, { Component } from 'react'
import './ActionOptions.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { isEqual, orderBy } from 'lodash'
import { updatePreflopGame } from 'actions/preflop-game.actions'
import { togglePopup } from 'actions/subscription.actions'
import { checkFreeTrialUser } from 'utils/utils'
import { MAXIMUM_NUMBER_OF_PREFLOP_ACTIONS } from 'config/constants'

class CActionOptions extends Component {
  constructor(props) {
    super(props);
    this.handleActionOnclick = this.handleActionOnclick.bind(this);
  }

  handleActionOnclick(action) {
    const { currentUser, togglePopup, data } = this.props;
    // Show Subscribe Now popup from the 5th node and afterward
    if(checkFreeTrialUser(currentUser.email) && this.props.preflopGame.history.length >= MAXIMUM_NUMBER_OF_PREFLOP_ACTIONS) {
      togglePopup('preflop-subscribe-now')
      return;
    }
    const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || currentUser.strategy_selection
    this.props.setIsFirst()
    this.props.updatePreflopGame(action, this.props.preflopGame, data, strategySelection.specs);
  }

  render() {
    const t = this.context.t
    let { currentUser, isFirst, actionOptions } = this.props
    const { strategy_selection: { street: StreetType } } = currentUser
    const dataActionOtpions = [...actionOptions]
    let preferences = currentUser.preferences || {}
    let hideExplanation = preferences.hide_explanation
    const isPostflopSim = [StreetType, JSON.parse(sessionStorage.getItem("strategy_selections"))].includes('postflop')

    return (
      <div className='actions row p-0 m-0 align-items-center'>
        {
          orderBy(dataActionOtpions, ['order'], ['asc']).map((action, index) => {
            let style = { backgroundColor: action.colorClass }
            return (
              <div key={index} className={`action-wrapper ${isFirst ? 'position-relative' : ''}`}>
                <div
                  className={`action font-weight-600`}
                  onClick={() => { this.handleActionOnclick(action) }}
                  style={style}>
                  <div className='action-buton font-size-12'>
                    <p className='my-0'>
                      { action.title }
                    </p>
                    <p className='my-0'>
                      {`${action.percentCombos}% | ${action.numCombos}c`}
                    </p>
                  </div>
                </div>
                { isFirst && isEqual(index, 0) && !hideExplanation && isPostflopSim &&
                  <div className='explanation w-100'>
                    <div className='handle-point-up w-100 mb-1'>
                      <div className='handle-point-up-icon mx-auto'></div>
                    </div>
                    <div className='font-size-13 text-center'>{t('action_option.title')}</div>
                  </div>
                }
              </div>
            )
          })
        }
      </div>
    );
  }
}

CActionOptions.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  preflopGame: store.preflopGame,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
  updatePreflopGame,
  togglePopup,
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CActionOptions)
