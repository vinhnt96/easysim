import React, { Component } from 'react'
import './RightSection.scss'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import { SUITS_FOR_FILTER } from 'config/constants'
import { withRouter } from 'react-router'
import { togglePopup } from 'actions/subscription.actions'
import loadable from '@loadable/component'
const RangeMatrix = loadable(() => import('components/MainPage/RangeMatrix/RangeMatrix'))
const PreflopHandMatrix = loadable(() => import('components/PreflopPage/PreflopHandMatrix/PreflopHandMatrix'))

class CRightSection extends Component {

  constructor(props) {
    super(props)

    this.state = {
      hoveredHand: '',
      handMatrixPopupStyle: {},
      showSubscribeNowPopup: false,
    }

    this.handleHoverHandItem = this.handleHoverHandItem.bind(this)
    this.onMouseMove = this.onMouseMove.bind(this)
    this.handleOnClickSubscribe = this.handleOnClickSubscribe.bind(this);
    this.closePopUp = this.closePopUp.bind(this);
  }

  handleHoverHandItem(handItem) {
    if(this.state.handItem === handItem) return
    this.setState({hoveredHand: handItem})
  }

  onMouseMove(e) {
    let handItemStyle = e.currentTarget.getBoundingClientRect()
    let handMatrixPopupStyle = { top: handItemStyle.top + 140, left: handItemStyle.left > 900 ? (handItemStyle.left - 100) : handItemStyle.left }
    this.setState({handMatrixPopupStyle: handMatrixPopupStyle})
  }

  renderHandMatrixPopup(data, heightPercentages) {
    const { weightedStrategy } = this.props;

    return (
      <div className={`hand-matrix-popup ${!!this.state.hoveredHand ? 'show' : ''}`}
            style={this.state.handMatrixPopupStyle}>
        <PreflopHandMatrix
          dataForEachCombos={data}
          hand={this.state.hoveredHand}
          heightPercentages={heightPercentages}
          weightedStrategy={weightedStrategy}
        />
      </div>
    )
  }

  closePopUp() {
    this.props.togglePopup('');
  }

  handleOnClickSubscribe() {
    this.props.history.push('subscribe');
  }

  renderSubscribePopup() {
    let t = this.context.t;

    return <div className='subscribe-popup'>
      <div className='header text-right'><span className='close-btn' onClick={this.closePopUp}>×</span></div>
      <div className='message'>{t('free_trial.subscribe_popup.unlock_preflop')}</div>
      <div className='subscribe-btn' onClick={this.handleOnClickSubscribe}>{t('free_trial.subscribe_popup.subscribe_now')}</div>
    </div>
  }

  render() {
    let { weightedStrategy, preflopGame: { history, currentPosition, trackedData }, subscriptions, data } = this.props;
    let heightPercentages =  currentPosition ? trackedData[currentPosition].heightPercentages : 100;
    let showPopUp = history.length >= 4 && subscriptions.popUp === 'preflop-subscribe-now';

    return (
        <div className={`right-section container`}>
          <div className={`${showPopUp ? 'blurring-10' : ''}`}>
            <RangeMatrix
              dataForEachCombos={data}
              view=''
              suitsForFilter={SUITS_FOR_FILTER}
              handleOnHoverOnHand={showPopUp ? () => {} : this.handleHoverHandItem}
              changeClickedHandItem={() => {}}
              rangeExplorerShowing={true}
              decisionFilter=''
              handClickedItem=''
              showHandItemPopup={false}
              isPreflopPage={true}
              weightedStrategy={weightedStrategy}
              handleMouseMove={this.onMouseMove}
              heightPercentages={heightPercentages}
            />
          </div>
          {this.renderHandMatrixPopup(data, heightPercentages)}
          {showPopUp && this.renderSubscribePopup()}
        </div>
    );
  }
}

CRightSection.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  preflopGame: store.preflopGame,
  subscriptions: store.subscriptions,
})

const mapDispatchToProps = {
  togglePopup,
}

export default withRouter(connect(
  mapStoreToProps,
  mapDispatchToProps
)(CRightSection))
