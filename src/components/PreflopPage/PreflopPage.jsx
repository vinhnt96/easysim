import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './PreflopPage.scss'
import { SUITS_FOR_FILTER, WEIGHTED_STRATEGY_DEFAULT_STATUS } from 'config/constants'
import { positionsForDisplayBuilder, positionsForQueryBuilder, alternativePositionsForDisplayBuilder } from 'services/game_play_service'
import { convertFlopCards, buildConvertSuitsTemplate } from 'services/convert_cards_services'
import { dataForEachComboGenerator } from 'services/matrix_service'
import { getMoreData, recieveFlopData, updateFetchTurnRiverSuccess, refeshGameData} from 'actions/game.actions'
import { initPreflopPage, updateInitialPreflopData, resetToInitialPreflopData } from 'actions/preflop-game.actions'
import { Redirect } from 'react-router-dom'
import { togglePopup } from 'actions/subscription.actions'
import { updateCardsConvertingTemplate } from 'actions/cards-converting.actions'
import { updateComboWithIndex,refeshHandMatrix } from 'actions/hand-matrix.actions'
import { freeTree, loadTreeRequest } from 'services/load_tree_request_service.js'
import {isEqual} from 'lodash'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import { renderCombosWithIndexToRedux } from 'utils/matrix'
import IdleTimer from 'react-idle-timer'
import CountDownNextButton from 'components/PreflopPage/CountDownNextButton/CountDownNextButton'
import loadable from '@loadable/component'
const RangeMatrix = loadable(() => import('components/MainPage/RangeMatrix/RangeMatrix'))
const TimedOutNotifyModal = loadable(() => import('components/MainPage/TimedOutNotifyModal/TimedOutNotifyModal'))
const UpperSection = loadable(() => import('./UpperSection'))

class CPreflopPage extends Component {

  constructor(props) {
    super(props);

    this.state = {
      weightedStrategy: WEIGHTED_STRATEGY_DEFAULT_STATUS,
      mainPage: false,
      isInactive: false
    }

    this.idleTimer = null
    this.handleOnIdle = this.handleOnIdle.bind(this)
    this.toggleWeightedStrategy = this.toggleWeightedStrategy.bind(this)
  }

  componentDidMount() {
    const { currentUser, preflopGame, updateComboWithIndex } = this.props;
    const { strategy_selection } = currentUser
    const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || strategy_selection
    sessionStorage.setItem("unMount", false);
    window.removeEventListener("beforeunload", this.handleUpdateTreeList, true)
    if(strategySelection) {
      if(strategySelection.street === 'postflop') {
        const cardsConvertingTemplate = buildConvertSuitsTemplate(strategySelection.flop_cards)
        const convertedFlopCards = convertFlopCards(strategySelection.flop_cards, cardsConvertingTemplate)
        const dataParams = {
          game_type: strategySelection.game_type,
          stack_size: strategySelection.stack_size,
          specs: strategySelection.specs,
          sim_type: strategySelection.sim_type,
          flops: convertedFlopCards.join(''),
          positions: positionsForQueryBuilder(strategySelection.positions),
          nodes: "r:0",
          node_count: 2,
          view: 'ranges_ip, ranges_oop'
        }
        updateComboWithIndex(renderCombosWithIndexToRedux(strategySelection.flop_cards))
        this.props.updateCardsConvertingTemplate(cardsConvertingTemplate)
        this.props.getMoreData(dataParams)

        // request load_tree here
        this.props.updateFetchTurnRiverSuccess('fetching')
        loadTreeRequest({
          ...strategySelection,
          convertedFlopCards,
          updateFetchTurnRiverSuccess
        }).then(res => {
          if (isEqual(res, 'success'))
            this.props.updateFetchTurnRiverSuccess('done')
        })
      }
      window.addEventListener("beforeunload", this.handleUpdateTreeList, true)
      sessionStorage.setItem("added", true)
      const currentPosition = this.setCurrentPosition(strategySelection);

      this.props.initPreflopPage(preflopGame, strategySelection, currentPosition);
    } else {
      this.props.history.push('/strategy-select')
    }
  }

  setCurrentPosition(strategySelection) {
    if(strategySelection.game_type === 'mtt') {
      return 'b5'
    } else {
      const specs = strategySelection.specs
      return specs === 'hu' ? 'b' : specs === '8max' ? 'b5' : 'b3'
    }
  }

  handleUpdateTreeList = (e) => {
    if(e !== undefined)
      e.preventDefault()
    if (sessionStorage.getItem("unMount") === 'false')
      this.freeKey()
  }

  freeKey() {
    const treeInfo = JSON.parse(sessionStorage.getItem("treeInfo")) || {}

    if(Object.keys(treeInfo).length > 0 ) {    // just call free_tree if this tab have loaded tree
      freeTree(treeInfo.channelId)
    }
  }

  removeSim(sim, object, num, id) {
    num > 0 ? object[sim.id] = num : delete object[sim.id]
    localStorage.setItem("requests", JSON.stringify(object))
    sessionStorage.setItem("added", false);
    const payload = { sim_ids: JSON.stringify(Object.keys(object)), add: false }
    if (sim) {
      window.axios.put(`/users/${id}`, payload)
    }
  }

  componentWillUnmount() {
    this.props.resetToInitialPreflopData();
    this.props.togglePopup('')
    if (!this.state.mainPage) {
      this.props.refeshGameData()
      this.props.refeshHandMatrix()
      this.freeKey()
      sessionStorage.setItem("unMount", true)
    }
  }

  handleNextBtnOnclick = () => {
    window.removeEventListener("beforeunload", this.handleUpdateTreeList, true)
    this.setState({ mainPage: true }, () => this.props.history.push('/main-page', { accept: true }))
  }

  handleOnIdle (event) {
    // lock screen here
    const { currentUser } = this.props;
    const { strategy_selection} = currentUser
    const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || strategy_selection

    if(strategySelection.street === 'postflop') {
      this.setState({ isInactive: true })
      this.handleUpdateTreeList()
    }
  }

  toggleWeightedStrategy() {
    this.setState({ weightedStrategy: !this.state.weightedStrategy }) 
  }

  render() {
    const { currentUser: { strategy_selection, preferences }, game, handMatrix } = this.props
    const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || strategy_selection
    const { ranges_ip, ranges_oop } = game
    const { stack_size, street, positions, sim_type, flop_cards: flopCards } = strategySelection
    const preferencesCurrent = preferences || {}
    const displayPositionsByDefault = preferencesCurrent.display_positions_by_default || false
    const combosWithIndex = handMatrix ? handMatrix.combosWithIndex : {}

    if(!strategySelection) {
      return <Redirect to='/strategy-select' />
    }

    const data_ip = dataForEachComboGenerator({ ranges_ip, flopCards, view: 'ranges_ip', combosWithIndex })
    const data_oop = dataForEachComboGenerator(({ ranges_oop, flopCards, view: 'ranges_oop', combosWithIndex }))

    const isPostflop = street === 'postflop'
    const positionsView = displayPositionsByDefault ? positionsForDisplayBuilder(positions) : alternativePositionsForDisplayBuilder(positions)
    const orderedPositions = isPostflop ? positionsView.split('v').map(position => position.trim()) : ''
    const orderedPositionsReverse = [...orderedPositions].reverse()
    const orderedPositionsIncludesBBSB = isEqual(orderedPositionsReverse, ['SB', 'BB'])
    return (
      <Fragment>
        <div className='preflop-page-container'>
          <IdleTimer
              ref={ref => { this.idleTimer = ref }}
              timeout={1000*60*15}
              onIdle={this.handleOnIdle}
              debounce={250}
            />
          <div className='preflop-page mx-auto'>
            <UpperSection 
              weightedStrategy={this.state.weightedStrategy}
              toggleWeightedStrategy={this.toggleWeightedStrategy}
              />
            {
              isPostflop && (
                <div className='bottom-section position-relative'>
                  <div className='position-matrixes no-gutters row position-relative justify-content-center'>
                    <div className='position-matrix row no-gutters col-md-5 col-sm-12'>
                      <div className='col-lg-3 col-0'></div>
                      <div className='col-lg-9'>
                        <RangeMatrix
                          dataForEachCombos={orderedPositionsIncludesBBSB ? data_ip : data_oop}
                          view={orderedPositionsIncludesBBSB ? 'ranges_ip' : 'ranges_oop'}
                          suitsForFilter={SUITS_FOR_FILTER}
                          handleOnHoverOnHand={() => {}}
                          changeClickedHandItem={() => {}}
                          rangeExplorerShowing={true}
                          decisionFilter=''
                          handClickedItem=''
                          showHandItemPopup={false}
                          weightedStrategy={true}
                        />
                      </div>
                    </div>
                    <div className='position-matrix col-md-2 col-sm-12 sim-info-block'>
                      <div className='sim-info'>
                        <div className='deck-info mx-auto row no-gutters'>
                          <div className='stack-size col-sm-6'>
                            {`${parseInt(stack_size) || '0'}bb`}
                          </div>
                          <div className='game-type col-sm-6'>
                            { (sim_type || 'nan').toUpperCase() }
                          </div>
                        </div>
                        <div className='positions d-flex justify-content-between mt-3'>
                          <div className='rounded-circle'>{ orderedPositions[0] }</div>
                          <div className='rounded-circle'>{ orderedPositions[1] }</div>
                          <p className='vs-word'>vs</p>
                        </div>
                      </div>
                    </div>
                    <div className='position-matrix row no-gutters col-md-5 col-sm-12'>
                      <div className='col-lg-9'>
                        <RangeMatrix
                          dataForEachCombos={orderedPositionsIncludesBBSB ? data_oop : data_ip}
                          view={orderedPositionsIncludesBBSB ? 'ranges_oop' : 'ranges_ip'}
                          suitsForFilter={SUITS_FOR_FILTER}
                          handleOnHoverOnHand={() => {}}
                          changeClickedHandItem={() => {}}
                          rangeExplorerShowing={true}
                          decisionFilter=''
                          handClickedItem=''
                          showHandItemPopup={false}
                          weightedStrategy={true}
                        />
                      </div>
                      <div className='col-lg-3 col-12 d-flex next-btn-block pr-0 text-right align-items-center'>
                        <CountDownNextButton
                          toMainPage={this.handleNextBtnOnclick}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              )
            }
          </div>
        </div>
        { this.state.isInactive &&
          <TimedOutNotifyModal
            modalShow={this.state.isInactive}/>
        }
      </Fragment>
    );
  }
}

CPreflopPage.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  game: store.game,
  preflopGame: store.preflopGame,
  handMatrix: store.handMatrix,
})

const mapDispatchToProps = {
  getMoreData,
  updateInitialPreflopData,
  resetToInitialPreflopData,
  togglePopup,
  updateCardsConvertingTemplate,
  updateFetchTurnRiverSuccess,
  updateCurrentUserAttributesToReduxTokenAuth,
  updateComboWithIndex,
  recieveFlopData,
  refeshGameData,
  refeshHandMatrix,
  initPreflopPage
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CPreflopPage)
