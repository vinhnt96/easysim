import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { buildRangeForEachCombos } from 'services/preflop_game_service'
import loadable from '@loadable/component'
const LeftSection = loadable(() => import('components/PreflopPage/LeftSection/LeftSection.jsx'))
const RightSection = loadable(() => import('components/PreflopPage/RightSection/RightSection.jsx'))

class CUpperSection extends Component {

  render() {
    const t = this.context.t
    const { preflopGame: { action_ranges } } = this.props
    const data = buildRangeForEachCombos(action_ranges);

    return (
      <Fragment>
        <div className='row top-section p-0 m-0'>
          <div className='col-lg-6 col-sm-12 p-0 m-0'>
            <LeftSection
              data={data}
            />
          </div>
          <div className='col-lg-6 col-sm-12 p-0 m-0'>
            <RightSection
              data={data}
              weightedStrategy={this.props.weightedStrategy}
            />
          </div>
        </div>
        <div className='text-right d-flex justify-content-end chk-weighted-strategy'>
          <span onClick={() => { this.props.toggleWeightedStrategy() }}
              className={`weighted-strategy-button ${this.props.weightedStrategy ? 'selected' : ''} d-flex align-items-center justify-content-center`}>
            {t('weighted_strategy')}
          </span>
        </div>
      </Fragment>
    );
  }
}

CUpperSection.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  preflopGame: store.preflopGame,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CUpperSection)
