import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import BlueClock from 'images/Icons/blue-clock.png'
import { DO_NOT_APPLY_DELAY_TIME_EMAILS } from 'config/constants'

class CCountDownNextButton extends Component {
  constructor(props) {
    super(props);

    this._isMounted = false;
    this.intervalId = null
    this.state = {
      countdown: 20,
    }

    this.handleNextBtnOnclick = this.handleNextBtnOnclick.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;

    if(DO_NOT_APPLY_DELAY_TIME_EMAILS.includes(this.props.currentUser.email) || this.props.currentUser.role === 'admin') {
      this.setState({countdown: 0})
    }
    this.intervalId = setInterval(() => {
      let newCount = this.state.countdown - 1;
      if(this._isMounted && newCount >= 0) {
        this.setState({ countdown: newCount });
      } else {
        clearInterval(this.intervalId );
      }
    }, 1000)
  }

  componentDidUpdate() {
    if(this.state.countdown === 0) {
      clearInterval(this.intervalId );
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleNextBtnOnclick() {
    this.props.toMainPage()
  }

  render() {
    const t = this.context.t
    return (
      <React.Fragment>
        {
          this.state.countdown > 0 ? (
            <div className='red-clock'>
              <div className='msg pb-1'>{t('countdown.title')}</div>
              <img src={BlueClock} alt=''/>
              <span className='time-text ml-1'>{`${this.state.countdown}s`}</span>
            </div>
          ) : (
            <button
              className='next-button btn font-size-12 font-weight-bold'
              onClick={this.handleNextBtnOnclick}
            >
              {t('countdown.title_button')}
            </button>
          )
        }
      </React.Fragment>
    );
  }
}

CCountDownNextButton.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CCountDownNextButton)
