import React, {Component, Fragment} from 'react'
import './style.scss'
import Header from '../Header/component'
import {connect} from 'react-redux'
import Footer from 'components/Footer/component'
import {withRouter} from 'react-router'
import { LINKS_LAYOUT } from 'config/constants'
import {toLower} from 'lodash'

class Layout extends Component {

  render(){
    let currentUser = this.props.currentUser
    let path = this.props.location.pathname
    let isSignInSignUpPage = ['/signin', '/signup'].includes(path)
    let style = document.documentElement.style;
    if(isSignInSignUpPage)
      style.setProperty('--header-height', 0);
    else 
      style.setProperty('--header-height', '75px');

    return <Fragment>
      {currentUser.isLoading ?
        <div> Loading ... </div> :
        <Fragment>
          <Header/>
          <div className={`content-container hide-scrollbar ${path === '/' || LINKS_LAYOUT.includes(toLower(path)) ? 'dark-background' : ''}`}>
            {this.props.children}
            <Footer />
          </div>
        </Fragment>
      }
    </Fragment>
  }

}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser,
})

export default withRouter(
  connect(mapStoreToProps, null)(Layout)
)
