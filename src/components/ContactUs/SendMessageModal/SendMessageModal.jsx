import React, { Component } from 'react'
import './SendMessageModal.scss'
import '../../../custom_style.scss'
import PropTypes from 'prop-types'
import { Form, Modal, Row, Col } from 'react-bootstrap'
import FormField from 'components/Shared/FormField/FormField'
import { buildSchema } from './validation'
import { connect } from 'react-redux'
import { Formik } from 'formik'
import LaddaButton from '../../LaddaButton/component'
import { notify } from 'utils/utils'

class CSendMessageModal extends Component {

  renderForm = formProp => {
    let {
      handleSubmit,
      handleChange,
      isSubmitting,
      errors,
      values,
      touched,
      handleBlur
    } = {...formProp}

    return (
      <Form
      className='fadeIn mb-4'
      onSubmit={handleSubmit}
      >
        <Row>
          <Col sm={6}>
            <Form.Group controlId='controlId-Name'>
              <FormField
                field={'name'}
                fieldType={'text'}
                autoComplete='on'
                error={touched.name && errors.name}
                isSubmitting={isSubmitting}
                handleChange={handleChange}
                values={values}
                isRequired={true}
                onBlur={handleBlur}
              />
            </Form.Group>
          </Col>
          <Col sm={6}>
            <Form.Group controlId='controlId-Email'>
              <FormField
                field={'email'}
                fieldType={'text'}
                autoComplete='on'
                error={touched.email && errors.email}
                isSubmitting={isSubmitting}
                handleChange={handleChange}
                values={values}
                isRequired={true}
                onBlur={handleBlur}
              />
            </Form.Group>
          </Col>
        </Row>
        <Row className='mt-2'>
          <Col sm={12}>
            <Form.Group controlId='controlId-message'>
                <FormField
                  field={'message'}
                  autoComplete='on'
                  error={touched.message && errors.message}
                  isSubmitting={isSubmitting}
                  handleChange={handleChange}
                  values={values}
                  rows={4}
                  as={'textarea'}
                  isRequired={true}
                  onBlur={handleBlur}
                />
              </Form.Group>

          </Col>
          <Col className='mt-4 mb-2' sm={12}>
            <LaddaButton
              type='submit'
              text='Send message'
              className='w-100 my-2-3 py-2 btn-primary font-size-20 font-weight-600'
              disabled={isSubmitting} />
          </Col>
        </Row>
      </Form>
    )
  }

  onSubmit(data, actions){
    let t = this.context.t
    let schemaValidation = buildSchema(this.props.i18nState.lang, t)
    let trimmedData = schemaValidation.cast(data)
    //send information
    import('react-toastify').then(reactToastify => {
      window.axios.post('contact_us_messages', { contact_message: trimmedData }).then(() => {
        notify("Message has been sent successfully!", reactToastify.toast.success);
      }).catch(() => {
        notify("Failed to send the message!", reactToastify.toast.error);
      })
      this.props.setModalShow('')
      //close modal
    })
  }


  render() {
    let t = this.context.t
    let schemaValidation = buildSchema(this.props.i18nState.lang, t)
    let { isShow, setModalShow } = this.props

    return (
      <Modal
      show={isShow}
      onHide={() => setModalShow('')}
      dialogClassName='send_message_modal d-flex flex-column justify-content-center'
      aria-labelledby='send_message_modal'
      >
        <Modal.Body>
          <Modal.Header bsPrefix='custom-modal-header mb-4' closeButton>
            <Modal.Title> </Modal.Title>
          </Modal.Header>
          <div className='content'>
            <div className='content-header mb-5 mt-2'>
              <h4 className='heading-font'> {t('contact.send_us_message')} </h4>
            </div>
            <Formik
                component={this.renderForm}
                validationSchema={schemaValidation}
                onSubmit={this.onSubmit.bind(this)}
                initialValues={{ name: '', email: '', message: ''}}/>
            <div className='seperate'></div>
            <div className='content-footer mt-4 text-center '>
              {t('contact.or_send_us')}
              <span className='text-primary'> 
                <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a>
              </span>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}

CSendMessageModal.contextTypes = {
  t : PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState
})

const mapDispatchToProps = (dispatch) => ({

})

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CSendMessageModal)
