import React, { Component } from 'react'
import './stylesheet.scss'
import '../../custom_style.scss'
import PropTypes from 'prop-types'

class CContactUs extends Component {

  render() {
    let t = this.context.t

    return (
     <div className='contact-us'>
        <div className='text-center w-100 title heading-font'> {t('contact_us')} </div>
        <div className='contact-content-container'>
          <div className='content'>
            <div className='for-billing text-center'>
              <h5 className='mb-4 font-weight-bold heading-font'>  {t('contact.for_billing')} </h5>
              <p>Email us at <a href='mailto:support@odinpoker.io' >support@odinpoker.io</a></p>
            </div>
            <div className='for-anything text-center'>
            <h5 className='mb-4 font-weight-bold heading-font'> {t('contact.for_any')} </h5>
              <p className='my-0'>{t('contact.discord_message')}</p>
              <span className='discord_link'>
                <a href={t('contact.discord_link')} target='blank' className='text-primary-color text-decoration-none'>
                  {t('contact.discord_link_text')}
                </a>
              </span>
            </div>
          </div>
        </div>
     </div>
    )
  }

}
CContactUs.contextTypes = {
  t: PropTypes.func
}

export default CContactUs
