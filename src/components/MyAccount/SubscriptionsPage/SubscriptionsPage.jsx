import React, { Component } from 'react'
import './SubscriptionsPage.scss'
import { DEFAULT_SUBSCRIPTION_ITEMS_PER_PAGE } from 'config/constants'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import axios from 'axios'
import { Button } from 'react-bootstrap'
import { isEmpty } from 'lodash'
import loadable from '@loadable/component'

const SubscriptionsTable = loadable(() => import('./SubscriptionsTable/SubscriptionsTable'))
const SubscriptionPagePopup = loadable(() => import('./SubscriptionsPagePopup/SubscriptionsPagePopup'))

class CSubscriptionsPage extends Component {
  constructor(props){
    super(props)

    this.state = {
      subscriptionsData: [],
      searchValue: '',
      activePage: 1,
      totalPages: 0,
      sortColumn: '',
      isDefaultDirection: true,
      subscriptionPagePopup: {
        showing: false,
        type: 'renew'
      }
    }

    this.handleClickColumnHeader = this.handleClickColumnHeader.bind(this)
    this.handleOnChangeSearchValue = this.handleOnChangeSearchValue.bind(this)
    this.handlePageClick = this.handlePageClick.bind(this)
    this.handleShowPopup = this.handleShowPopup.bind(this)
    this.loadSubscriptionsFromServer = this.loadSubscriptionsFromServer.bind(this)
  }

  componentDidMount() {
    this.loadSubscriptionsFromServer()
  }

  loadSubscriptionsFromServer() {
    const { activePage, searchValue } = this.state
    const params = { page: activePage, per_page: DEFAULT_SUBSCRIPTION_ITEMS_PER_PAGE, search: searchValue }
    axios.get('/subscriptions', {params: params}).then( res => {
      const { page, pages, data } = res
      this.setState({ subscriptionsData: data.subscriptions, activePage: page, totalPages: pages })
    })
    .catch( err => { console.error(err) })
  }

  handleClickColumnHeader(column){
    const { sortColumn, isDefaultDirection: isDefaultDirectionState } = this.state
    const newState = sortColumn === column ? {isDefaultDirection: !isDefaultDirectionState}
                                           : {sortColumn: column, isDefaultDirection: true}
    this.setState(newState, () => {
      this.loadSubscriptionsFromServer()
    })
  }

  handleOnChangeSearchValue(e){
    this.setState({searchValue: e.target.value })
  }

  handlePageClick(pageData) {
    this.setState({activePage: parseInt(pageData.selected + 1)}, () => { 
      this.loadSubscriptionsFromServer()
    })
  }
  
  handleShowPopup(popupType) {
    const { subscriptionPagePopup: subscriptionPagePopupState } = this.state
    this.setState({
      subscriptionPagePopup: {
        ...subscriptionPagePopupState,
        showing: !subscriptionPagePopupState.showing,
        type: popupType || subscriptionPagePopupState.type,
      }
    })
  }

  render(){
    const t = this.context.t
    const { subscriptionPagePopup: { showing: popupShowing, type: popupType }, 
    subscriptionsData, totalPages, sortColumn, isDefaultDirection } = this.state
    const { currentUser: { subscription } } = this.props
    const disableButtonChange = isEmpty(subscription) || subscription.status !== 'active'
    const disableButtonCancel = isEmpty(subscription) || ['canceled', 'deactivated'].includes(subscription.status)

    return (
      <React.Fragment>
        <div className='subscription-section'>
          <h4 className='section-header'> My Subscription </h4>
          <div className='subscription-table'>
            <SubscriptionsTable 
              totalPages={totalPages}
              subscriptionsData={subscriptionsData}
              sortColumn={sortColumn}
              isDefaultDirection={isDefaultDirection}
              handlePageClick={this.handlePageClick}
              handleClickColumnHeader={this.handleClickColumnHeader}
              handleOnChangeSearchValue={this.handleOnChangeSearchValue}
            />
            <p className='suport-info'>
              You are able to get instant refund in 24hrs after purchase. 
              Please contact <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a>
              for this refund and any other refund/cancellation concerns.
            </p>
            <div className='action-btn-group'>
              {/* <Button variant="primary" className='action-btn' onClick={() => this.handleShowPopup('renew')}>
                {t('subscription.auto_renewal')}
              </Button> */}
              <Button variant="primary" className='action-btn'
                onClick={() => this.handleShowPopup('canceled')}
                disabled={disableButtonCancel} >
                {t('subscription.cancel_subscription')}
              </Button>
              <Button variant="primary" className='action-btn'
                onClick={() => this.handleShowPopup('change')}
                disabled={disableButtonChange} >
                {t('subscription.change_subscription_plan')}
              </Button>
            </div>
          </div>
          {/* <div className='subscription-content'>
            <div className='current-subscription'>
              <div className='current-subscription-header'>
                <div className='title col-6 justify-content-center d-flex align-items-center'> Current Subscription </div>
                <div className='subscription-date col-6 justify-content-center d-flex align-items-center'> Renews 21/02/2021 </div>
              </div>
              <div className='current-subscription-content'>
                <p className='price'> $499.99/month</p>
                <p> MTTs </p>
                <p> 1755 Flops </p>
              </div>
            </div>
            <div className='arow-icon'>
              <img src={blueArrow} alt='blue-arrow'/>
            </div>
            <div className='subscribe-div'>
              <p className='subscribe-price'> $699.99/month </p>
              <p> MTT + Cash Games </p>
              <p> 1755 Flops </p>
              <div className='subscribe-now-btn-wrapper'>
                <div className='paypal-button-wrapper'>
                </div>
                <button className={`btn subscribe-now-btn`}>Subscribe Now</button>
              </div>
            </div>
          </div> */}
        </div>
        <SubscriptionPagePopup 
          show={popupShowing}
          handleShowPopup={this.handleShowPopup}
          loadSubscriptionsFromServer={this.loadSubscriptionsFromServer}
          popupType={popupType}
        />
      </React.Fragment>
    )
  }

}

CSubscriptionsPage.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  subscriptions: store.subscriptions,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

export default connect(
  mapStoreToProps,
  null
)(CSubscriptionsPage)

