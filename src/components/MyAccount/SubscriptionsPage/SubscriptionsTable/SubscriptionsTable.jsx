import './SubscriptionsTable.scss'
import React, { Component, Fragment } from 'react'
import ReactPaginate from 'react-paginate'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import SubscriptionList from './SubscriptionsList/SubscriptionsList'

class SubscriptionsTable extends Component {

  renderPaginate() {
    const { handlePageClick, totalPages } = this.props
    if(totalPages < 2)
      return <Fragment> </Fragment>

    return (
      <ReactPaginate
        previousLabel={'previous'}
        nextLabel={'next'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        pageCount={totalPages}
        marginPagesDisplayed={3}
        pageRangeDisplayed={5}
        onPageChange={handlePageClick}
        containerClassName={'pagination'}
        subContainerClassName={'pages pagination'}
        pageClassName={'page-item'}
        nextClassName={'page-item page-next text-uppercase'}
        previousClassName={'page-item page-previous text-uppercase'}
        activeClassName={'active'} />
    )
  }

  render(){
    let t = this.context.t
    let columnHeader = [
      t('subscription.plan'), t('subscription.billing_cycle'), t('subscription.payment'),
      t('subscription.subscribed_date'), t('subscription.renewal_date'), t('subscription.status')
    ]
    const { handleClickColumnHeader, sortColumn: sortColumnProps, isDefaultDirection, subscriptionsData } = this.props

    return (
      <div className='subscription-table-container'>
        <div className='content-body'>
          <div className='table-container'>
            <table className='subscription-table w-100 text-center'>
              <thead>
                <tr>
                  {
                    columnHeader.map( (header,idx) => {
                      let sortColumn = header.toLowerCase().replaceAll(' ', '_').replaceAll('-', '_')
                      return (
                        <th key = {idx + header}
                            onClick={() => handleClickColumnHeader(sortColumn)}
                            className={`${sortColumnProps === sortColumn ? (isDefaultDirection ? 'active sorted-desc' : 'active sorted-asc') : ''} no-select `}>
                          {header}
                        </th>
                      )
                  })}
                </tr>
              </thead>
              <tbody>
                <SubscriptionList 
                  data={subscriptionsData}
                />
              </tbody>
            </table>
          </div>
        {
            <div className='pagination justify-content-center'>
              {this.renderPaginate()}
            </div>
        }
        </div>
      </div>
    )
  }

}

SubscriptionsTable.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes
})

export default connect(
  mapStoreToProps,
  null
)(SubscriptionsTable)
