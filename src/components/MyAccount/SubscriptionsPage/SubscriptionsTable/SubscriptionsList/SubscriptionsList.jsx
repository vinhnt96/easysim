import React, { Component } from 'react'
import './SubscriptionsList.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { currencyFormat } from 'services/game_play_service'

class CSubscriptionsList extends Component {

  render() {
    let subscriptionsData = this.props.data || []

    return  subscriptionsData.map( (subscription, idx) => {
      const { id, active_subscription, billing_cycle, auto_renew, start_time, renew_time, status, price_discount } = subscription
      const subscribedDate = new Date(start_time).toISOString().split('T')[0]
      const renewalDate = new Date(renew_time).toISOString().split('T')[0]
      const statusText = status.charAt(0).toUpperCase() + status.slice(1).toLowerCase()
      const shouldShowAutoRenew = auto_renew && status === 'active'
      return (
        <tr key={id}>
          <td> {active_subscription} </td>
          <td> {billing_cycle} </td>
          <td> {currencyFormat(price_discount)} </td>
          <td> {subscribedDate} </td>
          <td> {shouldShowAutoRenew ? 'Auto-renew' : renewalDate} </td>
          <td>
            {statusText}
          </td>
        </tr>
      )
    })
  }

}

CSubscriptionsList.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CSubscriptionsList)
