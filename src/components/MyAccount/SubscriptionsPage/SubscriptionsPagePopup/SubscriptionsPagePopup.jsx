import React, { Component } from 'react'
import './SubscriptionsPagePopup.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import loadable from '@loadable/component'
const SubscriptionPageSection = loadable(() => import('components/SubscriptionPage/component.jsx'))

class CSubscriptionsPagePopup extends Component {
  constructor(props) {
    super(props)

    this.handleConfirmed = this.handleConfirmed.bind(this)
    this.handleCanceled = this.handleCanceled.bind(this)
  }

  renderRenewContent() {
    const t = this.context.t
    return (
      <div>
        <p className='info'> Do you want to renew the subscription automatically? </p>
        <div className='btn-container'>
          <div className='btn-primary' onClick={this.handleConfirmed}>
            {t('yes')}
          </div>
          <div className='btn-primary' onClick={this.handleCanceled}>
            {t('no')}
          </div>
        </div>
      </div>
    )
  }

  renderCanceledContent() {
    const { currentUser: { subscription: { renew_time } } } = this.props
    const renewalDate = new Date(renew_time).toISOString().split('T')[0]
    const t = this.context.t

    return (
      <div>
        <p className='info'>
          Your subscription plan will be valid until {renewalDate}. Are you sure to cancel the plan? 
        </p>
        <div className='btn-container'>
          <div className='btn-primary' onClick={this.handleConfirmed}>
            {t('yes')}
          </div>
          <div className='btn-primary' onClick={this.handleCanceled}>
            {t('no')}
          </div>
        </div>
      </div>
    )

  }

  renderChangeSubscriptionContent() {
    return (
      <SubscriptionPageSection
        isChangeSubscriptionView={true}
        handleConfirmedChangeSub={this.handleConfirmed}
      />
    )
  }

  renderPopupContent() {
    const { popupType } = this.props
    switch (popupType) {
      case 'renew': return this.renderRenewContent()
      case 'canceled': return this.renderCanceledContent()
      case 'change': return this.renderChangeSubscriptionContent()
      default: return this.renderRenewContent()
    }
  }

  handleConfirmed() {
    const { popupType } = this.props
    if(popupType === 'canceled') {
      window.axios.post('/subscriptions/cancel_current_subscription', { event: popupType }).then( res => {
        const { data: {user} } = res
        this.props.updateCurrentUserAttributesToReduxTokenAuth(user)
        this.props.loadSubscriptionsFromServer()
      }).catch(err => console.error(err))
    }
    if(popupType === 'change') {
      this.props.loadSubscriptionsFromServer()
    }
    this.props.handleShowPopup()
  }

  handleCanceled() {
    this.props.handleShowPopup()
  }

  render() {
    const { show, handleShowPopup, popupType } = this.props
    const dialogClassName = popupType === 'change' ? 'change-subscription-popup' : 'subscription-page-popup'
    
    return (
      <Modal
        show={show}
        onHide={handleShowPopup}
        dialogClassName={dialogClassName}
        backdrop="static"
        aria-labelledby={dialogClassName}
      >
        <Modal.Body bsPrefix='custom-modal-body'>
          <Modal.Header bsPrefix='custom-modal-header d-flex justify-content-between align-items-center' closeButton>
            <Modal.Title id="options-modal"> { popupType === 'change' ? 'Change Subscription' : 'Confirm Popup'} </Modal.Title>
          </Modal.Header>
          <div className='content'>
            {this.renderPopupContent()}
          </div>
        </Modal.Body>
      </Modal> 
    )
  }
}

CSubscriptionsPagePopup.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  subscriptions: store.subscriptions,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
  updateCurrentUserAttributesToReduxTokenAuth
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CSubscriptionsPagePopup)
