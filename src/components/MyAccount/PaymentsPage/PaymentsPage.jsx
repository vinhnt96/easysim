import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import PaymentsCard from '../PaymentsCard/PaymentsCard'

class CPaymentsPage extends Component {

  render() {
    let t = this.context.t

    return (
      <div className='payment-page'>
        <h4 className='section-header'> My Payments </h4>
        <div className='payment-details'>
          <h4 className='detail-header d-flex justify-content-between'>
            <span> {t('payment_detail')} </span>
            <span className='text-primary'> (edit) </span>
          </h4>
          <PaymentsCard />
        </div>
      </div>
    )
  }
}

CPaymentsPage.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({

})

export default connect(
  mapStoreToProps,
  null
)(CPaymentsPage)
