import * as yup from 'yup'
import {isEmpty} from 'lodash'

export const buildSchema = (translation, user, changePassword) => {
  const t = translation

  const emailRules = yup.string().trim()
      .required(t('required_field'))
      .email(t('valid_email'))

  const passwordRules = yup.string().trim()
      .matches(
        /^(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
          t('validations.password.sign_up_rules')
        )

  const firstNameRules = yup.string().trim()
      .required(t('validations.first_name.required'))

  const lastNameRules = yup.string().trim()
      .required(t('validations.last_name.required'))

  const countryRules = yup.string().required(t('required_field'))

  const object = {
    email: emailRules,
    first_name: firstNameRules,
    last_name: lastNameRules,
    country: countryRules,
    password: yup.string(),
    new_password: yup.string(),
    confirm_password: yup.string()
  }

  const confirmPasswordRules = yup.string().trim()
      .required(t('required_field'))
      .oneOf([yup.ref('new_password'), null], t('password_not_match'))
      .matches(
        /^(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
        t('validations.password.sign_up_rules')
      )

  const newPasswordRules = yup.string().trim()
      .required(t('required_field'))
      .matches(
        /^(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/,
          t('validations.password.sign_up_rules')
        )
      .when('password', {
        is: (password) => !isEmpty(password) && (/^(?=.*[A-Z])(?=.*[0-9])(?=.{8,})/).test(password),
        then: yup.string().trim().notOneOf([yup.ref('password'), null], t('password_match'))
      })



  if (!user && changePassword) {
    object['password'] = passwordRules.required(t('required_field'))
    object['new_password'] = newPasswordRules
    object['confirm_password'] = confirmPasswordRules
  }

  if (user && changePassword) {
    object['new_password'] = newPasswordRules.required(t('required_field'))
    object['confirm_password'] = confirmPasswordRules
  }

  return yup.object().shape(object)
}
