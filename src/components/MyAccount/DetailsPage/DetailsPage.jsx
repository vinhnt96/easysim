import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Formik } from 'formik'
import { Form, Row, Col, Button, Spinner } from 'react-bootstrap'
import { CountryDropdown } from 'react-country-region-selector'
import FormField from 'components/Shared/FormField/FormField'
import { buildSchema } from './validation'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'
import calendarIcon from 'images/calendar_icon.png'
import axios from 'axios'
import { isEmpty, isEqual, isString } from 'lodash'
import { toast } from 'react-toastify'
import { notify } from 'utils/utils'

const AUTH_HEADER_KEYS = [
  'access-token',
  'token-type',
  'client',
  'expiry',
  'uid',
]

class CDetailsPage extends Component {

  constructor(props) {
    super(props)

    this.state = {
      birthdayValue: '',
      loading: false,
      modeChangePassword: false
    }
    this.renderDetailsForm = this.renderDetailsForm.bind(this)
  }

  componentDidMount() {
    const { user, currentUser } = this.props
    let birthday = currentUser.birthday
    if (user)
      birthday = user.birthday
    this.setState({birthdayValue: birthday})
  }

  handleChangeBirthday = (date) => {
    import('moment').then(moment => {
      const bd = moment.default(date).format('YYYY-MM-DD')
      this.setState({birthdayValue: bd})
    })
  }

  onKeyDown = (event) => {
    if (event.which === 32) event.preventDefault()
  }

  handleOpenViewChangePassword() {
    const { modeChangePassword } = this.state
    this.setState({ modeChangePassword: !modeChangePassword })
  }

  handleSubmit = async (data, { setErrors, resetForm })  => {
    const { user } = this.props
    const { first_name, last_name, country, email, password, confirm_password, new_password } = data
    const date = new Date(this.state.birthdayValue)
    const newBirthday = date.toISOString().split('T')[0]
    const newInfo = {
      birthday: newBirthday,
      first_name,
      last_name,
      country,
      email,
    }
    if (!isEmpty(new_password)) {
      newInfo['password'] = new_password
      newInfo['confirm_password'] = confirm_password
    }
    this.setState({ loading: true })
    if (user) {
      this.updateInfoUser(newInfo)
    } else {
      if (isEmpty(new_password)) {
        this.updateInfoMyAccountDetail(newInfo)
      } else {
        newInfo['current_password'] = password
        this.updatePasswordMyAccount(newInfo, (e) => setErrors(e), (r) => resetForm(r))
      }
    }
  }

  updatePasswordMyAccount(params, setErrors, resetForm) {
    const t = this.context.t
    axios.put('/auth/password', params)
    .then(({ data }) => {
      const objErrors = {}
      if(data.status === 5) {
        if (data.message === 'incorrect')
          objErrors['password'] = t('incorrect')

        if (data.message === 'password_not_match')
          objErrors['confirm_password'] = t('password_not_match')
      }
      setErrors(objErrors)
      if (isEmpty(objErrors)) {
        resetForm({})
        AUTH_HEADER_KEYS.forEach(key => localStorage.setItem(key, data.headers[key]))
        this.setState({ loading: false, modeChangePassword: false })
        notify(t('details_page.success'), toast.success);
      } else {
        this.setState({ loading: false })
      }
    }).catch(error => {
      this.setState({ loading: false, modeChangePassword: false })
      notify(t('details_page.fail'), toast.error)
    })
  }

  updateInfoMyAccountDetail(newInfo) {
    const t = this.context.t
    axios.put('/auth', newInfo)
    .then( res => {
      if(res.status === 200) {
        this.props.updateCurrentUserAttributesToReduxTokenAuth({...this.props.currentUser, ...newInfo})
      }
      this.setState({ loading: false })
      notify(t('details_page.success'), toast.success);
    }).catch(error => {
      this.setState({ loading: false })
      notify(t('details_page.fail'), toast.error)
    })
  }

  updateInfoUser(newInfo) {
    const { user, handleUpdateUser } = this.props
    const url = `/admin/users/${user.id}`
    axios.put(url, newInfo).then(({ data }) => {
      this.setState({ loading: false })
      handleUpdateUser(data.user)
    })
  }

  renderDetailsForm(formProps) {
    const t = this.context.t
    const { user } = this.props;
    const { loading, modeChangePassword, birthdayValue } = this.state;
    const initialBirthdayValue = isEmpty(birthdayValue) ? new Date() : isString(birthdayValue) ? new Date(birthdayValue) : birthdayValue
    const detailsFields = [
      {name: 'first_name', type: 'text'},
      {name: 'last_name', type: 'text'},
      {name: 'email', type: 'text'},
    ]
    let {
      handleSubmit,
      handleChange,
      errors,
      values,
      touched,
      handleBlur,
      setErrors,
      resetForm
    } = {...formProps}

    return (
      <Form
        noValidate
        onSubmit={handleSubmit}>
        <Row>
          {
            detailsFields.map((field, idx) => {
              return (
                <Col key={idx} sm={6}>
                  <FormField
                    field={field.name}
                    fieldType={field.type}
                    error={touched[field.name] && errors[field.name]}
                    handleChange={handleChange}
                    values={values}
                    isRequired={true}
                    isSubmitting={!user && isEqual(field.name, 'email')}
                    onBlur={handleBlur}
                  />
                </Col>
              )
            })
          }
          <Col sm={6}>
            <Form.Group className='' controlId='controlId-birthday'>
              <Form.Label>
                {t('birthday')}&nbsp; <span className='text-red'>*</span>
              </Form.Label>
              <label className='date-picker-container'>
                <DatePicker name='birthday' selected={initialBirthdayValue} className='form-control date-picker-input'
                            dateFormat='MM-dd-yyyy' onChange={(date) => this.handleChangeBirthday(date)} />
                <img src={calendarIcon} alt='calendar-icon' className='calendar-icon' />
              </label>
              <Form.Control.Feedback type="invalid">
                {errors['birthday']}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
          </Row>
          { (!user || (user && !modeChangePassword)) && <Row>
          <Col sm={6}>
            <Form.Group controlId='controlId-current-password'>
              <Form.Label className="d-flex justify-content-between">
                <span> { user ? t('password') : t('details_page.current_password')} </span>
                <Button
                  className='change-password text-primary'
                  onClick={() => {this.handleOpenViewChangePassword(); setErrors({}); resetForm({})}}
                >
                  { modeChangePassword ? t('cancel') : t('change_password')}
                </Button>
              </Form.Label>
              <Form.Control
                name="password"
                type='password'
                disabled={!modeChangePassword}
                isInvalid={errors.password}
                value={values.password}
                classes="form-control"
                placeholder={user ? t('password_placeholder') :t('details_page.current_password_holder')}
                onChange={handleChange}
                isRequired={modeChangePassword}
                onKeyDown={this.onKeyDown}
              />
              <Form.Control.Feedback type="invalid">
                {errors.password}
              </Form.Control.Feedback>
            </Form.Group>
          </Col>
          <Col sm={6}>
            <Form.Group controlId='controlId-country'>
              <Form.Label>{t('country_of_residence')}</Form.Label>
              <CountryDropdown
                name="country"
                value={values.country}
                isInvalid={errors.country}
                isRequired={true}
                classes="form-control"
                onChange={(_, e) => handleChange(e)}/>
            </Form.Group>
            <Form.Control.Feedback type="invalid" style={{display: 'block'}}>
              {errors.country}
            </Form.Control.Feedback>
          </Col>
        </Row>}
        { modeChangePassword &&
          <Fragment>
            <Row>
              <Col sm={6}>
                <Form.Group controlId='controlId-new-password'>
                  <Form.Label className="d-flex justify-content-between">
                    <span> {t('details_page.new_password')} </span>
                    { user && <Button
                      className='change-password text-primary'
                      onClick={() => {this.handleOpenViewChangePassword(); setErrors({}); resetForm({})}}
                    >
                      { modeChangePassword ? t('cancel') : t('change_password')}
                    </Button>}
                  </Form.Label>
                  <Form.Control
                    name="new_password"
                    type='password'
                    isInvalid={errors.new_password}
                    value={values.new_password}
                    classes="form-control"
                    placeholder={t('details_page.new_password_holder')}
                    onChange={handleChange}
                    onKeyDown={this.onKeyDown}
                    isRequired={true}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.new_password}
                  </Form.Control.Feedback>
                </Form.Group>
              </Col>
              { user && <Col sm={6}>
                <Form.Group controlId='controlId-country'>
                  <Form.Label>{t('country_of_residence')}</Form.Label>
                  <CountryDropdown
                    name="country"
                    value={values.country}
                    isInvalid={errors.country}
                    isRequired={true}
                    classes="form-control"
                    onChange={(_, e) => handleChange(e)}/>
                </Form.Group>
                <Form.Control.Feedback type="invalid" style={{display: 'block'}}>
                  {errors.country}
                </Form.Control.Feedback>
              </Col>}
            </Row>
            <Row>
              <Col sm={6}>
                <Form.Group controlId='controlId-confirm-password'>
                  <Form.Label className="d-flex justify-content-between">
                    <span> {t('details_page.confirm_password')} </span>
                  </Form.Label>
                  <Form.Control
                    name="confirm_password"
                    type='password'
                    isInvalid={errors.confirm_password}
                    value={values.confirm_password}
                    classes="form-control"
                    placeholder={t('details_page.confirm_password_holder')}
                    onChange={handleChange}
                    isRequired={true}
                    onKeyDown={this.onKeyDown}
                  />
                  <Form.Control.Feedback type="invalid">
                    {errors.confirm_password}
                  </Form.Control.Feedback>
                </Form.Group>
              </Col>
            </Row>
            <Row>
              <Col sm={2}>
                <Button
                  disabled={!isEmpty(errors)}
                  type='submit'
                  className='update-password submit my-3 py-2 btn-primary font-size-18 font-weight-600'>
                  { loading && modeChangePassword ? <Spinner animation="border" size="sm" /> : t('details_page.update_password') }
                </Button>
              </Col>
            </Row>
          </Fragment>
        }
        <Row>
          <Col className={ modeChangePassword ? 'mt-1' : 'mt-4'} sm={12}>
            <Button
              disabled={modeChangePassword || !isEmpty(errors) || loading}
              type='submit'
              className='w-100 submit my-3 py-2 btn-primary font-size-18 font-weight-600'>
              {t('save_change')}
            </Button>
          </Col>
        </Row>
      </Form>
    )
  }

  render() {
    const t = this.context.t
    const { user, currentUser } = this.props
    const { email, firstName, lastName, country} = currentUser
    const { modeChangePassword } = this.state
    const schemaValidation = buildSchema(t, user, modeChangePassword)

    let initialValues = {
      email: email,
      first_name: firstName,
      last_name: lastName,
      password: '',
      country: country,
    }
    if (user) {
      const { email, first_name, last_name, country} = user
      initialValues = {
        email,
        first_name,
        last_name,
        password: '',
        country,
      }
    }

    return (
      <Fragment>
        <h4 className='section-header'>
          { user ? t('details_page.title_user') : t('title_details') }
        </h4>
        <div className='form-content'>
          <Formik
            component={this.renderDetailsForm}
            onSubmit={this.handleSubmit}
            validationSchema={schemaValidation}
            initialValues={initialValues}/>
        </div>
      </Fragment>

    )
  }
}

CDetailsPage.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  i18nState: store.i18nState,
})

const mapDispatchToProps = {
  updateCurrentUserAttributesToReduxTokenAuth: updateCurrentUserAttributesToReduxTokenAuth,
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CDetailsPage)
