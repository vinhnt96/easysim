import React, { Component } from 'react'
import './stylesheet.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { fetchPlansSucceeded } from 'actions/subscription.actions'
import axios from 'axios'
import { isEqual } from 'lodash'
import loadable from '@loadable/component'

const DetailsPage = loadable(() => import('./DetailsPage/DetailsPage'))
const SubscriptionsPage = loadable(() => import('./SubscriptionsPage/SubscriptionsPage'))
const PaymentsPage = loadable(() => import('./PaymentsPage/PaymentsPage'))
const componentMaps = {
  details: {
    component: DetailsPage,
  },
  subscriptions: {
    component: SubscriptionsPage,
  },
  payments: {
    component: PaymentsPage,
  }
}

class CMyAccount extends Component {

  constructor(props) {
    super(props)
    const { hash } = props.location
    this.pageShowing = hash.slice(1) && isEqual(hash.slice(1), 'subscriptions')
    this.state = {
      pageShowing: this.pageShowing || 'details'
    }

    this.pages = [
      { name: 'title_details', value: 'details'},
      { name: 'title_subscriptions', value: 'subscriptions'},
      { name: 'title_payments', value: 'payments'}
    ]
  }

  componentDidMount() {
    const self = this
    axios.get('/plans').then(({ data }) => {
      self.props.fetchPlansSucceeded(data.plans)
    })
  }

  handleShowPage = (pageName) => {
    const { pageShowing } = this.state
    if(!!window.location.hash)
      window.location.hash = ""

    if(!isEqual(pageShowing, pageName))
      this.setState({pageShowing: pageName})
  }

  render() {
    const t = this.context.t
    const { pageShowing } = this.state
    const ContentComponent = componentMaps[pageShowing].component
    return (
      <div className='my-account-container'>
        <h1 className='text-center title'>{t('my_account')}</h1>
        <div className='bottom-section text-right py-2'>
          <Link to='/contact-us' className="my-nav-link text-primary contact-text font-size-20">
            {t('contact_us_billing')}
          </Link>
        </div>
        <div className='row no-gutters mb-3'>
          <div className='col-3'>
            <ul className='page-list'>
              {
                this.pages.map( (page, idx) => {
                  return (
                    <li key={idx} onClick={() => this.handleShowPage(page.value)}
                        className={`${isEqual(pageShowing, page.value) ? 'active' : '' }`}>
                      {t(`${page.name}`)}
                    </li>
                  )
                })
              }
            </ul>
          </div>
          <div className='col-9'>
            <div className='my-account-content'>
              <ContentComponent />
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CMyAccount.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
})

const mapDispatchToProps = {
  fetchPlansSucceeded: fetchPlansSucceeded
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CMyAccount)
