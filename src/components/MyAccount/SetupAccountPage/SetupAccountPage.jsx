import React, { Component, Fragment } from 'react'
import './SetupAccountPage.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { buildSchema } from './validation'
import { Formik }  from 'formik';
import { Form, Row, Col, ProgressBar } from 'react-bootstrap'
import LaddaButton from 'components/LaddaButton/component'
import FormField from 'components/Shared/FormField/FormField'
import { CountryDropdown } from 'react-country-region-selector';
import queryString from 'query-string'
import {isEmpty} from 'lodash'
import { toast } from 'react-toastify';
import { notify } from 'utils/utils'
import axios from 'axios'
import { signInUser } from 'actions/redux-token-auth'

class SetupAccountPage extends Component {

  constructor(props) {
    super(props)

    this.state = {
      userDataState: {},
      invitationToken: ''
    }
  }

  componentDidMount() {
    const { location, history } = this.props
    const params =  queryString.parse(location.search)
    const canAccess = !isEmpty(Object.keys(params)) && params['invitation_token']
    if(!canAccess) return history.replace('/')

    axios.get(`/invitation_token?invitation_token=${params['invitation_token']}`).then( res => {
      if(res.data.user) {
        const userData = res.data.user
        const { email, first_name, last_name } = userData
        this.setState({
          userDataState: {email, first_name, last_name},
          invitationToken: params['invitation_token']
        })
      }
    }).catch( err => console.error(err) )
  }

  toggleCountryDropdownErrorMessage(value) {
    const countryErrorEle = document.getElementsByClassName("country-error")[0];

    if(value) {
      countryErrorEle.style.display = 'none'
    } else {
      countryErrorEle.style.display = 'block'
    }
  }

  onSubmit(user, actions) {
    const t = this.context.t
    const { signInUser } = this.props
    const params = { ...user, password_confirmation: user.confirm_password, invitation_token: this.state.invitationToken}
    axios.put('/auth/invitation', params).then(res => {
      notify(t('toast.update_info_success'), toast.success)
      signInUser(user).then(res => {
        window.location.replace('/')
      })
    }).catch(err => {
      console.error(err)
      notify(t('toast.update_info_failed'), toast.error)
    })
  }

  privateForm = formikProps => {
    const t = this.context.t
    let {
      handleSubmit,
      handleChange,
      isSubmitting,
      errors,
      values,
      touched,
      handleBlur,
      status,
    } = {...formikProps}

    const controlledFields = [
      {field: 'email', fieldType: 'text'},
      {field: 'first_name', fieldType: 'text'},
      {field: 'last_name', fieldType: 'text'},
      {field: 'birthday', fieldType: 'date'},
    ]

    return <Form
      noValidate
      className='fadeIn mb-4'
      onSubmit={handleSubmit}>
      <Row>
        {
          controlledFields.map((item, idx) => {
            const { field, fieldType } = item
            const isEmailField = field === 'email'
            return (
              <Col key={idx} sm={isEmailField ? 12 : 6}>
                <FormField
                  field={field}
                  fieldType={fieldType}
                  error={touched[field] && errors[field]}
                  status={status && status[field]}
                  isSubmitting={isSubmitting}
                  handleChange={handleChange}
                  values={values}
                  isRequired={true}
                  onBlur={handleBlur}
                  disabled={isEmailField}
                />
              </Col>
            )
          })
        }
        <Col sm={6}>
          <Form.Group controlId='controlId-country'>
            <Form.Label>{t('country_of_residence')}</Form.Label>
            <CountryDropdown
              name="country"
              value={values.country}
              classes="form-control"
              onChange={(_, e) => handleChange(e)}
              onBlur={this.toggleCountryDropdownErrorMessage}
            />
            <div className='country-error'>
              {errors.country}
            </div>
          </Form.Group>
        </Col>
        <Col sm={6}>
          <FormField
            field={'password'}
            fieldType={'password'}
            autocomplete='off'
            error={touched.password && errors.password}
            isSubmitting={isSubmitting}
            handleChange={handleChange}
            values={values}
            isRequired={true}
            onBlur={handleBlur}
          />
          <FormField
            field={'confirm_password'}
            fieldType={'password'}
            autocomplete='off'
            classNames='mb-0'
            error={touched.confirm_password && errors.confirm_password}
            isSubmitting={isSubmitting}
            handleChange={handleChange}
            values={values}
            isRequired={true}
            onBlur={handleBlur}
          />
        </Col>
        <Col className='mt-2' sm={6}>
          <div className='password-strength px-3 py-3'>
            <label className='font-weight-bold'>{t('password_strength')}</label>
            <ProgressBar
              now={values.password ? values.password.length : 0}
              max={16}
              min={4}
              />
            <div className='font-size-14'>{t('password_strength_message')}</div>
          </div>
        </Col>
        <Col className='mt-4' sm={12}>
          <LaddaButton
            type='submit'
            text={t('save')}
            className='w-100 submit my-3 py-2 btn-primary font-size-18 font-weight-600'
            disabled={isSubmitting}
            onClick={() => { this.toggleCountryDropdownErrorMessage(values.country) }}
          />
        </Col>
      </Row>
    </Form>
  }

  render() {
    const t = this.context.t
    const { userDataState } = this.state
    if(isEmpty(userDataState)) return <Fragment>Loading ... </Fragment>

    const { email, first_name, last_name } = userDataState
    const schemaValidation = buildSchema(this.props.i18nState.lang, t)
    const initialValues = {
      email: email,
      first_name: first_name,
      last_name: last_name,
      birthday: '',
      password: '',
      confirm_password: '',
      country: '',
    }

    return (
      <div className='setup-account-container'>
        <div className='setup-account-content'>
          <h4 className='header-tile'>
            {t('setup_account_title')}
          </h4>
          <div className='form-content'>
            <Formik
              component={this.privateForm}
              validationSchema={schemaValidation}
              onSubmit={this.onSubmit.bind(this)}
              initialValues={initialValues}
            />
          </div>
        </div>
      </div>

    )
  }
}

SetupAccountPage.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
})

const mapDispatchToProps = {
  signInUser
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(SetupAccountPage)
