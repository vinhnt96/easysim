import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import visaIcon from 'images/my_account/visa_icon.png'
import asterisksIcon from 'images/my_account/asterisks.png'

class CPaymentsCard extends Component {

  render() {
    return (
      <div className='payment-card'>
        <img src={visaIcon} alt='visa-icon' />
        <p className='card-number py-2'> <img src={asterisksIcon} alt='asterisks-icon' className='asterisks-icon' /> 1234</p>
        <p className='card-author'> JOHN DOE </p>
      </div>
    )
  }
}

CPaymentsCard.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({

})

export default connect(
  mapStoreToProps,
  null
)(CPaymentsCard)



