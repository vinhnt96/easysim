import React, { Component } from 'react'
import './UserProfilesView.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { Button } from 'react-bootstrap'
import { values, mapValues, groupBy, omit, isEqual, isEmpty } from 'lodash'
import axios from 'axios'
import { Modal } from 'react-bootstrap'
import InfiniteScroll from "react-infinite-scroll-component"
import loadable from '@loadable/component'
const ProfileModalView = loadable(() => import('./ProfileModalView/ProfileModalView'))
const UserProfilesTable = loadable(() => import('./UserProfilesTable/UserProfilesTable'))

class CUserProfilesView extends Component {

  constructor(props) {
    super(props)
    this.state = {
      user: {},
      searchValue: '',
      searchData: [],
      open: false,
      isConfirm: false,
      detailUser: false,
      loading: false,
      openConfirm: false,
      isIp: false,
      activityLog: false,
      role: {},
      sort: '-id',
      sortColumn: 'id',
      isDefaultDirection: false,
      activities: [],
      page: 1,
      per_page: 10,
      totalPages: 0
    }
  }

  handleSortColumn(column, asc) {
    switch(column) {
      case 'user_email':
        return asc ? 'email' : '-email'
      case 'api_key':
        return asc ? 'api_key' : '-api_key'
      case 'role':
        return asc ? 'role' : '-role'
      case 'date_created':
        return asc ? 'created_at' : '-created_at'
      default:
        return asc ? 'id' : '-id'
    }
  }

  handlePageClick = () => {
    this.props.setPage(this.props.currentPage)
  };

  openModalUpdate = (user) => {
    this.setState({ open: true, user, isConfirm: false, isIp: false, activityLog: false, detailUser: true })
  }

  openModalConfirm = (user) => {
    this.setState({ open: true, user, isConfirm: true, isIp: false, activityLog: false, detailUser: false })
  }

  openModalIpUser = (user) => {
    this.setState({ open: true, user, isIp: true, isConfirm: false, activityLog: false, detailUser: false })
  }

  handleOpenActivityLog = (user) => {
    this.handleGetActivities(user)
    this.setState({ open: true, user, activityLog: true, isIp: false, isConfirm: false, detailUser: false })
  }

  handleGetActivities(user) {
    this.setState({ loading: true })
    const { page, per_page } = this.state
    const payload = {
      user_id: user.id,
      page,
      per_page
    }
    axios.get('admin/activities', { params: payload }).then((resp) => {
      const { data: { activities }, pages } = resp
      const activitiesVal = values(mapValues(groupBy(activities, "begin_date"), (clist => clist.map(car => omit(car, 'begin_date')))))
      this.setState({ open: true, loading: false, activities: activitiesVal, totalPages: pages || 0 })
    }).catch(error => {
      this.setState({ open: false, loading: false, activities: [] })
    })
  }

  handleUpdateUser = (user) => {
    this.props.refeshUsers()
  }

  handleDeleteUser = (user) => {
    this.setState({ loading: true })
    const url = `/admin/users/${user.id}`
    axios.delete(url).then((res) => {
      this.props.refeshUsers()
      this.setState({ open: false, loading: false })
    }).catch(error => {
      this.setState({ open: false, loading: false })
    })
  }

  handleSearch = (value) => {
    if(this.state.searchValue === value) return
    this.setState({ searchValue: value })
  }

  handleClickColumnHeader(column) {
    if (isEqual(column, 'action'))
      return null
    const { sortColumn, isDefaultDirection } = this.state
    let sort = ''
    const obj = { sortColumn: column }
    if(sortColumn === column) {
      sort = this.handleSortColumn(column, !isDefaultDirection)
      obj['isDefaultDirection'] = !isDefaultDirection
      obj['sort'] = sort
    }
    else {
      sort = this.handleSortColumn(column, true)
      obj['isDefaultDirection'] = true
      obj['sort'] = sort
    }
    this.setState(obj, () => {
      this.props.refeshUsers(sort)
    })
  }

  handleUpdateRoleUser = (user, role) => {
    this.setState({ role, user, openConfirm: true })
  }

  handleSetPage = ({ selected }) => {
    const { user } = this.state
    this.setState({ page: parseInt(selected) + 1 }, () => {
      this.handleGetActivities(user)
    })
  }

  acceptUpdateRoleUser = () => {
    this.updateUser()
  }

  handleUpdateFullAccess = (user, item) => {
    let accessibility_game_type = ''
    switch (item) {
      case 'ultimate': accessibility_game_type = 'cash-mtts'; break
      case 'none': accessibility_game_type = 'free'; break
      default: accessibility_game_type = item
    }

    const payload = { accessibility_game_type }
    window.axios.put(`/admin/users/${user.id}`, payload).then(({ data }) => {
      const { user } = data
      this.props.updateData(user)
    }).catch(error => console.log('update full access for user failed with error: ',error))
  }

  handleUpdateAffiliate = (user, affiliateCode) => {
    this.setState({ user }, () => this.updateUser(affiliateCode))
  }

  updateUser(affiliateCode) {
    const { user, role } = this.state
    const payload = isEmpty(affiliateCode) ? { role_id: role.id } : { affiliate_code: affiliateCode }
    const url = `/admin/users/${user.id}`;
    this.setState({ loading: true })
    axios.put(url, payload).then(({ data }) => {
      const { user } = data;
      this.props.updateData(user)
      this.setState({ loading: false, openConfirm: false })
    }).catch(error => {
      this.setState({ loading: false, openConfirm: false })
      console.log('update role user failed with error: ',error)
    })
  }

  renderPopupConfirmUpdateRole() {
    const { openConfirm, loading } = this.state
    const t = this.context.t
    const classNameDefault = 'confirm-role-modal d-flex flex-column justify-content-center'
    if (!openConfirm)
      return null
    return (
      <Modal
        show={openConfirm}
        onHide={() => this.setState({ openConfirm: false })}
        dialogClassName={classNameDefault}
      >
      <Modal.Body className="mt-2">Are you sure you wanted to change role for this user?</Modal.Body>
      <Modal.Footer>
        <Button
          variant="secondary"
          onClick={() => this.setState({ openConfirm: false })}
        >
          {t('cancel')}
        </Button>
        <Button
          variant="primary"
          onClick={this.acceptUpdateRoleUser}
          disabled={loading}
        >
          {t('button.ok')}
        </Button>
      </Modal.Footer>
    </Modal>
    )
  }

  renderShowModal() {
    const { open, user, isConfirm, loading, openConfirm, isIp, activityLog, detailUser, activities, totalPages, page } = this.state

    if (open) {
      return (
        <ProfileModalView
          loading={loading}
          isConfirm={isConfirm}
          isIp={isIp}
          open={open}
          user={user}
          detailUser={detailUser}
          activityLog={activityLog}
          activities={activities}
          totalPages={totalPages}
          currentPage={page}
          setPage={this.handleSetPage}
          onClose={() => this.setState({open: false})}
          handleUpdateUser={this.handleUpdateUser}
          handleDeleteUser={this.handleDeleteUser}
        />
      )
    }

    if (openConfirm) {
      return this.renderPopupConfirmUpdateRole()
    }
  }

  render() {
    const { users, refeshUsers, hasMore, handleSearch } = this.props;
    const { searchValue, isDefaultDirection, sortColumn, sort } = this.state;
    const t = this.context.t
    const columnHeader = [
      t('user_profiles.id'),
      t('user_profiles.user_email'),
      t('user_profiles.api_key'),
      t('user_profiles.role'),
      t('user_profiles.affiliates'),
      t('user_profiles.date_created'),
      t('user_profiles.full_access'),
      t('user_profiles.action')
    ]
    return (
      <div>
        { this.renderShowModal() }
        <div className='user-profiles-view-container'>
          <div className='header d-flex align-items-center'>
            {t('user_profiles.title')}
          </div>
          <div className='form-search'>
            <div className='form-inline form-group search-input row'>
              <input className='form-control col-10 input-search' type='text' value={searchValue}
                     onChange={(e) => this.handleSearch(e.target.value)} placeholder={t('user_profiles.search_user')} />
              <FontAwesomeIcon icon={faSearch} className='col-2' onClick={() => handleSearch(searchValue)}/>
            </div>
          </div>
          <div className='content-body'>
            <div className='table-container'>
              <InfiniteScroll
                dataLength={users.length}
                next={this.handlePageClick}
                hasMore={hasMore}
                height={700}
                loader={<h4>Loading...</h4>}
                endMessage={
                  <p style={{ textAlign: "center" }}>
                    <b>Yay! You have seen it all</b>
                  </p>
                }
              >
                <table className='user-profiles-table w-100 text-center'>
                  <thead>
                    <tr>
                      {
                        columnHeader.map( (header,idx) => {
                          const column = header.toLowerCase().replaceAll(' ', '_').replaceAll('-', '_')
                          const className = `${isEqual(sortColumn, column) && !isEqual(column, 'action') ? (isDefaultDirection ? 'active sorted-desc' : 'active sorted-asc') : ''} no-select `
                          return (
                            <th
                              key = {idx + header}
                              onClick={() => this.handleClickColumnHeader(column)}
                              className={className}
                            >
                              {header}
                            </th>
                          )
                      })}
                    </tr>
                  </thead>
                  <tbody>
                    <UserProfilesTable
                      users={users}
                      refeshUsers={refeshUsers}
                      sort={sort}
                      openModalUpdate={this.openModalUpdate}
                      openModalConfirm={this.openModalConfirm}
                      openModalIp={this.openModalIpUser}
                      confirmRole={this.handleUpdateRoleUser}
                      activityLog={this.handleOpenActivityLog}
                      updateAffiliate={this.handleUpdateAffiliate}
                      handleUpdateAccessUser={this.handleUpdateFullAccess}
                    />
                  </tbody>
                </table>
              </InfiniteScroll>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CUserProfilesView.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({

})

export default connect(
  mapStoreToProps,
  null
)(CUserProfilesView)
