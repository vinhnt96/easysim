import React, { Component } from 'react'
import './UserProfilesTable.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { ButtonGroup, Button } from 'react-bootstrap'
import { DropdownToggle, DropdownMenu, DropdownItem, UncontrolledButtonDropdown } from 'reactstrap';
import{ isEmpty, keys, head }  from 'lodash'
import axios from 'axios'
import { notify } from 'utils/utils'
import { DISCOUNT_CODES_OBJECT, SUBSRIPTIONS_OBJECT } from 'config/constants'

class CUserProfilesTable extends Component {

  openModalUpdate(user) {
    this.props.openModalUpdate(user)
  }

  openModalConfirm(user) {
    this.props.openModalConfirm(user)
  }

  handleUpdateRoleUser(user, role) {
    this.props.confirmRole(user, role)
  }

  handleOpenModalIp(user) {
    this.props.openModalIp(user)
  }

  handleOpenActivityLog(user) {
    this.props.activityLog(user)
  }

  handleResentEmail(user) {
    const url = `/admin/users/${user.id}/resent_confirm_email`;
    import('react-toastify').then(reactToastify => {
      axios.post(url).then(res => {
        notify("Resend email successfully", reactToastify.toast.success);
      }).catch(error => notify("Failed to resend email. Please try again!", reactToastify.toast.error))
    })
  }

  renderAffiliate(code) {
    if (isEmpty(code) || ['no_affiliate', 'launch35'].includes(code)) {
      return 'No Affiliate'
    } else {
      return DISCOUNT_CODES_OBJECT[code]
    }
  }

  render() {
    const t = this.context.t
    const { roles, users } = this.props;

    return users.map((user, idx) => {
      const { email, created_at, role_id, api_key, email_confirmed, affiliate_code, role: roleName, admin_access } = user;
      const dateCreated = new Date(created_at).toISOString().split('T')[0];
      const role = head(roles.filter(item => role_id === item.id))
      const rolesFilter = roles.filter(i => i.name !== 'free_user')
      const freeUser = roleName === 'free_user'

      return (
        <tr key={idx}>
          <td>{ user.id }</td>
          <td className='align-email'>{ email }</td>
          <td>{ api_key }</td>
          <td>
            <UncontrolledButtonDropdown className='role-btn' {...freeUser && {isOpen: false}}>
              <DropdownToggle className='py-2 d-flex align-items-center text-white font-weight-400 role-title' caret>
                <div className='role-truncated'>
                  { freeUser ? t('user_profiles.free_user') : role.title }
                </div>
              </DropdownToggle>
              <DropdownMenu className='w-100 roles-menu-dropdown'>
                { rolesFilter.map((item, index) => {
                  return <DropdownItem key={index} className='text-white font-weight-bold w-100 roles-dropdown' onClick={() => this.handleUpdateRoleUser(user, item)}>
                     {item.title}
                  </DropdownItem>
                  })
                }
              </DropdownMenu>
            </UncontrolledButtonDropdown>
          </td>
          <td>
            <UncontrolledButtonDropdown className='affiliate-btn' >
              <DropdownToggle className='py-2 d-flex align-items-center text-white font-weight-400 affiliate-title' caret>
                <div className='affiliate-truncated'>
                  { this.renderAffiliate(affiliate_code) }
                </div>
              </DropdownToggle>
              <DropdownMenu className='w-100 affiliate-menu-dropdown'>
                { keys(DISCOUNT_CODES_OBJECT).map((key, index) => {
                  return <DropdownItem key={index} className='text-white font-weight-bold w-100 affiliate-dropdown' onClick={() => this.props.updateAffiliate(user, key)}>
                     {DISCOUNT_CODES_OBJECT[key]}
                  </DropdownItem>
                  })
                }
              </DropdownMenu>
            </UncontrolledButtonDropdown>
          </td>
          <td>{ dateCreated }</td>
          <td>
            <UncontrolledButtonDropdown className='access-btn'>
              <DropdownToggle className='py-2 d-flex align-items-center text-white font-weight-400 access-title' caret>
                <div className='access-display'>
                  {isEmpty(admin_access) ? '' : admin_access === 'cash-mtts' ? SUBSRIPTIONS_OBJECT['ultimate'] : SUBSRIPTIONS_OBJECT[admin_access] }
                </div>
              </DropdownToggle>
              <DropdownMenu className='w-100 access-menu-dropdown'>
                { keys(SUBSRIPTIONS_OBJECT).map((key, index) => {
                  return <DropdownItem key={index} className='text-white font-weight-bold w-100 access-dropdown' onClick={() => this.props.handleUpdateAccessUser(user, key)}>
                     {SUBSRIPTIONS_OBJECT[key]}
                  </DropdownItem>
                  })
                }
              </DropdownMenu>
            </UncontrolledButtonDropdown>
          </td>
          <td>
            <ButtonGroup className='btn-actions'>
              <Button className='ml-2 btn-edit' onClick={() => this.openModalUpdate(user)}>
                {t('edit')}
              </Button>
              <Button className='ml-2 btn-delete' onClick={() => this.openModalConfirm(user)}>
                {t('delete')}
              </Button>
              {!email_confirmed &&
                <Button className='ml-2 mr-2' onClick={() => this.handleResentEmail(user)}>
                  {t('user_profiles.resent')}
                </Button>
              }
              {email_confirmed &&
                <Button className='ml-2' onClick={() => this.handleOpenActivityLog(user)}>
                  {t('user_profiles.activity_log')}
                </Button>
              }
            </ButtonGroup>
          </td>
          <td className='ip-history-btn'>
            <Button onClick={() => this.handleOpenModalIp(user)} > {t('subscription.ip')} </Button>
          </td>
        </tr>
      )
    })
  }
}

CUserProfilesTable.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  roles: store.roles
})

export default connect(
  mapStoreToProps,
  null
)(CUserProfilesTable)
