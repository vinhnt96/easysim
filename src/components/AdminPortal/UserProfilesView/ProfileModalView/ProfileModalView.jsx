import React, { Component, Fragment } from 'react'
import './ProfileModalView.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Modal, ButtonGroup, Button } from 'react-bootstrap'
import moment from 'moment'
import ReactPaginate from 'react-paginate'
import { isEmpty, union } from 'lodash'
import loadable from '@loadable/component'
const DetailsPage = loadable(() => import('../../../MyAccount/DetailsPage/DetailsPage'))
const SimsInfoList = loadable(() => import('components/MySim/SimInfoList/SimInfoList'))

class CProfileModalView extends Component {

  constructor(props) {
    super(props)
    this.state = {
      tableSims: false,
      sims: []
    }
  }

  handleUpdateUser = (user) => {
    this.props.handleUpdateUser(user)
    this.props.onClose()
  }

  handleDelete = (user) => {
    this.props.handleDeleteUser(user)
  }

  renderConfirmBody() {
    const t = this.context.t
    const { user, onClose, loading } = this.props
    return (
      <div>
        <div>{t('modal_confirm.title')}</div>
        <ButtonGroup className='btn-actions mt-3 mb-2'>
          <Button
            className='mr-2'
            onClick={onClose}
          >
            {t('cancel')}
          </Button>
          <Button
            className='ml-2 btn-delete'
            onClick={() => this.handleDelete(user)}
            disabled={loading}
          >
            {t('confirm_diaglog.yes')}
          </Button>
        </ButtonGroup>
      </div>
    )
  }

  getDifferenceInMinutes(dates) {
    // get total seconds between the times
    let delta = 0
    dates.forEach(i => { delta += i.total_time_spent_logged })

    // calculate (and subtract) whole days
    const days = Math.floor(delta / 86400);
    delta -= days * 86400;

    // calculate (and subtract) whole hours
    const hours = Math.floor(delta / 3600) % 24;
    delta -= hours * 3600;

    // calculate (and subtract) whole minutes
    const minutes = Math.floor(delta / 60) % 60;
    delta -= minutes * 60;

    let spentTime = `${minutes}mins`
    if (hours > 0)
      spentTime = `${hours}hrs `.concat(spentTime)
    if (days > 0)
      spentTime = `${days}days `.concat(spentTime)

    return spentTime
  }

  simIds(activities) {
    let sims = activities[0].sim_ids
    activities.forEach((i, idx) => {
      if (idx > 0) {
        sims = union(sims, i.sim_ids)
      }
    })
    return sims
  }

  getSimsInfo(simIds, id){
    const params = {
      sim_ids: simIds || [],
      user_id: id,
      page: 1,
      per_page: 200
    }

    window.axios.get(`/my_sims`, {params}).then(res => {
      const { data } = res
      this.setState({
        tableSims: true,
        sims: data.user_strategy_selections
      })
    })
    .catch(err => console.log(err))
  }

  handlePageClick = (pageData) => {
    this.props.setPage(pageData)
  }

  handleNewTabSims = (sims) => {
    this.getSimsInfo(sims, this.props.user.id)
  }


  onCloseModal = () => {
    if (this.state.tableSims) {
      this.setState({tableSims: false, sims: []})
    } else {
      this.props.onClose()
    }
  }

  renderIpUserBody() {
    const t = this.context.t
    const { user } = this.props
    const { device, browser, current_signin_ip_v4_at, country_login, online } = user
    const time = isEmpty(current_signin_ip_v4_at) ? '' : `${moment(current_signin_ip_v4_at).format('YYYY-MM-DD')} at ${moment(current_signin_ip_v4_at).format('HH:mm')}`
    return (
      <div>
        <h4>{user.email}</h4>

        <div>{`${isEmpty(device) ? '' : device} ${isEmpty(country_login) ? online ? 'Undetected location' : '' : `: ${t(`countries.${country_login.toLowerCase()}`)}`}`}</div>
        <div>{`${isEmpty(browser) ? '' : `${browser}: `} ${online ? 'Active ' : ''} ${time}`}</div>
      </div>
    )
  }

  renderPaginate() {
    const { totalPages, currentPage } = this.props;
    if (totalPages <= 1)
      return null
    return (
      <div className='pagination justify-content-center'>
        <ReactPaginate
          previousLabel={'previous'}
          nextLabel={'next'}
          breakLabel={'...'}
          breakClassName={'break-me'}
          pageCount={totalPages}
          marginPagesDisplayed={3}
          pageRangeDisplayed={5}
          onPageChange={this.handlePageClick}
          containerClassName={'pagination'}
          subContainerClassName={'pages pagination'}
          pageClassName={'page-item'}
          nextClassName={'page-item page-next text-uppercase'}
          previousClassName={'page-item page-previous text-uppercase'}
          activeClassName={'active'}
          forcePage={currentPage > 1 ? currentPage - 1 : 0} />
      </div>
    )
  }

  renderTableSims() {
    const t = this.context.t
    const columnHeader = [
      t('mysim.board'), t('mysim.positions'), t('mysim.stack_size'),t('mysim.pot_type'),t('mysim.players'),
      t('mysim.game_type'),t('mysim.notes'),t('mysim.last_date_view')
    ]

    return (
      <div className='my-sim-container modal-detail'>
        <div className='content-body'>
          <div className='table-container'>
            <table className='my-sim-table w-100 text-center'>
              <thead>
                <tr>
                  {
                    columnHeader.map( (header,idx) => {
                      return (
                        <th
                          key = {idx + header}
                          className="no-select"
                        >
                          {header}
                        </th>
                      )
                    })}
                    <th></th>
                </tr>
              </thead>
              <tbody>
                <SimsInfoList
                  data={this.state.sims}
                  justRead
                />
              </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }

  renderTableActivities() {
    const t = this.context.t
    const { activities } = this.props
    return (
      <Fragment>
        <table className='activity-login-table w-100 text-center'>
          <thead>
            <tr>
              <th>{t('activity.date')}</th>
              <th>{t('activity.numberLog')}</th>
              <th>{t('activity.numberOpened')}</th>
              <th>{t('activity.timeSpent')}</th>
            </tr>
          </thead>
          <tbody>
            {
              activities && activities.map((items, index) => {
                const date = moment(items[0].logged_time).format('YYYY-MM-DD')
                const sims = this.simIds(items)
                return (
                  <tr key={index}>
                    <td>{date}</td>
                    <td>{items.length}</td>
                    <td><Button variant="link" onClick={() => this.handleNewTabSims(sims)}>{sims.length}</Button></td>
                    <td>{this.getDifferenceInMinutes(items)}</td>
                  </tr>
                )
              })
            }
          </tbody>
        </table>
        {this.renderPaginate()}
      </Fragment>
    )
  }

  renderActivityLog() {
    const { user } = this.props
    const { tableSims } = this.state
    return (
      <div>
        <h4 className='mb-4'>{user.email}</h4>
          { tableSims && this.renderTableSims() }
          { !tableSims && this.renderTableActivities() }
      </div>
    )
  }

  render() {
    const { open, user, isConfirm, isIp, activityLog, detailUser } = this.props;
    const classNameDefault = `profile-modal d-flex flex-column justify-content-center ${activityLog ? 'activity-login-container' : ''}`
    const className = isConfirm ? classNameDefault.concat(' confirm') : classNameDefault
    const headerClass = isConfirm ? 'mb-2': isIp || activityLog ? '' : 'mb-4'

    return (
      <Modal
        show={open}
        onHide={this.onCloseModal}
        dialogClassName={this.state.tableSims ? className.concat(' table-sims') : className}
        aria-labelledby='profile-modal'
      >
        <Modal.Body>
          <Modal.Header bsPrefix={`custom-modal-header ${headerClass}`} closeButton={!isConfirm}>
            <Modal.Title></Modal.Title>
          </Modal.Header>
          { isConfirm && this.renderConfirmBody() }
          { detailUser && <DetailsPage
              user={user}
              handleUpdateUser={this.handleUpdateUser}
            />
          }
          { isIp && this.renderIpUserBody() }
          { activityLog && this.renderActivityLog() }
        </Modal.Body>
      </Modal>
    )
  }
}

CProfileModalView.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
})

export default connect(
  mapStoreToProps,
  null
)(CProfileModalView)
