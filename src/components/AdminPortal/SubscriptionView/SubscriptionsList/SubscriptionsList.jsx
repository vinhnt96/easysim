import React, { Component } from 'react'
import './SubscriptionsList.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { capitalize, isEmpty, keys } from 'lodash'
import { DropdownToggle, DropdownMenu, DropdownItem, UncontrolledButtonDropdown } from 'reactstrap';
import { currencyFormat } from 'services/game_play_service'
import { SUBSRIPTIONS_OBJECT } from 'config/constants'

class CSubscriptionsList extends Component {

  render() {
    let subscriptionsData = this.props.data || []

    return subscriptionsData.map((subscription, idx) => {
      const { start_time, renew_time, id, email, active_subscription, paid, user_id, price_discount, admin_access } = subscription
      const dateCreated = new Date(start_time).toISOString().split('T')[0]
      const renewalDate = new Date(renew_time).toISOString().split('T')[0]
      const user = {id: user_id, email: email}
      const number = idx + 1
      const numberId = number < 10 ? `0${number}` : number
      const statusSub = paid ? 'paid' : 'failed'
      return (
        <tr key={id}>
          <td> {numberId} </td>
          <td> {email} </td>
          <td> {active_subscription} </td>
          <td> {currencyFormat(price_discount)} </td>
          <td> {dateCreated} </td>
          <td className="status-sub">
            <div className={statusSub}>{capitalize(statusSub)}</div>
          </td>
          <td> {paid && renewalDate} </td>
          <td>
            <UncontrolledButtonDropdown className='access-btn'>
              <DropdownToggle className='py-2 d-flex align-items-center text-white font-weight-400 access-title' caret>
                <div className='access-display'>
                  { isEmpty(admin_access) ? '' : admin_access === 'cash-mtts' ? SUBSRIPTIONS_OBJECT['ultimate'] : SUBSRIPTIONS_OBJECT[admin_access] }
                </div>
              </DropdownToggle>
              <DropdownMenu className='w-100 access-menu-dropdown'>
                { keys(SUBSRIPTIONS_OBJECT).map((key, index) => {
                  return <DropdownItem key={index} className='text-white font-weight-bold w-100 access-dropdown' onClick={() => this.props.handleUpdateAccessUser(user, key)}>
                     {SUBSRIPTIONS_OBJECT[key]}
                  </DropdownItem>
                  })
                }
              </DropdownMenu>
            </UncontrolledButtonDropdown>
          </td>
        </tr>
      )
    })
  }
}


CSubscriptionsList.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CSubscriptionsList)
