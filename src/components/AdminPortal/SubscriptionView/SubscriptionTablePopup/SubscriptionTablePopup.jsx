import React, { Component, Fragment } from 'react'
import './SubscriptionTablePopup.scss'
import {connect} from 'react-redux'
import PropTypes from 'prop-types'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faWindowClose } from '@fortawesome/free-solid-svg-icons'
import ReactPaginate from 'react-paginate'

class SubscriptionTablePopup extends Component {

  renderPaginate() {
    if(this.props.ipHistories.totalPages < 2)
    return <Fragment> </Fragment>

    return (
      <ReactPaginate
        previousLabel={'previous'}
        nextLabel={'next'}
        breakLabel={'...'}
        breakClassName={'break-me'}
        pageCount={this.props.ipHistories.totalPages}
        marginPagesDisplayed={3}
        pageRangeDisplayed={5}
        onPageChange={this.props.handlePageClick}
        containerClassName={'pagination'}
        subContainerClassName={'pages pagination'}
        pageClassName={'page-item'}
        nextClassName={'page-item page-next text-uppercase'}
        previousClassName={'page-item page-previous text-uppercase'}
        activeClassName={'active'} />
    )
  }

  renderConfirmFullAccessView() {
    let t = this.context.t
    let message = this.props.popupProp.type === 'remove' ? t('subscription.remove_full_access_message') : t('subscription.set_full_access_message')
    
    return (
      <Fragment>
        <div className='popup-title d-flex align-items-center'>
          {t('subscription.confirm_popup_title')}
        </div>       
        <div className='popup-content'>
          <div className='confirm-full-access'>
            <p> {message} </p>
          </div>
          <div className='group-btn'>
            <div className='row'>
              <div className='col-6'>
                <div className='btn-primary' onClick={() => this.props.handleChangeFullAccess()}>OK</div>
              </div>
              <div className='col-6'>
                <div className='btn-primary' onClick={() => this.props.setShowSubscriptionTablePopup()}>Cancel</div>
              </div>
            </div>
          </div>
        </div>  
      </Fragment>
    )
  }

  renderIpHistoryList() {
    return this.props.ipHistories.data.map( ipItem => {
      let loginTime = new Date(ipItem.created_at).toISOString().replace('T','--').replace('Z',' ')

      return (
        <tr key={ipItem.id}>
          <td> {ipItem.id} </td>
          <td> {ipItem.ip_info} </td>
          <td> {loginTime} </td>
        </tr>
      )
    })
  }
  
  renderIpHistoryView() {
    let t = this.context.t
    let columnHeader=[t('subscription.no'),t('ip'),t('login_time')]

    return (
      <div className='ip-history-view-container'>
        <div className='header d-flex align-items-center'>
          {this.props.user.email}
        </div>
        <div className='content-body'>
          <div className='table-container'>
            <table className='ip-history-table w-100 text-center'>
              <thead>
                <tr>
                  {
                    columnHeader.map( (header,idx) => {
                      return (
                        <th key = {idx + header} className='no-select'>
                          {header}
                        </th>
                      )
                  })}
                </tr>
              </thead>
              <tbody>
                  {this.renderIpHistoryList()}
              </tbody>
            </table>
          </div>
          {
            <div className='pagination justify-content-center'>
              {this.renderPaginate()}
            </div>
          }
        </div>
      </div>
    )
  }

  renderPopupContent() {
    return this.props.popupProp.view === 'confirm-view' ? this.renderConfirmFullAccessView() : this.renderIpHistoryView()
  }

  render() {

    return (
      <Fragment>
        <div className={`${!!this.props.popupProp.isShowing ? 'show' : '' } subscription-table-popup-container hide-scrollbar`}>
          <div className={`subscription-table-popup ${this.props.popupProp.view} fadeIn center no-scroll`}>
            { this.renderPopupContent() }
            <FontAwesomeIcon className='btn-close' icon={faWindowClose} onClick={() => this.props.setShowSubscriptionTablePopup()} />
          </div>
          <div className='popup-overlay' onClick={() => this.props.setShowSubscriptionTablePopup()}>  </div>
        </div>
      </Fragment>
    );
  }
}

SubscriptionTablePopup.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(SubscriptionTablePopup)
