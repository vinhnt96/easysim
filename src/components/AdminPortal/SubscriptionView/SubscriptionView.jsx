import React, { Component } from 'react'
import './SubscriptionsView.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import SubscriptionsList from './SubscriptionsList/SubscriptionsList'
import { DEFAULT_SUBSCRIPTION_ITEMS_PER_PAGE } from 'config/constants'
import { handleSortColumn } from 'services/subscription_table_service'
import InfiniteScroll from "react-infinite-scroll-component";

class CSubscriptionsView extends Component {

  constructor(props) {
    super(props)

    this.state = {
      subscriptionsData: [],
      user: {},
      searchTempValue: '',
      searchValue: '',
      activePage: 0,
      totalPages: 0,
      sortColumn: '',
      isDefaultDirection: true,
      hasMore: true,
      hideFailed: false
    }

    this.handleClickColumnHeader = this.handleClickColumnHeader.bind(this)
    this.handleOnChangeSearchValue = this.handleOnChangeSearchValue.bind(this)
    this.loadSubscriptionsFromServer = this.loadSubscriptionsFromServer.bind(this)
    this.toggleHideFailed = this.toggleHideFailed.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.handlePageClick = this.handlePageClick.bind(this)
  }

  componentDidMount() {
    this.loadSubscriptionsFromServer()
  }

  handlePageClick(pageData) {
    this.setState({activePage: parseInt(pageData.selected + 1)}, () => { 
      this.loadSubscriptionsFromServer()
    })
  }

  toggleHideFailed() {
    const hideFailed = !this.state.hideFailed
    let params = { page: 1, per_page: DEFAULT_SUBSCRIPTION_ITEMS_PER_PAGE, search: this.state.searchValue, status: hideFailed ? 'active' : '' }
    window.axios.get('/admin/subscriptions', {params: params}).then( res => {
      let { page, pages, data } = res
      const hasMore = page < pages
      this.setState({ subscriptionsData: data.subscriptions, activePage: 0, totalPages: pages, hasMore: hasMore, hideFailed: hideFailed })
    }).catch( err => { console.error(err) })
    
  }

  loadSubscriptionsFromServer() {
    const { activePage, searchValue, hideFailed } = this.state
    let params = { page: activePage + 1, per_page: DEFAULT_SUBSCRIPTION_ITEMS_PER_PAGE, search: searchValue, status: hideFailed ? 'active' : ''  }
    window.axios.get('/admin/subscriptions', {params: params}).then( res => {
      let { page, pages, data } = res
      const subscriptionsData = this.state.subscriptionsData.concat(data.subscriptions)
      const hasMore = page < pages
      this.setState({ subscriptionsData: subscriptionsData, totalPages: pages, hasMore: hasMore })
    })
    .catch( err => { console.error(err) })
  }

  handleSearch() {
    const { searchTempValue, hideFailed } = this.state
    let params = { page: 1, per_page: DEFAULT_SUBSCRIPTION_ITEMS_PER_PAGE, search: searchTempValue, status: hideFailed ? 'active' : ''  }
    window.axios.get('/admin/subscriptions', {params: params}).then( res => {
      let { page, pages, data } = res
      const hasMore = page < pages
      this.setState({ subscriptionsData: data.subscriptions, activePage: 0, totalPages: pages, hasMore: hasMore, searchValue: searchTempValue })
    })
    .catch( err => { console.error(err) })
  }
  
  handleClickColumnHeader(column){
    if(this.state.sortColumn === column) {
      this.setState({isDefaultDirection: !this.state.isDefaultDirection})
      this.state.subscriptionsData.sort(handleSortColumn(column, !this.state.isDefaultDirection))
    }
    else {
      this.setState({sortColumn: column, isDefaultDirection: true})
      this.state.subscriptionsData.sort(handleSortColumn(column, true))
    }
  }

  handleOnChangeSearchValue(e){
    this.setState({searchTempValue: e.target.value })
  }

  handleChangeFullAccess = (user, item) => {
    let accessibility_game_type = ''
    switch (item) {
      case 'ultimate': accessibility_game_type = 'cash-mtts'; break
      case 'none': accessibility_game_type = 'free'; break
      default: accessibility_game_type = item
    }

    const payload = { accessibility_game_type }
    window.axios.put(`/admin/users/${user.id}`, payload).then( res => {
      const { subscriptionsData } = this.state
      const subscriptionsDataClone = [...subscriptionsData]
      const subscriptionsDataFilter = subscriptionsData.filter(i =>  i.user_id === user.id )
      subscriptionsDataFilter.forEach(i => {
        const index = subscriptionsData.indexOf(i)
        i['admin_access'] = item === 'ultimate' ? 'cash-mtts' : item
        subscriptionsDataClone[index] = i
      })
      this.setState({ subscriptionsData: subscriptionsDataClone })
    }).catch(error => console.log('update full access for user failed with error: ',error))
  }

  render(){
    let t = this.context.t
    let columnHeader = [
      t('subscription.no'), t('email'), t('subscription.sub'), t('subscription.amount'), t('subscription.date_create'), t('subscription.status'),
      t('subscription.next_payment'),t('subscription.full_access')
    ]

    return (
      <div className='subscription-view-container'>
        <div className='header d-flex align-items-center'>
          {t('subscription.table')}          
        </div>
        <div className='form-search d-flex justify-content-between'>
          <div className='form-inline form-group search-input row'>
            <input className='form-control col-10 input-search' type='text' value={this.state.searchTempValue}
                    onChange={this.handleOnChangeSearchValue} placeholder={t('subscription.search_for_subscription')} />
            <FontAwesomeIcon icon={faSearch} className='col-2' onClick={this.handleSearch}/>
          </div>
          <button
            onClick={() => {this.toggleHideFailed()}}
            className={`btn hide-failed-button ${this.state.hideFailed ? 'selected' : ''}`}
          >
            Hide failed
          </button>
        </div>
        <div className='content-body'>
          <div className='table-container'>
            <InfiniteScroll
              dataLength={this.state.subscriptionsData.length}
              next={() => this.handlePageClick({selected: this.state.activePage})}
              hasMore={this.state.hasMore}
              height={700}
              loader={<h4>Loading...</h4>}
              endMessage={
                <p style={{ textAlign: "center" }}>
                  <b>Yay! You have seen it all</b>
                </p>
              }
            >
              <table className='subscription-table w-100 text-center'>
                <thead>
                  <tr>
                    {
                      columnHeader.map((header, idx) => {
                        let sortColumn = header.toLowerCase().replaceAll(' ', '_').replaceAll('-', '_')
                        return (
                          <th key={idx + header}
                              onClick={() => this.handleClickColumnHeader(sortColumn)}
                              className={`${this.state.sortColumn === sortColumn ? (this.state.isDefaultDirection ? 'active sorted-desc' : 'active sorted-asc') : ''} no-select `}>
                            {header}
                          </th>
                        )
                    })}
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  <SubscriptionsList
                    data={this.state.subscriptionsData}
                    handleUpdateAccessUser={this.handleChangeFullAccess}
                  />
                </tbody>
              </table>
            </InfiniteScroll>
          </div>
        </div>
      </div>
    )
  }

}

CSubscriptionsView.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes
})

export default connect(
  mapStoreToProps,
  null
)(CSubscriptionsView)
