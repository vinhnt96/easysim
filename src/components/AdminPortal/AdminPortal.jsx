import React, { Component } from 'react'
import './AdminPortal.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { parseInt, findIndex } from 'lodash'
import axios from 'axios'
import loadable from '@loadable/component'

const SubscriptionView = loadable(() => import('./SubscriptionView/SubscriptionView'))
const UserProfilesView = loadable(() => import('./UserProfilesView/UserProfilesView'))

class CAdminPortal extends Component {

  constructor(props) {
    super(props)

    this.state = {
      pageShowing: 'subscriptions',
      users: [],
      search: '',
      totalPages: 0,
      page: 0,
      hasMore: false
    }

    this.pages = [
      { name: 'User Profiles', value: 'user'},
      { name: 'Subscriptions', value: 'subscriptions'}
    ]

    this.handleShowPage = this.handleShowPage.bind(this)
  }

  componentDidMount() {
    this.listUsers()
  }

  handleShowPage(pageName) {
    if(this.state.pageShowing !== pageName) {
      this.setState({
        pageShowing: pageName,
        page: 0,
        users: []
      }, () => {
        if (pageName === 'user') {
          this.listUsers()
        }
      })
    }
  }

  listUsers(sort='-id') {
    const { search, page, users } = this.state;
    const payload = {
      sort,
      search: search.toLowerCase(),
      page: page + 1,
    }
    axios.get('/admin/users', { params: payload }).then((resp) => {
      const { data, pages, page } = resp
      const userData = users.concat(data.users)
      this.setState({
        users: userData,
        totalPages: pages,
        hasMore: page < pages
      })
    }).catch(error => {
      this.setState({ users: [] })
    })
  }

  refeshUsers = (sort) => {
    this.setState({
      page: 0,
      users: []
    }, () => this.listUsers(sort))
  }


  handleSetPage = (currentPage) => {
    this.setState({page: parseInt(currentPage) + 1}, () =>  this.listUsers())
  }

  handleSearch(value, sort='-id') {
    const searchValue = value.toLowerCase()
    const payload = {
      sort,
      search: searchValue,
      page: 1,
    }
    axios.get('/admin/users', { params: payload }).then((resp) => {
      const { data, pages, page } = resp
      this.setState({
        users: data.users,
        totalPages: pages,
        hasMore: page < pages,
        page: 0,
        search: searchValue
      })
    }).catch(error => {
      this.setState({ users: [] })
    })
  }

  handleUpdateData = (user) => {
    const { users } = this.state
    const usersClone = [...users]
    const index = findIndex(users, { id: user.id })
    usersClone[index] = user
    this.setState({ users: usersClone })
  }

  render() {
    if(this.props.currentUser.role !== 'admin')
      return <Redirect to='/' />
    const { pageShowing, users, totalPages, page, hasMore } = this.state;
    const t = this.context.t;
    return (
      <div className='admin-portal-container'>
        <h1 className='text-center title'> {t('admin_portal')} </h1>
        <div className='row no-gutters mb-3'>
          <div className='col-2'>
            <ul className='page-list'>
              {
                this.pages.map( (page, idx) => {
                  return (
                    <li key={idx} onClick={() => this.handleShowPage(page.value)}
                        className={`${pageShowing === page.value ? 'active' : '' }`}>
                      {page.name}
                    </li>
                  )
                })
              }
            </ul>
          </div>
          <div className='col-10'>
            <div className='admin-portal-content'>
              {pageShowing === 'user' &&
                <UserProfilesView
                  users={users}
                  refeshUsers={this.refeshUsers}
                  handleSearch={(value) => this.handleSearch(value)}
                  totalPages={totalPages}
                  setPage={this.handleSetPage}
                  currentPage={page}
                  hasMore={hasMore}
                  updateData={this.handleUpdateData}
                />
              }
              {pageShowing === 'subscriptions' &&
                <SubscriptionView />
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CAdminPortal.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes
})

export default connect(
  mapStoreToProps,
  null
)(CAdminPortal)
