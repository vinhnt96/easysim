import React, { Component } from 'react'
import './stylesheet.scss'

export default class Card extends Component {

  render() {
    let { size } = this.props
    let cardElement = size === 'big' ? this.bigCardElement() : this.smallCardElement()

    return cardElement;
  }

  bigCardElement() {
    let { className, rank, suit, index, handleOnDoubleClick, handleOnClick, miniSize } = this.props;
    let value = `${rank}${suit}`
    let cardClass = value ?  `playing-card background-size-contain ${miniSize ? 'mini-card' : ''} card-${value}` : 'empty-card'

    return (
      <span className={`${cardClass} d-inline-block ${className} big-card`}
        data-value={value}
        data-index={index}
        onDoubleClick={handleOnDoubleClick}
        onClick={handleOnClick}
        tabIndex='0'
      ></span>
    )
  }

  smallCardElement() {
    let { className, rank, suit } = this.props;

    return (
      <span className={`playing-card position-relative small-card ${className} `}>
        <p className='rank-suit font-weight-bold text-dark text-center my-0'>
          <span className='pr-1'>{rank}</span>
          <i className={`icon icon-${suit} background-size-contain`}></i>
        </p>
      </span>
    )
  }
}
