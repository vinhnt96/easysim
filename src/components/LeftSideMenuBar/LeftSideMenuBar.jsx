import React, { Component } from 'react'
import { connect } from 'react-redux'
import './LeftSideMenuBar.scss'
import Sidebar from "react-sidebar";
import { Link } from 'react-router-dom';
import { isTouchDevice } from "services/touch_device_service.js"
import {signOutUser} from 'actions/redux-token-auth'

class CLeftSideMenuBar extends Component {
  constructor() {
    super()

    this.state = {
      sidebarOpen: false,
      isTouchDevice: isTouchDevice()
    }
    this.toggleSideBar = this.toggleSideBar.bind(this);
    this.logOut = this.logOut.bind(this);

  }

  logOut(){
    this.props.signOutUser().then(() => {
      this.props.history.push('/signin')
    }).catch( () => {
    })
  }

  toggleSideBar() {
    if (this.state.sidebarOpen)
      this.props.toggleSideBarState()
    this.setState({ sidebarOpen: !this.state.sidebarOpen });
  }

  renderLeftSideBarContent() {
    return (
      <div className="left-side-bar-content">
        <ul className="list-unstyled menu-options">
          <li className={`d-flex justify-content-end align-items-center px-2 options ${this.state.sidebarOpen ? 'close-button' : 'd-none'} font-weight-bold`}>
            <Link to="#" onClick={this.toggleSideBar}>&lt;</Link>
          </li>
          <li className="d-flex align-items-center px-4 options main-options font-weight-bold"> <Link to="#">Link name</Link> </li>
          <li className="d-flex align-items-center px-4 options main-options font-weight-bold"> <Link to="#">Link name</Link> </li>
          <li className="d-flex align-items-center px-4 options main-options font-weight-bold"> <Link to="#">Link name</Link> </li>
          <li className="d-flex align-items-center px-4 options main-options font-weight-bold"> <Link to="#">Link name</Link> </li>
          <li className="d-flex flex-column px-4 pt-3 options main-options font-weight-bold">
            <Link to="#">Link name</Link>
            <ul className="d-flex flex-column list-unstyled menu-options">
              <li className="options sub-options my-2"> <Link to="#">Sublink name</Link> </li>
              <li className="options sub-options my-2"> <Link to="#">Sublink name</Link> </li>
              <li className="options sub-options my-2"> <Link to="#">Sublink name</Link> </li>
            </ul>
          </li>
          <li className="d-flex align-items-center px-4 options main-options font-weight-bold"> <Link to="#">Link name</Link> </li>
          <li className="d-flex align-items-center px-4 options main-options font-weight-bold cursor-pointer" onClick={this.logOut}> Log out </li>
        </ul>
      </div>
    )
  }

  getHeaderHeight() {
    const headerElement = document.getElementsByClassName("header clearfix")
    return headerElement[0].offsetHeight
  }

  renderMenuIcon() {
    if(!this.state.isTouchDevice)
      return ''
    return (
      <div className="burger-menu-button float-left" onClick={(e) => this.props.toggleMenuOnHover(e)}>
        <div></div>
        <div></div>
        <div></div>
      </div>
    )
  }

  render() {
    return (
      <Sidebar
        sidebar={this.renderLeftSideBarContent()}
        rootClassName={`left-side-bar-container ${!this.state.sidebarOpen && !this.state.isTouchDevice ? "left-side-bar-hidden" : ''}`}
        sidebarClassName={"left-side-bar"}
        open={this.state.sidebarOpen}
        onSetOpen={this.toggleSideBar}
        children=''
        styles={{ sidebar: { zIndex: 3, top: 75, overflowY: "visible", position: "fixed" }, root: {position: ''} }}>
        {this.renderMenuIcon()}
        <></>  {/* Don't remove this, sidebar must have a children */}
      </Sidebar>
    )
  }
}

const mapStoreToProps = (store) => ({
})

const mapDispatchToProps = {
  signOutUser
}

const LeftSideMenuBar = connect(
  mapStoreToProps,
  mapDispatchToProps,
  null,
  { forwardRef: true }
)(CLeftSideMenuBar)

export default LeftSideMenuBar
