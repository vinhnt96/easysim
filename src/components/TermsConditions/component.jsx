import React, { Component } from 'react'
import './style.scss'
import PropTypes from 'prop-types'

class CTermsConditions extends Component {

  render() {
    let t = this.context.t

    return (
     <div className='terms-conditions'>
        <div className='text-center w-100 title heading-font'>Odin Poker Terms of Service</div>
        <div className='content-container'>
          <div className='content'>
            <div>These are the terms of service of Odin Poker under which we supply products to our customers. We expect all visitors and customers to have carefully read this page and be fully aware of its contents. “We”, “Our” or “Us” always refers to Odin Poker, “you” always refers to you as a visitor, user and/or customer of the official Odin Poker website www.odinpoker.io</div>
            <div className='mb-4 mt-4'>Date of publication: March 16th, 2021.</div>
            <div className='header font-weight-bold'>1. Notice of agreement</div>
            <div className='mb-4 mt-4'>By accessing and using odinpoker.io you accept and agree to be bound by the terms and conditions of this agreement. In addition, when using services of Odin Poker, such as visiting our website or purchasing goods from it, you constitute acceptance of this agreement. Please read these terms and conditions as well as our privacy policy warily before purchasing products from odinpoker.io. All terms are subject to our contracts and if you happen to have a problem with them, miss any important information or simply want to discuss any of them, feel free to contact us on <a href={`mailto:${t('odin_support_email')}`}>{t('odin_support_email')}.</a></div>
            <div className='mb-4 mt-4'>If you do not agree to our terms of service, please step back from using our services. Terms of service are legally binding and can therefore prevent users from abusing or misusing the services of odinpoker.io.</div>
            <div className='header font-weight-bold'>2. Property rights and copyright</div>
            <div className='mb-4 mt-4'>All text, pictures, fonts, logos, videos and audio is the sole property of Odin Poker of Odin Poker’s valued partners who have therefore provided written permission to Odin Poker to use their footage. Any duplication or use apart from private purposes is prohibited.</div>
            <div className='mb-4 mt-4'>For further information see "content abuse" in these Terms of Service.</div>
            <div className='mb-4 mt-4'>The site and its original content, features and functionality are owned by Odin Poker and are protected by international copyright, trademark, patent, trade secret and other intellectual property and proprietary rights laws.</div>
            <div className='header font-weight-bold'>3. Limitation of liability</div>
            <div className='mb-4 mt-4'>If you think that there' s bugs or errors on odinpoker.io, feel free to contact us under <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a></div>
            <div className='mb-4 mt-4'>We will try to do our best to correct them and provide fair products that meet your expectations.</div>
            <div className='mb-4 mt-4'>However, we can not be held responsible for any misinformation or content you don' t agree with.</div>
            <div className='mb-4 mt-4'>We are not liable for if the product is suitable for your needs or not.</div>
            <div className='mb-4 mt-4'>We do not ensure that the information on this website is correct, we do not warrant its completeness or accuracy; nor do we promise to ensure that the website remains available or that the material is kept up to date.</div>
            <div className='header font-weight-bold'>4. Accounts and subscriptions</div>
            <div className='mb-4 mt-4'>By opening an account on odinpoker.io you fully agree with our privacy policy as well as terms of service. It is up to us to freeze or terminate your account if we have reasonable incentive to do so.</div>
            <div className='mt-4'>Reasons for that could be</div>
            <div>- missing payment</div>
            <div>- content abuse</div>
            <div>- defamation</div>
            <div className='mb-4'>- other reasons</div>
            <div className='mb-4 mt-4'>During the registration, you accept to provide and maintain right, valid, and complete information about yourself as indicated by the Service’s registration form.</div>
            <div className='header font-weight-bold'>5. Legal disclaimer</div>
            <div className='mb-4 mt-4'>BWe do limit our liability to the extent it is lawful to do so. If you happen to experience death or personal injury, fraud or any breach of your personal rights caused by negligence of Odin Poker or one of its employees, we will, by law, do everything in our duty to protect the rights of everyone involved. Odin Poker’s products are only provided for your private personal use.</div>
            <div className='header font-weight-bold'>6. Severability</div>
            <div className='mb-4 mt-4'>If a part of this contract is held to be illegal or otherwise unenforceable, the remainder of the contract still applies.</div>
            <div className='header font-weight-bold'>7. Content abuse</div>
            <div className='mb-4 mt-4'>Odin Poker offers highly valuable products for individuals who want to improve their poker game. If we happen to find strong evidence that you are abusing our services, we may terminate our contract immediately and there is no chance for a refund for you.</div>
            <div className='mt-4'>Without our prior written permission it is strictly forbidden for you to</div>
            <div>- share your account with others or loan your account temporarily to someone else</div>
            <div>- download any content from odinpoker.io - save any content offline from odinpoker.io</div>
            <div>- spread or republish any content from odinpoker.io</div>
            <div>- share or redistribute any content from odinpoker.io</div>
            <div className='mb-4'>- sell, rent or sub-license material from odinpoker.io</div>
            <div className='header font-weight-bold'>8. Information disclaimer</div>
            <div className='mb-4 mt-4'>All the information provided on odinpoker.io are the intellectual property of Odin Poker and by no means a guarantee to be rightfully proven. Our goal is to do our best providing you the best poker content that we are able to offer. If you happen to find incorrect information or tips that might lead you to lose money there is no way Odin Poker is liable for that. It is your sole responsibility what you do with your money and also which poker games you play and what decisions you make at the table.</div>
            <div className='header font-weight-bold'>9. Payment policy</div>
            <div className='mb-4 mt-4'>All Odin Poker prices will be declared and charged in the currency Odin Poker decides. All Odin Poker prices are total prices including all tax and fees. Such tax is based on the bill-to address and the national tax rate in effect at the time of your purchase. Foreign currency exchange rates are due to the bank of the account you purchase an Odin poker product from. We reserve the right to refuse to make a contract with you and deny payment. It is your responsibility to pay in a timely manner and that your payment method is valid. By you having access to one of our accepted payment methods we expect you to be fully aware of buying a product online. All sales are final.</div>
            <div className='mb-4 mt-4'>Prices may change over time and the services do not include any price protection or refunds in the event of a price reduction or promotional offering. Refunds will not take place unless you can provide specific reasons for cancelling your subscription, which we'll evaluate from case to case. However, after our successful acceptance of your use of one of our offered payment methods, we will deliver the purchased product in a timely manner. If a product becomes unavailable following a transaction but prior to download, your sole remedy is a refund. If any technical problems occur or unreasonable delay in delivery of over seven days after purchase of the product, your exclusive and sole choice is a refund of the price paid. For detailed information see the next clause of our terms of service, product delivery.</div>
            <div className='header font-weight-bold'>10. Subscription Termination</div>
            <div className='mb-4 mt-4'>Any customer has the right to end any contract upon end of any billing cycle at any moment. This can either happen through his account management section on odinpoker.io, or through contacting our customer support team via <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a></div>
            <div className='header font-weight-bold'>11. Product delivery</div>
            <div className='mb-4 mt-4'>We promise to grant you access to your purchased product within seven days unless you' re taking place in a presale or other terms are explicitly stated in the specific campaign. Most of our products will be delivered instantly after successful payment. Odin Poker has the right to refuse service with you for any reasons. Payment will then be refunded. Our products will be delivered and open for your access until termination of Odin Poker.</div>
            <div className='header font-weight-bold'>12. General</div>
            <div className='mb-4 mt-4'>We may adjust our terms of service as well as our privacy policy from time to time. The changes will not be retroactive and the most current version of the terms will govern our relationship with you. We'll try to notify you of any changes via the communication channels you enabled us to have contact with you. By continued usage of our service after such adjustments might take place, you agree to be bound by the updated terms as well as our privacy policy. Also the adjustment of prices for our services might be subject of changes. Temporarily by discounts or permanently by us changing the pricing of our products in general.You will not gain any rights because we adjust our prices.</div>
            <div className='header font-weight-bold'>13. Contact</div>
            <div className='mb-4 mt-4'>Feel free to direct all questions, comments or enquiries to <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a></div>
            <div className='mb-4 mt-4'>Thank you for accepting our ToS and we hope you enjoy our products!</div>
            <div className='mb-4 mt-4'>Sincerely,</div>
            <div className='mb-4 mt-4'>The Odin Poker Team.</div>

          </div>
        </div>
     </div>
    )
  }
}

CTermsConditions.contextTypes = {
  t: PropTypes.func
}

export default CTermsConditions
