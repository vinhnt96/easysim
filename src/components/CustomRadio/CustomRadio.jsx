import React, { Component } from 'react'
import './CustomRadio.scss'

export default class CustomRadio extends Component {

  render() {
    const { data, name, checked, handleOnClick } = this.props;
    return (
      <label className="radio-container">
        <input
          type="radio"
          name={name}
          value={data}
          checked={checked}
          onChange={handleOnClick}
        />
        <span className="checkmark"></span>
      </label>
    )
  }
}
