import React , { Component } from 'react'
import ReactDOM from "react-dom"
import {connect} from 'react-redux'
import { withRouter } from 'react-router'
import './stylesheet.scss'

const PayPalButton = window.paypal.Buttons.driver("react", { React, ReactDOM });

class CPaypalSubscriptionButton extends Component {

  render() {
    const { amount, currency, createSubscription, onApprove, catchError,onError, onCancel} = this.props
    const paypalKey = process.env.PAYPAL_CLIENT_ID

    return (
      <PayPalButton
        amount={amount}
        currency={currency}
        createSubscription={(data, details) => createSubscription(data, details)}
        onApprove={(data, details) => onApprove(data, details)}
        onError={(err) => onError(err)}
        catchError={(err) => catchError(err)}
        onCancel={(err) => onCancel(err)}
        options={{
          clientId: paypalKey,
          vault: true
        }}
        style={{
          shape: 'rect',
          color: 'blue',
          layout: 'horizontal',
          label: 'subscribe',
          height: 55,
        }}
      />
    );
  }
}

const mapStoreToProps = (store) => ({
})

const mapDispatchToProps = {}

const PaypalSubscriptionButton = connect(
  mapStoreToProps,
  mapDispatchToProps
)(CPaypalSubscriptionButton)

export default withRouter(PaypalSubscriptionButton)
