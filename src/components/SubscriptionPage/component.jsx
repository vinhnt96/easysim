import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import './stylesheet.scss'
import Slider from "react-slick"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
import logoFilled from 'images/odin-logo-filled.png'
import logo from '../../images/Logos/odin_icon.png'
import { fetchPlansSucceeded } from 'actions/subscription.actions'
import axios from 'axios'
import { isEqual, isEmpty, groupBy, includes, toUpper, toLower, filter, cloneDeep } from 'lodash'
import FeatureSection from 'components/Shared/FeatureSection/FeatureSection'
import { toast } from 'react-toastify'
import { notify } from 'utils/utils'
import { codeDiscount } from 'actions/subscription.actions'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import { codeDiscountAfiliate, revertCodeSubscribe } from 'utils/utils'
import pokercodeLogo from '../../images/pokercode-logo.png'
import ggPlatinumLogo from 'images/GGTeam-PlatinumCommunity.png'
import { PERCENT_DISCOUNT } from 'config/constants'
import loadable from '@loadable/component'
const SubscriptionOptions = loadable(() => import('./SubscriptionOptions'))
const BillingCyclePopup = loadable(() => import('./BillingCyclePopup/BillingCyclePopup'))
const NotifyModal = loadable(() => import('components/NotifyModal/component'))

class CSubscriptionPage extends Component {

  constructor(props){
    super(props)

    this.slideData = [
      {
        avatar: '', comment: 'Amazing tools 1',
        text: 'Lorem isp sada s d asd ad usda sahd  akhda dahj sa dajhs dah jsda jkLorem isp sada s d asd ad usda sahd  akhda dahj sa dajhs dah jsda jk',
        author: '-Jame K, Job Title'
      },
      {
        avatar: '', comment: 'Amazing tools 2',
        text: 'Lorem isp sada s d asd ad usda sahd  akhda dahj sa dajhs dah jsda jkLorem isp sada s d asd ad usda sahd  akhda dahj sa dajhs dah jsda jk',
        author: '-Jame K, Job Title'
      },
      {
        avatar: '', comment: 'Amazing tools 3',
        text: 'Lorem isp sada s d asd ad usda sahd  akhda dahj sa dajhs dah jsda jkLorem isp sada s d asd ad usda sahd  akhda dahj sa dajhs dah jsda jk',
        author: '-Jame K, Job Title'
      },
      {
        avatar: '', comment: 'Amazing tools 4',
        text: 'Lorem isp sada s d asd ad usda sahd  akhda dahj sa dajhs dah jsda jkLorem isp sada s d asd ad usda sahd  akhda dahj sa dajhs dah jsda jk',
        author: '-Jame K, Job Title'
      }
    ]

    this.subscriptionOptionsPokercodeMonthly = [
      {id: '', price: 49.99, isSpecial: false, product_id: ''},
      {id: '', price: 124.99, isSpecial: true, product_id: ''},
      {id: '', price: 99.99, isSpecial: false, product_id: ''}
    ]

    const pokercode = props.currentUser && props.currentUser.role === 'pokercode_user'

    this.state = {
      errorPopup: {
        showing: false,
        errorInfo: [],
      },
      subscriptionOptionData: pokercode ? this.subscriptionOptionsPokercodeMonthly : [],
      processing: false,
      showOptionPopUp: false,
      selectedSubscription: {},
      planId: null,
      billingCycleOptionData: [],
    }

    this.handleClickSubscribe = this.handleClickSubscribe.bind(this)
    this.handleShowPopup = this.handleShowPopup.bind(this)
    this.handleShowErrorPopup = this.handleShowErrorPopup.bind(this)
    this.generateErrorMessage = this.generateErrorMessage.bind(this)
  }

  componentDidMount() {
    let self = this
    if (!isEmpty(localStorage.getItem('code')) && isEmpty(this.props.currentUser)) {
      this.props.updateDiscountCode(localStorage.getItem('code'))
    }
    axios.get('/plans').then((res) => {
      const subscriptionOptionData = res.data.plans
      const { isChangeSubscriptionView, currentUser } = this.props
      if (isChangeSubscriptionView) {
        subscriptionOptionData.forEach(option => {
          const { subscription: { plan: { billing_frequency, billing_period, price } } } = currentUser
          const { billing_frequency: optionBillingFrequency, billing_period: optionBillingPeriod, price: optionPrice } = option
          const isSameTimeSpan = isEqual(billing_frequency, optionBillingFrequency) && isEqual(billing_period, optionBillingPeriod)
          const shouldEnableOption = isSameTimeSpan && optionPrice > price
          option['enableOption'] = shouldEnableOption
        })
      }
      this.setState({subscriptionOptionData: subscriptionOptionData})
      self.props.fetchPlansSucceeded(subscriptionOptionData)
    })

    window.onFastspringPopupClose = (data) => {
      // reset Fastspring cart each time checkout popup is closed
      window.fastspring.builder.reset();
    }

    window.onFastspringWebhookReceived = (data) => {
      // TODO: handle after user make subscribe successfully at here
      const { history, isChangeSubscriptionView, handleConfirmedChangeSub } = this.props
      const { items } = data || []
      const item = items[0] || {}
      const { not_sign_in, user_email, first_name, last_name, affiliate_code } = data.tags

      const payload = {
        code_discount: isEmpty(data['coupons']) ? '' : data['coupons'][0],
        paid: true,
        status: 'active',
        fastspring_subscription_id: item.subscription,
        product_id: item.product,
        auto_renew: true,
        email: user_email,
        not_sign_in,
        first_name,
        last_name,
        affiliate_code
      }
      window.axios.post('/subscriptions', payload).then(res => {
        const { user } = res.data
        if(!not_sign_in) this.props.updateCurrentUserAttributesToReduxTokenAuth(user)
        if(isChangeSubscriptionView)
          handleConfirmedChangeSub()
        else
          history.push({
            pathname: '/thank-you',
            state: {
              details: data,
              notSignIn: not_sign_in
            }
          })
      })
      notify("Thanks for your subscription!", toast.success);
    }

    window.onFastSpringErrorCallback = (errorCode, errorMessage) => {
      // TODO: handle on Fastspring checkout error 
      this.handleShowErrorPopup(true, errorCode, errorMessage)
    }

    window.popupEventReceived = (data) => {
      const { ecommerce, event } = data
      if (!isEmpty(event) && !isEmpty(ecommerce) && isEqual(event, 'FSC-checkoutStep5')) {
        const { products } = ecommerce.checkout
        this.setState({ planId: products[0].id || '' })
      }
    }
  }

  handleClickSubscribe(selectedSubscription) {
    const billingCycleOptionData = this.generateBillingCycleOption(selectedSubscription.parent_code)
    const plan = billingCycleOptionData.find(option => includes(option.product_id, '3-monthly'))
    this.setState({
      selectedSubscription,
      billingCycleOptionData,
      planId: plan.id,
    })
  }

  handleShowErrorPopup(showing, errorCode = 0, errorMessage = '') {
    const newError = { errorCode, errorMessage }
    const errorExisted = this.state.errorPopup.errorInfo.filter(err => err.errorCode === 0).length > 0
    const newErrorInfo = errorExisted ? cloneDeep(this.state.errorPopup.errorInfo) : [...this.state.errorPopup.errorInfo, newError]
    const newErrorInfoState = showing ? newErrorInfo : []
    this.setState({ errorPopup: { showing, errorInfo: newErrorInfoState } })
  }

  generateErrorMessage() {
    return this.state.errorPopup.errorInfo.map( error => {
      const t = this.context.t
      const message = isEqual(error.errorCode, 0) ? '[FastSpring API] (!) Error received: Failed to load resource: net::ERR_INTERNET_DISCONNECTED' : t(`fs_popup_error.${error.errorMessage}`)
      
      return <p>ERROR: {error.errorCode}: {message}</p>
    })
  }

  handleShowPopup() {
    this.setState({showOptionPopUp: !this.state.showOptionPopUp})
  }

  generateBillingCycleOption(parentCode) {
    const { subscriptionOptionData } = this.state
    const listPlansGroup = groupBy(subscriptionOptionData, 'parent_code')
    const billingCycleOptionData = listPlansGroup[parentCode] || []   

    return billingCycleOptionData
  }

  handleUpdateSelectedSubscription = (prodId) => {
    const { subscriptionOptionData } = this.state
    const plan = subscriptionOptionData.find(i => i.product_id === prodId)
    this.setState({ planId: plan.id })
  }

  featuresSection() {
    return (
      !this.props.isChangeSubscriptionView &&
      <div className='feature-section-container text-center'>
        <h3 className='feature-section-container-title heading-font'> What you get </h3>
        <FeatureSection />
      </div>
    )
  }

  render() {
    var settings = {
      dots: false,
      infinite: true,
      slidesToShow: 2,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 4000,
      pauseOnHover: true,
      initialSlide: 0,
      responsive: [
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 1
          }
        }
      ]
    }
    const {
      subscriptionOptionData, processing, showOptionPopUp, billingCycleOptionData, planId,
      errorPopup: { showing: showErrorPopup } 
    } = this.state
    const { currentUser, isChangeSubscriptionView, isPartOfLandingPage, handleConfirmedChangeSub } = this.props
    const { affiliate_code, role } = currentUser
    const pathname = window.location.pathname
    const plans = filter(subscriptionOptionData, (i) => i.parent )
    const pokercode = currentUser && role === 'pokercode_user'
    const code = isEmpty(currentUser) ? revertCodeSubscribe(pathname) : isEmpty(affiliate_code) ? 'launch35' : affiliate_code
    const percent = PERCENT_DISCOUNT[code]
    const textCodes = `ENTER ${codeDiscountAfiliate(code)} INTO THE PROMOTION CODE ON CHECKOUT TO RECEIVE ${percent * 100}% OFF`
    const errorMessage = showErrorPopup ? this.generateErrorMessage() : ''

    return (
      <div className='subcribe-page subcribe-content padding-content'>
        <NotifyModal
          modalShow={showErrorPopup}
          infomation={{title: 'Warning', message: errorMessage}}
          handleOnClose={() => this.handleShowErrorPopup(false)}
          />
        { isPartOfLandingPage && this.featuresSection() }
        <SubscriptionOptions
          subscriptionOptions={plans}
          handleClickSubscribe={this.handleClickSubscribe}
          pokercode={pokercode}
          showBillingCyclePopup={this.handleShowPopup}
        />
          <Fragment>
            { ((['/pokercode/subscribe', '/pokercode'].includes(toLower(pathname)) && isEmpty(currentUser)) || affiliate_code === 'pokercode') && <div className='exclusive-discount row no-gutters font-weight-bold justify-content-center'>
              <div className='discount-content'>
                <span>exclusive discount for</span>
                <span className='pokercode-logo'><img className='w-100' src={pokercodeLogo} alt='' /></span>
                <span> members </span>
              </div>
            </div> }
            <div className='code-discount d-flex text-center font-weight-bold justify-content-center'>
              {code === 'ggplatinum' && <img src={ggPlatinumLogo} alt='' />}
              <div className="mx-2">{toUpper(textCodes)}</div>
              {code === 'ggplatinum' && <img src={ggPlatinumLogo} alt='' />}
            </div>
          </Fragment>
          { !isPartOfLandingPage && this.featuresSection() }
        {false && <div className='slider-section text-center'>
          <div className='slider-intro'>
            <h2 className='slider-intro-title heading-font'> Don’t just take our word for it!</h2>
            <p className='slider-intro-content'>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            </p>
          </div>
          <div className='slider-container'>
            <Slider {...settings} className='slider'>
              {
                this.slideData.map( (slideItem, idx) => {
                  return (
                    <div key={idx} className='silde-item'>
                      <div className='slide-content text-center'>
                        <div className='avatar'>
                          <img alt='logo' className='logo' src={logo}/>
                        </div>
                        <div className='comment heading-font font-weight-bold'>
                            "{slideItem.comment}"
                        </div>
                        <div className='text'>
                          {slideItem.text}
                        </div>
                        <div className='author'>
                          {slideItem.author}
                        </div>
                      </div>
                    </div>
                  )
                })
              }
            </Slider>
          </div>
        </div>}
        {processing && <div className={`fetching-div ${ processing ? '' : 'hide' }`}>
          <div className='fetching-overlay'> </div>
          <div className='fetching-image'>
            <div className='image-overlay'>
              <img  src={logoFilled} alt='logo-filled'/>
            </div>
            <div className='image'> </div>
          </div>
        </div>}
        {showOptionPopUp &&
          <BillingCyclePopup
            show={showOptionPopUp}
            planId={planId}
            showBillingCyclePopup={this.handleShowPopup}
            handleUpdateSelectedSubscription={this.handleUpdateSelectedSubscription}
            handleConfirmedChangeSub={handleConfirmedChangeSub}
            billingCycleOptionData={billingCycleOptionData}
            isChangeSubscriptionView={isChangeSubscriptionView}
          />
        }
      </div>
    )
  }
}

CSubscriptionPage.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  isSignedIn: store.reduxTokenAuth.currentUser.isSignedIn,
})

const mapDispatchToProps = {
  fetchPlansSucceeded: fetchPlansSucceeded,
  updateDiscountCode: codeDiscount,
  updateCurrentUserAttributesToReduxTokenAuth,
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CSubscriptionPage)
