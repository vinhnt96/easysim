import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import { isEmpty, toLower, round } from 'lodash'
import CheckLarge from 'images/Icons/check-large.png'
import PokerCodeUserCheckLarge from 'images/Icons/pokercode_user_check-large.png'
import Check from 'images/Icons/check.png'
import PokerCodeUserCheck from 'images/Icons/pokercode_user_check.png'
import SaleTicket from 'images/sale-ticket.png'
import { PERCENT_DISCOUNT, HUNDRED_PERCENT } from 'config/constants'
import { revertCodeSubscribe } from 'utils/utils'

class CSubscriptionOptions extends Component {

  constructor(props){
    super(props)

    this.benifitFirst = [
      'Every single spot for 100bb 6max and HUNL',
      'Every possible unique flop (1755)',
      '70k+ unique sims'
    ]

    this.benifitThird = [
      'Every single spot for 15,20,25,30,40,50bb stack depths',
      'Every possible unique flop (1755)',
      '465k+ unique sims'
    ]
  }

  handleClickSubscribeNow(selectedSubscription) {
    this.props.handleClickSubscribe(selectedSubscription)
    this.props.showBillingCyclePopup()
  }

  render() {
    const t = this.context.t
    const { subscriptionOptions, pokercode, currentUser } = this.props
    const { affiliate_code } = currentUser
    const pathname = window.location.pathname
    const code = isEmpty(currentUser) ? revertCodeSubscribe(pathname) : isEmpty(affiliate_code) ? 'launch35' : affiliate_code
    let percentDiscount = PERCENT_DISCOUNT[code]

    let pokerCodeView = false
    if (['/pokercode/subscribe', '/pokercode'].includes(toLower(pathname)) && isEmpty(currentUser)) {
      pokerCodeView = true
      percentDiscount = PERCENT_DISCOUNT['pokercode']
    }

    return (
      <Fragment>
        <div className={`payment-section text-center ${pokerCodeView ? 'pokercode-view' : ''}`}>
          <div className='payment-btn-group row no-gutters'>
            <div className='payment-content row no-gutters'>
            {  subscriptionOptions.map((option, idx) => {
                const benifits = idx === 0 ? this.benifitFirst : this.benifitThird
                const isSpecial = option.parent_code === 'cash-mtts'
                return (
                  <div key={idx} className={`payment-btn-container col-md-4 ${isSpecial && 'big-size hight-light'}`}>
                    { 
                      isSpecial &&
                      <div className='ticket'>
                        <img src={SaleTicket} alt='ticket' />
                        <p className='ticket-content'> {t('subscribe.ticket')} </p>
                      </div>
                    }
                    <div className='title'>
                      {option.name}
                    </div>
                    {idx === 1 &&
                      <Fragment>
                        <div className="rectangle-container hight-light">
                          { pokercode ? 
                            <div className="check-large"><img src={PokerCodeUserCheckLarge} alt=''/></div> : 
                            <div className="check-large"><img src={CheckLarge} alt=''/></div>
                          }
                          <div className="rectangle">
                            <p>Full access to all of the</p>
                            <p>features of both packages </p>
                            <p className='mt-3'>530k+ unique sims</p>
                          </div>
                        </div>
                      </Fragment>
                    }
                    {idx !== 1 &&
                      <div className="rectangle-container">
                        <div className="rectangle">
                          {benifits.map((benifit, ind) => {
                            return (
                              <div key={ind}>
                                <p className={idx === 0 && ind === 0 ? 'first-payment': ''}>{benifit}</p>
                                { pokercode ? <img src={PokerCodeUserCheck}  alt=''/> : <img src={Check}  alt=''/> }
                              </div>
                            )
                          })}
                        </div>
                      </div>
                    }
                    <div className={`price heading-font ${isSpecial && 'special-price'} code-discount-option`}>
                      <span>${option.price}/</span>month
                    </div>
                     <div className={`mt-0 price heading-font ${isSpecial && 'special-price'}`}>
                      <span>${round(option.price * (HUNDRED_PERCENT - percentDiscount), 2)}/</span>month
                    </div>
                    <div className={`subscribe-now-btn-wrapper ${isSpecial && 'btn-gradient'} ${false ? 'disabled' : ''}`}>
                      <div className='paypal-button-wrapper'></div>
                      <button disabled={!this.props.subscriptionOptions.length} className={`btn subscribe-now-btn ${false ? 'disabled' : ''}`}
                              onClick={this.handleClickSubscribeNow.bind(this, option)}>
                        {t('subscribe.button')}
                      </button>
                    </div>
                  </div>
                )
              })
            }
            </div>
          </div>
        </div>
      </Fragment>
    )
  }
}

CSubscriptionOptions.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  subscriptions: store.subscriptions,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
  updateCurrentUserAttributesToReduxTokenAuth
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CSubscriptionOptions)
