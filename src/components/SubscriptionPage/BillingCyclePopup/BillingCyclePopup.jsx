import React, { Component, Fragment } from 'react'
import './BillingCyclePopup.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Modal from 'react-bootstrap/Modal'
import { isEmpty, includes, isEqual, round } from 'lodash'
import { codeDiscountAfiliate, revertCodeSubscribe } from 'utils/utils'
import { PERCENT_DISCOUNT, HUNDRED_PERCENT } from 'config/constants'
import loadable from '@loadable/component'

const ContactPopup = loadable(() => import('./ContactPopup/ContactPopup'))
const CheckInfoPopup = loadable(() => import('./CheckInfoPopup/CheckInfoPopup'))

class BillingCyclePopup extends Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedOption: '3 Monthly',
      productId: props.billingCycleOptionData.find(option => option.id === props.planId).product_id || '',
      changeSubscriptionPopup: {
        selectedPlan: '',
        showing: false,
      },
      checkInfoPopup: {
        selectedPlan: '',
        code: '',
        showing: false,
      }
    }
    this.handleSave = this.handleSave.bind(this)
    this.handleShowContactPopup = this.handleShowContactPopup.bind(this)
    this.findPlanCondition = this.findPlanCondition.bind(this)
    this.handleShowCheckInfoPopup = this.handleShowCheckInfoPopup.bind(this)
  }

  handleChangeOptions(optionName, productId) {
    if(this.state.selectedOption === optionName) return
    this.setState({
      selectedOption: optionName,
      productId
    })
    this.props.handleUpdateSelectedSubscription(productId)
  }

  handleShowContactPopup() {
    this.setState({changeSubscriptionPopup: {...this.state.changeSubscriptionPopup, showing: !this.state.changeSubscriptionPopup.showing} })
  }

  findPlanCondition(option) {
    const { productId } = this.state
    return isEmpty(productId) ? includes(option.product_id, '3-monthly') : isEqual(option.product_id, productId)
  }

  handleShowCheckInfoPopup() {
    this.setState({checkInfoPopup: {...this.state.checkInfoPopup, showing: !this.state.checkInfoPopup.showing} })
  }

  handleSave() {
    const { billingCycleOptionData, currentUser, isChangeSubscriptionView } = this.props
    const { email, affiliate_code } = currentUser    
    const selectedPlan = billingCycleOptionData.find(option => this.findPlanCondition(option))
    const pathname = window.location.pathname
    const isNotSignIn = isEmpty(currentUser)
    const code = isNotSignIn ? revertCodeSubscribe(pathname) : isEmpty(affiliate_code) ? 'launch35' : affiliate_code

    if(isChangeSubscriptionView) {
      if(selectedPlan.enableOption) this.props.showBillingCyclePopup()

      this.setState({ changeSubscriptionPopup: { selectedPlan: selectedPlan, showing: true } })
    }
    else {
      if(isNotSignIn) {
        this.setState({ checkInfoPopup: { selectedPlan: selectedPlan, code: code, showing: true } })
      }
      else {
        window.fastspring.builder.reset()    // ensure empty cart
        window.fastspring.builder.add(selectedPlan.product_id)
        window.fastspring.builder.tag({ 'user_email': email })
        window.fastspring.builder.promo(codeDiscountAfiliate(code))
        window.fastspring.builder.checkout()
      }
      this.props.showBillingCyclePopup()
    }
  }

  nameOfPlans(productId, parentCode) {
    switch(productId) {
      case `${parentCode}`:
        return 'Month'
      case `${parentCode}-3-monthly`:
        return '3 Monthly'
      default:
        return 'Yearly'
    }
  }

  saleValue(productId, parentCode) {
    switch(productId) {
      case `${parentCode}`:
        return ''
      case `${parentCode}-3-monthly`:
        return 'save 10%'
      default:
        return 'save 25%'
    }
  }

  priceOfPlan(productId, parentCode, price) {
    const { currentUser } = this.props
    const { affiliate_code } = currentUser
    const pathname = window.location.pathname
    const code = isEmpty(currentUser) ? revertCodeSubscribe(pathname) : isEmpty(affiliate_code) ? 'launch35' : affiliate_code
    const percentDiscount = PERCENT_DISCOUNT[code] || 0.35

    const pricePerMonth = price * (HUNDRED_PERCENT - percentDiscount)
    switch(productId) {
      case `${parentCode}`:
        return `$${round(pricePerMonth, 2)}/month`
      case `${parentCode}-3-monthly`:
        return `$${round(pricePerMonth/3, 2)}/month`
      default:
        return `$${round(pricePerMonth/12, 2)}/month`
    }
  }

  onClosePopup = () => {
    this.setState({ selectedOption: '3 Monthly' })
    this.props.showBillingCyclePopup()
  }

  render() {
    const t = this.context.t
    const { show, billingCycleOptionData, handleConfirmedChangeSub } = this.props
    const { selectedOption, changeSubscriptionPopup: {showing: showChangeSubscriptionPopup, selectedPlan: changePlan},
      checkInfoPopup: {showing: showCheckInfoPopup, code: checkoutCode, selectedPlan} } = this.state

    return (
      <Fragment>
        <Modal
        show={show}
        onHide={this.onClosePopup}
        dialogClassName='subscribe-billing-cycle-options-modal'
        backdrop="static"
        aria-labelledby='subscribe-billing-cycle-options-modal'
        >
          <Modal.Body bsPrefix='custom-modal-body'>
            <Modal.Header bsPrefix='custom-modal-header d-flex justify-content-between align-items-center' closeButton>
              <Modal.Title id="options-modal"> {t('subscribe.popup_title')} </Modal.Title>
            </Modal.Header>
            <div className='content pt-2 pb-0 px-0'>
            {
              billingCycleOptionData.map( option => {
                const { price, parent_code, product_id, id} = option
                const name = this.nameOfPlans(product_id, parent_code)
                const saleValue = this.saleValue(product_id, parent_code)
                const priceItem = this.priceOfPlan(product_id, parent_code, price)
                const isSelectedOptions = name === selectedOption
                const saleOptionElement = isSelectedOptions ? <span className='option-sale'> {saleValue} </span> 
                                                          : <span className='option-sale-inactive'> ({saleValue}) </span>
                return (
                  <div className={`option d-flex justify-content-between align-items-center ${isSelectedOptions && 'active'}`} key={id}  onClick={() => this.handleChangeOptions(name, product_id)}>
                    <span> {name} {saleValue && saleOptionElement } </span>
                    <div className='option-price'> {priceItem} </div>
                  </div>
                )
              })
            }
            </div>
            <button className='btn subscribe-now-btn' onClick={this.handleSave}>
              {t('subscribe.button')}
            </button>
          </Modal.Body>
        </Modal> 
        <ContactPopup 
          show={showChangeSubscriptionPopup}
          selectedPlan={changePlan}
          handleShowContactPopup={this.handleShowContactPopup}
          handleConfirmedChangeSub={handleConfirmedChangeSub}
        />
        <CheckInfoPopup
          show={showCheckInfoPopup}
          selectedPlan={selectedPlan}
          code={checkoutCode}
          handleShowCheckInfoPopup={this.handleShowCheckInfoPopup}
        />
      </Fragment>
    )
  }
}

BillingCyclePopup.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  subscriptions: store.subscriptions,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

export default connect(
  mapStoreToProps,
  null
)(BillingCyclePopup)
