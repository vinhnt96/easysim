import * as yup from 'yup';

export const buildSchema = (locale, translation) => {
  let t = translation

  let emailRules = locale === 'en' ?
    yup.string().trim().required(t('required_field')).email('Must be a valid email') :
    yup.string().trim()
       .required(t('validation.required'))
       .email(t('validation.wrong_format'))

  let firstNameRules = locale === 'en' ?
    yup.string().trim().required(t('required_field')):
    yup.string().trim()
      .required(t('validation.required'))

  let lastNameRules = locale === 'en' ?
    yup.string().trim().required(t('required_field')):
    yup.string().trim()
      .required(t('validation.required'))

  return yup.object({
    email: emailRules,
    first_name: firstNameRules,
    last_name: lastNameRules,
  })
}
