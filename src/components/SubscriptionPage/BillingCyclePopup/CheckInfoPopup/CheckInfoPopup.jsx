import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './CheckInfoPopup.scss'
import { connect } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { Button } from 'react-bootstrap'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import { codeDiscountAfiliate } from 'utils/utils'
import { buildSchema } from './validation'
import { Link } from 'react-router-dom'
import { Formik }  from 'formik';
import { Form, Row, Col } from 'react-bootstrap'
import FormField from 'components/Shared/FormField/FormField'
import OdinLogo from 'components/OdinLogo/component'

class CheckInfoPopup extends Component {

  constructor(props) {
    super(props)

    this.state = {
      showMessage: false,
      isSubmitting: false
    }

    this.onSubmit = this.onSubmit.bind(this)
    this.onChange = this.onChange.bind(this)
  }

  privateForm = formikProps => {
    const t = this.context.t
    const { showMessage, isSubmitting } = this.state
    let {
      handleSubmit,
      handleChange,
      errors,
      values,
      touched,
      handleBlur,
      status,
    } = {...formikProps}

    const signUpFields = [
      {field: 'email', fieldType: 'text'},
      {field: 'first_name', fieldType: 'text'},
      {field: 'last_name', fieldType: 'text'},
    ]

    return <Form
      noValidate
      className='fadeIn mb-4'
      onSubmit={handleSubmit}>
      <Row>
        {
          signUpFields.map((item, idx) => {
            const { field, fieldType } = item
            return (
              <Col key={idx} sm={field === 'email' ? 12 : 6}>
                <FormField
                  field={field}
                  fieldType={fieldType}
                  error={touched[field] && errors[field]}
                  status={status && status[field]}
                  handleChange={(e) => this.onChange(e, handleChange)}
                  values={values}
                  isRequired={true}
                  onBlur={handleBlur}
                />
                {
                  field === 'email' && showMessage &&
                  <p className='font-size-14 text-secondary pb-2'>
                    <span> This email already exists!. Try other email or </span>
                    <span>  <Link to={{ pathname: '/signin', state: { backToLandingPage: true } }}
                            className='text-blue' tabIndex="-1"><b>{t('log_in')}</b></Link> </span>
                    <span> to use this email for subscribe </span> 
                  </p>
                }
              </Col>
            )
          })
        }
        <Col className='mt-2' sm={12}>
          <Button
            type='submit'
            className='w-100 submit my-3 py-2 btn-primary font-size-18 font-weight-600'
            disabled={isSubmitting}
          >
            {t('confirm')}
          </Button>
        </Col>
      </Row>
    </Form>
  }

  onChange(e, handleChange) {
    handleChange(e)
    if(this.state.showMessage) this.setState({ showMessage: false, isSubmitting: false })
  }

  onSubmit(userInfo, action) {
    const { selectedPlan, code, handleShowCheckInfoPopup } = this.props
    const { email, first_name, last_name } = userInfo
    this.setState({ isSubmitting: true })

    window.axios.post('/users/check_exists', {email}).then( res => {
      if (res.data.exists_code === 0) //account not exists
      {
        window.fastspring.builder.reset()    // ensure empty cart
        window.fastspring.builder.add(selectedPlan.product_id)
        window.fastspring.builder.tag({ user_email: email, not_sign_in: true, first_name, last_name, affiliate_code: code })
        window.fastspring.builder.promo(codeDiscountAfiliate(code))
        window.fastspring.builder.recognize({
          email: email,
          firstName: first_name,
          lastName: last_name
        })
        window.fastspring.builder.checkout()
        handleShowCheckInfoPopup()
        this.setState({ isSubmitting: false })        
      }
      else {
        this.setState({ showMessage: true })
      }
    }).catch(err => { console.error(err) })
  }

  render() {
    const t = this.context.t
    const { show, handleShowCheckInfoPopup, i18nState } = this.props
    const schemaValidation = buildSchema(i18nState.lang, t)
    const initialValues = { email: '', first_name: '', last_name: '' }

    return (
      <Modal
      show={show}
      onHide={handleShowCheckInfoPopup}
      dialogClassName='check-info-popup'
      backdrop="static"
      aria-labelledby='check-info-popup'
      >
        <Modal.Body bsPrefix='custom-modal-body'>
          <Modal.Header bsPrefix='custom-modal-header d-flex justify-content-between align-items-center' closeButton>
            <Modal.Title id="options-modal"> {t('check_info_popup')} </Modal.Title>
          </Modal.Header>
          <div className='content'>
            <div className='form-wrappper sign-up'>
              <div className='form'>
                <div className='text-center w-100 d-flex align-items-center justify-content-center mb-2'>
                  <OdinLogo />
                </div>
                <Formik
                  component={this.privateForm}
                  validationSchema={schemaValidation}
                  onSubmit={this.onSubmit}
                  initialValues={initialValues}
                />
              </div>
            </div>
          </div>
              
        </Modal.Body>
      </Modal> 
    )
  }
}

CheckInfoPopup.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
  updateCurrentUserAttributesToReduxTokenAuth
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CheckInfoPopup)