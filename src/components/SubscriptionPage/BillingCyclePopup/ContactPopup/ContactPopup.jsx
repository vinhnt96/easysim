import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './ContactPopup.scss'
import { connect } from 'react-redux'
import { Modal } from 'react-bootstrap'
import { isEmpty, round } from 'lodash'
import { currencyFormat } from 'services/game_play_service'
import { PERCENT_DISCOUNT } from 'config/constants'
import { Button } from 'react-bootstrap'
import { updateCurrentUserAttributesToReduxTokenAuth } from 'actions/user.actions'
import { notify } from 'utils/utils'
import { toast } from 'react-toastify'

class ContactPopup extends Component {

  constructor(props) {
    super(props)

    this.state = {
      processingChangeSub: false,
    }

    this.handleConfirmChangeSub = this.handleConfirmChangeSub.bind(this)
  }

  renderContactView() {
    const t = this.context.t
    const { handleShowContactPopup } = this.props

    return (
      <div className='content'>
        <p> 
          Please contact 
          <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a>
          for help with this matter 
        </p>
        <div className='row justify-content-center'>
          <div className='col-6'>
            <div className='btn-primary' onClick={handleShowContactPopup}>
              {t('ok')}
            </div>
          </div>
        </div>
      </div>
    )
  }

  renderChangeSubView(){
    const { processingChangeSub } = this.state
    const { currentUser, selectedPlan, handleShowContactPopup } = this.props
    const { subscription: currentSubscription, affiliate_code } = currentUser
    const { price_discount, plan } = currentSubscription    
    const code = isEmpty(affiliate_code) ? 'launch35' : affiliate_code !== 'pokercode' ? affiliate_code : 'pokercode'

    const numOfDayInCurrentSubPeriod = round((new Date(currentSubscription.renew_time).getTime() - new Date(currentSubscription.start_time).getTime()) / (1000 * 3600 * 24))
    const utilizedDayCurrentSub = round((new Date().getTime() - new Date(currentSubscription.start_time).getTime()) / (1000 * 3600 * 24))
    const utilizedPriceCurrentSub = price_discount * (utilizedDayCurrentSub / numOfDayInCurrentSubPeriod)    
    const numOfDayUnUseCurrentSub = numOfDayInCurrentSubPeriod - utilizedDayCurrentSub
    const priceUnUseCurrentSub = price_discount - utilizedPriceCurrentSub

    const newSubPrice = round(selectedPlan.price - (selectedPlan.price * PERCENT_DISCOUNT[code]), 2)
    const numOfDayInNewSubPeriod = numOfDayInCurrentSubPeriod // set this for now because we only change sub with the same billing period
    const numOfremainDayNewSub = numOfDayUnUseCurrentSub // set this for now because we only change sub with the same billing period
    const remainPriceNewSub = newSubPrice * (numOfremainDayNewSub / numOfDayInNewSubPeriod)

    const dateCreated = new Date(currentSubscription.start_time).toISOString().split('T')[0]
    const renewalDate = new Date(currentSubscription.renew_time).toISOString().split('T')[0]
    const entryDate = new Date().toISOString().split('T')[0]

    return (
      <div className='content'>
        <div className='current-sub pb-3'>
          <p className='font-weight-bold font-italic font-size-14 text-muted'> {`Before Subscription Update: ${currencyFormat(price_discount)}/${plan.billing_frequency} ${plan.billing_period}`} </p>
          <p className=''> {plan.name} </p>
          <div className='row no-gutters'>
            <div className='col-6'>
              <div> Utilized: </div>
              <p> <span> Day: {utilizedDayCurrentSub} </span> <span> Price: {currencyFormat(utilizedPriceCurrentSub)} </span> </p>
            </div>
            <div className='col-6'>
              <div> Credit: </div>
              <p> <span> Day: {numOfDayUnUseCurrentSub} </span> <span> Price: {currencyFormat(priceUnUseCurrentSub)} </span> </p>
            </div>
          </div>
        </div>
        <hr />
        <div className='new-sub pb-3'>
          <p className='font-weight-bold font-italic font-size-14 text-muted'> {`After Subscription Update: ${currencyFormat(newSubPrice)}/${selectedPlan.billing_frequency} ${selectedPlan.billing_period}`} </p>
          <p className=''> {selectedPlan.name} </p>
          <div>
            <p> Remaining days of billing cycle : {numOfremainDayNewSub} </p>
            <p> Value of remaining days : {currencyFormat(remainPriceNewSub)} </p>
          </div>
        </div>
        <div className='pb-2 table-container'>
          <h4>Extra Amount</h4>
          <table className="table">
            <thead>
              <tr>
                <th scope="col">Start Date</th>
                <th scope="col">Update Date</th>
                <th scope="col">End Date</th>
                <th scope="col">Extra Amount</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>{dateCreated}</td>
                <td>{entryDate}</td>
                <td>{renewalDate}</td>
                <td>{currencyFormat(remainPriceNewSub - priceUnUseCurrentSub)}</td>
              </tr>
            </tbody>
          </table>
        </div>
        <div className='row no-gutter justify-content-end pb-3'>
          <div className='col-6 row no-gutters justify-content-end'>
            <Button className='mr-2' onClick={handleShowContactPopup} disabled={processingChangeSub}> Cancel </Button>
            <Button className='mr-2' onClick={this.handleConfirmChangeSub} disabled={processingChangeSub}> Confirm </Button>
          </div>
        </div>
      </div>
    )
  }

  handleConfirmChangeSub() {
    this.setState({ processingChangeSub: true })
    const { selectedPlan: { product_id }, handleConfirmedChangeSub } = this.props
    window.axios.post('/subscriptions/change_subscription', {product_id}).then( res => {
      const { data: {result} } = res
      if(result === 'success') {
        setTimeout(() => window.location.reload(), 2500)
        notify("Processing change subscription ..., your subscription will update if charged payment successfully. Check your mail to more info!", toast.success)
      } else {
        notify("Change subscription failed.", toast.error)
      }
      handleConfirmedChangeSub()
    }).catch(err => console.error(err))
  }

  render() {
    const t = this.context.t
    const { show, handleShowContactPopup, selectedPlan: { enableOption: canCheckout } } = this.props

    return (
      <Modal
      show={show}
      onHide={handleShowContactPopup}
      dialogClassName={canCheckout ? 'change-sub-popup' : 'contact-popup'}
      backdrop="static"
      aria-labelledby={canCheckout ? 'change-sub-popup' : 'contact-popup'}
      >
        <Modal.Body bsPrefix='custom-modal-body'>
          <Modal.Header bsPrefix='custom-modal-header d-flex justify-content-between align-items-center' closeButton>
            <Modal.Title id="options-modal"> {canCheckout ? t('subscription.change_subscription') : t('contact_us')} </Modal.Title>
          </Modal.Header>
          {canCheckout ? this.renderChangeSubView() : this.renderContactView()}         
        </Modal.Body>
      </Modal> 
    )
  }
}

ContactPopup.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  subscriptions: store.subscriptions.plans
})

const mapDispatchToProps = {
  updateCurrentUserAttributesToReduxTokenAuth
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(ContactPopup)