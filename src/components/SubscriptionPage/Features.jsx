import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import databaseIconBlue from 'images/Logos/database_icon.png'
import startsIconBlue from 'images/Logos/starts_icon.png'
import laptopIconBlue from 'images/Logos/laptop_icon.png'
import analysisIconBlue from 'images/Logos/analysis_icon.png'
import databaseIconOrange from 'images/Logos/database_icon_orange.png'
import startsIconOrange from 'images/Logos/starts_icon_orange.png'
import laptopIconOrange from 'images/Logos/laptop_icon_orange.png'
import analysisIconOrange from 'images/Logos/analysis_icon_orange.png'

class CFeatures extends Component {

  render() {
    let t = this.context.t
    let { role } = this.props.currentUser;
    let databaseIcon = databaseIconBlue;
    let startsIcon = startsIconBlue;
    let laptopIcon = laptopIconBlue;
    let analysisIcon = analysisIconBlue;

    if(role === 'pokercode_user') {
      databaseIcon = databaseIconOrange;
      startsIcon = startsIconOrange;
      laptopIcon = laptopIconOrange;
      analysisIcon = analysisIconOrange;
    }

    return (
      <div className='row'>
        <div className='col-md-3 col-sm-6'>
          <div className='feature-icon'>
            <img src={databaseIcon} className='icon' alt='database-icon' />
          </div>
          <h4 className='feature-title heading-font mb-3'> {t('subscription-page.huge_database')} </h4>
          <ul className='feature-description'>
            <li> Every single HU spot for MTT & Cash Games </li>
            <li> 279 spots and counting </li>
            <li> 500k unique sims </li>
          </ul>
        </div>
        <div className='col-md-3 col-sm-6'>
          <div className='feature-icon'>
            <img src={startsIcon} className='icon' alt='starts-icon' />
          </div>
          <h4 className='feature-title heading-font mb-3'> {t('landing_page.land.high_quality')} </h4>
          <ul className='feature-description'>
            <li>Created by elite level players</li>
            <li> More bet sizings on every street than any other site </li>
            <li> Multiple unique bet sizings </li>
          </ul>
        </div>
        <div className='col-md-3 col-sm-6'>
          <div className='feature-icon'>
            <img src={laptopIcon} className='icon' alt='laptop-icon' />
          </div>
          <h4 className='feature-title heading-font mb-3'> {t('landing_page.easy_use')} </h4>
          <ul className='feature-description'>
            <li>No extra software required</li>
            <li> You will never have to run a sim ever again </li>
            <li> Browser based, log in from anywhere </li>
          </ul>
        </div>
        <div className='col-md-3 col-sm-6'>
          <div className='feature-icon'>
            <img src={analysisIcon} className='icon' alt='analysis-icon' />
          </div>
          <h4 className='feature-title heading-font mb-3'> {t('landing_page.analysis_tools')} </h4>
          <ul className='feature-description'>
            <li> All the best features & tools found in PioSolver </li>
            <li> Preflop viewer to explore entire monkersolver trees </li>
            <li> Integrated notes system and sim history page to track progress </li>
          </ul>
        </div>
      </div>
    )
  }
}

CFeatures.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser
})

const mapDispatchToProps = {

}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CFeatures)
