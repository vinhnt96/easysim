import React, { Component, Fragment } from 'react'
import './CustomCheckbox.scss'

export default class CustomCheckbox extends Component {

  render() {
    let { data, disabled, checked, handleChecked } = this.props;
    return (
      <Fragment>
        <input
          className="inp-cbx"
          id={`cbx-${data}`}
          checked={checked}
          type="checkbox"
          style={{display: 'none'}}
          onChange={(e) => handleChecked(e, data)}
          disabled={disabled}
        />
        <label
          className={`cbx ${disabled ? 'cursor-not-allowed' : ''}`}
          htmlFor={`cbx-${data}`}
        >
          <span>
            <svg width="12px" height="10px" viewBox="0 0 12 10">
              <polyline points="1.5 6 4.5 9 10.5 1" />
            </svg>
          </span>
        </label>
      </Fragment>
    )
  }
}
