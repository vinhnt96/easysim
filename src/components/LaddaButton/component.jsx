import React, {Component} from 'react'
import {
  Button,
  Spinner
} from 'react-bootstrap'
import PropTypes from 'prop-types'

export default class LaddaButton extends Component {

  render(){
    const t = this.context.t
    const { disabled, text } = this.props
    const textButton = text || t('submit')

    return <Button {...this.props} >
      {disabled ?
        <Spinner animation="border" size="sm" /> :
        textButton }
    </Button>
  }

}

LaddaButton.contextTypes = {
  t: PropTypes.func
}

// https://reactjs.org/docs/typechecking-with-proptypes.html#default-prop-values
LaddaButton.defaultProps = {
  variant: 'info'
}
