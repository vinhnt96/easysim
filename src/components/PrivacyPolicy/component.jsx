import React, { Component } from 'react'
import '../TermsConditions/style.scss'
import PropTypes from 'prop-types'

class CTermsConditions extends Component {

  render() {
    let t = this.context.t

    return (
     <div className='terms-conditions'>
        <div className='text-center w-100 title heading-font'>Odin Poker Privacy Policy</div>
        <div className='content-container'>
          <div className='content'>
            <div>By visiting and using odinpoker.io you agree to our privacy policy as well as our cookies policy and terms of service.“We”, “Our” or “Us” always refers to Odin Poker, “you” always refers to you as a visitor, user and/or customer of the official Odin Poker Website odinpoker.io</div>
            <div className='mb-4 mt-4'>Date of publication: 16/03/2021</div>
            <div className='header font-weight-bold'>Contacting Odin Poker</div>
            <div className='mb-4 mt-4'>By contacting us you consent that we may use the provided data to service your inquiry and save the data accordingly for the timespan needed. We might anonymize your data for statistical and/or marketing purposes. All of Odin Poker’s privacy policy is protected by law.</div>
            <div className='mb-4 mt-4'>If any provisions of Odin Poker’s privacy policy are held to be invalid or unenforceable, all remaining provisions hereof will remain in full force and effect. You can contact us via: <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a> if you have any questions or queries regarding our data privacy policy.</div>
            <div className='header font-weight-bold'>Data Retention</div>
            <div className='mb-4 mt-4'>We hereby inform you that, for the purpose of fulfilling a contract, we save the information provided by you in your Odin account such as e-mail-address, social media profile (if used for registration), name and username. Also, the origin of your IP-address is saved for tax purposes as well as the location of your IP-address to fit with our content-abuse clause in our terms of service. In the case of successful conclusion of a contract by purchasing an Odin Poker product, we save all contract-related information for a seven years’ time span due to tax regulations.</div>
            <div className='header font-weight-bold'>Cookies Policy</div>
            <div className='mb-4 mt-4'>For the simplification of purchasing Odin Poker products as well as subsequent conclusions we save your IP-data as well as name, address, payment information and/or all other information you provide for contract fulfillment through cookies. Cookies are small text files which are saved on your browser as well as gadget without producing any harm or damage. Cookies are industry standard to offer our services without any bugs or errors. Cookies stay saved on your gadget until you delete them or opt-out of the usage of cookies.However, this may lead to restricted use of Odin Poker.</div>
            <div className='header font-weight-bold'>Web Analytics Services</div>
            <div className='mb-4 mt-4'>Our website uses web analytics services such as</div>
            <div className='header font-weight-bold'>Google Analytics</div>
            <div className='mb-4 mt-4'>Odin Poker uses google analytics, a web analytics tool from Google Inc., located in Mountain View, California. Google analytics uses cookies which enable us to analyse your usage of our website.The information regarding your use is normally transferred to a server from google in the US and saved there. Only in extraordinary circumstances is your full IP-address transferred to a google server in the US and transformed there. In our mission, google will use these informations google analytics collects, to do reporting about user activity of Odin Poker and fulfil other services regarding website usage and user custom. The data collected will not be united with other data in possession of Google Inc.</div>
            <div className='header font-weight-bold'>Further Data Elicitation</div>
            <div className='mb-4 mt-4'>To service you with fitting information, we might process data you have provided voluntarily like birth date, zip code, interests, hobbies etc.</div>
            <div className='header font-weight-bold'>Your Rights</div>
            <div className='mb-4 mt-4'>You principally have the right to request access, correction, deletion, restriction, transfer or withdrawal of your personal data. Contact us for information and further guidance. Be noticed that it might take up to 30 days to come up with the appropriate fulfilment of your request and that altering your data might lead to restricted use of odinpoker.io. Also, you can contact us if you believe to find a breach in data privacy laws through <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a> as well as at your local data protection authority.</div>
            <div className='header font-weight-bold'>Changes to this Privacy Policy</div>
            <div className='mb-4 mt-4'>This privacy policy is effective of March 16th 2021 and will remain in effect as long as odinpoker.io services remain active. We reserve the right to change or update our privacy policy at any time and hence feel free to check it periodically. Your continued use of the service after we post any modifications to the privacy policy on this page will constitute your acknowledgment of the modifications and your consent to abide and be bound by the modified privacy policy. If we happen to make any material changes to this privacy policy, we will notify our customers through the e-mail addresses provided or by placing a prominent notice on our website as well as social media and other official channels of odinpoker.io.</div>
            <div className='header font-weight-bold'>Contact</div>
            <div className='mb-4 mt-4'>For further information on Odin Poker’s privacy policy feel free to contact us here:  <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a></div>
          </div>
        </div>
     </div>
    )
  }
}
CTermsConditions.contextTypes = {
  t: PropTypes.func
}

export default CTermsConditions
