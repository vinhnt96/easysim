import * as yup from 'yup';

export const buildSchema = (locale, translation) => {
  let t = translation

  let typeRules = locale === 'en' ?
    yup.string().trim().required(t('required_field')) :
    yup.string().trim()
       .required(t('validation.required'))
       .email(t('validation.wrong_format'))

  let subjectRules = locale === 'en' ?
    yup.string().trim().required(t('required_field')):
    yup.string().trim()
       .required(t('validation.required'))

  let descriptionRules = locale === 'en' ?
  yup.string().trim().required(t('required_field')):
  yup.string().trim()
    .required(t('validation.required'))

  return yup.object({
    bug_type: typeRules,
    subject: subjectRules,
    description: descriptionRules,
  })
}
