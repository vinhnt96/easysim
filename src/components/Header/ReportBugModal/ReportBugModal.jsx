import React, { Component } from 'react'
import './ReportBugModal.scss'
import PropTypes from 'prop-types'
import { Form, Modal, Row, Col } from 'react-bootstrap'
import FormField from 'components/Shared/FormField/FormField'
import { buildSchema } from './validation'
import { connect } from 'react-redux'
import { Formik } from 'formik'
import LaddaButton from 'components/LaddaButton/component'
import { toast } from 'react-toastify';
import { notify } from 'utils/utils'

class CReportBugModal extends Component {

  renderForm = formProp => {
    let {handleSubmit,
      handleChange,
      isSubmitting,
      errors,
      values,
      touched,
      handleBlur
    } = {...formProp}

    return (
      <Form
      className='fadeIn mb-4'
      onSubmit={handleSubmit}
      >
        <Row>
          <Col sm={3}>
            <Form.Group controlId='controlId-bug-type'>
              <FormField
                field={'bug_type'}
                fieldType={'text'}
                autoComplete='on'
                error={touched.bug_type && errors.bug_type}
                isSubmitting={isSubmitting}
                handleChange={handleChange}
                values={values}
                isRequired={true}
                onBlur={handleBlur}
              />
            </Form.Group>
          </Col>
          <Col sm={9}>
            <Form.Group controlId='controlId-subjectl'>
              <FormField
                field={'subject'}
                fieldType={'text'}
                autoComplete='on'
                error={touched.subject && errors.subject}
                isSubmitting={isSubmitting}
                handleChange={handleChange}
                values={values}
                isRequired={true}
                onBlur={handleBlur}
              />
            </Form.Group>
          </Col>
        </Row>
        <Row className='mt-2'>
          <Col sm={12}>
            <Form.Group controlId='controlId-description'>
                <FormField
                  field={'description'}
                  autoComplete='on'
                  error={touched.description && errors.description}
                  isSubmitting={isSubmitting}
                  handleChange={handleChange}
                  values={values}
                  rows={4}
                  as={'textarea'}
                  isRequired={true}
                  onBlur={handleBlur}
                />
              </Form.Group>

          </Col>
          <Col className='mt-4 mb-2' sm={12}>
            <LaddaButton
              type='submit'
              text='Submit'
              className='submit-btn w-100 my-2-3 py-2 btn-primary font-size-20 font-weight-600'
              disabled={isSubmitting} />
          </Col>
        </Row>
      </Form>
    )
  }

  onSubmit(data, actions) {
    let t = this.context.t
    let schemaValidation = buildSchema(this.props.i18nState.lang, t)
    let trimmedData = schemaValidation.cast(data);

    window.axios.post('bug_reports', { report_bug_info: { ...trimmedData, page: window.location.href } }).then((res) => {
      notify("Bug report has been sent successfully!", toast.success);
    }).catch(() => {
      notify("Failed to send the bug report!", toast.error);
    })
    this.props.setModalShow('')
  }


  render() {
    let t = this.context.t
    let schemaValidation = buildSchema(this.props.i18nState.lang, t)
    let { isShow, setModalShow } = this.props

    return (
      <Modal
        show={isShow}
        onHide={() => setModalShow('')}
        dialogClassName='report_bug_modal d-flex flex-column justify-content-center'
        aria-labelledby='report_bug_modal'
      >
        <Modal.Body>
          <Modal.Header bsPrefix='custom-modal-header mb-4' closeButton>
            <Modal.Title> </Modal.Title>
          </Modal.Header>
          <div className='content'>
            <div className='content-header mb-5 mt-2'>
              <h4 className='heading-font font-weight'> {t('contact.report_issue')} </h4>
            </div>
            <Formik
                component={this.renderForm}
                validationSchema={schemaValidation}
                onSubmit={this.onSubmit.bind(this)}
                initialValues={{ bug_type: '', subject: '', description: ''}}/>
            <div className='seperate'></div>
            <div className='content-footer mt-4 text-center '>
              {t('contact.or_send_us')}
              <span className='text-primary'> 
                <a href={`mailto:${t('odin_support_email')}`}> {t('odin_support_email')}.</a>
              </span>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    )
  }
}

CReportBugModal.contextTypes = {
  t : PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState
})

const mapDispatchToProps = (dispatch) => ({

})

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CReportBugModal)
