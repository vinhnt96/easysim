import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import {withRouter} from 'react-router'
import './BasicAuth.scss'
import logo from '../../images/Logos/vertical_odin_logo.png'
import discordLogo from 'images/Logos/discord_logo_white.svg'
import loadable from '@loadable/component'
const PopupAuth = loadable(() => import('./PopupAuth'))

class CBasicAuth extends Component {

  constructor(props) {
    super(props)
    this.state = { open: false }
  }

  componentDidMount() {
    const { state } = this.props.location
    if (process.env.REACT_APP_BASIC_AUTH_ENVIRONMENT === 'production' && state !== 'vipEarlyAccess') {
      window.location.replace('/')
    }
  }

  render() {
    const t = this.context.t
    return (
      <div className='basic-auth-accept-screen padding-content'>
        <PopupAuth
          open={this.state.open}
          onClose={() => this.setState({open: true}) }
          history={this.props.history}
        />
        <Button
          className='btn-accept-login font-size-24 font-weight-600'
          onClick={() =>  this.setState({open: true})}>{t('log_in')}</Button>
        <div className='logo-image text-center'>
          <img alt='logo' className='logo w-100' src={logo}/>
          <p className='text-white text-center title'> {t('landing_page.comming_soon')} </p>
          <div className='d-inline-block'>
            <a href={t('contact.discord_link')} target='blank' className='join-discord font-size-20 text-primary-color text-decoration-none'>
              {t('join_our_discord')}
              <div className="discord-logo-container">
                <img src={discordLogo} alt='discord-logo' className='h-100 discord-logo' />
              </div>
            </a>
          </div>
        </div>
      </div>

    )
  }
}

CBasicAuth.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
})

const mapDispatchToProps = {
}

const BasicAuth = connect(
  mapStoreToProps,
  mapDispatchToProps
)(CBasicAuth)

export default withRouter(BasicAuth)
