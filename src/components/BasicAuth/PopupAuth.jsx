import React, {Component} from 'react'
import PropTypes from 'prop-types'
import { Formik } from 'formik';
import { Form, Modal } from 'react-bootstrap'
import LaddaButton from '../LaddaButton/component'
import { getAccess } from 'services/basic_auth_service.js'

class PopupAuth extends Component {

  privateForm = formikProps => {
    const t = this.context.t
    let {
      handleSubmit,
      handleChange,
      isSubmitting,
      values
    } = {...formikProps}

    return <Form
      noValidate
      className='fadeIn'
      onSubmit={handleSubmit}>
      <Form.Group controlId="signinform-email">
        <Form.Label>{t('username')}<span className='text-red'> *</span></Form.Label>
        <Form.Control
          readOnly={isSubmitting}
          autoFocus
          onChange={handleChange}
          defaultValue={values.username}
          name='username'
          type="text"
          placeholder={t('username_placeholder')} />
      </Form.Group>
      <Form.Group controlId="signinform-password">
        <div className='d-flex justify-content-between align-items-center'>
          <Form.Label>{t('password')}<span className='text-red'> *</span></Form.Label>
        </div>
        <Form.Control
          autoComplete="off"
          readOnly={isSubmitting}
          onChange={handleChange}
          defaultValue={values.password}
          name='password'
          type="password"
          placeholder={t('password_placeholder')} />
      </Form.Group>
      <Form.Group className='d-flex justify-content-center align-items-center'>
        <LaddaButton
          className='w-100 submit py-2 btn-primary font-size-18 font-weight-600'
          type='submit'
          text='Submit'
          disabled={isSubmitting} />
      </Form.Group>
    </Form>
  }

  onSubmit(user, actions){
    const { vipEarlyAccess, history, onClose } = this.props
    if(getAccess(user)) {
      vipEarlyAccess ? onClose() : history.push('/');
    } else {
      alert('Your account is incorrectly. Please contact the site administrator.')
      actions.setSubmitting(false)
    }
  }

  render() {
    const { open, onClose } = this.props
    const initialValues = {
      username: '',
      password: '',
    }
    return <Modal
        show={open}
        onHide={onClose}
        dialogClassName='auth-modal d-flex flex-column justify-content-center'
      >
      <Modal.Body>
        <Modal.Header bsPrefix='custom-modal-header mb-4'>
          <Modal.Title>
            <div className='mb-3 d-flex justify-content-center font-size-24 font-weight-bold'>
              Please Login to Get Access
            </div>
          </Modal.Title>
        </Modal.Header>
        <Formik
          component={this.privateForm}
          onSubmit={this.onSubmit.bind(this)}
          initialValues={initialValues}
        />
      </Modal.Body>
    </Modal>
  }
}

PopupAuth.contextTypes = {
  t: PropTypes.func,
};

PopupAuth.defaultProps = {
  vipEarlyAccess: false
};

export default PopupAuth;
