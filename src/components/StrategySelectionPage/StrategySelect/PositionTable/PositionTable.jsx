import React, { Component } from 'react'
import './PositionTable.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  CASH_GAME_PLAYER_POSITIONS,
  MTT_GAME_PLAYER_POSITIONS,
  HU_GAME_PLAYER_POSITIONS,
  DOWNCASE_POSITIONS_MAPPING
} from 'config/constants'
import { isEqual } from 'lodash'
import dealer_button from 'images/coins/dealer_button.png'

class CPositionTable extends Component {

  getPlayerPositions(players, gameType) {
    let playerPositions = []
    if(isEqual(gameType, 'cash')) {
      playerPositions = isEqual(players, 'hu') ? HU_GAME_PLAYER_POSITIONS : isEqual(players, '8max') ? MTT_GAME_PLAYER_POSITIONS : CASH_GAME_PLAYER_POSITIONS
    } else {
      playerPositions = MTT_GAME_PLAYER_POSITIONS
    }

    return playerPositions
  }

  playerPositionEles() {
    const { currentUser, gameType, positions, handleOnChange, players } = this.props
    const playerPositions = this.getPlayerPositions(players, gameType)
    const preferences = currentUser.preferences || {}
    const displayPositionsByDefault = preferences.display_positions_by_default || false

    return playerPositions.map((position, index) => {
      const highlighted = positions.includes(position)
      const positionDisplay = displayPositionsByDefault ? position.toUpperCase() : DOWNCASE_POSITIONS_MAPPING[position].toUpperCase()
      const selectedPositionClass = positions.includes(position) ? 'selected-position' : ''

      return (
        <div
          key={index}
          className={`position-block ${position} ${players}`}
          onClick={() => { handleOnChange(position) }}
        >
          <div className={`position-point ${highlighted ? 'active' : ''} ${selectedPositionClass}`}>
            <div className='position font-size-15'>{positionDisplay}</div>
          </div>
          {
            position === 'b' &&
              <img className='dealer-button' src={dealer_button} alt=''/>
          }
        </div>
      )
    });
  }

  render() {
    const { gameType, players } = this.props
    const positionsClass = gameType === 'mtt' ? gameType : players === '8max' ? 'mtt' : 'cash'
    return (
      <div className='positions-table'>
        <div className='sim-deck'>
          <div className='deck-container d-flex position-relative'>
            <div className='deck d-flex flex-column align-items-center justify-content-center'>
              <div className={`${positionsClass}`}>
                { this.playerPositionEles() }
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CPositionTable.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CPositionTable)

