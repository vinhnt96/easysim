import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import {
  STREET_SELECT_OPTIONS,
  GAME_TYPES,
  DEFAULT_STACK_OPTIONS,
  SITE_OPTIONS,
  DEFAULT_STAKE_OPTIONS,
  OPEN_SIZE_OPTIONS,
  PLAYERS_OPTIONS,
} from 'config/constants'
import { isEqual, startCase, toUpper, isEmpty} from 'lodash'
import { userSubscriptionInvalid, fullAccessWithFreeAWeek } from 'utils/utils'
import CustomSelect from 'components/StrategySelectionPage/CustomSelect/component'
import PositionTable from 'components/StrategySelectionPage/StrategySelect/PositionTable/PositionTable'

class CLeftSection extends Component {

  constructor(props) {
    super(props)
    const { affiliate_code, createdAt, email } = props.currentUser
    this.inValid = userSubscriptionInvalid(props.currentUser)
    this.freeAWeek = fullAccessWithFreeAWeek(affiliate_code, createdAt, email)
  }

  convertTitleMtt(value) {
    if (value === 'mtts') return 'mtt'
    return value
  }

  enabledInput() {
    const { street } = this.props.strategySelection
    return this.freeAWeek || isEqual(street, 'preflop') || !this.inValid
  }

  labelColor(equal, child) {
    const labelColor = this.enabledInput() && equal ? 'selected' : 'deepblue'
    return <div className={`rounded-circle font-weight-bold text-center ${labelColor}`}>{child}</div>
  }

  labelColorForPlayers(isEqual, label) {
    const labelColor = this.enabledInput() && isEqual ? 'selected' : 'deepblue'
    return (
      <Fragment>
        <div className={`rounded-circle font-weight-bold text-center ${labelColor} ${label === '8MAX' ? 'position-relative' : ''}`}>
          {label}
          { label === '8MAX' && 
            <div className={`font-size-11 ${label === '8MAX' ? 'position-absolute' : ''}`}>
              ante
            </div>
          }
        </div>
      </Fragment>
    )
  }

  streetOptionsElements() {
    const { strategySelection: { street }, handleChangeValues } = this.props;
    return STREET_SELECT_OPTIONS.map((option, index) => {
      const equal = isEqual(street, option)
      const label = this.labelColor(equal, startCase(option))

      return <CustomSelect
        key={index}
        type='radio'
        name='street'
        label={label}
        value={option}
        checked={this.enabledInput() && equal}
        disabled={isEqual(option, 'postflop') && !this.freeAWeek && this.inValid}
        handleOnChange={handleChangeValues}
      />
    })
  }

  playersOptionsElements() {
    const { strategySelection: { players }, handleChangeValues } = this.props;
    return PLAYERS_OPTIONS.map((option, index) => {
      const equal = isEmpty(players) ? index === 0 : isEqual(players, option)
      const label = this.labelColorForPlayers(equal, toUpper(option))
      return <CustomSelect
        key={index}
        type='radio'
        name='players'
        label={label}
        value={option}
        checked={equal}
        handleOnChange={handleChangeValues}
      />
    })
  }

  gameTypeOptionsElements() {
    const { currentUser: { admin_access, accessable_game_type }, strategySelection: { game_type, street }, handleChangeValues } = this.props
    const nonAccess = isEmpty(admin_access) || isEqual(admin_access, 'free')
    const fullAccess = isEqual(admin_access, 'cash-mtts') || (nonAccess && isEqual(accessable_game_type, 'cash-mtts')) || this.freeAWeek || isEqual(street, 'preflop')
    const gameTypeList = fullAccess ? GAME_TYPES : nonAccess ? [this.convertTitleMtt(accessable_game_type)] : [this.convertTitleMtt(admin_access)]
    return gameTypeList.map((type, index) => {
      const equal = isEqual(game_type, type)
      const label = this.labelColor(equal, type.toUpperCase())

      return <CustomSelect
        key={index}
        type='radio'
        name='game_type'
        label={label}
        value={type}
        checked={this.enabledInput() && equal}
        disabled={!this.enabledInput()}
        handleOnChange={handleChangeValues}
      />
    })
  }

  stackOptionElements() {
    const { strategySelection, handleChangeValues } = this.props
    const { stack, game_type } = strategySelection
    const stackOptions = DEFAULT_STACK_OPTIONS[game_type] || []
    return stackOptions.map((value, index) => {
      const equal = isEqual(parseFloat(stack), parseFloat(value))
      const label = this.labelColor(equal, value)
      return <CustomSelect
        key={index}
        type='radio'
        name='stack'
        label={label}
        value={value}
        defaultChecked={this.enabledInput() && equal}
        disabled={!this.enabledInput()}
        handleOnClick={handleChangeValues}
      />
    })
  }

  typeOptionElements() {
    const t = this.context.t
    const { strategySelection: { sim_type }, typeOptions, handleChangeValues } = this.props
    return typeOptions.length === 0 ?
      <p>{t('no_options')}</p> :
      typeOptions.map((value, index) => {
        const equal = isEqual(sim_type, value)
        const label = this.labelColor(equal, t(`type_options.${value}`))

        return <CustomSelect
          key={index}
          type='radio'
          name='sim_type'
          label={label}
          value={value}
          checked={this.enabledInput() && equal}
          disabled={!this.enabledInput()}
          handleOnChange={handleChangeValues}
          />
      })
  }

  siteOptionsElements() {
    const { strategySelection: { site }, handleChangeValues } = this.props;
    return SITE_OPTIONS.map((value, index) => {
      const equal = isEqual(site, value)
      const label = this.labelColor(equal, startCase(value))
      return <CustomSelect
        key={index}
        type='radio'
        name='site'
        label={label}
        value={value}
        checked={this.enabledInput() && equal}
        disabled={!this.enabledInput()}
        handleOnChange={handleChangeValues}
      />
    })
  }

  stakeOptionElements() {
    const { strategySelection: { site, stake }, handleChangeValues } = this.props;
    const stakeOptions = DEFAULT_STAKE_OPTIONS[site] || []
    return stakeOptions.map((option, index) => {
      const equal = isEqual(stake, option.value)
      const label = this.labelColor(equal, option.label)
      return <CustomSelect
        key={index}
        type='radio'
        name='stake'
        label={label}
        value={option.value}
        defaultChecked={this.enabledInput() && equal}
        disabled={!this.enabledInput()}
        handleOnClick={handleChangeValues}
      />
    })
  }

  opensizeOptionElements() {
    const { strategySelection: { open_size }, handleChangeValues } = this.props;

    return OPEN_SIZE_OPTIONS.map((size, index) => {
      const equal = isEqual(open_size, size)
      const label = this.labelColor(equal, size)
      return <CustomSelect
        key={index}
        type='radio'
        name='open_size'
        label={label}
        value={size}
        checked={this.enabledInput() && equal}
        disabled={!this.enabledInput()}
        handleOnChange={handleChangeValues}
      />
    })
  }

  render() {
    const t = this.context.t
    const { strategySelection: { street, game_type, positions, players }, handlePositionsOnChange } = this.props
    return (
      <div className='col-md-3 pl-0'>
        <div className='left-container mb-3 h-100'>
          <div className='street-select p-3'>
            <p className='mb-4 font-weight-bold'>{ t('street_select_label') }</p>
            {this.streetOptionsElements()}
          </div>
          <div className='game-type-select p-3'>
            <p className='mb-4 font-weight-bold'>{ t('game_type_select_label') }</p>
            {this.gameTypeOptionsElements()}
          </div>
          { game_type === 'cash' &&
            <div className='game-type-select p-3'>
              <p className='mb-4 font-weight-bold'>{ t('players_select_label') }</p>
              {this.playersOptionsElements()}
            </div>
          }
          <div className='stack-select p-3'>
            <p className='mb-4 font-weight-bold'>{ t('stack_select_label') }</p>
            {this.stackOptionElements()}
          </div>
          {
            !isEqual(street, 'preflop') && (
              <Fragment>
                <div className='positions-select p-3'>
                  <p className='mb-4 font-weight-bold'>{ t('positions_select_label') }</p>
                  <PositionTable
                    gameType={game_type}
                    positions={positions}
                    players={players}
                    handleOnChange={handlePositionsOnChange}
                  >
                  </PositionTable>
                </div>
                <hr className='mx-3'></hr>
                <div className='type-select p-3'>
                  <p className='mb-4 font-weight-bold'>{ t('type_select_label') }</p>
                  {this.typeOptionElements()}
                </div>
              </Fragment>
            )
          }
          {
            (isEqual(street, 'preflop') && isEqual(game_type, 'cash') && !isEqual(players, '8max')) && (
              <Fragment>
                <div className='site-select p-3'>
                  <p className='mb-4 font-weight-bold'>{ t('site_select_label') }</p>
                  {this.siteOptionsElements()}
                </div>
                <div className='stake-select p-3'>
                  <p className='mb-4 font-weight-bold'>{ t('stake_select_label') }</p>
                  {this.stakeOptionElements()}
                </div>
                <div className='open-size-select p-3'>
                  <p className='mb-4 font-weight-bold'>{ t('open_size_select_label') }</p>
                  {this.opensizeOptionElements()}
                </div>
              </Fragment>
            )
          }
        </div>
      </div>
    )
  }
}

CLeftSection.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CLeftSection)

