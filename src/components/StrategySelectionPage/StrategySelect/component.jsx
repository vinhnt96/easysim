import React, { Component, Fragment } from 'react'
import { connect } from 'react-redux'
import './stylesheet.scss'
import PropTypes from 'prop-types'
import {
  DEFAULT_STACK_SIZE,
  POSITIONS_COUNT,
  DEFAULT_POSITIONS,
  DEFAULT_STREET_OPTIONS,
  TYPE_OPTIONS,
  FLOP_CARDS_COUNT,
  DEFAULT_STAKE_OPTIONS,
  PREFLOP_CASH_BLANK_FIELDS,
  PREFLOP_MMT_BLANK_FIELDS,
  POSTFLOP_BLANK_FIELDS,
  INIT_STRATEGY_SELECTION,
  DEFAULT_PLAYERS,
  PREFLOP_CASH_8MAX_FIELDS,
  INIT_PREFLOP_FIELDS
} from 'config/constants'
import { handleOnOpeningNewSim } from '../../../actions/game.actions'
import { convertFlopCards, buildConvertSuitsTemplate } from 'services/convert_cards_services'
import {
  checkRemoveTypeSRP,
  checkRemoveTypeLimp,
  notify,
  userSubscriptionInvalid,
  fullAccessWithFreeAWeek
} from 'utils/utils'
import { Modal } from 'react-bootstrap'
import { cloneDeep, isEqual, isEmpty } from 'lodash'
import { toast } from 'react-toastify'
import loadable from '@loadable/component'

const LeftSection = loadable(() => import('./LeftSection/LeftSection'))
const RightSection = loadable(() => import('./RightSection/RightSection'))
const NotifyModal = loadable(() => import('components/NotifyModal/component'))
const NAME_LIST = ['game_type', 'stack', 'positions', 'players']

class CStrategySelect extends Component {

  constructor(props) {
    super(props);

    const { currentUser: { strategy_selection, affiliate_code, createdAt, email, admin_access, accessable_game_type }, location: { state } } = props
    this.freeAWeek = fullAccessWithFreeAWeek(affiliate_code, createdAt, email)
    this.initStrategySelection = cloneDeep(INIT_STRATEGY_SELECTION)
    const streetValue = !this.freeAWeek && userSubscriptionInvalid(props.currentUser) ? 'preflop' : DEFAULT_STREET_OPTIONS
    const gameType = this.defaultGameType()
    this.initStrategySelection = {
      ...this.initStrategySelection,
      street: streetValue,
      game_type: gameType,
      stack: DEFAULT_STACK_SIZE[gameType],
      positions: DEFAULT_POSITIONS[gameType]
    }
    this.access = !((isEmpty(admin_access) || admin_access === 'free') && isEmpty(accessable_game_type))
    if ( state && state.new_sim && this.access ) {
      const { street, game_type, stack_size, positions: stratePositions, sim_type, site, stake, open_size, flop_cards, specs } = strategy_selection
      if ( street === 'preflop' || ((!isEmpty(admin_access) && admin_access.includes(game_type)) || (!isEmpty(accessable_game_type) && accessable_game_type.includes(game_type)))) {
        this.initStrategySelection = {
          positions: [...stratePositions],
          flops: [...flop_cards],
          stack: stack_size,
          game_type,
          street,
          players: specs,
          sim_type,
          site,
          stake,
          open_size,
        }
      } else {
        this.initStrategySelection = {...this.initStrategySelection, positions: [...stratePositions], flops: [...flop_cards] }
      }
    }

    this.state = {
      currentFlopCardPosition: 0,
      typeOptions: [],
      strategySelection: this.initStrategySelection,
      sidebarOpen: false,
      open: false,
      loading: false,
      disabled: false,
      valid: true,
      invalidLoadingTree: false,
      sims: []
    }

    this.handlePositionsOnChange = this.handlePositionsOnChange.bind(this);
    this.handleFlopCardsOnChange = this.handleFlopCardsOnChange.bind(this);
    this.handleCardOnClick = this.handleCardOnClick.bind(this);
    this.handleNextBtnOnclick = this.handleNextBtnOnclick.bind(this);
    this.checkMissingInput = this.checkMissingInput.bind(this);
    this.missingInputMessage = this.missingInputMessage.bind(this);
  }

  componentDidMount() {
    const { state } = this.props.location;
    if (!isEmpty(state) && state.login) {
      this.setState({ open: true })
    }
    if (!isEqual(this.state.strategySelection.street, 'preflop')) {
      this.fetchTypeOptions()
    }
    sessionStorage.setItem("treeInfo", null)
  }

  defaultGameType() {
    const { admin_access, accessable_game_type } = this.props.currentUser
    if (isEmpty(admin_access) || isEqual(admin_access, 'free')) {
      if (isEqual(accessable_game_type, 'cash-mtts') || isEqual(accessable_game_type, 'cash') || this.freeAWeek)
        return 'cash'
      return 'mtt'
    } else {
      if (isEqual(admin_access, 'cash-mtts') || isEqual(admin_access, 'cash'))
        return 'cash'
      return 'mtt'
    }
  }

  fetchTypeOptions = () => {
    const { game_type, stack, positions, players } = this.state.strategySelection
    const payload = {
      game_type,
      stack,
      positions,
      specs: players === '8max' ? players : ''
    }
    this.setState({ disabled: true })
    window.axios.get('type_options_mappings/get_type_options', { params: payload }).then(({ data }) => {
      const { strategySelection } = this.state;
      let typeOptions = data.type_options || [];
      if (checkRemoveTypeSRP(strategySelection))
        typeOptions = typeOptions.filter(option => !isEqual(option, 'srp')) || TYPE_OPTIONS

      if (checkRemoveTypeLimp(strategySelection))

        typeOptions = typeOptions.filter(option => !isEqual(option, 'limped')) || TYPE_OPTIONS
      const newStrategySelection = { ...strategySelection, sim_type: isEmpty(typeOptions) ? '' : typeOptions[0] }
      this.setState({
        disabled: false,
        strategySelection: newStrategySelection,
        typeOptions
      });
    })
  }

  checkMissingInput = () => {
    const { strategySelection, typeOptions } = this.state;
    const { street, positions, flops, sim_type } = strategySelection

    if(isEqual(street, 'postflop')) {
      if(positions.length < POSITIONS_COUNT)
        return 'positions'

      if(isEmpty(typeOptions))
        return 'no_options'

      if(typeOptions.length > 0 && isEmpty(sim_type))
        return 'sim_type'

      if(flops.length !== FLOP_CARDS_COUNT ||
        flops.some(card => isEmpty(card)))
        return 'flop_cards'
    }
    return '';
  }

  missingInputMessage = (name) => {
    const t = this.context.t
    const { flops, positions } = this.state.strategySelection
    const selectedCardsCount = flops.filter(card => !isEmpty(card)).length;
    const selectedPositionsCount = positions.filter(position => !isEmpty(position)).length;

    switch(name) {
      case 'positions':
        return t('select_more_positions', { number: POSITIONS_COUNT - selectedPositionsCount });
      case 'flop_cards':
        return t('select_more_cards', { number: FLOP_CARDS_COUNT - selectedCardsCount });
      case 'no_options':
        return t('no_options');
      default:
        return t('missing_input', { input_name: t(`input_name.${name}`)});
    }
  }

  handleChangeValues = ({ target }) => {
    const { location: { state } } = this.props
    const { name, value } = target
    const newStrategy = {...this.newStrategySelection(name, value)}

    this.setState({ strategySelection: newStrategy }, () => {
      if ((NAME_LIST.includes(name) && isEqual(newStrategy['street'], 'postflop')) ||
          (['street'].includes(name) && state && state.new_sim )) {
        this.fetchTypeOptions()
      }
    });
  }

  newStrategySelection(name, value) {
    const { currentUser: { admin_access, accessable_game_type }, location: { state } } = this.props
    let { strategySelection } = this.state
    const { players, street, game_type } = strategySelection
    strategySelection[`${name}`] = value
    switch (name) {
      case 'street':
        if(state && state.new_sim) {
          if(value === 'postflop') {
            strategySelection['positions'] = players === 'hu' ? ['bb', 'b'] : DEFAULT_POSITIONS['cash']
          }
        }
        if (value === 'preflop' && game_type === 'cash' && players === '8max') {
          strategySelection = {...strategySelection, ...PREFLOP_CASH_8MAX_FIELDS }
        } else if( value === 'preflop' && game_type === 'cash') {
          strategySelection = {...strategySelection, ...INIT_PREFLOP_FIELDS }
        }
        if ( value === 'postflop' && ((!isEmpty(admin_access) && !admin_access.includes(strategySelection['game_type'])) || (!isEmpty(accessable_game_type) && !accessable_game_type.includes(strategySelection['game_type'])))) {
          const gt = this.defaultGameType()
          strategySelection['game_type'] = gt
          strategySelection['stack'] = DEFAULT_STACK_SIZE[gt]
        }
        return strategySelection
      case 'site':
        strategySelection['stake'] = DEFAULT_STAKE_OPTIONS[value][0].value
        return strategySelection
      case 'game_type':
        strategySelection['stack'] = DEFAULT_STACK_SIZE[value]
        strategySelection['positions'] = DEFAULT_POSITIONS[value]
        strategySelection['players'] = DEFAULT_PLAYERS
        if(state && state.new_sim && street === 'preflop' && value === 'cash') {
          strategySelection = {...strategySelection, ...INIT_PREFLOP_FIELDS}
        }
        if (street === 'preflop' && value === 'cash' && players === '8max') {
          strategySelection = {...strategySelection, ...PREFLOP_CASH_8MAX_FIELDS }
        } else if (street === 'preflop' && value === 'cash') {
          strategySelection = {...strategySelection, ...INIT_PREFLOP_FIELDS }
        }
        return strategySelection
      case 'players':
        strategySelection['positions'] = value === 'hu' ? ['bb', 'b'] : DEFAULT_POSITIONS['cash']
        if(value === 'hu') strategySelection['alternate_blind_vs_blind_strategies'] = ''
        if (street === 'preflop' && game_type === 'cash' && value === '8max') {
          strategySelection = {...strategySelection, ...PREFLOP_CASH_8MAX_FIELDS }
        } else if (street === 'preflop' && game_type === 'cash') {
          strategySelection = {...strategySelection, ...INIT_PREFLOP_FIELDS }
        }
        return strategySelection
      default:
        return strategySelection
    }
  }

  handlePositionsOnChange(position) {
    const { strategySelection } = this.state
    const { players } = strategySelection
    if(players === 'hu')
      return

    let positionsValue = [...strategySelection.positions]

    if(positionsValue.includes(position)) {
      strategySelection['positions'] = positionsValue.filter(value => value !== position)
    } else {
      if(positionsValue.length < POSITIONS_COUNT) {
        strategySelection['positions'] = positionsValue.concat(position)
      } else if(positionsValue.length === POSITIONS_COUNT) {
        positionsValue[0] = position;
        strategySelection['positions'] = positionsValue.reverse()
      }
    }

    this.setState({ strategySelection: strategySelection }, () => {
      if (strategySelection['positions'].length === 2) {
        this.fetchTypeOptions()
      }
    });
  }

  handleFlopCardsOnChange({ target }) {
    const { strategySelection, currentFlopCardPosition } = this.state
    const { flops } = strategySelection
    const { checked, value } = target
    if (checked) {
      let index = currentFlopCardPosition;
      flops[index] = value;
      const emptyCardIndex = flops.indexOf('');
      const nextIndex = emptyCardIndex > -1 ? emptyCardIndex : (++index % FLOP_CARDS_COUNT)
      this.setState({
        currentFlopCardPosition: nextIndex,
        strategySelection: { ...strategySelection, flops }
      });
    }
  }

  handleCardOnClick({ target }) {
    const { strategySelection } = this.state
    const { flops } = strategySelection
    const { value, index } = target.dataset
    if (value && flops.includes(value)) {
      const idx = flops.indexOf(value);
      flops[idx] = '';
    }
    this.setState({
      currentFlopCardPosition: parseInt(index),
      strategySelection: {...strategySelection, flops }
    })
  }

  inValidFlopsCard() {
    const { game_type, street, stack, positions, sim_type } = this.state.strategySelection
    const inValidPositons = [['b1', 'b2'].sort(), ['b', 'b1'].sort()]
    if (isEqual(street, 'postflop') &&
        isEqual(game_type, 'mtt') &&
        isEqual(stack, '50') &&
        isEqual(sim_type, '4bp') &&
        (isEqual(inValidPositons[0], positions.sort()) || isEqual(inValidPositons[1], positions.sort()))
      )
      return true
    return false
  }

  handleNextBtnOnclick() {
    if (this.inValidFlopsCard()) {
      this.setState({ valid: false })
    } else {
      const { strategySelection } = this.state
      const { game_type, street, players } = strategySelection
      const preflopBlankFields = isEqual(game_type, 'cash') ?
        PREFLOP_CASH_BLANK_FIELDS :
        PREFLOP_MMT_BLANK_FIELDS
      const specs = isEqual(game_type, 'cash') ? players : ''
      const payload = isEqual(street, 'preflop') ?
                        { ...strategySelection, ...preflopBlankFields, specs: specs } :
                        { ...strategySelection, ...POSTFLOP_BLANK_FIELDS, specs: specs }

      this.setState({ loading: true })
      this.getStategySelection(payload)
    }
  }

  getStategySelection(payload) {
    window.axios.post('strategy_selections', { strategy_selection: payload }).then(({ data }) => {
      const { strategy_selection, user_strategy_selection, status } = data
      if(parseInt(status) === 4) {
        notify("Some options on your Strategy selection are incorrect. Please try agains.", toast.error, {time: 3000})
        this.setState({
          strategySelection: this.initStrategySelection,
          loading: false 
        })
        this.fetchTypeOptions()
      } else {
        const { currentUser, history } = this.props
        const { flop_cards } = strategy_selection
        const cardsConvertingTemplate = buildConvertSuitsTemplate(flop_cards)
        const convertedFlopCards = convertFlopCards(flop_cards, cardsConvertingTemplate)
        const dataStrategySelection = { ...strategy_selection, convertedFlopCards }
        sessionStorage.setItem("strategy_selections", JSON.stringify(dataStrategySelection))
        sessionStorage.removeItem("user_strategy_selection");
        this.props.handleOnOpeningNewSim(cardsConvertingTemplate, dataStrategySelection, {...currentUser, strategy_selection, user_strategy_selection})
        // this.setState({ loading: false })
        history.push('/preflop-page')
      }
    }, err => { alert("Something went wrong!") })
  }

  handleOnCloseNotifyModal() {
    if (this.state.valid) {
      this.props.history.push('/')
    } else {
      this.setState({ valid: true })
    }
  }

  //delete only for a short times
  renderModalIPAddress() {
    let t = this.context.t
    const { open } = this.state;
    const { currentUser } = this.props;
    if (open) {
      return (
        <Modal
          show={open}
          dialogClassName='sign-up-success-popup'
          backdrop="static"
          aria-labelledby='sign-up-success-popup'
        >
          <Modal.Body bsPrefix='custom-modal-body'>
            <div className='p-3'>{t('ip_title', {ip: currentUser.ip})}</div>
            <div className='text-center mb-3'>
              <div className='btn btn-primary ok-btn left' onClick={() => this.setState({open: false})}>
                {t('button.ok')}
              </div>
            </div>
          </Modal.Body>
        </Modal>
      )
    }
  }

  render() {
    const t = this.context.t
    const { currentUser: { role } } = this.props
    const { strategySelection, currentFlopCardPosition, typeOptions, loading, disabled, valid, invalidLoadingTree } = this.state
    const { positions } = strategySelection

    return (
      <Fragment>
        <div id='strategy-select-page'>
          <div className='strategy-select-container row mx-auto h-100'>
            <LeftSection
              strategySelection={strategySelection}
              handleChangeValues={(event) => this.handleChangeValues(event)}
              handlePositionsOnChange={this.handlePositionsOnChange}
              typeOptions={typeOptions}
            />
            <RightSection
              strategySelection={strategySelection}
              currentFlopCardPosition={currentFlopCardPosition}
              handleCardOnClick={this.handleCardOnClick}
              handleFlopCardsOnChange={this.handleFlopCardsOnChange}
              checkMissingInput={this.checkMissingInput}
              missingInputMessage={this.missingInputMessage}
              handleNextBtnOnclick={this.handleNextBtnOnclick}
              loading={loading}
              disabled={disabled}
              positions={positions}
            />
          </div>
        </div>
        <NotifyModal
          modalShow={!valid || (invalidLoadingTree && !isEqual(role, 'admin'))}
          infomation={{title: t('notify_modal.title'), message: invalidLoadingTree ? t('notify_modal.can_not_open_more_than_1_sim_message') : ''}}
          handleOnClose={this.handleOnCloseNotifyModal.bind(this)}
          />
      </Fragment>
    )
  }
}

CStrategySelect.contextTypes = {
  t: PropTypes.func
}


const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
  handleOnOpeningNewSim
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CStrategySelect)

