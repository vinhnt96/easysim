import React, { Component, Fragment } from 'react'
import './RightSection.scss'
import { Spinner } from 'react-bootstrap'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { CARD_OPTIONS } from 'config/constants'
import Card from 'components/Card/component'
import CustomSelect from 'components/StrategySelectionPage/CustomSelect/component'
import { checkFreeTrialUser } from 'utils/utils'
import { withRouter } from 'react-router'
import { isEmpty, isEqual } from 'lodash'

class CRightSection extends Component {
  constructor(props) {
    super(props);

    this.handleSubscribeBtnOnClick = this.handleSubscribeBtnOnClick.bind(this);
    this.handleViewSimsBtnOnClick = this.handleViewSimsBtnOnClick.bind(this);
  }

  flopCardElements() {
    const { strategySelection: { flops }, currentFlopCardPosition, handleCardOnDoubleClick, handleCardOnClick } = this.props

    return flops.map((card, index) => {
      const highlighted = currentFlopCardPosition === index ? 'highlighted' : ''

      return <Card
          key={index}
          rank={card.slice(0, -1) || ''}
          suit={card[card.length-1] || ''}
          className={`mx-2 my-2 ${highlighted}`}
          size='big'
          index={index}
          handleOnDoubleClick={handleCardOnDoubleClick}
          handleOnClick={handleCardOnClick}
        />
    })
  }

  cardOptionElements() {
    const { strategySelection: { flops }, handleFlopCardsOnChange } = this.props;

    return CARD_OPTIONS.map((card, index) => {
      const { rank, suit } = card
      const value =`${rank}${suit}`;
      const additionalClass = flops.includes(value) ? 'invisible' : '';
      const label = <Card
        key={index}
        rank={rank}
        suit={suit}
        className={`py-2 text-center ${additionalClass}`}
        size='small'
      />

      return <CustomSelect
        key={index}
        type='checkbox'
        name='flop_cards[]'
        label={label}
        value={value}
        checked={flops.includes(value)}
        handleOnChange={handleFlopCardsOnChange}
      />
    })
  }

  handleSubscribeBtnOnClick() {
    this.props.history.push('/subscribe');
  }

  handleViewSimsBtnOnClick() {
    this.props.history.push('/my-sims');
  }

  showButtonsBasedOnSubscription() {
    const t = this.context.t
    const { strategySelection: { street }, currentUser: { email }, checkMissingInput, missingInputMessage, handleNextBtnOnclick, loading, disabled } = this.props;
    const nextBtnDisabled = !isEmpty(checkMissingInput())
    const nextBtnText =  nextBtnDisabled ? missingInputMessage(checkMissingInput()) : t('next');

    return checkFreeTrialUser(email) ?
      <div className='free-user-buttons text-center'>
        <button
          className={`btn subscribe-btn`}
          onClick={this.handleSubscribeBtnOnClick}
        >
          {t('free_trial.subscribe')}
        </button>
        <span className='px-4'>or</span>
        <button
          className={`btn view-sims-btn`}
          onClick={this.handleViewSimsBtnOnClick}
        >
          {t('free_trial.view_free_sims')}
        </button>
      </div> :
      <button
        className={`btn next-btn float-right ${nextBtnDisabled ? 'cursor-not-allowed' : ''} ${isEqual(street, 'preflop') && 'btn-large'}`}
        disabled={loading || disabled || nextBtnDisabled}
        onClick={handleNextBtnOnclick}
      >
        {loading ? <Spinner animation="border" size="sm" /> : nextBtnText}
      </button>
  }

  render() {
    const t = this.context.t
    const { strategySelection: { street } } = this.props

    return (
      <div className='col-md-9 pr-0'>
        <div className={'right-container mb-3 h-100 ' + street}>
          <p className='select-flop-title p-3'>{t('select_flop_card').toUpperCase()}</p>
            {
              !isEqual(street, 'preflop') && (
                <Fragment>
                  <div className='d-flex justify-content-center my-5'>
                    <div className='flop-cards d-inline-block'>
                      {this.flopCardElements()}
                    </div>
                  </div>
                  <div className='d-flex justify-content-center mb-5'>
                    <div className='card-options d-inline-block'>
                      {this.cardOptionElements()}
                    </div>
                  </div>
                </Fragment>
              )
            }
          <div className='d-flex justify-content-center'>
            <div className='next-buttons d-inline-block mb-5 mr-4'>
              {this.showButtonsBasedOnSubscription()}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

CRightSection.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
}

export default withRouter(connect(
  mapStoreToProps,
  mapDispatchToProps
)(CRightSection));

