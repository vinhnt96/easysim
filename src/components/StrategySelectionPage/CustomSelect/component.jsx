import React, { Component } from 'react'
import './stylesheet.scss'

export default class CustomSelect extends Component {
  render() {
    const { type, name, label, value, checked, handleOnChange, handleOnClick, disabled } = this.props;
    const id = `${name}-${value}`

    return (
      <span className="custom-select-options">
        <label htmlFor={id} className={`option-label mb-0 ${disabled ? 'disabled' : ''}`}>
          {label}
        </label>
        <input
          type={type}
          name={name}
          value={value}
          id={id}
          className='d-none'
          checked={checked}
          disabled={disabled}
          onChange={handleOnChange}
          onClick={handleOnClick}
        />
      </span>
    )
  }
}
