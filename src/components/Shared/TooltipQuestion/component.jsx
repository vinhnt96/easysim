import React, { Component } from 'react'
import ReactTooltip from 'react-tooltip'
import QuestionIcon from 'components/Shared/QuestionIcon/component'

class TooltipQuestion extends Component {
  render() {
    let {explanationName, place, type, offset, questionIconClassName, className, tooltipClassName} = this.props

    return (
      <span className={`tooltip-question-mark ${explanationName} ${className}`}>
        <span data-tip data-for={explanationName} className={questionIconClassName} >
          <QuestionIcon />
        </span>
        <ReactTooltip className={tooltipClassName} id={explanationName} place={place} type={type} effect='solid' offset={!!offset ? offset : {}}>
          {this.props.children}
        </ReactTooltip>
      </span>
    )
  }
}

export default TooltipQuestion
