import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { ButtonGroup, Button} from 'react-bootstrap'
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import {
  generateDecisionFromFlop,
  buildNodeFromDecision,
  isDoubleCheckDecisions,
  calculateStrategyKey,
  positionsForQueryBuilder,
  handleBackToHistory
} from 'services/game_play_service'
import { convertFlopCards } from 'services/convert_cards_services'
import { rollbackDecision, refreshDecisionsTree } from 'actions/decision.actions'
import { makeDecision, updateComparedDecisions, refeshGameData, fetchDataForTurnRiverRound } from 'actions/game.actions'
import {
  ROOT_NODE,
  DEFAULT_POT_IP,
  DEFAULT_POT_OOP,
  DEFAULT_BEGIN_ROUND_POSITION,
  DEFAULT_NODE_COUNT,
} from 'config/constants'
import './Header.scss'
import { changeMatrixView, refeshHandMatrix } from 'actions/hand-matrix.actions'
import { formatToBigBlind, objectWithoutProperties, decisionCombosButtons, userSubscriptionInvalid, fullAccessWithFreeAWeek } from 'utils/utils'
import { isEqual, capitalize, isEmpty } from 'lodash'
import Node from 'components/Shared/Header/Node/Node'
import Loader from '../Loader/Loader'

class CHeader extends Component {
  constructor(props) {
    super(props)

    this.state = {
      dropdownOpen: false,
      fetching: false
    };
    const { affiliate_code, createdAt, email } = props.currentUser
    this.freeAWeek = fullAccessWithFreeAWeek(affiliate_code, createdAt, email)
    this.timer = 0
    this.toggle = this.toggle.bind(this);
    this.handleOnClickDecision = this.handleOnClickDecision.bind(this)
    this.handleBackToRootNode = this.handleBackToRootNode.bind(this)
    this.handleOnReplaceCurrentSim = this.handleOnReplaceCurrentSim.bind(this)
    this.handleOnClickNewTab = this.handleOnClickNewTab.bind(this)
  }

  componentDidUpdate(prevProps) {
    const prevGame = prevProps.game || {}
    const game = this.props.game || {}
    if((prevGame.node_count !== game.node_count || prevGame.shouldReloaded !== game.shouldReloaded) && this.state.fetching) {
      this.setState({
        fetching: false
      })
    }
  }

  toggle() {
    this.setState({ dropdownOpen: !this.state.dropdownOpen });
  }

  handleOnClickDecision(selectedAction) {
    clearTimeout(this.timer)
    const {
      game,
      decisionsTree: { decisions },
      currentUser: { strategy_selection },
      handMatrix: { view: handMatrixView },
      cardsConvertingTemplate,
      makeDecision
    } = this.props
    this.setState({ fetching: true })
    const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || strategy_selection
    const { positions, flop_cards } = strategySelection
    const { currentRound, turnCard, riverCard, currentPosition, nodes, ranges_oop, ranges_ip, pot_main, stackSize, bettedPot } = game
    const { action, num_combos_ip, num_combos_oop, pot_value, pot_oop, pot_ip, pot_value_in_bb, action_display, betPercentOfPot } = selectedAction
    const object = objectWithoutProperties(selectedAction, ['id', 'action_display', 'action', 'bet_level', 'sort_order', 'position', 'order_num'])
    const dataParams = {
      ...strategySelection,
      ...object,
      positions: positionsForQueryBuilder(positions),
      flops: convertFlopCards(flop_cards, cardsConvertingTemplate).join(''),
      view: handMatrixView,
      turnCard,
      riverCard,
      currentRound,
      stackSize,
      bettedPot,
    }
    const decision = {
      order_num: decisions.length + 1,
      position: currentPosition,
      action_display: isEqual(action_display, 'CHECK') ? 'CH' : action_display,
      round: currentRound,
      active: true,
      action,
      nodes,
      ranges_oop,
      ranges_ip,
      num_combos_ip,
      num_combos_oop,
      pot_value,
      pot_oop,
      pot_ip,
      pot_value_in_bb,
      betPercentOfPot,
    }
    const node = buildNodeFromDecision(decision)
    const betCompleted = this.checkBetCompleted(dataParams, nodes, node)
    const currentPositionParam = isEqual(action_display, 'CALL') ?
                                  currentPosition :
                                  isEqual(currentPosition, 'oop') ? 'ip' : 'oop'
    const potMainDisplay = isEqual(currentRound, 'flop') ?
                             formatToBigBlind(pot_main) :
                             game[`potMain${capitalize(currentRound)}Round`]

    decision['betCompleted'] = betCompleted
    dataParams['betCompleted'] = betCompleted
    dataParams['nodes'] = nodes + ':' + node
    dataParams['node_count'] = dataParams['nodes'].split(':').length
    dataParams['currentPosition'] = currentPositionParam
    dataParams['potMainDisplay'] = potMainDisplay
    this.timer = setTimeout(() => { makeDecision(dataParams, decision, game)}, 500);
    this.updateDecisionsMatrixView()
  }

  handleOnReplaceCurrentSim() {
    this.props.refeshGameData()
    this.props.refeshHandMatrix()
    this.props.refreshDecisionsTree()
  }

  checkBetCompleted(dataParams, nodes, node) {
    const { pot_ip, pot_oop } = dataParams
    if(isDoubleCheckDecisions(nodes, node))
      return true
    const endLetter = nodes.charAt(nodes.length - 1)
    if (isEqual(node, 'c') && !['c', 'd', 's', 'h'].includes(endLetter))
      return pot_ip > 0 && pot_oop > 0 && pot_ip === pot_oop
    return false
  }

  handleBackToRootNode() {
    const {
      currentUser: { strategy_selection },
      handMatrix: { view: handMatrixView },
      game: { pot_main },
      cardsConvertingTemplate,
      rollbackDecision
    } = this.props
    const strategySelection = JSON.parse(sessionStorage.getItem("strategy_selections")) || strategy_selection
    const { positions, flop_cards } = strategySelection
    const dataParams = {
      ...strategySelection,
      positions: positionsForQueryBuilder(positions),
      flops: convertFlopCards(flop_cards, cardsConvertingTemplate).join(''),
      view: handMatrixView,
      bettedPot: {},
    }
    this.setState({ fetching: true })
    dataParams['nodes'] = ROOT_NODE
    dataParams['node_count'] = DEFAULT_NODE_COUNT
    dataParams['currentPosition'] = DEFAULT_BEGIN_ROUND_POSITION
    dataParams['pot_ip'] = DEFAULT_POT_IP
    dataParams['pot_oop'] = DEFAULT_POT_OOP
    dataParams['currentRound'] = 'flop'
    dataParams['potMainDisplay'] = formatToBigBlind(pot_main)
    rollbackDecision(dataParams, [])
    this.updateDecisionsMatrixView()
  }

  handleBackHistory = (decision) => {
    this.setState({ fetching: true })
    handleBackToHistory(this.props, decision)
  }

  updateDecisionsMatrixView() {
    const { handMatrix: { view }, updateComparedDecisions, changeMatrixView } = this.props
    if(isEqual(view, 'compare-ev')) {
      updateComparedDecisions([]);
      changeMatrixView('');
    }
  }

  handleOnClickNewTab() {
    const { currentUser: {role}, setShowNotifyModal } = this.props

    if(isEqual(role, 'admin')) {
      window.open("/strategy-select", "_blank")
    } else {
      setShowNotifyModal(true)
    }
  }

  newSimDropdownBtn() {
    const t = this.context.t
    const { showDropDownButton } = this.props
    const dropDownButtonElement = (
      <ButtonDropdown isOpen={this.state.dropdownOpen} toggle={this.toggle}>
        <DropdownToggle className='btn-primary p-2 text-white font-weight-600 btn-new-sim' caret >
          {t('new_sim')}
        </DropdownToggle>
        <DropdownMenu className='mt-2'>
          <DropdownItem className='text-white font-weight-500 p-0'>
            <Link
              to={{
                pathname: '/strategy-select',
                state: { new_sim: true }
              }}
              onClick={this.handleOnReplaceCurrentSim}
              className='font-size-14 text-white font-weight-bold  p-3 w-100'
              style={{ textDecoration: 'none' }}
            >
              {t('new_sim.replace_current')}
            </Link>
          </DropdownItem>
          <DropdownItem divider />
          <DropdownItem className='text-white font-weight-500 p-0'>
            <div
              className='font-size-14 text-white font-weight-bold px-3 w-100'
              onClick={this.handleOnClickNewTab}
            >
              {t('new_sim.new_tab')}
            </div>
          </DropdownItem>
        </DropdownMenu>
      </ButtonDropdown>
    )
    const simpleNewSimButtonElement = (
      <Button className='btn-primary text-white font-weight-600 btn-simple-new-sim'>
        <Link
          to={{
            pathname: '/strategy-select',
            state: { new_sim: true }
          }}
          onClick={this.handleOnReplaceCurrentSim}
          className='font-size-14 text-white font-weight-bold w-100 link-direct'
          style={{ textDecoration: 'none' }}
        >
          {t('new_sim')}
        </Link>
      </Button>
    )

    return showDropDownButton ? dropDownButtonElement : simpleNewSimButtonElement
  }

  render(){
    const t = this.context.t
    const { game, currentUser, showNodes, decisionsTree: { decisions } } = this.props
    const { fetching } = this.state
    const { preferences } = currentUser
    const { currentPosition, flops } = game
    const { fold_color, check_and_call_color, bet_and_raise_color, hide_explanation } = preferences
    const listKeyDisplay = decisionCombosButtons(game, preferences, currentPosition, decisions).slice(0, -1).map( decision => decision.value)
    const filterFlops = flops.filter(({ nodes, node_count }) => {
      const node = nodes.split(':')[node_count - 1]
      return listKeyDisplay.includes(node)
    })
    const smallFlopButtons = filterFlops.length >= 4 ? 'small-flop-btn' : ''
    const show = !hide_explanation && decisions.length === 0
    
    return (
      <div>
        <div className={`header-left-section d-flex ${ !this.freeAWeek && userSubscriptionInvalid(currentUser) && 'm-left-100'}`}>
          { (this.freeAWeek || !userSubscriptionInvalid(currentUser)) && this.newSimDropdownBtn()}
          { showNodes && (
            <Fragment>
              <Node
                handleBackToRootNode={this.handleBackToRootNode}
                handleBack={this.handleBackHistory}
              />
              <div className='current-decision'>
              { !fetching && !isEmpty(filterFlops) &&
                <ButtonGroup vertical>
                  {
                    filterFlops.map((flop, idx) => {
                      const flopOption = generateDecisionFromFlop(flop, game, decisions)
                      const { action, bet_level, action_display, betPercentOfPot } = flopOption
                      const equalB = isEqual(action, 'b')
                      const equalC = isEqual(action, 'c')
                      const colorScheme = equalB ? `${bet_and_raise_color}-scheme` : equalC ? `${check_and_call_color}-scheme` : `${fold_color}-scheme`
                      const actionDisplay = equalB ? calculateStrategyKey(action_display, game) : action_display
                      const colorLevel = bet_level > 0 ? `level-${bet_level}` : 'base'
                      return <Button
                              variant='default'
                              key={idx}
                              onClick={(e) => this.handleOnClickDecision(flopOption)}
                              className={`${colorScheme} ${colorLevel} ${smallFlopButtons}`}
                             >
                              {`${formatToBigBlind(actionDisplay)}`}
                              {betPercentOfPot}
                            </Button>

                    })
                  }
                </ButtonGroup>
              }
              { fetching && <Loader /> }
              </div>
            </Fragment>
          )}
        </div>
        <div className='root-node d-flex'>
          { show && !isEmpty(filterFlops) &&
            <div className='explanation d-flex
              '>
              <div className='title-point-up font-size-13 w-50 text-center'>{t('header.text')}</div>
              <div className='handle-point-up handle-point-up-icon'>
              </div>
            </div>
          }
        </div>
      </div>
    )
  }
}

CHeader.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  i18nState: store.i18nState,
  game: store.game,
  decisionsTree: store.decisionsTree,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  handMatrix: store.handMatrix,
  cardsConvertingTemplate: store.cardsConverting.cardsConvertingTemplate
})

const mapDispatchToProps = {
  updateComparedDecisions,
  changeMatrixView,
  refeshGameData,
  refreshDecisionsTree,
  fetchDataForTurnRiverRound,
  makeDecision: makeDecision,
  rollbackDecision: rollbackDecision,
  refeshHandMatrix
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CHeader)
