import React, { Component, Fragment } from 'react'
import './Node.scss'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { formatToBigBlind } from 'utils/utils'
import { updateComparedDecisions } from 'actions/game.actions'
import { changeMatrixView } from 'actions/hand-matrix.actions'
import { rollbackDecision } from 'actions/decision.actions' // rollbackDecision is passed as prop to handleBackToHistory
import arrow from '../../../../images/decision-arrow.svg'

class CNode extends Component {

  calculateStrategyKeyForDecisionNode(decision) {
    const { currentRound, bettedPot } = this.props.game
    const { action_display, round: decisionRound, position: decisionPosition } = decision
    if(currentRound === 'flop') return action_display

    switch(decisionRound) {
      case 'flop': return action_display
      case 'turn': return parseInt(action_display) - parseInt(bettedPot.flop[`pot_${decisionPosition}`])
      case 'river': return parseInt(action_display) - parseInt(bettedPot.turn[`pot_${decisionPosition}`])
      default: return "Couldn't calculate"
    }
  }

  backToHistory(decision) {
    if(this.props.disableHistoryRollback) return;
    this.props.handleBack(decision)
  }

  render() {
    let { decisionsTree } = this.props;
    let decisions = decisionsTree.decisions || [];

    return (
      <div>
        <div className='ml-1 decisions d-flex flex-wrap align-items-center'>
          <Fragment>
            <div className='decision root'>ROOT</div>
            { !!decisions.filter(decision => decision.active).length &&
              <div className='arrow'>
                <img src={arrow} alt='decision-arrow' className='decision-arrow'/>
              </div>
            }
          </Fragment>
          {
            decisions.filter(decision => decision.active).map((decision, idx) => {
              const { action, action_display } = decision

              let actionDisplay = action === 'b' ? this.calculateStrategyKeyForDecisionNode(decision) : action_display
              return (
                <Fragment key={`decision-${idx}`}>
                  {
                    ['select_turn_card', 'select_river_card'].includes(action) ? (
                      <div onClick={(e) => this.backToHistory(decision)} className='d-inline-block font-weight-bold decision b-shadow'>
                        <span>{action_display.slice(0,-1)}</span>
                        <i className={`icon icon-${action_display.slice(-1)} background-size-contain`}></i>
                      </div>
                    ) : <div onClick={(e) => this.backToHistory(decision)} className={`decision ${decision.position}`}>
                          {formatToBigBlind(actionDisplay)}
                        </div>
                  }
                  { idx !== decisions.filter(i => i.active).length - 1 &&
                    <div className='arrow'>
                      <img src={arrow} alt='decision-arrow' className='decision-arrow'/>
                    </div>
                  }
                </Fragment>
              )
            })
          }
        </div>
      </div>
    )
  }
}

CNode.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  decisionsTree: store.decisionsTree,
  game: store.game,
  currentUser: store.reduxTokenAuth.currentUser.attributes,
  handMatrix: store.handMatrix,
  cardsConvertingTemplate: store.cardsConverting.cardsConvertingTemplate
})

const mapDispatchToProps = {
  updateComparedDecisions,
  changeMatrixView,
  rollbackDecision,
}

export default connect(
  mapStoreToProps,
  mapDispatchToProps
)(CNode)
