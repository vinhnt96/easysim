import React, { Component } from 'react'
import './FeatureSection.scss'
import PropTypes from 'prop-types'
import databaseIcon from 'images/Logos/database_icon.png'
import startsIcon from 'images/Logos/starts_icon.png'
import laptopIcon from 'images/Logos/laptop_icon.png'
import analysisIcon from 'images/Logos/analysis_icon.png'

class CFeatureSection extends Component {

  render() {
    let t = this.context.t

    return (
      <div className={`row text-center feature-section no-gutters ${this.props.className}`}>
        <div className='col-lg-3 col-sm-6 feature-item'>
          <div className='feature-icon'>
            <img src={databaseIcon} className='icon' alt='database-icon' />
          </div>
          <h4 className='feature-title'> {t('landing_page.largest_database')} </h4>
          <ul className='feature-description'>
            <li> Every sim you will ever need </li>
            <li> MTTs and cash games </li>
            <li> 500k+ simulations </li>
          </ul>
        </div>
        <div className='col-lg-3 col-sm-6 feature-item'>
          <div className='feature-icon'>
            <img src={startsIcon} className='icon' alt='starts-icon' />
          </div>
          <h4 className='feature-title'> {t('landing_page.land.high_quality')} </h4>
          <ul className='feature-description'>
            <li> Designed by the world's best </li>
            <li> Every bet size you need </li>
          </ul>
        </div>
        <div className='col-lg-3 col-sm-6 feature-item'>
          <div className='feature-icon'>
            <img src={laptopIcon} className='icon' alt='laptop-icon' />
          </div>
          <h4 className='feature-title'> {t('landing_page.easy_use')} </h4>
          <ul className='feature-description'>
            <li> No extra software required </li>
            <li> Browser based </li>
          </ul>
        </div>
        <div className='col-lg-3 col-sm-6 feature-item'>
          <div className='feature-icon'>
            <img src={analysisIcon} className='icon' alt='analysis-icon' />
          </div>
          <h4 className='feature-title'> {t('landing_page.analysis_tools')} </h4>
          <ul className='feature-description'>
            <li> All the best features of PioSolver </li>
            <li> Explore entire preflop trees </li>
            <li> Advanced notes system </li>
          </ul>
        </div>
      </div>
    )
  }
}


CFeatureSection.contextTypes = {
  t: PropTypes.func
}

export default CFeatureSection