import React, { Component } from 'react'
import './SubscribeNowPopup.scss'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Modal from 'react-bootstrap/Modal'
import { Button } from 'react-bootstrap'
import {withRouter} from 'react-router'

class CSubscribeNowPopup extends Component {
  constructor(props) {
    super(props);

    this.handleSubscribe = this.handleSubscribe.bind(this);
  }

  handleSubscribe() {
    this.props.history.push('subscribe');
  }

  render() {
    let t = this.context.t;
    let { isShown, handleClose, message } = this.props;

    return (
      <Modal
        show={isShown}
        onHide={handleClose}
        dialogClassName='subscribe-now-popup'
        aria-labelledby='subscribe-now-popup'
        backdrop="static"
      >
        <Modal.Header closeButton>
        </Modal.Header>
        <Modal.Body>
          {message}
        </Modal.Body>
        <Modal.Footer>
          <Button className='subscribe-btn' onClick={this.handleSubscribe}>
            {t('free_trial.subscribe_popup.subscribe_now')}
          </Button>
        </Modal.Footer>
      </Modal>    )
  }
}

CSubscribeNowPopup.contextTypes = {
  t: PropTypes.func
}

const mapStoreToProps = (store) => ({
  currentUser: store.reduxTokenAuth.currentUser.attributes,
})

const mapDispatchToProps = {
}

export default withRouter(connect(
  mapStoreToProps,
  mapDispatchToProps
)(CSubscribeNowPopup))

