import { odinLoadingActions } from '../constants/index';

const initialState = {
  fetching: '',
};

export function odinLoading(state = initialState, action) {
  switch (action.type) {
    case odinLoadingActions.UPDATE_FETCHING_VALUE:
      return {
        ...state,
        fetching: action.payload
      }
    default:
      return state;
  }
}
