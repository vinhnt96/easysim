import { decisionsTree } from './decision.reducer'
import { game } from './game.reducer'
import { handMatrix } from './hand-matrix.reducer'
import { rangeExplorer } from './range-explorer.reducer'
import { preflopGame } from './preflop-game.reducer'
import { userData } from './user.reducer'
import { subscriptions } from './subscription.reducer'
import { roles } from './role.reducer'
import { cardsConverting } from './cards-converting.reducer'
import { odinLoading } from './odin-loading.reducer'

export default {
  decisionsTree,
  game,
  handMatrix,
  rangeExplorer,
  preflopGame,
  userData,
  subscriptions,
  roles,
  cardsConverting,
  odinLoading
}
