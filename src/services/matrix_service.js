import { COMBOS, EV_EQ_PRECISION } from 'config/constants';
import {
  buildStrategyForEachCombos,
  buildStrategyEvForEachCombos,
  buildCompareEvForEachCombos
} from './game_play_service'
import { blindRatio, addPotValueToRawEvRescaleData } from 'utils/utils'
import { convertHandCards } from './convert_cards_services'
import { isEmpty, isEqual, isInteger, round } from 'lodash'
import { roundValueToNearest } from './../utils/matrix'
import { calEvRescale, calEvPerEq } from 'services/backgroundServices/shared'

const buildDataEVForEachCombos = (raw_data, combos) => {
  const data = {}
  const ratio = blindRatio()
  Object.keys(combos).forEach((combo) => {
    const idx = combos[combo]
    const val = raw_data.split(' ')[idx]
    data[combo] = isNaN(val) ? val : parseFloat(val) / ratio
  })
  return data;
}

const buildDataEVRescaleForEachCombos = (raw_data, totalPot, combos,  pot_oop, pot_ip) => {
  const data = {}
  if(totalPot === 0) {
    return 0;
  }

  Object.keys(combos).forEach((combo) => {
    const idx = combos[combo]
    let val = raw_data.split(' ')[idx]
    const comboData = isNaN(val) ? val : parseFloat(val) / (totalPot - Math.abs(pot_oop - pot_ip)) * 100;

    data[combo] = round(comboData, 3)
  })
  return data;
}

const buildDataForEachCombos = (raw_data, flopCards, view, combos) => {
  const data = {}
  const combosEmpty = isEmpty(combos)
  const listCombos = combosEmpty ? COMBOS : Object.keys(combos)
  listCombos.forEach((combo) => {
    let idx = 0
    if (combosEmpty) {
      const convertedCombo = convertHandCards(flopCards, combo)
      idx = COMBOS.findIndex(item => isEqual(item, convertedCombo))
    } else {
      idx = combos[combo]
    }

    if (isInteger(idx)) {
      let val = raw_data.split(' ')[idx]
      const valueNearest = roundValueToNearest(parseFloat(val), 0.05)
      val = view.startsWith('ranges') ? round(valueNearest, EV_EQ_PRECISION) : round(parseFloat(val), EV_EQ_PRECISION)
      val = isNaN(val) || parseFloat(val) <= 0.0 ? 0.0 : val
      data[combo] = val
    }
  })
  return data;
}


export const dataForEachComboGenerator = (dataInput) => {
  let {
    view,
    ranges_ip,
    ranges_oop,
    ev_oop,
    ev_ip,
    equity_ip,
    equity_oop,
    flops,
    nodes,
    currentPosition,
    preferences,
    comparedDecisions,
    flopCards,
    totalPot,
    pot_oop,
    pot_ip,
    combosWithIndex
  } = dataInput

  let calculatedEvOopData = addPotValueToRawEvRescaleData(ev_oop, pot_oop)
  let calculatedEvIpData = addPotValueToRawEvRescaleData(ev_ip, pot_ip)
  const potDiff = Math.abs(pot_oop - pot_ip)
  const compareEvParams = { flops, nodes, currentPosition, comparedDecisions, flopCards, combos: combosWithIndex, potValue: dataInput[`pot_${currentPosition}`] }
  switch(view) {
    case 'ranges_ip':
      return buildDataForEachCombos(ranges_ip, flopCards, view, combosWithIndex)
    case 'ranges_oop':
      return buildDataForEachCombos(ranges_oop, flopCards, view, combosWithIndex)
    case 'equity_oop':
      return buildDataForEachCombos(equity_oop, flopCards, view, combosWithIndex)
    case 'equity_ip':
      return buildDataForEachCombos(equity_ip, flopCards, view, combosWithIndex)
    case 'ev_oop':
      return buildDataEVForEachCombos(calculatedEvOopData, combosWithIndex)
    case 'ev_ip':
      return buildDataEVForEachCombos(calculatedEvIpData, combosWithIndex)
    case 'ev_per_eq_oop':
      return calcEvPerEqDataForEachCombos(calculatedEvOopData, equity_oop, totalPot, EV_EQ_PRECISION, combosWithIndex, potDiff)
    case 'ev_per_eq_ip':
      return calcEvPerEqDataForEachCombos(calculatedEvIpData, equity_ip, totalPot, EV_EQ_PRECISION, combosWithIndex, potDiff)
    case 'strategy-ev':
      return buildStrategyEvForEachCombos(flops, nodes, currentPosition, flopCards, combosWithIndex, {pot_oop, pot_ip})
    case 'compare-ev':
      return buildCompareEvForEachCombos(compareEvParams)
    case 'ev_rescale_oop':
      return buildDataEVRescaleForEachCombos(calculatedEvOopData, totalPot, combosWithIndex, pot_oop, pot_ip)
    case 'ev_rescale_ip':
      return buildDataEVRescaleForEachCombos(calculatedEvIpData, totalPot, combosWithIndex, pot_oop, pot_ip)
    default:
      return buildStrategyForEachCombos(flops, nodes, preferences, flopCards, combosWithIndex)
  }
}

const calcEvPerEqDataForEachCombos = (ev_raw, equity_raw, totalPot, precision=2, combos, potDiff=0) => {
  const ratio = blindRatio()
  const ev_array = ev_raw.split(' ').map(item => {
    if(isNaN(item)) {
      return item
    } else {
      let evVal = parseFloat(item)/ratio
      return calEvRescale(evVal, totalPot, precision, potDiff / 100)    // calculate value in BB as here => potDiff should be divide 100
    }
  })
  const equity_array = equity_raw.split(' ').map(item => {
    return isNaN(item) ? 0.0 : parseFloat(item)
  })

  const data = {}
  Object.keys(combos).forEach((combo) => {
    const idx = combos[combo]
    if(isNaN(ev_array[idx])) {
      data[combo] = ev_array[idx]
    } else {
      const evValue = parseFloat(ev_array[idx])
      const equityValue = equity_array[idx]
      const comboData = calEvPerEq(evValue, equityValue, precision)
      data[combo] = comboData
    }
  })
  return data
}
