
export const handleSortColumn = (sortColumn, isDefaultDirection) => {
  return (subscriptionA, subscriptionB) => {
    let dateCreatedA = new Date(subscriptionA.start_time)
    let dateCreatedB = new Date(subscriptionB.start_time)
    let nextPaymentA = new Date(subscriptionA.next_payment)
    let nextPaymentB = new Date(subscriptionB.next_payment)

    switch (sortColumn) {
      case "no":
        return isDefaultDirection ? subscriptionA.no - subscriptionB.no : subscriptionB.no - subscriptionA.no 
      case "email_address":
        if(subscriptionA.email < subscriptionB.email) return isDefaultDirection ? -1 : 1
        else if(subscriptionA.email > subscriptionB.email) return isDefaultDirection ? 1 : -1
        else return 0
      case "active_subscription":
        break
      case "date_created":
        return isDefaultDirection ? dateCreatedB - dateCreatedA
                                  : dateCreatedA - dateCreatedB
      case "next_payment":
        return isDefaultDirection ? nextPaymentB - nextPaymentA
                                  : nextPaymentA - nextPaymentB
      case "full_access":
        if(!!subscriptionA.full_access && !subscriptionB.full_access) return isDefaultDirection ? -1 : 1
        else if(!subscriptionA.full_access && !!subscriptionB.full_access) return isDefaultDirection ? 1 : -1
        else return 0
      default:
        return isDefaultDirection ? dateCreatedB - dateCreatedA
                                  : dateCreatedA - dateCreatedB
    }
  }
}