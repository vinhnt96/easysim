import {
  INITIAL_HAND_CATEGORY_DATA,
  ACE_LOW_CARD_RANK_ORDER,
  HAND_CATEGORY_MAP,
  CARD_RANK_ORDER,
  COMBOS
} from 'config/constants'
import  { convertHandCards } from './convert_cards_services'
import { cloneDeep, sum, isEqual, orderBy, isString, isNumber, round } from 'lodash'

export const convertToCardArray = (cards) => {
  return Array.isArray(cards) ? cards : cards.match(/.{1,2}/g).map(card => card.replace('T', '10'))
}

export const getCardRank = (card) => {
  return card.replace(/s|d|c|h/g, '')
}

export const getCardSuit = (card) => {
  return card.slice(-1)
}

export const isSameRank = (firstCard, secondCard) => {
  return getCardRank(firstCard) === getCardRank(secondCard)
}

export const cardRankCompare = (firstCard, secondCard) => {
  return CARD_RANK_ORDER[getCardRank(firstCard)] - CARD_RANK_ORDER[getCardRank(secondCard)]
}

export const sortCardsByRanks = (cards) => {
  return cloneDeep(cards).sort((a,b) => CARD_RANK_ORDER[getCardRank(a)] - CARD_RANK_ORDER[getCardRank(b)])
}

//Function count occurrences of ranks (in rank array) or suit (in suit array)
export const countOccurrences = (array) => {
  return array.reduce((obj, item) => {
    obj[item] = obj[item] + 1 || 1
    return obj
  }, {})
}

export const countTheNumberOfPair = (cards) => {
  const rankArray = cards.map(card => getCardRank(card))
  const occurrencesRankObj = countOccurrences(rankArray) // get occurrences and save in object
  const numberOfPair = Object.keys(occurrencesRankObj).reduce((numOfPair, key) => {
    return occurrencesRankObj[key] === 2 ? numOfPair + 1 : numOfPair
  }, 0)

  return numberOfPair
}

export const containsRank = (cards, rank) => {
  const rankArray = cards.map(card => getCardRank(card))
  return rankArray.includes(rank)
}

export const distinctArray = (arr) => {
  return [...new Set(arr)]
}

export const hasCardsWithSameSuit = (cards, cardNum) => {
  const suitArray = cards.map(card => getCardSuit(card))
  const occurrencesSuitObj = countOccurrences(suitArray) // get occurrences and save in object
  return Object.values(occurrencesSuitObj).includes(cardNum)
}

export const hasCardsWithSameRank = (cards, cardNum) => {
  const rankArray = cards.map(card => getCardRank(card))
  const occurrencesRankObj = countOccurrences(rankArray) // get occurrences and save in object
  return Object.values(occurrencesRankObj).includes(cardNum)
}

export const getConsecutiveCards = (cards, cardNum) => {
  const sortedRanksArray = sortCardsByRanks(cards).map(card => getCardRank(card))
  const distinctSortedRanks = distinctArray(sortedRanksArray)
  let straightCards = [distinctSortedRanks[0]]

  let lowAceSortedRanks = []
  if(distinctSortedRanks[distinctSortedRanks.length - 1] === 'A') {
    lowAceSortedRanks = ['A', ...distinctSortedRanks.slice(0, -1)]
  }
  let aceLowStraightCards = [lowAceSortedRanks[0]]

  for(let i = 0; i < distinctSortedRanks.length - 1; i++) {
    if(CARD_RANK_ORDER[distinctSortedRanks[i]] - CARD_RANK_ORDER[distinctSortedRanks[i+1]] === -1) {
      straightCards.push(distinctSortedRanks[i+1])
    } else {
      if(straightCards.length < cardNum) {
        straightCards = [distinctSortedRanks[i+1]]
      }
    }

    if(ACE_LOW_CARD_RANK_ORDER[lowAceSortedRanks[i]] - ACE_LOW_CARD_RANK_ORDER[lowAceSortedRanks[i+1]] === -1) {
      aceLowStraightCards.push(lowAceSortedRanks[i+1])
    } else {
      if(aceLowStraightCards.length < cardNum) {
        aceLowStraightCards = [lowAceSortedRanks[i+1]]
      }
    }
  }

  return [
    straightCards.length === cardNum ? straightCards : null,
    aceLowStraightCards.length === cardNum ? aceLowStraightCards : null
  ].filter(e => e)
}

// Section for build data function
const calculateComboRange = (rangeObj) => {
  const rangeValueArray = Object.values(rangeObj).map(value => parseFloat(value))
  return sum(rangeValueArray)
}

export const calculateHandTypes = (flopCards, ranges, totalCombosNum, position, handCategoryData) => {
  const data = cloneDeep(INITIAL_HAND_CATEGORY_DATA)
  const madeHandMatrix = handCategoryData.made_hands.split(' ')
  const drawHandMatrix = handCategoryData.draw_hands.split(' ')

  COMBOS.forEach((combo) => {
    const convertedCombo = convertHandCards(flopCards, combo)
    const idx = COMBOS.findIndex(combo => combo === convertedCombo)
    const madeHandType = HAND_CATEGORY_MAP.madeHand[madeHandMatrix[idx]]
    const drawHandType = HAND_CATEGORY_MAP.drawHand[drawHandMatrix[idx]]
    const rangeValue = calculateComboRange(ranges[combo])
    data[madeHandType] += rangeValue
    if(!isEqual(drawHandType, 'not_exists')) data[drawHandType] += rangeValue
  })

  let dataArray = []
  for(let key in data) {
    let comboValue = isNaN(data[key]) ? 0 : data[key]
    let percent = totalCombosNum === 0 ? 0 : round((comboValue / totalCombosNum) * 100, 1)
    let info = `${round(comboValue, 1)} combos (${percent}%)`
    dataArray.push({ name_key: `hand_type.${key}`, info: info, percent: percent, type: `${position}_${key}`, value: comboValue })
  }
  dataArray = orderBy(dataArray, ['value'], ['desc'])

  return dataArray
}

export const filterMatrixByHandType = (data, flopCards, handTypeFilter, handCategoryData) => {
  const selectedHandType = handTypeFilter.replace('ip_','').replace('oop_','')
  const madeHandMatrix = handCategoryData.made_hands.split(' ')
  const drawHandMatrix = handCategoryData.draw_hands.split(' ')
  let filteredData = {}

  for(let combo in data) {
    const convertedCombo = convertHandCards(flopCards, combo)
    const idx = COMBOS.findIndex(combo => combo === convertedCombo)
    const madeHandType = HAND_CATEGORY_MAP.madeHand[madeHandMatrix[idx]]
    const drawHandType = HAND_CATEGORY_MAP.drawHand[drawHandMatrix[idx]]
    const isSelectedHandType = [madeHandType, drawHandType].includes(selectedHandType)
    let comboData = isSelectedHandType ? data[combo] : setComboDataValue(data[combo], 0)
    filteredData[combo] = comboData
  }

  return filteredData
}

export const filterDataToAppendExistedData = (data) => {
  COMBOS.forEach( combo => {
    let isShouldGetKey = false
    if(isOrtherView(data[combo]))
      isShouldGetKey = parseFloat(data[combo]) !== 0
    else
      isShouldGetKey = Object.values(data[combo]).some( dataObject => Object.values(dataObject).some( dataValue => parseFloat(dataValue) !== 0) )

    if(!isShouldGetKey)
      delete data[combo]
  })
  return data
}

export const convertDataToZeroValue = (data) => {
  let newData = cloneDeep(data)
  COMBOS.forEach( combo => newData[combo] = setComboDataValue(data[combo], 0) )
  return newData
}

const isOrtherView = (dataValue) => {
  return isString(dataValue) || isNumber(dataValue)
}

const setComboDataValue = (comboData, value) => {
  return isOrtherView(comboData) ? value : setAllValueForObject(comboData, value)
}

export const setAllValueForObject = (obj, value) => {
  const cloneObj = {}
  Object.keys(obj).forEach(key => {
    if(typeof obj[key] !== "object" ){
      cloneObj[key] = value
    }else{
      cloneObj[key] = setAllValueForObject(obj[key], value)
    }
  })
  return cloneObj
  }