import {
  PREFLOP_POSITIONS_ORDER,
  SKIPPED_ACTIONS,
  INITIAL_POSITION_NODES,
  POSITION_NODE_PARAMS,
  COMBOS,
  COLORS_SCHEME,
} from 'config/constants'
import { roundValueToNearest, combosGenerator, calcAvgStrategy } from 'utils/matrix'
import { isEqual, round } from 'lodash'

export const getNextPosition = (currentPosition, trackedData, game_type, specs) => {
  const positionArray = specs === 'hu' ? PREFLOP_POSITIONS_ORDER[specs] : PREFLOP_POSITIONS_ORDER[game_type];
  let nextOrder = (positionArray.indexOf(currentPosition) + 1) % positionArray.length;
  let loopTimes = 0;

  while(loopTimes < positionArray.length) {
    const nextPosition = positionArray[nextOrder];
    const latestAction = trackedData[nextPosition]['lastAction'];

    if(!SKIPPED_ACTIONS.includes(latestAction)) {
      return nextPosition;
    } else {
      nextOrder = (nextOrder + 1) % positionArray.length;
    }

    loopTimes ++;
  }
}

export const generateNewNode = (selectedAction, currentNode, currentPosition, nextPosition) => {
  const isFold = isEqual(selectedAction, 'fold')
  if(isFold && currentNode.split('_').length === 2) {
    return INITIAL_POSITION_NODES[nextPosition];
  } else {
    const nextPositionParam = POSITION_NODE_PARAMS[nextPosition];
    let newNode = '';

    const currentNodeWithoutCurrentPosition = currentNode.split('_').slice(0, -2).join('_');
    const indexCurrentNodeWithoutCurrentPosition = currentNodeWithoutCurrentPosition.indexOf(`${POSITION_NODE_PARAMS[currentPosition]}_`)
    if(isFold && indexCurrentNodeWithoutCurrentPosition === -1) {
      newNode = currentNodeWithoutCurrentPosition + `_${nextPositionParam}_strategy`;
    } else {
      newNode = currentNode.replace('strategy', '') +
      nodeActionParams(selectedAction) +
      `_${nextPositionParam}` +
      '_strategy';
    }

    return newNode
  }
}

export const nodeActionParams = (action) => {
  switch(action) {
    case 'call':
      return 'Call';
    case 'fold':
      return 'Fold';
    case 'allin':
      return 'AllIn';
    default:
      return 'Raise';
  }
}

export const actionNameDisplay = (action) => {
  switch(action) {
    case 'call':
      return 'CALL';
    case 'fold':
      return 'FOLD';
    case 'allin':
      return 'All In';
    default:
      return action;
  }
}

export const buildRangeForEachCombos = (actionRanges) => {
  const ranges = {}
  const newActionRanges = {}
  Object.keys(actionRanges).reverse().map( key => newActionRanges[key] = actionRanges[key] )
  const rangesArray = Object.keys(newActionRanges).map((action) => {
    return newActionRanges[action].split(' ');
  })
  COMBOS.forEach((combo, idx) => {
    ranges[combo] = {}
    Object.keys(newActionRanges).forEach((action, index) => {
      ranges[combo][action] = roundValueToNearest(rangesArray[index][idx], 0).toString()
    })
  })

  return ranges
}

export const preflopBackgroundColorForCombo = (actionRanges, combos, heightPercentage, preferences) => {
  let tempRatio = 0
  let style = `linear-gradient(to top,`

  Object.keys(actionRanges).forEach((action, index) => {
    const ratio =  round(parseFloat(actionRanges[action]) * 100, 100);
    const ratioFromTo = `${tempRatio}% ${tempRatio + ratio}%`
    tempRatio = tempRatio + ratio

    const colorCode = getActionColor(action, preferences)
    const sectionColor = index === Object.keys(actionRanges).length - 1 ? ` ${colorCode} ${ratioFromTo}` : ` ${colorCode} ${ratioFromTo},`
    style = style + sectionColor
  })
  style = style + `)`
  style = tempRatio === 0 ? {background: ''} : {background: style}
  const handItemWeightedStrategy = weightedStrategy(heightPercentage, combos)
  if(handItemWeightedStrategy.height > 0) {
    style['height'] = `${handItemWeightedStrategy.height}%`
    style['borderTopLeftRadius'] = handItemWeightedStrategy.borderRadius
    style['borderTopRightRadius'] = handItemWeightedStrategy.borderRadius
  } else {
    style['height'] = '100%';
  }

  return style
}

export const preflopBackgroundColorForHandItem = (handItem, rangesForEachCombos, heightPercentage, preferences, flopCards) => {
  const combos = combosGenerator(handItem)
  if(combos.length === 0) return {}
  const avgRange = calcAvgStrategy(combos, rangesForEachCombos)
  return preflopBackgroundColorForCombo(avgRange, combos, heightPercentage, preferences)
}

export const weightedStrategy = (heightPercentage, combos) => {
  return { height: heightPercentage, borderRadius: round(heightPercentage / 100, 1) < 0.9 ? '0px' : null }
}

export const generateStackSizeParam = (strategySelection) => {
  const { stack_size, game_type, open_size, specs } = strategySelection

  if(game_type === 'mtt' || (game_type === 'cash' && specs === '8max')) {
    return stack_size
  } else {
    const openSize = open_size || '2.5bb';
    const formattedOpenSize = specs === 'hu' ? '25xhus10ks' : openSize.replace('bb', 'x').replace('.', '')
    return `${stack_size}-${formattedOpenSize}`;
  }
}

export const getActionColor = (action, preferences) => {
  const { fold_color, check_and_call_color, bet_and_raise_color } = preferences
  switch(action) {
    case 'fold':
      return COLORS_SCHEME[fold_color].base;
    case 'call':
      return COLORS_SCHEME[check_and_call_color].base;
    case 'allin':
      return COLORS_SCHEME[bet_and_raise_color].level_6;
    case 'check':
      return COLORS_SCHEME[check_and_call_color].base;
    default:
      return COLORS_SCHEME[bet_and_raise_color].level_4;
  }
}

export const preflopBackgroundColorForComboItem = (actionRanges, preferences) => {
  let tempRatio = 0
  let style = `linear-gradient(to right,`
  Object.keys(actionRanges).forEach((action, index) => {
    const ratio =  round(parseFloat(actionRanges[action]) * 100, 100);
    const ratioFromTo = `${tempRatio}% ${tempRatio + ratio}%`
    tempRatio = tempRatio + ratio
    const colorCode = getActionColor(action, preferences)
    const sectionColor = index === Object.keys(actionRanges).length - 1 ? ` ${colorCode} ${ratioFromTo}` : ` ${colorCode} ${ratioFromTo},`
    style = style + sectionColor
  })
  style = style + `)`

  style = tempRatio === 0 ? {background: ''} : {background: style}
  return style
}

export const roundedRaiseAction = (action, decimal=1) => {
  return action.includes('bb') ? `${round(action.replace('bb', ''), decimal)}bb` : action;
}
