import {
  DEFAULT_TOTAL_EQ_EV,
  EV_EQ_PRECISION,
  NUMBER_OF_COLOR_SCHEME_LEVELS,
} from 'config/constants';
import { sum, round, map, isEmpty} from 'lodash'
import { formatToBigBlind } from 'utils/utils'
import { handItemBackgroundColorGenerator } from './backgroundServices/rangeMatrixBGColorService'
import { comboBackgroundColorGenerator } from './backgroundServices/handMatrixBGColorService'
import { filterEqEvToArrayWithoutNaNValue, renderColorCode, calculateEvRescaleArray } from 'services/backgroundServices/shared'

export const backgroundColorForPreview = (strategy, preferences) => {
  let style = `linear-gradient(to right,`
  let tempRatio = 0
  let betLevel = NUMBER_OF_COLOR_SCHEME_LEVELS
  const betLevelStep = isEmpty(strategy) ? 1 : round(8 / Object.keys(strategy).length)
  map(strategy, (value, key) => {
    const ratio = round(parseFloat(value) * 100, 2);
    const ratioFromTo = `${tempRatio}% ${tempRatio + ratio}%`
    tempRatio = tempRatio + ratio

    const colorCode = renderColorCode(preferences, key[0], betLevel)
    betLevel -= betLevelStep;
    const sectionColor = key === Object.keys(strategy)[Object.keys(strategy).length - 1] ? ` ${colorCode} ${ratioFromTo}` : ` ${colorCode} ${ratioFromTo},`
    style = style + sectionColor
  })
  style = style + `)`
  return tempRatio === 0 ? {background: ''} : {background: style}
}

const multiplyArrays = (firstMatrix, secondMatrix) => {
  let result = [];

  for(let i = 0; i < firstMatrix.length; i++) {
    result[i] = firstMatrix[i] * secondMatrix[i];
  }

  return result;
}

export const calculateFigures = (view, position, game, totalPot=0) => {
  let figure = 0;
  let result = 0;

  switch(view) {
    case 'ev_oop':
    case 'ev_ip':
    case 'strategy-ev':
      const evArray = filterEqEvToArrayWithoutNaNValue(game, `ev_${position}`)
      const evArray2 = filterEqEvToArrayWithoutNaNValue(game, `ev_${position}2`)
      const evMultipliedArray = multiplyArrays(evArray, evArray2);

      figure = sum(evMultipliedArray) / sum(evArray2);
      break;
    case 'equity_oop':
    case 'equity_ip':
      const oopEqArray = filterEqEvToArrayWithoutNaNValue(game, 'equity_oop')
      const oopRangesArray = filterEqEvToArrayWithoutNaNValue(game, 'ranges_oop')
      const oopMultipliedArray = multiplyArrays(oopEqArray, oopRangesArray);
      let oopFigure = (sum(oopMultipliedArray) / sum(oopRangesArray))*100;

      const ipEqArray = filterEqEvToArrayWithoutNaNValue(game, 'equity_ip')
      const ipRangesArray = filterEqEvToArrayWithoutNaNValue(game, 'ranges_ip')
      const ipMultipliedArray = multiplyArrays(ipEqArray, ipRangesArray);
      let ipFigure = (sum(ipMultipliedArray) / sum(ipRangesArray))*100;

      if(oopFigure > ipFigure) {
        oopFigure -= (oopFigure + ipFigure) - 100;
      } else {
        ipFigure -= (oopFigure + ipFigure) - 100;
      }

      figure = position === 'oop' ? oopFigure : ipFigure;
      break;
    case 'ev_rescale_oop':
    case 'ev_rescale_ip':
      const evRescaleArray = calculateEvRescaleArray(game, `ev_${position}`, totalPot);
      const evArr2 = filterEqEvToArrayWithoutNaNValue(game, `ev_${position}2`)
      const evRescaleMultipliedArray = multiplyArrays(evRescaleArray, evArr2);
      figure = sum(evRescaleMultipliedArray) / sum(evArr2);
      break;
    default:
      figure = DEFAULT_TOTAL_EQ_EV;
      break;
  }

  if(['equity_oop', 'equity_ip'].includes(view)) {
    result = round(figure, EV_EQ_PRECISION) + '%';
  } else if(['ev_oop', 'ev_ip', 'strategy-ev'].includes(view)) {
    result = formatToBigBlind(round( figure + game[`pot_${position}`] ), EV_EQ_PRECISION)
  } else {
    result = round(figure, EV_EQ_PRECISION);
  }

  return  result
}

export const backgroundServices = {
  handItemBackgroundColorGenerator,
  comboBackgroundColorGenerator
}
