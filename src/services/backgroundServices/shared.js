import {
  COMPARE_EV_COLOR_STRIP,
  COLORS_SCHEME,
  EV_COLOR_STRIP,
  COLOR_BY_ROLE,
} from 'config/constants';
import { isEmpty, isEqual, round } from 'lodash';
import { EV_EQ_PRECISION } from 'config/constants';

export const backgroundColorForCombo = (strategy, game, preferences, decisionFilter='', userRole='', view) => {
  const { flops, nodes } = game
  const role = userRole === 'free_user' ? 'odin_user' : userRole
  const matrixSquareBackgroundColor = COLOR_BY_ROLE[role]['matrixSquareBackgroundColor'];
  let tempRatio = 0
  let colorCode = ''
  let style = `linear-gradient(to right,`
  const selectedFlop = flops.filter(flop => {
    const nodesArray = flop.nodes.split(':');
    return nodesArray[nodesArray.length - 1] === decisionFilter;
  })[0] || {}

  if(!isEmpty(decisionFilter) && Object.keys(strategy).includes(decisionFilter) && selectedFlop) {
    colorCode = renderColorCode(preferences, decisionFilter[0], selectedFlop.bet_level)
    const valueFloat = ['', 'strategy-ev'].includes(view) ? strategy[decisionFilter]['rounded'] : strategy[decisionFilter]
    tempRatio = Math.round(parseFloat(valueFloat) * 10000) / 100;
    style += ` ${colorCode} ${tempRatio}%, ${matrixSquareBackgroundColor} ${tempRatio}%)`;
  } else {
    flops.forEach((flop, index) => {
      const node = flop.nodes.replace(`${nodes}:`, '')
      const valueFloat = ['', 'strategy-ev'].includes(view) ? strategy[node]['rounded'] : strategy[node]
      const raito = Math.round(parseFloat(valueFloat) * 10000) / 100
      const ratioFromTo = `${tempRatio}% ${tempRatio + raito}%`
      tempRatio = tempRatio + raito
      colorCode = renderColorCode(preferences, node[0], flop.bet_level)

      const sectionColor = index === flops.length - 1 ? ` ${colorCode} ${ratioFromTo}` : ` ${colorCode} ${ratioFromTo},`
      style = style + sectionColor

    })
    style = style + `)`
  }
  return tempRatio === 0 ? {background: ''} : {background: style}
}

export const renderColorCode = (preferences, value, betLevel) => {
  const { bet_and_raise_color, check_and_call_color, fold_color } = preferences
  switch (value) {
    case 'b': return COLORS_SCHEME[bet_and_raise_color][`level_${betLevel}`]
    case 'c': return COLORS_SCHEME[check_and_call_color]['base']
    case 'f': return COLORS_SCHEME[fold_color]['base']
    default: return 'NaN'
  }
}

export const findColorInRange = (view, maxValue, value, minValue = 0) => {
  let level = 0
  const isCompareEvView = isEqual(view, 'compare-ev')
  const numOfLevel = isCompareEvView ? 5 : 7
  if(maxValue === minValue) {
    level = numOfLevel
  }
  else {
    const distance = Math.abs(minValue - maxValue) / numOfLevel
    level = value > minValue ? Math.floor((value - minValue) / distance) + 1 : 1
    if (level >= numOfLevel) level = numOfLevel
  }
  const colorCode = isCompareEvView ? COMPARE_EV_COLOR_STRIP[`level_${level}`] : EV_COLOR_STRIP[`level_${level}`]
  
  return colorCode
}

export const filterEqEvToArrayWithoutNaNValue = (game, view, precision='') => {
  return game[view].split(' ').map(item => {
    let value = isNaN(item) ? 0 : parseFloat(item)
    return !precision ? value : round(value, precision);
  })
}

export const filterEqEvToArrayWithoutNegativeAndNaNValue = (game, view, precision='') => {
  return game[view].split(' ').map(item => {
    let value = (isNaN(item) || parseFloat(item) < 0) ? 0 : parseFloat(item)
    return !precision ? value : round(value, precision);
  })
}

export const calEvRescale = (evVal, totalPot, precision='', diffBets=0) => {
  if(totalPot === 0) {
    return 0;
  }
  let value = evVal / (totalPot - diffBets);
  return precision === '' ? value : round(value, precision);
}

export const calEvPerEq = (evVal, eqVal, precision='') => {
  if(eqVal === 0) {
    return 0;
  }

  let value = evVal * (100 / eqVal);
  return precision === '' ? value : round(value, precision);
}

export const calculateEvRescaleArray = (game, view, totalPot) => {
  const { pot_ip, pot_oop } = game
  const potValue = parseFloat(game[`pot_${view.replace("ev_", "")}`])
  const diffBets = Math.abs(pot_ip - pot_oop)

  return game[view].split(' ').map(item => {
    if(item === 'nan') { return 0; }

    const value = parseFloat(item) + potValue;
    return calEvRescale(value, totalPot, EV_EQ_PRECISION, diffBets);
  })
}

export const calculateEvPerEqArray = (game, currentPosition, totalPot) => {
  const ev_array = calculateEvRescaleArray(game, `ev_${currentPosition}`, totalPot);
  const eq_array = filterEqEvToArrayWithoutNaNValue(game, `equity_${currentPosition}`, EV_EQ_PRECISION);
  let ev_per_eq_array = [];

  for(let i = 0; i < ev_array.length; i++) {
    let evPerEqValue = calEvPerEq(ev_array[i], eq_array[i], EV_EQ_PRECISION);
    ev_per_eq_array.push(evPerEqValue);
  }

  return ev_per_eq_array;
}
