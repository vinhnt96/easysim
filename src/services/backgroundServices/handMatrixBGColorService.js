import { COLORS_SCHEME, EV_EQ_PRECISION } from 'config/constants';
import { blindRatio } from 'utils/utils'
import { isEmpty, round, max, min, sum } from 'lodash'
import { buildStrategyEvMinMax } from '../game_play_service'
import {
  backgroundColorForCombo,
  findColorInRange,
  filterEqEvToArrayWithoutNaNValue,
  calculateEvRescaleArray,
  calculateEvPerEqArray,
} from './shared'

//////// BACKGROUND COLOR FOR EACH COMBO ////////
export const comboBackgroundColorGenerator = (params) => {
  let {
    view,
    comboData,
    game,
    preferences,
    totalPot,
    userRole,
  } = params;

  if(isEmpty(comboData) && isNaN(comboData)) {
    return {};
  }

  let decisionFilter = params.decisionFilter || '';
  let dataForEachCombos = params.dataForEachCombos || {};
  let currentPosition = game.currentPosition;

  switch(view) {
    case 'ranges_ip':
    case 'ranges_oop':
      return parseFloat(comboData) ? {background: COLORS_SCHEME.yellow.base} : {}
    case 'equity_oop':
    case 'equity_ip':
      return round(parseFloat(comboData), EV_EQ_PRECISION) ? backgroundColorEVEQForCombo(view, comboData, 1, EV_EQ_PRECISION) : {};
    case 'ev_oop':
    case 'ev_ip':
      const arrayEV = filterEqEvToArrayWithoutNaNValue(game, view)
      return isNaN(comboData) ? {} : backgroundColorEVEQForCombo(view, comboData, max(arrayEV)/blindRatio(), EV_EQ_PRECISION);
    case 'ev_rescale_oop':
    case 'ev_rescale_ip':
      const evRescaleArray = calculateEvRescaleArray(game, `ev_${view.replace("ev_rescale_", "")}`, totalPot);
      const maxEvRescale = round(max(evRescaleArray), EV_EQ_PRECISION);
      return isNaN(comboData) ? {} : backgroundColorEVEQForCombo(view, comboData, maxEvRescale, EV_EQ_PRECISION);
    case 'ev_per_eq_oop':
    case 'ev_per_eq_ip':
      let ev_per_eq_array = calculateEvPerEqArray(game, currentPosition, totalPot);
      let maxEvPerEqValue = max(ev_per_eq_array);
      return isNaN(comboData) ? {} : backgroundColorEVEQForCombo(view, comboData, maxEvPerEqValue, EV_EQ_PRECISION);
    case 'strategy-ev':
      let { minValue, maxValue } = buildStrategyEvMinMax(game.flops, currentPosition);
      let comboValues = Object.values(comboData).map(v => parseFloat(v))
      let num = comboValues.filter(v => v !== 0).length;
      let average = num === 0 ? 0 : sum(comboValues)/num;
      return average ? backgroundColorEVEQForCombo(view, average, maxValue, 1, minValue) : {};
    case 'compare-ev':
      let compareEVdata = Object.values(dataForEachCombos).map((v) => Math.abs(Object.values(v)[0] - Object.values(v)[1]))
      let minEV = min(compareEVdata);
      let maxEV = max(compareEVdata);

      const comboDataValues = Object.values(comboData);
      const distance = Math.abs(comboDataValues[0] - comboDataValues[1]);

      return comboDataValues[0] === 0 && comboDataValues[1] === 0 ? {} : backgroundColorEVEQForCombo(view, distance, maxEV, 1, minEV )
    default:
      return backgroundColorForCombo(comboData, game, preferences, decisionFilter, userRole, view)
  }
}

export const backgroundColorEVEQForCombo = (view, comboData, maxVal, decimal = 1, minVal = 0) => {
  let colorCode = findColorInRange(view, maxVal, round(parseFloat(comboData), decimal), minVal)
  let style = `linear-gradient(to right, ${colorCode} 0% 100%)`
  return {background: style}
}
