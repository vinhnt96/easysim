import { COLORS_SCHEME, COMBOS, EV_EQ_PRECISION } from 'config/constants';
import { blindRatio } from 'utils/utils'
import { calcAvgStrategy, combosGenerator, roundValueToNearest } from 'utils/matrix'
import { filterCombosBySuit } from '../filter_combos_by_suit_service'
import { convertHandCards } from '../convert_cards_services'
import { round, max, min, isEmpty, pick, isEqual, findIndex, pickBy, cloneDeep, sortBy, map} from 'lodash'
import {
  backgroundColorForCombo,
  findColorInRange,
  filterEqEvToArrayWithoutNaNValue,
  calculateEvRescaleArray,
  calculateEvPerEqArray,
} from './shared'

//////// BACKGROUND COLOR FOR EACH HAND ////////
export const handItemBackgroundColorGenerator = (params) => {
  let {
    view,
    handItem,
    game,
    dataForEachCombos,
    preferences,
    suitsForFilter,
    totalPot,
    userRole,
    weightedStrategy,
    combosWithIndex
  } = params;

  let style = {}
  if(Object.keys(dataForEachCombos).length === 0) { return style }

  const decisionFilter = params.decisionFilter || '';
  const { currentPosition } = game
  const viewOption = view.includes('ip') || view.includes('oop') ? view : currentPosition
  let maxEvRescale = 0
  if(['ev_rescale_oop', 'ev_rescale_ip'].includes(viewOption)) {       // should be moved out to run 1 time
    const evRescaleArray = calculateEvRescaleArray(game, `ev_${viewOption.replace("ev_rescale_", "")}`, totalPot);
    maxEvRescale =  round(max(evRescaleArray), EV_EQ_PRECISION);
  }
  switch(view) {
    case 'ranges_ip':
    case 'ranges_oop':
      style = backgroundColorRangesForHand(handItem, dataForEachCombos);
      break;
    case 'equity_oop':
    case 'equity_ip':
      style = backgroundColorEVEQForHand(handItem, 1, dataForEachCombos, suitsForFilter, game, viewOption, weightedStrategy, combosWithIndex);
      break;
    case 'ev_oop':
    case 'ev_ip':
      const arrayEV = filterEqEvToArrayWithoutNaNValue(game, view)
      style = backgroundColorEVForHand(handItem, max(arrayEV)/blindRatio(), dataForEachCombos, suitsForFilter, game, viewOption, weightedStrategy, combosWithIndex);
      break;
    case 'ev_rescale_oop':
    case 'ev_rescale_ip':
      style = backgroundColorEVRescaleForHand(handItem, maxEvRescale, dataForEachCombos, suitsForFilter, game, viewOption, weightedStrategy, combosWithIndex);
      break;
    case 'ev_per_eq_oop':
    case 'ev_per_eq_ip':
      let ev_per_eq_array = calculateEvPerEqArray(game, currentPosition, totalPot);
      let maxEvPerEqValue = max(ev_per_eq_array);
      style = backgroundColorEVForHand(handItem, maxEvPerEqValue, dataForEachCombos, suitsForFilter, game, viewOption, weightedStrategy, combosWithIndex);
      break;
    case 'strategy-ev':
    case '': // view strat
      style = backgroundColorForHandItem(handItem, game, dataForEachCombos, preferences, suitsForFilter, decisionFilter, userRole, weightedStrategy, view, combosWithIndex);
      break;
    default:
      let compareEVdata = Object.values(dataForEachCombos).map((v) => Math.abs(Object.values(v)[0] - Object.values(v)[1]))
      let minEV = min(compareEVdata);
      let maxEV = max(compareEVdata);
      style = backgroundColorCompareEVForHand(handItem, maxEV, dataForEachCombos, suitsForFilter, minEV);
  }

  let handItemSquare = handItemSquareSizeGenerator(handItem, game, viewOption)

  if(handItemSquare.height) {
    style['height'] = `${handItemSquare.height*100}%`
  } else {
    style['padding'] = '0px'
  }

  if(!!handItemSquare.borderRadius) {
    style['borderTopLeftRadius'] = handItemSquare.borderRadius
    style['borderTopRightRadius'] = handItemSquare.borderRadius
  }

  return {...style}
}

// Note: this service return background color for hand item base on Strategy data, we should refactor this asap :)
export const backgroundColorForHandItem = (handItem, game, strategyForEachCombos, preferences, suitsForFilter, decisionFilter='', userRole='', weightedStrategy = false, view, combosWithIndex) => {
  const { currentPosition, ranges_ip, ranges_oop } = game
  let combos = combosGenerator(handItem)
  combos = filterCombosBySuit(handItem, combos, suitsForFilter)
  if(combos.length === 0) return {}

  const combosWithIdx = isEmpty(combosWithIndex) ? renderCombosWithIndex(combos, game.flopCards) : pick(combosWithIndex, combos)

  if(weightedStrategy) {
    const ranges = isEqual(currentPosition, 'ip') ? ranges_ip : ranges_oop
    let sumRanges = 0
    combos.forEach(combo => {
      const index = combosWithIdx[combo]
      const value = roundValueToNearest(parseFloat(ranges.split(' ')[index]), 0.05)
      sumRanges += value
    })
    if(sumRanges === 0) return {}
  }
  const avgStrategy = calcAvgStrategy(combos, strategyForEachCombos, combosWithIdx, game, weightedStrategy, view)
  return backgroundColorForCombo(avgStrategy, game, preferences, decisionFilter, userRole, view)
}


const renderCombosWithIndex = (combos, flopCards) => {
  const combosWithIndex = {}
  combos.forEach(item => {
    const realHandCards = convertHandCards(flopCards, item)
    // realHandCards is key to check data index into values array ranges_oop or ranges_ip
    return combosWithIndex[`${item}`] = findIndex(COMBOS, (c) => isEqual(c, realHandCards))
  })
  return combosWithIndex
}


// ======== RANGES ========
const backgroundColorRangesForHand = (handItem, dataForEachCombos) => {
  let combos = combosGenerator(handItem)
  if(combos.length === 0) return {};

  let filteredData = pickBy(dataForEachCombos, (value, key) => combos.includes(key) && parseFloat(value) > 0)
  if(Object.keys(filteredData).length === 0)  return {}

  return { backgroundColor: COLORS_SCHEME.yellow.base };
}
// ========= END ============


// ======== EV, EQ ========
const backgroundColorEVEQForHand = (handItem, maxVal, dataForEachCombos, suitsForFilter, game, viewOption, weightedStrategy, combosWithIndex) => {
  const { ranges_ip, ranges_oop } = game;
  const arrayRangesTemp = []
  let combos = combosGenerator(handItem)
  combos = filterCombosBySuit(handItem, combos, suitsForFilter)
  if(combos.length === 0) return {}

  const dataForEachCombosCopy = cloneDeep(dataForEachCombos)
  if (weightedStrategy) {
    const combosWithIdx = isEmpty(combosWithIndex) ? renderCombosWithIndex(combos, game.flopCards) : pick(combosWithIndex, combos)
    const ranges = viewOption.includes('ip') ? ranges_ip : ranges_oop
    Object.keys(combosWithIdx).forEach(combo => {
      const index = combosWithIdx[combo]
      const rangeFloat = roundValueToNearest(parseFloat(ranges.split(' ')[index]), 0.05)
      if ( !['ev_rescale_oop', 'ev_rescale_ip'].includes(viewOption) && round(rangeFloat, EV_EQ_PRECISION) <= 0) {
        arrayRangesTemp.push(combo)
      }
    })
  }
  // filter-out combo that have value less than or equal 0
  let filteredData = pickBy(dataForEachCombosCopy, (value, key) => !arrayRangesTemp.includes(key) && combos.includes(key) && round(parseFloat(value), EV_EQ_PRECISION) > 0)
  if (Object.keys(filteredData).length === 0) return {}
  return { background: renderStyle(viewOption, filteredData, maxVal) }
}
// ========= END ============


// ======== EV ========
const backgroundColorEVForHand = (handItem, max, dataForEachCombos, suitsForFilter, game, viewOption, weightedStrategy, combosWithIndex) => {
  const { ranges_ip, ranges_oop } = game;
  const arrayRangesTemp = []
  let combos = combosGenerator(handItem)
  combos = filterCombosBySuit(handItem, combos, suitsForFilter)
  if(combos.length === 0) return {}

  const dataForEachCombosCopy = cloneDeep(dataForEachCombos)
  if (weightedStrategy) {
    const combosWithIdx = isEmpty(combosWithIndex) ? renderCombosWithIndex(combos, game.flopCards) : pick(combosWithIndex, combos)
    const ranges = viewOption.includes('ip') ? ranges_ip : ranges_oop
    Object.keys(combosWithIdx).forEach(combo => {
      const index = combosWithIdx[combo]
      const rangeFloat = roundValueToNearest(parseFloat(ranges.split(' ')[index]), 0.05)
      if (round(rangeFloat, EV_EQ_PRECISION) <= 0) {
        arrayRangesTemp.push(combo)
      }
    })
  }

  let filteredData = pickBy(dataForEachCombosCopy, (value, key) => !arrayRangesTemp.includes(key) && combos.includes(key) && !isNaN(value))
  if (Object.keys(filteredData).length === 0) return {}
  return { background: renderStyle(viewOption, filteredData, max) }
}


// ======== EV RESCALE ========
const backgroundColorEVRescaleForHand = (handItem, maxVal, dataForEachCombos, suitsForFilter, game, viewOption, weightedStrategy, combosWithIndex) => {
  const { ranges_ip, ranges_oop } = game;
  const arrayRangesTemp = []
  let combos = combosGenerator(handItem)
  combos = filterCombosBySuit(handItem, combos, suitsForFilter)
  if(combos.length === 0) return {}

  const dataForEachCombosCopy = cloneDeep(dataForEachCombos)
  if (weightedStrategy) {
    const combosWithIdx = isEmpty(combosWithIndex) ? renderCombosWithIndex(combos, game.flopCards) : pick(combosWithIndex, combos)
    const ranges = viewOption.includes('ip') ? ranges_ip : ranges_oop
    Object.keys(combosWithIdx).forEach(combo => {
      const index = combosWithIdx[combo]
      const rangeFloat = roundValueToNearest(parseFloat(ranges.split(' ')[index]), 0.05)
      if (round(rangeFloat, EV_EQ_PRECISION) <= 0) {
        arrayRangesTemp.push(combo)
      }
    })
  }

  let filteredData = pickBy(dataForEachCombosCopy, (value, key) => !arrayRangesTemp.includes(key) && combos.includes(key) && !isNaN(value))

  if (Object.keys(filteredData).length === 0) return {}
  
  return { background: renderStyle(viewOption, filteredData, maxVal) }
}
// ========= END ============


// ======== COMPARE EV ========
const backgroundColorCompareEVForHand = (handItem, maxVal, dataForEachCombos, suitsForFilter, minVal) => {
  let combos = combosGenerator(handItem)
  combos = filterCombosBySuit(handItem, combos, suitsForFilter)
  if(combos.length === 0) return {}

  let filteredData = pickBy(dataForEachCombos, (v, k) => combos.includes(k) && Object.values(v)[0] !== 0 && Object.values(v)[1] !==0)
  if(Object.keys(filteredData).length === 0) return {}

  return { background: renderStyle('compare-ev', filteredData, maxVal, minVal) }
}
// ========= END ============


const renderStyle = ( view, filteredData, maxVal, minVal ) => {
  const ratioForEach = 100 / Object.keys(filteredData).length
  let index = 0
  let style = `linear-gradient(to right,`
  let colorCode = ''
  const isEmptyMin = minVal === '' || minVal === undefined
  if (!isEmptyMin)
    filteredData = Object.values(filteredData).map(v => 
      view === 'compare-ev' ? 
      Math.abs(parseFloat(Object.values(v)[0] - Object.values(v)[1])) :
      parseFloat(Object.values(v)[0] - Object.values(v)[1])
    );
  filteredData = sortBy(filteredData, (value) =>  parseFloat(value));

  map(filteredData, (value) => {
    let ratio_from_to = `${index * ratioForEach}% ${(index + 1) * ratioForEach}%`
    if (isEmptyMin) {
      colorCode = findColorInRange(view, maxVal, round(parseFloat(value), EV_EQ_PRECISION))
    } else {
      colorCode = findColorInRange(view, maxVal, parseFloat(value), minVal);
    }
    style += ` ${colorCode} ${ratio_from_to},`
    if (index === Object.keys(filteredData).length - 1)
      style = style.slice(0, -1)
    index += 1
  })
  style += ')'
  return style
}


export const handItemSquareSizeGenerator = (handItem, game, viewOption) => {
  const { flopCards, ranges_ip, ranges_oop } = game
  const arrTotal = []
  const ranges = viewOption.includes('ip') ? ranges_ip : ranges_oop
  const rangesArray = ranges.split(' ') || []
  const combos = combosGenerator(handItem)
  let sum = 0.0
  combos.forEach((combo) => {
    const realHandCards = convertHandCards(flopCards, combo)
    const idx = COMBOS.findIndex(cb => isEqual(cb, realHandCards))
    const value = roundValueToNearest(parseFloat(rangesArray[idx], 0.05))
    arrTotal.push(value)
  })
  arrTotal.forEach(data => sum += data)
  const length = arrTotal.filter(data => data > 0).length
  if(length === 0) return { height: 0 }
  const heightPercent = round( sum / length, EV_EQ_PRECISION )
  return {height: heightPercent, borderRadius: Math.round(heightPercent*10) / 10 < 0.9 ? '0px' : null}
}
