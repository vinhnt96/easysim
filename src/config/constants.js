export const STREET_SELECT_OPTIONS = ['preflop', 'postflop']
export const DEFAULT_STREET_OPTIONS = STREET_SELECT_OPTIONS[1]
export const GAME_TYPES = ['cash', 'mtt']
export const PLAYERS_OPTIONS = ['6max', 'hu', '8max']
export const DEFAULT_GAME_TYPE = GAME_TYPES[0]
export const DEFAULT_PLAYERS = PLAYERS_OPTIONS[0]

export const CASH_TYPE_STACK_OPTIONS = ['100']
export const MMT_TYPE_STACK_OPTIONS = ['15', '20', '25', '30', '40', '50', '100']
export const DEFAULT_STACK_SIZE = {
  cash: CASH_TYPE_STACK_OPTIONS[0],
  mtt: MMT_TYPE_STACK_OPTIONS[0]
}
export const DEFAULT_STACK_OPTIONS = {
  cash: CASH_TYPE_STACK_OPTIONS,
  mtt: MMT_TYPE_STACK_OPTIONS
}

export const SUBSRIPTIONS_OBJECT = {
  free: 'None',
  mtts: 'MTTs',
  cash: 'CASH',
  ultimate: 'ULTIMATE'
}

export const CASH_TYPE_POSITION_OPTIONS = ['bb', 'sb', 'b', 'b1', 'b2', 'b3']
export const MMT_TYPE_POSITION_OPTIONS = ['bb', 'sb', 'b', 'b1', 'b2', 'b3', 'b4', 'b5']
export const POSITIONS_COUNT = 2
export const DEFAULT_POSITION_OPTIONS = {
  cash: CASH_TYPE_POSITION_OPTIONS,
  mtt: MMT_TYPE_POSITION_OPTIONS
}
export const DEFAULT_POSITIONS = {
  cash: CASH_TYPE_POSITION_OPTIONS.slice(0, 2),
  mtt: MMT_TYPE_POSITION_OPTIONS.slice(0, 2)
}

export const TWO_MINUTES = 120000

export const DISCOUNT_CODES = [
  'ggplatinum', 'pokercode', 'ginge', 'elliot', 'bts', 'streamteam', 'cavalitopoker', 'launch35',
  'pc', 'kmart', 'vipearlyaccess', 'pc50', 'pads', 'pav', 'jnandez', 'reglife',
]
export const LINKS_LAYOUT = [
  '/gg', '/pokercode', '/ginge', '/elliot', '/bts', '/subscribe', '/pokercode/subscribe',
  '/vipearlyaccess', '/streamteam', '/cavalitopoker', '/pc', '/kmart', '/pads', '/pav', '/jnandez',
  '/reglife',
]
export const LINKS = [
  '/', '/gg', '/pokercode', '/ginge', '/elliot', '/bts', '/vipearlyaccess', '/streamteam', '/cavalitopoker',
  '/pc', '/kmart', '/pads', '/pav', '/jnandez', '/reglife',
]

export const PERCENT_DISCOUNT = {
  pokercode: 0.45,
  ggplatinum: 0.35,
  ginge: 0.35,
  elliot: 0.35,
  bts: 0.35,
  streamteam: 0.35,
  cavalitopoker: 0.35,
  launch35: 0.35,
  pads: 0.35,
  pc: 0.35,
  kmart: 0.35,
  pc50: 0.5,
  pav: 0.35,
  jnandez: 0.35,
  reglife: 0.35,
}

export const DISCOUNT_CODES_OBJECT = {
  ggplatinum: 'GG',
  pokercode: 'Pokercode',
  ginge: 'Ginge',
  elliot: 'Elliot',
  bts: 'BTS',
  streamteam: 'Streamteam',
  cavalitopoker: 'Cavalitopoker',
  pc: 'PC',
  launch35: 'No Affiliate',
  kmart: 'Kmart',
  pav: 'PAV35',
  jnandez: 'JNANDEZ',
  pads: 'PADS',
  reglife: 'REGLIFE',
}

export const HUNDRED_PERCENT = 1

export const TYPE_OPTIONS = []

export const SITE_OPTIONS = ['stars', 'party']
export const DEFAULT_SITE = SITE_OPTIONS[0]

export const STARS_SITE_STAKE_OPTIOINS = [{ label: '$2.50 / 5 Zoom', value: '$2.50_per_5_zoom' }]
export const PARTY_SITE_STAKE_OPTIOINS = [{ label: '$2.50 / 5 Fastforward', value: '$2.50_per_5_fastforward' }]
export const DEFAULT_STAKE_OPTIONS = {
  stars: STARS_SITE_STAKE_OPTIOINS,
  party: PARTY_SITE_STAKE_OPTIOINS,
}

export const OPEN_SIZE_OPTIONS = ['2.5bb']
export const DEFAULT_OPEN_SIZE = OPEN_SIZE_OPTIONS[0]

export const ALTERNATE_BLIND_VS_BLIND_STRATEGIES_OPTIONS = ['4x','4x & Limp','3x & Limp','Limp only']
export const DEFAULT_ALTERNATE_BLIND_VS_BLIND_STRATEGIES = ALTERNATE_BLIND_VS_BLIND_STRATEGIES_OPTIONS[0]

export const PREFLOP_CASH_8MAX_FIELDS = {
  positions: ['', ''],
  flops: ['', '', ''],
  site: '',
  stake: '',
  open_size: '',
  sim_type: '',
  alternate_blind_vs_blind_strategies: ''
}

export const PREFLOP_CASH_BLANK_FIELDS = {
  positions: ['', ''],
  sim_type: '',
  flops: ['', '', ''],
}
export const PREFLOP_MMT_BLANK_FIELDS = {
  positions: ['', ''],
  sim_type: '',
  flops: ['', '', ''],
  site: '',
  stake: '',
  open_size: ''
}
export const POSTFLOP_BLANK_FIELDS = {
  site: '',
  stake: '',
  open_size: '',
}

export const CARD_OPTIONS = [
  { rank: '2', suit: 'c' }, { rank: '3', suit: 'c' }, { rank: '4', suit: 'c' },
  { rank: '5', suit: 'c' }, { rank: '6', suit: 'c' }, { rank: '7', suit: 'c' },
  { rank: '8', suit: 'c' }, { rank: '9', suit: 'c' }, { rank: '10', suit: 'c' },
  { rank: 'J', suit: 'c' }, { rank: 'Q', suit: 'c' }, { rank: 'K', suit: 'c' },
  { rank: 'A', suit: 'c' },

  { rank: '2', suit: 'd' }, { rank: '3', suit: 'd' }, { rank: '4', suit: 'd' },
  { rank: '5', suit: 'd' }, { rank: '6', suit: 'd' }, { rank: '7', suit: 'd' },
  { rank: '8', suit: 'd' }, { rank: '9', suit: 'd' }, { rank: '10', suit: 'd' },
  { rank: 'J', suit: 'd' }, { rank: 'Q', suit: 'd' }, { rank: 'K', suit: 'd' },
  { rank: 'A', suit: 'd' },

  { rank: '2', suit: 'h' }, { rank: '3', suit: 'h' }, { rank: '4', suit: 'h' },
  { rank: '5', suit: 'h' }, { rank: '6', suit: 'h' }, { rank: '7', suit: 'h' },
  { rank: '8', suit: 'h' }, { rank: '9', suit: 'h' }, { rank: '10', suit: 'h' },
  { rank: 'J', suit: 'h' }, { rank: 'Q', suit: 'h' }, { rank: 'K', suit: 'h' },
  { rank: 'A', suit: 'h' },

  { rank: '2', suit: 's' }, { rank: '3', suit: 's' }, { rank: '4', suit: 's' },
  { rank: '5', suit: 's' }, { rank: '6', suit: 's' }, { rank: '7', suit: 's' },
  { rank: '8', suit: 's' }, { rank: '9', suit: 's' }, { rank: '10', suit: 's' },
  { rank: 'J', suit: 's' }, { rank: 'Q', suit: 's' }, { rank: 'K', suit: 's' },
  { rank: 'A', suit: 's' }
]

export const FLOP_CARDS_COUNT = 3
export const DEFAULT_FLOP_CARDS = ['', '', '']

export const STREET_OPTIONS = [
  { label: 'Preflop', value: 'preflop'},
  { label: 'Flop', value: 'flop'},
  { label: 'Flop and turn', value: 'flop_and_turn'},
  { label: 'Flop, turn and river', value: 'flop_turn_and_river'},
];

export const PLAYER_OPTIONS = [
  { label: 'OOP', value: 'oop' },
  { label: 'IP', value: 'ip' }
]

export const SIMPLIFIER = {
  hideStrategiesLessThan: {
    defaultValue: 0.02,
    minValue: 0.0,
    maxValue: 0.1,
    stepValue: 0.05,
    percentageOptions: [
      {label: '0%', value: 0.0 },
      {label: '0.1%', value: 0.001 },
      {label: '0.5%', value: 0.005 },
      {label: '1%', value: 0.01 },
      {label: '2%', value: 0.02 },
      {label: '3%', value: 0.03 },
      {label: '4%', value: 0.04 },
      {label: '5%', value: 0.05 },
      {label: '7.5%', value: 0.075 },
      {label: '10%', value: 0.1 }
    ]
  },
  roundStrategiesToClosest: {
    defaultValue: 0.0,
    minValue: 0.0,
    percentageOptions: [
      {label: '0%', value: 0.00 },
      {label: '1%', value: 0.01 },
      {label: '5%', value: 0.05 },
      {label: '10%', value: 0.1 },
      {label: '20%', value: 0.2 },
      {label: '25%', value: 0.25 },
      {label: '50%', value: 0.5 },
      {label: '100%', value: 1 }
    ]
  }
}

export const CARD_RANK_ORDER = {
  '2': 1, '3': 2, '4': 3, '5': 4, '6': 5, '7': 6, '8': 7, '9': 8,
  '10': 9, 'J': 10, 'Q': 11, 'K': 12, 'A': 13
}
export const ACE_LOW_CARD_RANK_ORDER = {
  'A': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6, '7': 7, '8': 8, '9': 9,
  '10': 10, 'J': 11, 'Q': 12, 'K': 13
}

export const MIN_EV_PER_EQ_COLOR = "#E23752"
export const MAX_EV_PER_EQ_COLOR = "#06E1C2"

/* merge 2 constants files*/
export const LEFT_SIZE_COORDINATE = 0
export const NUMBER_OF_ROW_IN_EACH_COL = 4
export const OPTIONS_IN_A_ROW = 2
export const OPTIONS_LEFT = [
  { label: 'strategy', value: 'strategy' },
  { label: 'range', value: 'ranges' },
  { label: 'equity', value: 'equity' },
  { label: 'ev', value: 'ev' }
]
export const OPTIONS_RIGHT = [
'range-explorer', 'compare-ev', 'tree-building',
'runouts-explorer', 'notes', 'options'
]

export const ROOT_NODE = "r:0"
export const DEFAULT_INITIAL_STACK_SIZE = 1300
export const DEFAULT_POT_IP = 0
export const DEFAULT_POT_OOP = 0
export const DEFAULT_POT_MAIN = 2300
export const DEFAULT_TOTAL_EQ_EV = 500
export const DEFAULT_BEGIN_ROUND_POSITION = "oop"
export const DEFAULT_NODE_COUNT = 2
export const COLOR_SECTION_TITLES = ['fold', 'check_and_call', 'bet_and_raise']
export const DEFAULT_SIM_ITEM_PER_PAGE = 20
export const DEFAULT_UNIVERSAL_NOTE_ITEMS_PER_PAGE = 10
export const DEFAULT_SUBSCRIPTION_ITEMS_PER_PAGE = 12
export const DEFAULT_IP_HISTORY_ITEM_PER_PAGE = 2

export const HANDS_LIST = [
  "AA", "AKs", "AQs", "AJs", "ATs", "A9s", "A8s", "A7s", "A6s", "A5s", "A4s", "A3s", "A2s",
  "AKo", "KK", "KQs", "KJs", "KTs", "K9s", "K8s", "K7s", "K6s", "K5s", "K4s", "K3s", "K2s",
  "AQo", "KQo", "QQ", "QJs", "QTs", "Q9s", "Q8s", "Q7s", "Q6s", "Q5s", "Q4s", "Q3s", "Q2s",
  "AJo", "KJo", "QJo", "JJ", "JTs", "J9s", "J8s", "J7s", "J6s", "J5s", "J4s", "J3s", "J2s",
  "ATo", "KTo", "QTo", "JTo", "TT", "T9s", "T8s", "T7s", "T6s", "T5s", "T4s", "T3s", "T2s",
  "A9o", "K9o", "Q9o", "J9o", "T9o", "99", "98s", "97s", "96s", "95s", "94s", "93s", "92s",
  "A8o", "K8o", "Q8o", "J8o", "T8o", "98o", "88", "87s", "86s", "85s", "84s", "83s", "82s",
  "A7o", "K7o", "Q7o", "J7o", "T7o", "97o", "87o", "77", "76s", "75s", "74s", "73s", "72s",
  "A6o", "K6o", "Q6o", "J6o", "T6o", "96o", "86o", "76o", "66", "65s", "64s", "63s", "62s",
  "A5o", "K5o", "Q5o", "J5o", "T5o", "95o", "85o", "75o", "65o", "55", "54s", "53s", "52s",
  "A4o", "K4o", "Q4o", "J4o", "T4o", "94o", "84o", "74o", "64o", "54o", "44", "43s", "42s",
  "A3o", "K3o", "Q3o", "J3o", "T3o", "93o", "83o", "73o", "63o", "53o", "43o", "33", "32s",
  "A2o", "K2o", "Q2o", "J2o", "T2o", "92o", "82o", "72o", "62o", "52o", "42o", "32o", "22"
];

export const COMBOS = [
  '2d2c', '2h2c', '2h2d', '2s2c', '2s2d', '2s2h', '3c2c', '3c2d', '3c2h', '3c2s', '3d2c', '3d2d', '3d2h', '3d2s', '3d3c', '3h2c', '3h2d', '3h2h', '3h2s', '3h3c', '3h3d',
  '3s2c', '3s2d', '3s2h', '3s2s','3s3c', '3s3d', '3s3h', '4c2c', '4c2d', '4c2h', '4c2s', '4c3c', '4c3d', '4c3h', '4c3s', '4d2c', '4d2d', '4d2h', '4d2s', '4d3c', '4d3d',
  '4d3h', '4d3s', '4d4c', '4h2c', '4h2d', '4h2h', '4h2s', '4h3c', '4h3d','4h3h', '4h3s', '4h4c', '4h4d', '4s2c', '4s2d', '4s2h', '4s2s', '4s3c', '4s3d', '4s3h', '4s3s',
  '4s4c', '4s4d', '4s4h', '5c2c', '5c2d', '5c2h', '5c2s', '5c3c', '5c3d', '5c3h', '5c3s', '5c4c', '5c4d', '5c4h','5c4s', '5d2c', '5d2d', '5d2h', '5d2s', '5d3c', '5d3d',
  '5d3h', '5d3s', '5d4c', '5d4d', '5d4h', '5d4s', '5d5c', '5h2c', '5h2d', '5h2h', '5h2s', '5h3c', '5h3d', '5h3h', '5h3s', '5h4c', '5h4d', '5h4h', '5h4s','5h5c', '5h5d',
  '5s2c', '5s2d', '5s2h', '5s2s', '5s3c', '5s3d', '5s3h', '5s3s', '5s4c', '5s4d', '5s4h', '5s4s', '5s5c', '5s5d', '5s5h', '6c2c', '6c2d', '6c2h', '6c2s', '6c3c', '6c3d',
  '6c3h', '6c3s', '6c4c','6c4d', '6c4h', '6c4s', '6c5c', '6c5d', '6c5h', '6c5s', '6d2c', '6d2d', '6d2h', '6d2s', '6d3c', '6d3d', '6d3h', '6d3s', '6d4c', '6d4d', '6d4h',
  '6d4s', '6d5c', '6d5d', '6d5h', '6d5s', '6d6c', '6h2c', '6h2d','6h2h', '6h2s', '6h3c', '6h3d', '6h3h', '6h3s', '6h4c', '6h4d', '6h4h', '6h4s', '6h5c', '6h5d', '6h5h',
  '6h5s', '6h6c', '6h6d', '6s2c', '6s2d', '6s2h', '6s2s', '6s3c', '6s3d', '6s3h', '6s3s', '6s4c', '6s4d','6s4h', '6s4s', '6s5c', '6s5d', '6s5h', '6s5s', '6s6c', '6s6d',
  '6s6h', '7c2c', '7c2d', '7c2h', '7c2s', '7c3c', '7c3d', '7c3h', '7c3s', '7c4c', '7c4d', '7c4h', '7c4s', '7c5c', '7c5d', '7c5h', '7c5s', '7c6c','7c6d', '7c6h', '7c6s',
  '7d2c', '7d2d', '7d2h', '7d2s', '7d3c', '7d3d', '7d3h', '7d3s', '7d4c', '7d4d', '7d4h', '7d4s', '7d5c', '7d5d', '7d5h', '7d5s', '7d6c', '7d6d', '7d6h', '7d6s', '7d7c',
  '7h2c', '7h2d','7h2h', '7h2s', '7h3c', '7h3d', '7h3h', '7h3s', '7h4c', '7h4d', '7h4h', '7h4s', '7h5c', '7h5d', '7h5h', '7h5s', '7h6c', '7h6d', '7h6h', '7h6s', '7h7c',
  '7h7d', '7s2c', '7s2d', '7s2h', '7s2s', '7s3c', '7s3d','7s3h', '7s3s', '7s4c', '7s4d', '7s4h', '7s4s', '7s5c', '7s5d', '7s5h', '7s5s', '7s6c', '7s6d', '7s6h', '7s6s',
  '7s7c', '7s7d', '7s7h', '8c2c', '8c2d', '8c2h', '8c2s', '8c3c', '8c3d', '8c3h', '8c3s', '8c4c','8c4d', '8c4h', '8c4s', '8c5c', '8c5d', '8c5h', '8c5s', '8c6c', '8c6d',
  '8c6h', '8c6s', '8c7c', '8c7d', '8c7h', '8c7s', '8d2c', '8d2d', '8d2h', '8d2s', '8d3c', '8d3d', '8d3h', '8d3s', '8d4c', '8d4d', '8d4h','8d4s', '8d5c', '8d5d', '8d5h',
  '8d5s', '8d6c', '8d6d', '8d6h', '8d6s', '8d7c', '8d7d', '8d7h', '8d7s', '8d8c', '8h2c', '8h2d', '8h2h', '8h2s', '8h3c', '8h3d', '8h3h', '8h3s', '8h4c', '8h4d', '8h4h',
  '8h4s','8h5c', '8h5d', '8h5h', '8h5s', '8h6c', '8h6d', '8h6h', '8h6s', '8h7c', '8h7d', '8h7h', '8h7s', '8h8c', '8h8d', '8s2c', '8s2d', '8s2h', '8s2s', '8s3c', '8s3d',
  '8s3h', '8s3s', '8s4c', '8s4d', '8s4h', '8s4s','8s5c', '8s5d', '8s5h', '8s5s', '8s6c', '8s6d', '8s6h', '8s6s', '8s7c', '8s7d', '8s7h', '8s7s', '8s8c', '8s8d', '8s8h',
  '9c2c', '9c2d', '9c2h', '9c2s', '9c3c', '9c3d', '9c3h', '9c3s', '9c4c', '9c4d', '9c4h','9c4s', '9c5c', '9c5d', '9c5h', '9c5s', '9c6c', '9c6d', '9c6h', '9c6s', '9c7c',
  '9c7d', '9c7h', '9c7s', '9c8c', '9c8d', '9c8h', '9c8s', '9d2c', '9d2d', '9d2h', '9d2s', '9d3c', '9d3d', '9d3h', '9d3s', '9d4c','9d4d', '9d4h', '9d4s', '9d5c', '9d5d',
  '9d5h', '9d5s', '9d6c', '9d6d', '9d6h', '9d6s', '9d7c', '9d7d', '9d7h', '9d7s', '9d8c', '9d8d', '9d8h', '9d8s', '9d9c', '9h2c', '9h2d', '9h2h', '9h2s', '9h3c', '9h3d',
  '9h3h', '9h3s', '9h4c', '9h4d', '9h4h', '9h4s', '9h5c', '9h5d', '9h5h', '9h5s', '9h6c', '9h6d', '9h6h', '9h6s', '9h7c', '9h7d', '9h7h', '9h7s', '9h8c', '9h8d', '9h8h',
  '9h8s', '9h9c', '9h9d', '9s2c', '9s2d', '9s2h', '9s2s', '9s3c', '9s3d', '9s3h', '9s3s', '9s4c', '9s4d', '9s4h', '9s4s', '9s5c', '9s5d', '9s5h', '9s5s', '9s6c', '9s6d',
  '9s6h', '9s6s', '9s7c', '9s7d', '9s7h', '9s7s', '9s8c', '9s8d', '9s8h', '9s8s','9s9c', '9s9d', '9s9h', 'Tc2c', 'Tc2d', 'Tc2h', 'Tc2s', 'Tc3c', 'Tc3d', 'Tc3h', 'Tc3s',
  'Tc4c', 'Tc4d', 'Tc4h', 'Tc4s', 'Tc5c', 'Tc5d', 'Tc5h', 'Tc5s', 'Tc6c', 'Tc6d', 'Tc6h', 'Tc6s', 'Tc7c', 'Tc7d', 'Tc7h','Tc7s', 'Tc8c', 'Tc8d', 'Tc8h', 'Tc8s', 'Tc9c',
  'Tc9d', 'Tc9h', 'Tc9s', 'Td2c', 'Td2d', 'Td2h', 'Td2s', 'Td3c', 'Td3d', 'Td3h', 'Td3s', 'Td4c', 'Td4d', 'Td4h', 'Td4s', 'Td5c', 'Td5d', 'Td5h', 'Td5s', 'Td6c','Td6d',
  'Td6h', 'Td6s', 'Td7c', 'Td7d', 'Td7h', 'Td7s', 'Td8c', 'Td8d', 'Td8h', 'Td8s', 'Td9c', 'Td9d', 'Td9h', 'Td9s', 'TdTc', 'Th2c', 'Th2d', 'Th2h', 'Th2s', 'Th3c', 'Th3d',
  'Th3h', 'Th3s', 'Th4c', 'Th4d','Th4h', 'Th4s', 'Th5c', 'Th5d', 'Th5h', 'Th5s', 'Th6c', 'Th6d', 'Th6h', 'Th6s', 'Th7c', 'Th7d', 'Th7h', 'Th7s', 'Th8c', 'Th8d', 'Th8h',
  'Th8s', 'Th9c', 'Th9d', 'Th9h', 'Th9s', 'ThTc', 'ThTd', 'Ts2c', 'Ts2d','Ts2h', 'Ts2s', 'Ts3c', 'Ts3d', 'Ts3h', 'Ts3s', 'Ts4c', 'Ts4d', 'Ts4h', 'Ts4s', 'Ts5c', 'Ts5d',
  'Ts5h', 'Ts5s', 'Ts6c', 'Ts6d', 'Ts6h', 'Ts6s', 'Ts7c', 'Ts7d', 'Ts7h', 'Ts7s', 'Ts8c', 'Ts8d', 'Ts8h', 'Ts8s','Ts9c', 'Ts9d', 'Ts9h', 'Ts9s', 'TsTc', 'TsTd', 'TsTh',
  'Jc2c', 'Jc2d', 'Jc2h', 'Jc2s', 'Jc3c', 'Jc3d', 'Jc3h', 'Jc3s', 'Jc4c', 'Jc4d', 'Jc4h', 'Jc4s', 'Jc5c', 'Jc5d', 'Jc5h', 'Jc5s', 'Jc6c', 'Jc6d', 'Jc6h','Jc6s', 'Jc7c',
  'Jc7d', 'Jc7h', 'Jc7s', 'Jc8c', 'Jc8d', 'Jc8h', 'Jc8s', 'Jc9c', 'Jc9d', 'Jc9h', 'Jc9s', 'JcTc', 'JcTd', 'JcTh', 'JcTs', 'Jd2c', 'Jd2d', 'Jd2h', 'Jd2s', 'Jd3c', 'Jd3d',
  'Jd3h', 'Jd3s', 'Jd4c','Jd4d', 'Jd4h', 'Jd4s', 'Jd5c', 'Jd5d', 'Jd5h', 'Jd5s', 'Jd6c', 'Jd6d', 'Jd6h', 'Jd6s', 'Jd7c', 'Jd7d', 'Jd7h', 'Jd7s', 'Jd8c', 'Jd8d', 'Jd8h',
  'Jd8s', 'Jd9c', 'Jd9d', 'Jd9h', 'Jd9s', 'JdTc', 'JdTd', 'JdTh','JdTs', 'JdJc', 'Jh2c', 'Jh2d', 'Jh2h', 'Jh2s', 'Jh3c', 'Jh3d', 'Jh3h', 'Jh3s', 'Jh4c', 'Jh4d', 'Jh4h',
  'Jh4s', 'Jh5c', 'Jh5d', 'Jh5h', 'Jh5s', 'Jh6c', 'Jh6d', 'Jh6h', 'Jh6s', 'Jh7c', 'Jh7d', 'Jh7h', 'Jh7s','Jh8c', 'Jh8d', 'Jh8h', 'Jh8s', 'Jh9c', 'Jh9d', 'Jh9h', 'Jh9s',
  'JhTc', 'JhTd', 'JhTh', 'JhTs', 'JhJc', 'JhJd', 'Js2c', 'Js2d', 'Js2h', 'Js2s', 'Js3c', 'Js3d', 'Js3h', 'Js3s', 'Js4c', 'Js4d', 'Js4h', 'Js4s','Js5c', 'Js5d', 'Js5h',
  'Js5s', 'Js6c', 'Js6d', 'Js6h', 'Js6s', 'Js7c', 'Js7d', 'Js7h', 'Js7s', 'Js8c', 'Js8d', 'Js8h', 'Js8s', 'Js9c', 'Js9d', 'Js9h', 'Js9s', 'JsTc', 'JsTd', 'JsTh', 'JsTs',
  'JsJc', 'JsJd','JsJh', 'Qc2c', 'Qc2d', 'Qc2h', 'Qc2s', 'Qc3c', 'Qc3d', 'Qc3h', 'Qc3s', 'Qc4c', 'Qc4d', 'Qc4h', 'Qc4s', 'Qc5c', 'Qc5d', 'Qc5h', 'Qc5s', 'Qc6c', 'Qc6d',
  'Qc6h', 'Qc6s', 'Qc7c', 'Qc7d', 'Qc7h', 'Qc7s', 'Qc8c','Qc8d', 'Qc8h', 'Qc8s', 'Qc9c', 'Qc9d', 'Qc9h', 'Qc9s', 'QcTc', 'QcTd', 'QcTh', 'QcTs', 'QcJc', 'QcJd', 'QcJh',
  'QcJs', 'Qd2c', 'Qd2d', 'Qd2h', 'Qd2s', 'Qd3c', 'Qd3d', 'Qd3h', 'Qd3s', 'Qd4c', 'Qd4d', 'Qd4h','Qd4s', 'Qd5c', 'Qd5d', 'Qd5h', 'Qd5s', 'Qd6c', 'Qd6d', 'Qd6h', 'Qd6s',
  'Qd7c', 'Qd7d', 'Qd7h', 'Qd7s', 'Qd8c', 'Qd8d', 'Qd8h', 'Qd8s', 'Qd9c', 'Qd9d', 'Qd9h', 'Qd9s', 'QdTc', 'QdTd', 'QdTh', 'QdTs', 'QdJc','QdJd', 'QdJh', 'QdJs', 'QdQc',
  'Qh2c', 'Qh2d', 'Qh2h', 'Qh2s', 'Qh3c', 'Qh3d', 'Qh3h', 'Qh3s', 'Qh4c', 'Qh4d', 'Qh4h', 'Qh4s', 'Qh5c', 'Qh5d', 'Qh5h', 'Qh5s', 'Qh6c', 'Qh6d', 'Qh6h', 'Qh6s', 'Qh7c',
  'Qh7d','Qh7h', 'Qh7s', 'Qh8c', 'Qh8d', 'Qh8h', 'Qh8s', 'Qh9c', 'Qh9d', 'Qh9h', 'Qh9s', 'QhTc', 'QhTd', 'QhTh', 'QhTs', 'QhJc', 'QhJd', 'QhJh', 'QhJs', 'QhQc', 'QhQd',
  'Qs2c', 'Qs2d', 'Qs2h', 'Qs2s', 'Qs3c', 'Qs3d','Qs3h', 'Qs3s', 'Qs4c', 'Qs4d', 'Qs4h', 'Qs4s', 'Qs5c', 'Qs5d', 'Qs5h', 'Qs5s', 'Qs6c', 'Qs6d', 'Qs6h', 'Qs6s', 'Qs7c',
  'Qs7d', 'Qs7h', 'Qs7s', 'Qs8c', 'Qs8d', 'Qs8h', 'Qs8s', 'Qs9c', 'Qs9d', 'Qs9h', 'Qs9s','QsTc', 'QsTd', 'QsTh', 'QsTs', 'QsJc', 'QsJd', 'QsJh', 'QsJs', 'QsQc', 'QsQd',
  'QsQh', 'Kc2c', 'Kc2d', 'Kc2h', 'Kc2s', 'Kc3c', 'Kc3d', 'Kc3h', 'Kc3s', 'Kc4c', 'Kc4d', 'Kc4h', 'Kc4s', 'Kc5c', 'Kc5d', 'Kc5h','Kc5s', 'Kc6c', 'Kc6d', 'Kc6h', 'Kc6s',
  'Kc7c', 'Kc7d', 'Kc7h', 'Kc7s', 'Kc8c', 'Kc8d', 'Kc8h', 'Kc8s', 'Kc9c', 'Kc9d', 'Kc9h', 'Kc9s', 'KcTc', 'KcTd', 'KcTh', 'KcTs', 'KcJc', 'KcJd', 'KcJh', 'KcJs', 'KcQc',
  'KcQd', 'KcQh', 'KcQs', 'Kd2c', 'Kd2d', 'Kd2h', 'Kd2s', 'Kd3c', 'Kd3d', 'Kd3h', 'Kd3s', 'Kd4c', 'Kd4d', 'Kd4h', 'Kd4s', 'Kd5c', 'Kd5d', 'Kd5h', 'Kd5s', 'Kd6c', 'Kd6d',
  'Kd6h', 'Kd6s', 'Kd7c', 'Kd7d', 'Kd7h','Kd7s', 'Kd8c', 'Kd8d', 'Kd8h', 'Kd8s', 'Kd9c', 'Kd9d', 'Kd9h', 'Kd9s', 'KdTc', 'KdTd', 'KdTh', 'KdTs', 'KdJc', 'KdJd', 'KdJh',
  'KdJs', 'KdQc', 'KdQd', 'KdQh', 'KdQs', 'KdKc', 'Kh2c', 'Kh2d', 'Kh2h', 'Kh2s','Kh3c', 'Kh3d', 'Kh3h', 'Kh3s', 'Kh4c', 'Kh4d', 'Kh4h', 'Kh4s', 'Kh5c', 'Kh5d', 'Kh5h',
  'Kh5s', 'Kh6c', 'Kh6d', 'Kh6h', 'Kh6s', 'Kh7c', 'Kh7d', 'Kh7h', 'Kh7s', 'Kh8c', 'Kh8d', 'Kh8h', 'Kh8s', 'Kh9c', 'Kh9d','Kh9h', 'Kh9s', 'KhTc', 'KhTd', 'KhTh', 'KhTs',
  'KhJc', 'KhJd', 'KhJh', 'KhJs', 'KhQc', 'KhQd', 'KhQh', 'KhQs', 'KhKc', 'KhKd', 'Ks2c', 'Ks2d', 'Ks2h', 'Ks2s', 'Ks3c', 'Ks3d', 'Ks3h', 'Ks3s', 'Ks4c', 'Ks4d','Ks4h',
  'Ks4s', 'Ks5c', 'Ks5d', 'Ks5h', 'Ks5s', 'Ks6c', 'Ks6d', 'Ks6h', 'Ks6s', 'Ks7c', 'Ks7d', 'Ks7h', 'Ks7s', 'Ks8c', 'Ks8d', 'Ks8h', 'Ks8s', 'Ks9c', 'Ks9d', 'Ks9h', 'Ks9s',
  'KsTc', 'KsTd', 'KsTh', 'KsTs','KsJc', 'KsJd', 'KsJh', 'KsJs', 'KsQc', 'KsQd', 'KsQh', 'KsQs', 'KsKc', 'KsKd', 'KsKh', 'Ac2c', 'Ac2d', 'Ac2h', 'Ac2s', 'Ac3c', 'Ac3d',
  'Ac3h', 'Ac3s', 'Ac4c', 'Ac4d', 'Ac4h', 'Ac4s', 'Ac5c', 'Ac5d', 'Ac5h','Ac5s', 'Ac6c', 'Ac6d', 'Ac6h', 'Ac6s', 'Ac7c', 'Ac7d', 'Ac7h', 'Ac7s', 'Ac8c', 'Ac8d', 'Ac8h',
  'Ac8s', 'Ac9c', 'Ac9d', 'Ac9h', 'Ac9s', 'AcTc', 'AcTd', 'AcTh', 'AcTs', 'AcJc', 'AcJd', 'AcJh', 'AcJs', 'AcQc','AcQd', 'AcQh', 'AcQs', 'AcKc', 'AcKd', 'AcKh', 'AcKs',
  'Ad2c', 'Ad2d', 'Ad2h', 'Ad2s', 'Ad3c', 'Ad3d', 'Ad3h', 'Ad3s', 'Ad4c', 'Ad4d', 'Ad4h', 'Ad4s', 'Ad5c', 'Ad5d', 'Ad5h', 'Ad5s', 'Ad6c', 'Ad6d', 'Ad6h','Ad6s', 'Ad7c',
  'Ad7d', 'Ad7h', 'Ad7s', 'Ad8c', 'Ad8d', 'Ad8h', 'Ad8s', 'Ad9c', 'Ad9d', 'Ad9h', 'Ad9s', 'AdTc', 'AdTd', 'AdTh', 'AdTs', 'AdJc', 'AdJd', 'AdJh', 'AdJs', 'AdQc', 'AdQd',
  'AdQh', 'AdQs', 'AdKc','AdKd', 'AdKh', 'AdKs', 'AdAc', 'Ah2c', 'Ah2d', 'Ah2h', 'Ah2s', 'Ah3c', 'Ah3d', 'Ah3h', 'Ah3s', 'Ah4c', 'Ah4d', 'Ah4h', 'Ah4s', 'Ah5c', 'Ah5d',
  'Ah5h', 'Ah5s', 'Ah6c', 'Ah6d', 'Ah6h', 'Ah6s', 'Ah7c', 'Ah7d','Ah7h', 'Ah7s', 'Ah8c', 'Ah8d', 'Ah8h', 'Ah8s', 'Ah9c', 'Ah9d', 'Ah9h', 'Ah9s', 'AhTc', 'AhTd', 'AhTh',
  'AhTs', 'AhJc', 'AhJd', 'AhJh', 'AhJs', 'AhQc', 'AhQd', 'AhQh', 'AhQs', 'AhKc', 'AhKd', 'AhKh', 'AhKs','AhAc', 'AhAd', 'As2c', 'As2d', 'As2h', 'As2s', 'As3c', 'As3d',
  'As3h', 'As3s', 'As4c', 'As4d', 'As4h', 'As4s', 'As5c', 'As5d', 'As5h', 'As5s', 'As6c', 'As6d', 'As6h', 'As6s', 'As7c', 'As7d', 'As7h', 'As7s','As8c', 'As8d', 'As8h',
  'As8s', 'As9c', 'As9d', 'As9h', 'As9s', 'AsTc', 'AsTd', 'AsTh', 'AsTs', 'AsJc', 'AsJd', 'AsJh', 'AsJs', 'AsQc', 'AsQd', 'AsQh', 'AsQs', 'AsKc', 'AsKd', 'AsKh', 'AsKs',
  'AsAc', 'AsAd', 'AsAh'
]

export const CARDS = [
  '2c', '3c', '4c', '5c', '6c', '7c', '8c', '9c', 'Tc', 'Jc', 'Qc', 'Kc', 'Ac',
  '2d', '3d', '4d', '5d', '6d', '7d', '8d', '9d', 'Td', 'Jd', 'Qd', 'Kd', 'Ad',
  '2s', '3s', '4s', '5s', '6s', '7s', '8s', '9s', 'Ts', 'Js', 'Qs', 'Ks', 'As',
  '2h', '3h', '4h', '5h', '6h', '7h', '8h', '9h', 'Th', 'Jh', 'Qh', 'Kh', 'Ah',
];

export const COLORS_SCHEME = {
  blue: {
    base: '#46ABF9',
    level_1: '#D2ECFF',
    level_2: '#BBE2FF',
    level_3: '#A4D8FF',
    level_4: '#8DCDFE',
    level_5: '#75C2FC',
    level_6: '#5EB6FB',
    level_7: '#52B1FA',
    level_8: '#46ABF9',
    level_9: '#2B4FDB',
    level_10: '#1A7ECA',
    level_11: '#0467B2',
    level_12: '#035899',
  },
  orange: {
    base: '#F59B53',
    level_1: '#FFE8C9',
    level_2: '#FFDCAE',
    level_3: '#FFD093',
    level_4: '#FDC383',
    level_5: '#FAB673',
    level_6: '#F8A863',
    level_7: '#F7A25B',
    level_8: '#F59B53',
    level_9: '#EB7F44',
    level_10: '#E26435',
    level_11: '#D84826',
    level_12: '#B9EE21',
  },
  yellow: {
    base: '#FFD254',
    level_1: '#FFF9C8',
    level_2: '#FFF6AD',
    level_3: '#FFF391',
    level_4: '#FFEB82',
    level_5: '#FFE373',
    level_6: '#FFDA63',
    level_7: '#FFD65C',
    level_8: '#FFD254',
    level_9: '#F2BF38',
    level_10: '#E6AC1C',
    level_11: '#D99900',
    level_12: '#BA8300',
  },
  green: {
    base: '#2AD699',
    level_1: '#C6FDEA',
    level_2: '#AAFCDF',
    level_3: '#8DFBD4',
    level_4: '#74F2C5',
    level_5: '#5CE9B7',
    level_6: '#43DFA8',
    level_7: '#0EB379',
    level_8: '#2AD699',
    level_9: '#1CC589',
    level_10: '#0EB379',
    level_11: '#00A269',
    level_12: '#008B5A',
  },
  red: {
    base: '#E23752',
    level_1: '#FFCADC',
    level_2: '#FFB0CA',
    level_3: '#FF95B8',
    level_4: '#F87E9F',
    level_5: '#F16685',
    level_6: '#E94F6C',
    level_7: '#E6435F',
    level_8: '#E23752',
    level_9: '#C82A43',
    level_10: '#AF1D33',
    level_11: '#951024',
    level_12: '#800E1F',
  },
  purple: {
    base: '#BD80E3',
    level_1: '#FFEAF0',
    level_2: '#FFDFE8',
    level_3: '#FFD4E0',
    level_4: '#EFBFE1',
    level_5: '#DEAAE2',
    level_6: '#CE95E2',
    level_7: '#C68BE3',
    level_8: '#BD80E3',
    level_9: '#A25FDA',
    level_10: '#883ED0',
    level_11: '#6D1DC7',
    level_12: '#5D19AB',
  }
}

export const MIN_EV_COLOR = "#E23752"
export const MAX_EV_COLOR = "#2AD699"

export const EV_COLOR_STRIP = {
  level_1: '#AF1D33',
  level_2: '#E23752',
  level_3: '#EB7F44',
  level_4: '#FFD254',
  level_5: '#AAFCDC',
  level_6: '#37DBA1',
  level_7: '#00A269',
}

export const COMPARE_EV_COLOR_STRIP = {
  level_1: '#C82A43',
  level_2: '#FFA500',
  level_3: '#FFFF00',
  level_4: '#90EE90',
  level_5: '#B3FF03'
}

export const POSITION_WITH_ORDER = [
  { position: 'sb', order: 6 },
  { position: 'bb', order: 7 },
  { position: 'b',  order: 0 },
  { position: 'b1', order: 1 },
  { position: 'b2', order: 2 },
  { position: 'b3', order: 3 },
  { position: 'b4', order: 4 },
  { position: 'b5', order: 5 }
]

export const RUNOUTS_EXPLORER_VIEWS = [
  { text: 'OOP Equity', value: 'eq_oop' },
  { text: 'IP Equity', value: 'eq_ip' },
  { text: 'OOP EV', value: 'ev_oop' },
  { text: 'IP EV', value: 'ev_ip' },
  { text: 'Strategy', value: 'strategy' },
];

export const RUNOUTS_EXPLORER_DEFAULT_VIEW = 'strategy';
export const RUNOUTS_EXPLORER_EV_DECIMAL = 2;
export const RUNOUTS_EXPLORER_STRATEGY_DECIMAL = 2;

export const VIEW_OPTIONS = [
  { label: 'Strategy', value: '' },
  { label: 'EV rescaled', value: 'ev_rescale'},
  { label: 'Equity', value: 'equity'},
  { label: 'EV/Equity', value: 'ev_per_eq' }
];

export const DUAL_RANGE_VIEW_OPTIONS = [
  { label: 'EV rescaled', value: 'ev_rescale'},
  { label: 'Equity', value: 'equity'},
  { label: 'EV/Equity', value: 'ev_per_eq' },
  { label: 'Strategy', value: '' },
];

export const CARDS_ORDERED_BY_RANK_AND_SUIT = [
  { card: 'As', order: 1 }, { card: 'Ah', order: 2 }, { card: 'Ad', order: 3 }, { card: 'Ac', order: 4 },
  { card: 'Ks', order: 5 }, { card: 'Kh', order: 6 }, { card: 'Kd', order: 7 }, { card: 'Kc', order: 8 },
  { card: 'Qs', order: 9 }, { card: 'Qh', order: 10 }, { card: 'Qd', order: 11 }, { card: 'Qc', order: 12 },
  { card: 'Js', order: 13 }, { card: 'Jh', order: 14 }, { card: 'Jd', order: 15 }, { card: 'Jc', order: 16 },
  { card: 'Ts', order: 17 }, { card: 'Th', order: 18 }, { card: 'Td', order: 19 }, { card: 'Tc', order: 20 },
  { card: '9s', order: 21 }, { card: '9h', order: 22 }, { card: '9d', order: 23 }, { card: '9c', order: 24 },
  { card: '8s', order: 25 }, { card: '8h', order: 26 }, { card: '8d', order: 27 }, { card: '8c', order: 28 },
  { card: '7s', order: 29 }, { card: '7h', order: 30 }, { card: '7d', order: 31 }, { card: '7c', order: 32 },
  { card: '6s', order: 33 }, { card: '6h', order: 34 }, { card: '6d', order: 35 }, { card: '6c', order: 36 },
  { card: '5s', order: 37 }, { card: '5h', order: 38 }, { card: '5d', order: 39 }, { card: '5c', order: 40 },
  { card: '4s', order: 41 }, { card: '4h', order: 42 }, { card: '4d', order: 43 }, { card: '4c', order: 44 },
  { card: '3s', order: 45 }, { card: '3h', order: 46 }, { card: '3d', order: 47 }, { card: '3c', order: 48 },
  { card: '2s', order: 49 }, { card: '2h', order: 50 }, { card: '2d', order: 51 }, { card: '2c', order: 52 },
]

export const CONVERTED_CARDS_ORDERED_BY_RANK_AND_SUIT = [
  { card: 'As', order: 1 }, { card: 'Ad', order: 2 }, { card: 'Ac', order: 3 }, { card: 'Ah', order: 4 },
  { card: 'Ks', order: 5 }, { card: 'Kd', order: 6 }, { card: 'Kc', order: 7 }, { card: 'Kh', order: 8 },
  { card: 'Qs', order: 9 }, { card: 'Qd', order: 10 }, { card: 'Qc', order: 11 }, { card: 'Qh', order: 12 },
  { card: 'Js', order: 13 }, { card: 'Jd', order: 14 }, { card: 'Jc', order: 15 }, { card: 'Jh', order: 16 },
  { card: 'Ts', order: 17 }, { card: 'Td', order: 18 }, { card: 'Tc', order: 19 }, { card: 'Th', order: 20 },
  { card: '9s', order: 21 }, { card: '9d', order: 22 }, { card: '9c', order: 23 }, { card: '9h', order: 24 },
  { card: '8s', order: 25 }, { card: '8d', order: 26 }, { card: '8c', order: 27 }, { card: '8h', order: 28 },
  { card: '7s', order: 29 }, { card: '7d', order: 30 }, { card: '7c', order: 31 }, { card: '7h', order: 32 },
  { card: '6s', order: 33 }, { card: '6d', order: 34 }, { card: '6c', order: 35 }, { card: '6h', order: 36 },
  { card: '5s', order: 37 }, { card: '5d', order: 38 }, { card: '5c', order: 39 }, { card: '5h', order: 40 },
  { card: '4s', order: 41 }, { card: '4d', order: 42 }, { card: '4c', order: 43 }, { card: '4h', order: 44 },
  { card: '3s', order: 45 }, { card: '3d', order: 46 }, { card: '3c', order: 47 }, { card: '3h', order: 48 },
  { card: '2s', order: 49 }, { card: '2d', order: 50 }, { card: '2c', order: 51 }, { card: '2h', order: 52 },
]

export const CARDS_ORDERED_BY_RANK = [
  { card: 'As', order: 1 }, { card: 'Ad', order: 1 }, { card: 'Ac', order: 1 }, { card: 'Ah', order: 1 },
  { card: 'Ks', order: 2 }, { card: 'Kd', order: 2 }, { card: 'Kc', order: 2 }, { card: 'Kh', order: 2 },
  { card: 'Qs', order: 3 }, { card: 'Qd', order: 3 }, { card: 'Qc', order: 3 }, { card: 'Qh', order: 3 },
  { card: 'Js', order: 4 }, { card: 'Jd', order: 4 }, { card: 'Jc', order: 4 }, { card: 'Jh', order: 4 },
  { card: 'Ts', order: 5 }, { card: 'Td', order: 5 }, { card: 'Tc', order: 5 }, { card: 'Th', order: 5 },
  { card: '9s', order: 6 }, { card: '9d', order: 6 }, { card: '9c', order: 6 }, { card: '9h', order: 6 },
  { card: '8s', order: 7 }, { card: '8d', order: 7 }, { card: '8c', order: 7 }, { card: '8h', order: 7 },
  { card: '7s', order: 8 }, { card: '7d', order: 8 }, { card: '7c', order: 8 }, { card: '7h', order: 8 },
  { card: '6s', order: 9 }, { card: '6d', order: 9 }, { card: '6c', order: 9 }, { card: '6h', order: 9 },
  { card: '5s', order: 10 }, { card: '5d', order: 10 }, { card: '5c', order: 10 }, { card: '5h', order: 10 },
  { card: '4s', order: 11 }, { card: '4d', order: 11 }, { card: '4c', order: 11 }, { card: '4h', order: 11 },
  { card: '3s', order: 12 }, { card: '3d', order: 12 }, { card: '3c', order: 12 }, { card: '3h', order: 12 },
  { card: '2s', order: 13 }, { card: '2d', order: 13 }, { card: '2c', order: 13 }, { card: '2h', order: 13 },
]

export const CASH_GAME_PLAYER_POSITIONS = [
  'b', 'sb', 'bb', 'b1', 'b2', 'b3'
]

export const HU_GAME_PLAYER_POSITIONS = [ 'b', 'bb' ]

export const MTT_GAME_PLAYER_POSITIONS = [
  'b', 'sb', 'bb', 'b1', 'b2', 'b3', 'b4', 'b5'
]

export const SUITS_FOR_FILTER = {
  excluded: {first: "", second: ""},
  included: {first: "", second: ""}
}

export const INITIAL_POSITION_NODES = {
  'b': 'B_strategy',
  'sb': 'SB_strategy',
  'bb': 'BB_strategy',
  'b1': 'B+1_strategy',
  'b2': 'B+2_strategy',
  'b3': 'B+3_strategy',
  'b4': 'B+4_strategy',
  'b5': 'B+5_strategy',
}

export const PREFLOP_POSITIONS_ORDER = {
  cash: ['b3', 'b2', 'b1', 'b', 'sb', 'bb'],
  mtt: ['b5', 'b4', 'b3', 'b2', 'b1', 'b', 'sb', 'bb'],
  hu: ['b', 'bb']
}

export const SKIPPED_ACTIONS = ['fold', 'allin'];

export const POSITION_NODE_PARAMS = {
  'b': 'B',
  'sb': 'SB',
  'bb': 'BB',
  'b1': 'B+1',
  'b2': 'B+2',
  'b3': 'B+3',
  'b4': 'B+4',
  'b5': 'B+5',
}

export const CASH_GAME_INITIAL_TOTAL_POT = 1.5;
export const MTT_GAME_INITIAL_TOTAL_POT = 2.5;
export const BIG_BLIND_BET = 1;

export const DEFAULT_BLINDS = {
  small_blind: 50,
  big_blind: 100
}

export const SPECIAL_SIMS = [
  '50-limp-BBvSB',
  '40-srp-BBvB+4',
  '20-srp-BBvSB',
  '20-limp-BBvSB',
  '20-srp-BBvB+4',
  '20-srp-SBvB+4',
  '20-srp-BvB+4',
  '15-srp-BBvB',
  '15-srp-BBvB+2',
  '15-srp-BvB+2',
  '15-limp-BBvB',
  '15-limp-BBvB+2'
]

export const COLOR_BY_ROLE = {
  'odin_user': {
    'primaryColor': '#3D64F8',
    'darkerPrimaryColor': '#001F95',
    'positionCircleBorderColor': '#4FAEFF',
    'deckDarkerColor': '#001F95',
    'deckColor': '#2147D8',
    'positionCircleShadowColor': '#4FAEFF',
    'selectedBorderColor': '#3D64F8',
    'selectedBackgroundColor': '#3D64F8',
    'textColor': '#4FAEFF',
    'headerBackgroundColor': '#1E1E31',
    'pageBackgroundColor': '#2A2A44',
    'pageBackgroundDeepColor': '#1E1E31',
    'pageBackgroundLightColor': '#31314D',
    'buttonBackgroundColor': '#3E3E5D',
    'lineDividerColor': '#4C4C6C',
    'matrixSquareBackgroundColor': '#616181',
    'positionCircleBorderColorSelected': '#3D93F8',
    'buttonSubscribeDarkColor': '#47475F',
    'buttonSubscribeLightColor': '#47475F',
    'buttonSubscribeShadow': 'rgba(0, 0, 0, 0.282353)',
    'backgroundSubscriptionOptionDark': '#3D93F8',
    'backgroundSubscriptionOptionLight': '#3D64F8',
    'specialPriceColor': '#ffffff',
    'buttonSpecialSubscribeDarkColor': '#3D93F8',
    'buttonSpecialSubscribeLightColor': '#3D64F8',
    'titleSubscription': '#ffffff',
  },
  'pokercode_user': {
    'primaryColor': '#3D64F8',
    'darkerPrimaryColor': '#D54935',
    'positionCircleBorderColor': '#FE5000',
    'deckDarkerColor': '#001F95',
    'deckColor': '#2147D8',
    'positionCircleShadowColor': '#e6cd97',
    'selectedBorderColor': '#E16E4B',
    'selectedBackgroundColor': '#E16E4B',
    'textColor': '#4FAEFF',
    'headerBackgroundColor': '#1E1E31',
    'pageBackgroundColor': '#2A2A44',
    'pageBackgroundDeepColor': '#1E1E31',
    'pageBackgroundLightColor': '#31314D',
    'buttonBackgroundColor': '#3E3E5D',
    'lineDividerColor': '#4C4C6C',
    'matrixSquareBackgroundColor': '#616181',
    'positionCircleBorderColorSelected': '#F2653F',
    'buttonSubscribeDarkColor': '#E16E4B',
    'buttonSubscribeLightColor': '#E16E4B',
    'buttonSubscribeShadow': 'rgb(61 100 248 / 20%)',
    'backgroundSubscriptionOptionDark': '#ffffff',
    'backgroundSubscriptionOptionLight': '#ffffff',
    'specialPriceColor': '#000000',
    'buttonSpecialSubscribeDarkColor': '#E16E4B',
    'buttonSpecialSubscribeLightColor': '#E16E4B',
    'titleSubscription': '#E16E4B',

  },
  'black_design_user': {
    'primaryColor': '#E16E4B',
    'darkerPrimaryColor': '#D54935',
    'positionCircleBorderColor': '#FE5000',
    'deckDarkerColor': '#D54935',
    'deckColor': '#D47358',
    'positionCircleShadowColor': '#E6CD97',
    'selectedBorderColor': '#E16E4B',
    'selectedBackgroundColor': '#E16E4B',
    'textColor': '#4FAEFF',
    'headerBackgroundColor': '#1F1F1F',
    'pageBackgroundColor': '#1F1F1F',
    'pageBackgroundDeepColor': '#101010',
    'pageBackgroundLightColor': '#252525',
    'buttonBackgroundColor': '#404040',
    'lineDividerColor': '#404040',
    'matrixSquareBackgroundColor': '#505050',
    'positionCircleBorderColorSelected': '#3D93F8',
    'buttonSubscribeDarkColor': '#47475F',
    'buttonSubscribeLightColor': '#47475F',
    'buttonSubscribeShadow': 'rgba(0, 0, 0, 0.282353)',
    'backgroundSubscriptionOptionDark': '#3D93F8',
    'backgroundSubscriptionOptionLight': '#3D64F8',
    'specialPriceColor': '#ffffff',
    'buttonSpecialSubscribeDarkColor': '#3D93F8',
    'buttonSpecialSubscribeLightColor': '#3D64F8',
    'titleSubscription': '#ffffff',
  },
  "admin": {
    'primaryColor': '#3D64F8',
    'darkerPrimaryColor': '#001F95',
    'positionCircleBorderColor': '#4FAEFF',
    'deckDarkerColor': '#001F95',
    'deckColor': '#2147D8',
    'positionCircleShadowColor': '#4FAEFF',
    'selectedBorderColor': '#3D64F8',
    'selectedBackgroundColor': '#3D64F8',
    'textColor': '#4FAEFF',
    'headerBackgroundColor': '#1E1E31',
    'pageBackgroundColor': '#2A2A44',
    'pageBackgroundDeepColor': '#1E1E31',
    'pageBackgroundLightColor': '#31314D',
    'buttonBackgroundColor': '#3E3E5D',
    'lineDividerColor': '#4C4C6C',
    'matrixSquareBackgroundColor': '#616181',
    'positionCircleBorderColorSelected': '#3D93F8',
    'buttonSubscribeDarkColor': '#47475F',
    'buttonSubscribeLightColor': '#47475F',
    'buttonSubscribeShadow': 'rgba(0, 0, 0, 0.282353)',
    'backgroundSubscriptionOptionDark': '#3D93F8',
    'backgroundSubscriptionOptionLight': '#3D64F8',
    'specialPriceColor': '#ffffff',
    'buttonSpecialSubscribeDarkColor': '#3D93F8',
    'buttonSpecialSubscribeLightColor': '#3D64F8',
    'titleSubscription': '#ffffff',
  }
}

export const IP_OOP_POSITIONS_ORDER = ['sb', 'bb', 'b5', 'b4', 'b3', 'b2', 'b1', 'b'];

export const SUITED_CARD_NUMBER = 13;

export const DOWNCASE_POSITIONS_MAPPING = {
  'bb': 'bb',
  'sb': 'sb',
  'b': 'btn',
  'b1': 'co',
  'b2': 'hj',
  'b3': 'lj',
  'b4': 'utg+1',
  'b5': 'utg'
}

export const UPPERCASE_POSITIONS_MAPPING = {
  'BB': 'BB',
  'SB': 'SB',
  'B': 'BTN',
  'B+1': 'CO',
  'B+2': 'HJ',
  'B+3': 'LJ',
  'B+4': 'UTG+1',
  'B+5': 'UTG'
}

export const EV_EQ_PRECISION = 2;
export const RANGES_PRECISION = 2
export const INITIAL_TREE_INFO = {
  oop: { flop: {}, turn: {}, river: {}},
  ip: { flop: {}, turn: {}, river: {}},
  fetching: false
}
export const DISPLAY_IN_BB = true
export const NUMBER_OF_COLOR_SCHEME_LEVELS = 12

export const CLEAR_ICON_TEXT_LIST = ['', '<p></p>']
export const EMAILS_ABLE_TO_VIEW_BLACK_DESIGN = [
  'fedor@pokercode.com',
  'holzfedor@googlemail.com',
  'kentest@gmail.com',
];
export const DO_NOT_APPLY_DELAY_TIME_EMAILS = [
  'teachingfishes@gmail.com',
  'ryananthonyotto@gmail.com',
  'roryyoungpoker@gmail.com',
  'hamish.crawshaw@gmail.com',
  'zack.lowrie@icloud.com',
  'adrianattenborough@gmail.com',
  'bigbadbobby6666@gmail.com',
  'bbuutteerrss@gmail.com',
  'ken@gmail.com',
  'heimdallrpoker@hotmail.com',
  'kahleburns@gmail.com',
]

export const FREE_TRIAL_USER_EMAILS = [
  'odinfreetrialuser@yopmail.com',
];

export const MAXIMUM_NUMBER_OF_PREFLOP_ACTIONS = 4;
export const PREFLOP_ANTES = {
  cash: 0,
  mtt: 1,
}

export const PREFLOP_ACTION_DECIMAL = 2
export const PREFLOP_NUM_COMBOS_PRECISION = 2
export const PREFLOP_PERCENT_COMBOS_PRECISION = 2
export const PREFLOP_POT_STACK_PRECISION = 2
export const DECISION_PERCENT_COMBOS_PRECISION = 1
export const DECISION_NUM_COMBOS_PRECISION = 1
export const COMPARE_EV_PRECISION = 2

export const PAIRED_BOARDWAY_1 = ['JJT', 'QQT', 'KKT', 'KKQ', 'AAT', 'AAJ', 'AAQ', 'AAK']
export const PAIRED_BOARDWAY_2 = ['JTT', 'QTT', 'KTT', 'ATT', 'AJJ', 'KQQ', 'AQQ', 'AKK']
export const BROADWAY_CARD = ["10", "J", "Q", "K", "A"]

export const INITIAL_NOTE_POPUP_DATA = {
  streetView: {
    general: {
      note: '',
      default: []
    },
    flop: {
      note: '',
      oop: [],
      ip: []
    },
    turn: {
      note: '',
      default: [],
      oop: [],
      ip: []
    },
    river: {
      note: '',
      default: [],
      oop: [],
      ip: []
    }
  },
  notepadView: {
    note: ''
  }
}

export const SELECT_CARD_POPUP = {
  left_position_of_left_pop_up: 50,
  left_position_of_right_pop_up: 270,
  height: 370
}

export const NOTES_POPUP_ROUND = ['general', 'flop', 'turn', 'river']

export const INIT_PREFLOP_FIELDS = {
  site: DEFAULT_SITE,
  stake: DEFAULT_STAKE_OPTIONS[DEFAULT_SITE][0].value,
  open_size: DEFAULT_OPEN_SIZE,
}

export const INIT_STRATEGY_SELECTION = {
  street: DEFAULT_STREET_OPTIONS,
  game_type: DEFAULT_GAME_TYPE,
  players: DEFAULT_PLAYERS,
  stack: DEFAULT_STACK_SIZE[DEFAULT_GAME_TYPE],
  positions: DEFAULT_POSITIONS[DEFAULT_GAME_TYPE],
  sim_type: TYPE_OPTIONS,
  site: DEFAULT_SITE,
  stake: DEFAULT_STAKE_OPTIONS[DEFAULT_SITE][0].value,
  open_size: DEFAULT_OPEN_SIZE,
  flops: DEFAULT_FLOP_CARDS,
}

export const HAND_CATEGORY_MAP = {
  madeHand: {
    '0':	'nothing',
    '1':	'king_high',
    '2':	'ace_high',
    '3':	'low_pair',
    '4':	'third_pair',
    '5':	'second_pair',
    '6':	'underpair',
    '7':	'top_pair',
    '8':	'top_pair',
    '9':	'overpair',
    '10':	'two_pair',
    '11':	'trips',
    '12':	'set',
    '13':	'straight',
    '14':	'flush',
    '15':	'fullhouse',
    '16':	'fullhouse',
    '17':	'quads',
    '18':	'straight_flush',
  },
  drawHand: {
    '-1': 'not_exists',
    '0':	'no_draw',
    '1':	'four_out_straight_draw',
    '2':	'eight_out_straight_draw',
    '3':	'flush_draw',
    '4':	'combo_draw',
  }
}

export const INITIAL_HAND_CATEGORY_DATA = {
  nothing: 0,
  king_high: 0,
  ace_high: 0,
  low_pair: 0,
  third_pair: 0,
  second_pair: 0,
  underpair: 0,
  top_pair: 0,
  overpair: 0,
  two_pair: 0,
  trips: 0,
  set: 0,
  straight: 0,
  flush: 0,
  fullhouse: 0,
  quads: 0,
  straight_flush: 0,
  no_draw: 0,
  four_out_straight_draw: 0,
  eight_out_straight_draw: 0,
  flush_draw: 0,
  combo_draw: 0
}

export const INITIAL_HAND_CATEGORY_MATRIX = {
  board: '',
  made_hands: '',
  draw_hands: ''
}

export const WEIGHTED_STRATEGY_DEFAULT_STATUS = true

export const HUNL_CASH_GAME_STRATEGY = {
  street: "postflop",
  game_type: "cash",
  players: "hu",
  stack: "100",
  positions: ["bb", "b"],
  sim_type: "3bet",
  site: "",
  stake: "",
  open_size: "",
  flops: ["Js", "8d", "7d"],
  specs: "hu"
}

export const SIX_MAX_CASH_GAME_STRATEGY = {
  flops: ["Ad", "Kd", "5s"],
  game_type: "cash",
  open_size: "",
  players: "6max",
  positions: ["sb", "b"],
  sim_type: "3bet",
  site: "",
  specs: "6max",
  stack: "100",
  stake: "",
  street: "postflop"
}

export const MTT_GAME_STRATEGY = {
  flops: ["6s", "4d", "3s"],
  game_type: "mtt",
  open_size: "",
  players: "6max",
  positions: ["b", "bb"],
  sim_type: "srp",
  site: "",
  specs: "",
  stack: "25",
  stake: "",
  street: "postflop"
}

export const DELETE_SIM_TYPE = ['select-item','all']

export const PADS_ACCESS_EMAILS_LIST = [
  'jackharris1@hotmail.co.uk',
  'itwasaghost@hotmail.com',
  'stozstoz@hotmail.com',
  'jcmb1000@gmail.com',
  'tomrhughes93@gmail.com',
  'ebaycappo@gmail.com',
  'yater22@gmail.com',
  'raphael.dutreuil@yahoo.fr',
  'lucashorbylon@gmail.com',
  'oscolaik@gmail.com',
  'joeyweissman88@gmail.com',
  'cargrc@gmail.com',
  'jamesaselliott@gmail.com',
  'gbachiyski@gmail.com',
  'poker330330@gmail.com',
  'josham1995@gmail.com',
  'patsharpe1155@gmail.com',
  'thomastano@hotmail.com',
  'anhbuicp@gmail.com'
]

export const URL_TO_AFFILIATE_CODE_MAPPING = {
  '/': 'launch35',
  '/vipearlyaccess': 'launch35',
  '/gg': 'ggplatinum',
  '/pokercode': 'pokercode',
  '/ginge': 'ginge',
  '/elliot': 'elliot',
  '/bts': 'bts',
  '/streamteam': 'streamteam',
  '/cavalitopoker': 'cavalitopoker',
  '/pc': 'pc',
  '/kmart': 'kmart',
  '/pads': 'pads',
  '/pav': 'pav',
  '/jnandez': 'jnandez',
  '/reglife': 'reglife',
}
