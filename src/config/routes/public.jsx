import SignIn from '../../components/SignIn/component';
import SignUp from '../../components/SignUp/component';
import Home from '../../components/Home/component'
import ContactUs from '../../components/ContactUs/component'
import Subcribe from '../../components/SubcribePage/component'
import ThankYouPage from 'components/ThankYouPage/component'
import SetupAccountPage from 'components/MyAccount/SetupAccountPage/SetupAccountPage'

export default {
  Home: {
    component: Home,
    path: '/'
  },
  SignIn: {
    component: SignIn,
    path: '/signin'
  },
  SignUp: {
    component: SignUp,
    path: '/signup'
  },
  ContactUs: {
    component: ContactUs,
    path: '/contact-us'
  },
  Subcribe: {
    component: Subcribe,
    path: '/subscribe'
  },
  ThankYouPage: {
    component: ThankYouPage,
    path: '/thank-you'
  },
  SetupAccountPage: {
    component: SetupAccountPage,
    path: '/setup-account'
  }
};
